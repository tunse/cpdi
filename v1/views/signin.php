<?php
include('output_fns.php');
    require_once('min_auto_fns.php');
if(isset($_POST['submit'])){
$username = $_POST['email'];
$passwd = $_POST['password'];
      try
      {
          login($username, $passwd);
          // if they are in the database register the user id
          $userfetch = selectContent($conn, "read_users", ['input_email' => $_POST['email']]);
          $_SESSION['valid_user'] = $userfetch[0]['id'];
             header("Location: /myaccount");
             exit();
          //Display the booking details

      }
      catch(Exception $e)
      {

        $error = 'Email or Password is Incorrect';
        }
}
 ?>

<?php include "includes/header.php" ?>


<style type="text/css">

    .register-form{
      margin: 0px auto;
      padding: 25px 20px;
      background: #3a1975;
      box-shadow: 2px 2px 4px #ab8de0;
      border-radius: 5px;
      color: #fff;
    }
    .register-form h2{
      margin-top: 0px;
      margin-bottom: 15px;
      padding-bottom: 5px;
      border-radius: 10px;
      border: 1px solid #25055f;
    }
  </style>

  <section id="services" class="section-bg">
      <div class="container">

        <div class="row">

		      <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">


              <header class="section-header"><br />
                <br /><br /><h3>Login</h3>
              </header>
	    <div>
		<form action="" method="post">
      <!-- <div class="col-md-3"> -->
        <span class="text-success"><?php if(isset($_GET['success'])){
          echo str_replace('_', ' ',$_GET['success']);} ?></span>
        <span class="text-danger"><?php if(isset($_GET['err'])){ echo str_replace('_', ' ',$_GET['err']);} ?></span>
        <span class="text-danger"><?php if(isset($_GET['vr'])){ echo str_replace('_', ' ',$_GET['vr']);} ?></span>
        <span class="text-danger"><?php if(isset($_GET['ne'])){ echo str_replace('_', ' ',$_GET['ne']);} ?></span>
        <span class="text-danger"><?php if(isset($error)){ echo $error;} ?></span>
      <!-- </div> -->
		  <div class="form-group">
			<input type="email" name="email" class="form-control" placeholder="Email" required="required">
		  </div>
		  <div class="form-group">
			<input type="password" name="password" class="form-control" placeholder="Password" required="required">
		  </div>

		  <div class="row">
			<div class="col-md-6 col-xs-12">
			  <div class="form-group">
				<input type="submit" name="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7" style="background-color:#970C7B; border-color: #970C7B;">
			  </div>
			</div>

		  </div>
		</form>
	</div>
  <a href="/password_reset" style="color:#970C7B;">Reset Password</a>
          </div>
    <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
      <header class="section-header"><br />
        <br /><br />        <h3 style="color:#970C7B;">Sign Up/ Register</h3>
      </header>

      <div class="row justify-content-center">
        <div class="col-md-6">

          <div class="" style="width: 100%;padding-top: 60%;background:url(CPDI_Global_studio.jpg);background-size:cover; background-repeat:no-repeat;background-position:center">

          </div>
          <!-- <img src="CPDI_Global_studio.jpg" height="150" width="200" alt=""><br> -->
          <a href="/courses?active=yes" class="btn btn-primary btn-block btn-lg" style="background-color:#970C7B; border-color: #970C7B;">Global Studio Courses</a>
        </div>
        <div class="col-md-6">
        <div class="" style="width: 100%;padding-top: 60%;background:url(CPDI_Competition.jpg);background-size:cover; background-repeat:no-repeat;background-position:center">

        </div>
        <!-- <img src="CPDI_Competition.jpg" height="150" width="200" alt=""><br> -->
        <a href="/competitions" class="btn btn-primary btn-block btn-lg" style="background-color:#970C7B; border-color: #970C7B;">Design Competiton</a>
        </div>
        <div class="col-md-6">
        <div class="" style="width: 100%;padding-top: 60%;background:url(/cpdiblack.jpg);background-size:cover; background-repeat:no-repeat;background-position:center">

        </div>
        <!-- <img src="CPDI_Competition.jpg" height="150" width="200" alt=""><br> -->
        <a href="https://canvas.instructure.com/login/canvas
" class="btn btn-primary btn-block btn-lg" target="_blank" style="background-color:#970C7B; border-color: #970C7B;">CANVAS Classroom</a>
        </div>
      </div>

      </div>
      </div>
    </section><!-- #services -->
  <?php include "includes/footer.php" ?>
