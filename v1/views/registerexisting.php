<?php

	  // session_start();

	  // include function files for this application
	  require_once('db_fns.php');
	  include('output_fns.php');
	  include('min_auto_fns.php');
	  $conn1 = db_connect();

	  $course = $_GET['product'];

	  if ($course=="")
	  {
		header("Location: /home");

	  }

	  //User logged in proceed
	if (!isset($_SESSION['valid_user']) && empty($_SESSION['valid_user']))
	{
		header("Location: /home");
	}


	  $paypal_url='https://www.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
	  $paypal_id='design@southernsahara.com'; // Business email ID



	  // start session which may be needed later
	  // start it now because it must go before headers


	//   $result1 = selectCourse($course);
	//
	// if($result1)
	// {
	// 	$num_result = $result1->num_rows;
	// 	if($num_result > 0)
	// 	{
	// 		for ($i=0; $i<$num_result; $i++)
	// 		{
	// 			$row = $result1->fetch_assoc();
	// 			$courseName = $row['input_course_name'];
	// 			$courseDescription = $row['text_course_description'];
	// 			$coursePromotion = $row['input_course_promotion'];
	// 			$courseDetails = $row['text_course_details'];
	// 		}
	// 	}
	//
	// }

	$selectcourse = selectContent($conn, "panel_courses", ['id' => $course]);
				$courseName = $selectcourse[0]['input_course_name'];
				$courseDescription = $selectcourse[0]['text_course_description'];
				$coursePromotion = $selectcourse[0]['input_course_promotion'];
				$courseDetails = $selectcourse[0]['text_course_details'];
				$courseSession = $selectcourse[0]['select_session'];

?>

<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <br /><br /><h3>Proceed to Payment</h3>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">

              <h4 class="title"> <?php echo "$courseName"; ?></h4>
              <p class="description"><?php echo "$courseDescription"; ?><br /><br /> <b>Amount:</b> $<?php echo $coursePromotion; ?></p>




            </div>
          </div>

		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2" style="color:#495057;">
<?php

				// $result4 = selectUserByEmail($_SESSION['valid_user']);
				//
				// if($result4)
				// {
				// 	$num_result = $result4->num_rows;
				// 	if($num_result > 0)
				// 	{
				// 		for ($i=0; $i<$num_result; $i++)
				// 		{
				// 			$row = $result4->fetch_assoc();
				// 			$userId = $row['id'];
				// 			$fname = $row['input_first_name'];
				// 			$lname = $row['input_last_name'];
				// 			$email = $row['input_email'];
				// 			$phone = $row['input_phone'];
				//
				// 		}
				// 	 }
				// }

				$fetchuser = selectContent($conn, "read_users", ['id' => $_SESSION['valid_user']]);

				$userId = $fetchuser[0]['id'];
				$fname = $fetchuser[0]['input_first_name'];
				$lname = $fetchuser[0]['input_last_name'];
				$email = $fetchuser[0]['input_email'];
				$phone = $fetchuser[0]['input_phone'];
				$address = $fetchuser[0]['input_address_line_one'];
				$country = $fetchuser[0]['input_country'];
				$ethnic = $fetchuser[0]['input_ethnic'];
				$profStatus = $fetchuser[0]['input_professor_status'];
				$linkedIn = $fetchuser[0]['input_linkedIn'];
				$facebook = $fetchuser[0]['input_facebook'];


				$to      = $email;
				$subject = 'CPDI Africa';
				$message = '<h3>	Dear Scholar, Welcome to CPDI Africa GSACA!</h3><br>
				<p>Thank you for your registration at the CPDI Africa Global Studio for African Centered Architecture
				(GSACA), the pioneering academy for teaching a new pedagogy in African architecture, heritage
				preservation and sustainable design. You have joined an exceptional body of scholars who are
				developing new African inspired design languages that are culturally and environmentally sound for the
				Global built environment.</p></br>

				<p><b>Next Steps!</b></p>


					<ol>
						<li>Log back into your My Account at <a href="http://cpdiafrica.org">cpdiafrica.org</a>, and complete your payment using our <u>PayPal, Flutterwave, or Bank Transfer options.</u></li>
						<li>In your payment notes, make sure you include the current/active email address you will use to access your CPDI Africa Classroom on CANVAS. This is how we will reach you to confirm your admissions and enrollment.</li>
						<li>Anticipate your Invitation to CPDI Africa GSACA classroom on CANVAS email, within 10 days of your payment. CANVAS is the CPDI Africa preferred online learning management system (LMS).</li>
					</ol>

				<br><br>

				<p><b>Class Commencement!</b></p>

				<ul>
					<li>Admissions to our Global Studio courses are activated <b><i>Every Monday</i></b>. Check the email that <u>you provided us on your mode of payment</u> on Mondays, within 10 days of completing payment for your course.</li>
					<li>You should anticipate your welcome request to <u>create your account and log in</u> details on CPDI Africa <b>CANVAS Classroom</b> within the 10 days of your payment.</li>
					<li>Once you receive the invitation and activate your <b>CANVAS Classroom Account</b>, you can begin to enjoy your <b>On Demand</b> CPDI Africa GSACA course in our Canvas classroom!</li>
					<li>Earn your <b>Certificate in African Centered Architecture</b> with the completion of courses in our CPDI Africa GSACA programs!</li>
				</ul>
				<br><br>

				<b>For further inquiries and information on your course and admissions, CONTACT US at the emails provided below.</b> Welcome once again to CPDI Africa Global Studio for African Centered Architecture!
				<br><br>
				<b>Nmadili Okwumabua</b> <br>
				<p>Founder / Executive Director,</p>
				<p>CPDI Africa Global Studio for African Centered Architecture.</p>
				<br><br>

				<p>Email: <a href="mailto:design@cpdiafrica.org">design@cpdiafrica.org</a> / <a href="mailto:cpdiafricascholarships@gmail.com">cpdiafricascholarships@gmail.com</a></p>
				<p>Tel: +234 809 155 6480 (NIG) WhatsApp.</p>

				';


				$headers ="MIME-Version :1.0"."\r\n";
				$headers .="Content-type:text/html; charset=iso-8859-1"."\r\n";
				$headers .= 'From: design@cpdiafrica.org' . "\r\n" .
					'Reply-To: design@cpdiafrica.org' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();



				mail($to, $subject, $message, $headers);
				//
				// $conn1->close();






$check = selectContent($conn, 'read_product_order', ['user_id' => $userId, 'course_id' => $course]);
// die(var_dump($check));
if(count($check) < 1){
				// $time = time();


				$rnd = rand(0000000000,9999999999);
				$split = explode(" ",'african chartered');
				$id = $rnd.cleans($split['0']);
				$hash_id = str_shuffle($id.'cpdi');
				// if ok, put in db
				$name = $fname." ".$lname;


								$post['hash_id'] = $hash_id;
								$post['input_amount'] = $coursePromotion;
								$post['user_id'] = $userId;
								$post['input_name'] = $name;
								$post['input_phone'] = $phone;
								$post['input_email'] = $email;
								$post['course_id'] = $course;
								$post['input_course_name'] = $courseName;
								$post['select_session'] = $courseSession;
								$post['pay_pal_id'] = "BLANK";
								$post['bool_status'] = 0;
								$post['input_address_line_one'] = $address;
								$post['input_payer_email'] = "BLANK";
								$post['input_country'] = $country;
								$post['input_ethnic'] = $ethnic;
								$post['input_professor_status'] = $profStatus;
								$post['input_linkedIn'] = $linkedIn;
								$post['input_facebook'] = $facebook;

								$post['time'] = time();
								$post['date'] = time();
								$post['visibility'] = "show";
								$post["date_created"] = date("Y-m-d");
								$post["time_created"] = date("h:i:s");

								insertSafe($conn, "read_product_order", $post);

								$order = selectContent($conn, "read_product_order", ['hash_id' => $hash_id]);
								$order_id = $order[0]['id'];

								$invoice_check = selectContent($conn,"invoice",['event_id'=>$order_id]);




								if (count($invoice_check) < 1) {
									$new['phonenumber'] = $phone;
								$new['invoice_id'] = "CRS"."_".rand(10000,99999)."_".str_shuffle("INVOICECODE")."_".time();
									$new['email'] = $email;
									$new['event_id'] = $order_id;
									$new['user_id'] = $_SESSION['valid_user'];

									$new['status'] = "Unpaid";
									$new['title'] = $courseName;
									$new['description'] = "Payment for ".$courseName;
									$new['place'] = "place";
									$new['company'] = ('company');
									$new['time_created'] = date("h:i:s");
									$new['date_created'] = date("Y-m-d");
									$new['hash_id'] = time()."_".rand(1000,9000);
									$new['quantity'] = "quantity";
									$new['amount_due'] = intval($coursePromotion);
									$new['payment_plan'] = "Course";
									$new['plan_id'] = $course;
									$new['name'] = $name;



									insertSafe($conn,'invoice',$new);
										// header("Location:/payment_invoice?id=".$new['hash_id']."&type=EVT");
								}else{
									$new['hash_id'] = $invoice[0]['hash_id'];
								}

								updateContent($conn, "read_product_order", ['input_order_id' => $order_id], ['hash_id' => $hash_id]);


				// $sqlOrder = "insert into read_product_order values (null,'$hash_id', '$course', '$coursePromotion', '$userId','$name','$phone','$email', '$time', 'BLANK', 0, 'BLANK', '$time','show',NOW(),NOW())";
				//
				// if (mysqli_query($conn1, $sqlOrder)) {


					echo 'Your order has been created. <br /><br /> Please make a payment to confirm your order for the course.';

				// } else {
				//    echo "Error: " . $sql . "" . mysqli_error($conn1);
				// }
				//
				// $order_id =  mysqli_insert_id($conn1);







				$to      = $email;
				$subject = 'CPDI Africa';
				$message = "Dear $fname $lname, \r\n \r\n Thank you for your registration for $courseName \r\n \r\nYour registration will be confirmed once we receive payment. \r\n \r\n Kind regards, \r\n CPDI Africa";
				$headers = 'From: design@cpdiafrica.org' . "\r\n" .
					'Reply-To: design@cpdiafrica.org' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();

				mail($to, $subject, $message, $headers);

				$conn1->close();


?>
				<br /><br />
				 <form action='<?php echo $paypal_url; ?>' method='post'>
				<input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
				<input type='hidden' name='cmd' value='_xclick'>
				<input type='hidden' name='item_name' value='<?php echo "$courseName"; ?>'>
				<input type='hidden' name='item_number' value='<?php echo "$order_id"; ?>'>
				<input type='hidden' name='amount' value='<?php echo "$coursePromotion"; ?>'>
				<input type='hidden' name='no_shipping' value='1'>
				<input type='hidden' name='currency_code' value='USD'>
				<input type='hidden' name='cancel_return' value='http://www.cpdiafrica.org/cancel.php'>
				<input type='hidden' name='return' value='https://www.cpdiafrica.org/success.php'>
				<input type="image" src="unnamedp.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <!-- <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->
				</form> <br />
				<div class="form-group">
				<a href="/payment_invoice?id=<?php echo $new['hash_id'] ?>&type=CRS" style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnamedf.png" ></a>
				</div>

				<div class="form-group">
				<a href="banktransfer?order=<?php echo $check[0]['input_order_id']; ?>"  style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnameb.png" ></a>
				</div>
        <br />
<?php
        $urlEncodedText = "I'm interested in the course: $courseName";
        $urlEncodedText = urlencode($urlEncodedText);
        echo "<a href='https://wa.me/16786509145?text=$urlEncodedText' target='_blank' style='color:#008000;'>Send a WhatsApp message <img src='/cpdi/img/whatsapp.png'></a>";


				header("Location:".$_SERVER['REQUEST_URI']);
				exit();
}else{
	echo "Your Order ID is " . $check[0]['input_order_id'];
			$invoice_check = selectContent($conn,"invoice",['event_id'=>$check[0]['input_order_id']]);
			$new['hash_id'] = $invoice_check[0]['hash_id'];

?>

				<br /><br />
				 <br />
        <div class="col-md-6 row justify-content-center ">





            <div class="form-group">
							<form action='<?php echo $paypal_url; ?>' method='post'>
							<input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
							<input type='hidden' name='cmd' value='_xclick'>
							<input type='hidden' name='item_name' value='<?php echo "$courseName"; ?>'>
							<input type='hidden' name='item_number' value='<?php echo $check[0]['input_order_id']; ?>'>
							<input type='hidden' name='amount' value='<?php echo "$coursePromotion"; ?>'>
							<input type='hidden' name='no_shipping' value='1'>
							<input type='hidden' name='currency_code' value='USD'>
							<input type='hidden' name='cancel_return' value='http://www.cpdiafrica.org/cancel.php'>
							<input type='hidden' name='return' value='https://www.cpdiafrica.org/success.php'>
							<input type="image" src="/unnamedp.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			                <!-- <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->
							</form>
            </div>

            <div class="form-group">
            <a href="/payment_invoice?id=<?php echo $new['hash_id'] ?>&type=CRS" style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnamedf.png" ></a>
            </div>

            <div class="form-group">
            <a href="banktransfer?order=<?php echo $check[0]['input_order_id']; ?>"  style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnameb.png" ></a>
            </div>

        </div>
        <br />
		<?php
		        $urlEncodedText = "I'm interested in the course: $courseName";
		        $urlEncodedText = urlencode($urlEncodedText);
		        echo "<a href='https://wa.me/16786509145?text=$urlEncodedText' target='_blank' style='color:#008000;'>Send a WhatsApp message <img src='/cpdi/img/whatsapp.png'></a>";
}
 ?>
		  </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
