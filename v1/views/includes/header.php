
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <?php if (isset($page_title)): ?>
    <title><?php echo $page_title ?> - CPDI Africa</title>
  <?php else: ?>
    <title>African Centered Architecture - CPDI Africa</title>
  <?php endif; ?>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="Afrocentric,Afrocentric architecture,interior design,African Design,African Buildings,traditional African Architecture,Modern African Architecture,contemporary African architecture,CPDI Africa,CPDI,Africa,African Architecture,Architecture,African Centered Architecture" name="keywords">
  <meta content="<?php echo $description ?>" name="description">
  <meta name="google-site-verification" content="Ma_iGbyaw-YRLL9FuHqyoEctfKrKQwRgJutLEAOtoIw" />

  <!-- Favicons -->
  <link href="<?php echo $logo_directory ?>" rel="icon">
  <link href="<?php echo $logo_directory ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <link rel="stylesheet" href="/da/assets/fonts/fontawesome/css/fontawesome-all.min.css">
  <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="/da/assets/css/style.css">
  <link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/lib/animate/animate.min.css" rel="stylesheet">
  <link href="/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-T075BPHPNX"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-T075BPHPNX');
</script>
  <!-- Main Stylesheet File -->

  <link href="/cpdi/css/style.css" rel="stylesheet">
<script src="/ajax/ajax.js"></script>
<style media="screen">

  .nav-link.active{
    color: #970C7B !important;
  }
  .nav-link{
    color: #fff !important;
  }
</style>
<style media="screen">
  .nav-tabs .nav-link{
    background-color: none;

  }

  /* .nav-tabs .nav-link.active{
    color:#970c7b !important;
  } */

</style>
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="float-left">
        <a href="/home" class="scrollto">
      <img src="headerlogg.jpg" width ="70" alt=""></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
      <?php

              		   if (!isset($_SESSION['valid_user']) && empty($_SESSION['valid_user']))
              		  {
                      ?>
          <ul>
          <li><a href="/about-us">About</a></li>
          <!-- <li><a href="/courses?active=yes">Global Studio</a></li> -->
          <li><a href="https://global-studio.cpdiafrica.org">Global Studio</a></li>
          <li><a href="/cada-hub">CADA Hub</a></li>
          <li><a href="/competitions">Competitions</a></li>
          <li><a href="/compendium">Compendium</a></li>
          <li><a href="/services">Services</a></li>
          <li><a href="/resources">Publications</a></li>
          <li><a href="/gallery">Photos</a></li>
          <li><a href="/connect">Connect</a></li>

            <!-- <li><a href="/signin">My Account</a></li> -->
        </ul>
        		  <?php }
        		  else
        		  { ?>
<?php $user = selectContent($conn, "read_users", ['id'=> $_SESSION['valid_user']]);
  ?>
                <ul>
              	<li><a href="/about-us">About</a></li>
              	<li><a href="/courses?active=yes">Global Studio</a></li>
                <li><a href="/resource-center">Resource Center</a></li>
              	<li><a href="/competitions">Competitions</a></li>
              	<li><a href="/compendium">Compendium</a></li>
              	<li><a href="/services">Services</a></li>
              	<li><a href="/resources">Publications</a></li>
                <li><a href="/gallery">Photos</a></li>
              	<li><a href="/connect">Connect</a></li>

              			<!-- <li class="drop-down"><a href="#">My Account</a>
              					<ul>
              			<li><p style='color:#970C7B; padding: 10px;'>Welcome: <?php echo $user[0]['input_first_name'] ?></p></li>
                      <?php
              				if ($user[0]['input_admin_status'] == "YES" || $user[0]['input_professor_status'] == "YES")
              				{
                        ?>
              				<li><a href="/admin_menu" style='color:#970C7B'><i class="fa fa-bars"></i>  Admin Menu</a></li>
                      <?php
                    }
                    //else{?>

                      <li><a href="/myaccount" style='color:#970C7B'><i class="fa fa-user"></i>  My Dashboard</a></li>
                  <?php  //}
                      ?>
              			<li><a href="/signout" style='color:#970C7B'><i class="fa fa-sign-out"></i> Sign Out</a></li></ul>
              			</li> -->
                  </ul>
          <?php } ?>

      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->
