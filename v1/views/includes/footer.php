	<!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <style media="screen">

          .btnget{
            font-family: "Open Sans";
            font-size: 14px;
            font-weight: 600;
            letter-spacing: 1px;
            display: inline-block;
            padding: 10px 32px;
            border-radius: 50px;
            transition: 0.5s;
            margin: 0 20px 20px 0;
            color: #fff;
            background: #970C7B;
            border: 2px solid #970C7B;
            color: #fff;
          }
          .btnget2{
            font-family: "Open Sans";
            font-size: 14px;
            font-weight: 600;
            letter-spacing: 1px;
            display: inline-block;
            padding: 10px 32px;
            border-radius: 50px;
            transition: 0.5s;
            margin: 0 20px 20px 0;
            color: #fff;
            border: 2px solid #fff;
          }
          .btnget .btnget2 :hover{
            background: #970C7B;
            border-color: #fff;
            color: #fff;
          }
          .cpdi_google_map{
            /* border:1px solid green; */
            width:100%;
            margin-bottom:24px;
            border-radius:12px;
          }
          @media (max-width:425px){
            .cpdi_google_map{
              height:300px;
            }
          }
        </style>
        <!-- <div class="d-flex justify-content-center">
          <a href="https://cpdiafrica.blogspot.com/p/register-your-interest.html" target="_blank" class="btnget scrollto">Get Involved</a>
          <a href="https://www.gofundme.com/f/cpdi-africa-global-studio" target="_blank" class="btnget2 scrollto">Donate</a>
        </div> -->
        <iframe class="cpdi_google_map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.6869233626667!2d7.419425273672871!3d9.00092828943052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e733ec975c7a5%3A0x2d2d373a8f08b1f7!2sAfrican%20University%20of%20Science%20and%20Technology!5e0!3m2!1sen!2sng!4v1738929431570!5m2!1sen!2sng" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
	  <div><p style="text-align: center; font-size: 18px"><b>#AfricaDefinesArchitecture</b></p> </div>
        <div class="row">


          <div class="col-lg-4 col-md-6 footer-info">
		  	<h4>Connect with Us</h4>
				<a href="http://www.cpdiafrica.org" class="fa fa-dribbble" target="_blank"> &nbsp; www.cpdiafrica.org </a><br />

				<a href="https://cpdiafrica.blogspot.com/" class="fa fa-blogger" target="_blank" style="color: #fff">B &nbsp; www.cpdiafrica.blogspot.com</a><br />

                <a href="https://www.instagram.com/cpdi_africa/" class="fa fa-instagram" target="_blank"> &nbsp; cpdi_africa</a><br />

                <a href="https://www.facebook.com/cpdiafrica" class="fa fa-facebook" target="_blank"> &nbsp; www.facebook.com/cpdiafrica</a> <br />

                <a href="https://www.youtube.com/channel/UCfSEFKPbnzHIE4bZieatTgQ" class="fa fa-youtube" target="_blank"> &nbsp; CPDI Africa</a><br />

                <a href="https://twitter.com/cpdi_africa" class="fa fa-twitter" target="_blank"> &nbsp; cpdi_africa </a><br />

                <a href="https://www.pinterest.ca/cpdiafrica" class="fa fa-pinterest" target="_blank"> &nbsp; cpdiafrica</a><br />
                <a href = "mailto: design@cpdiafrica.org" class="fa fa-email" target="_blank" style="color: #fff">@  &nbsp; email</a> <br />

                <a href="https://www.linkedin.com/company/cpdi-africa-global-studio/" class="fa fa-linkedin" target="_blank"> &nbsp; CPDI Africa Global Studio </a><br />



          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="/home">Home</a></li>
              <li><a href="/about-us">About us</a></li>
			  <li><a href="/courses">Courses</a></li>
			  <li><a href="/competitions">Competitions</a></li>
              <li><a href="/compendium">Compendium</a></li>
              <li><a href="/services">Services</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <strong>Phone (USA):</strong> +1 404 641 3557 <br />
            <strong>Phone (NG):</strong> +234 809 155 6480<br />

            <b>The Founder:</b> <br /> <a href="mailto: design@cpdiafrica.org"> design@cpdiafrica.org</a>, <a href="mailto: design@southernsahara.com"> design@southernsahara.com</a><br />
            <b>General Information:</b> <a href="mailto: cpdiafricaconnect@gmail.com"> cpdiafricaconnect@gmail.com</a><br />
            <!-- <b>Competitions:</b> compete@cpdiafrica.org<br />
            <b>Internships:</b> jobs@cpdiafrica.org<br />
            <b>Outreach:</b> ambassador@cpdiafrica.org<br />
            <b>Inquiries:</b> info@cpdiafrica.org -->






          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>SIGN UP TO OUR NEWSLETTER</h4>
            <a href="https://us15.campaign-archive.com/home/?u=82858656ff28d4df9d68eeeb0&id=d3cb2d4ce8" target="_blank"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></a>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>CPDI Africa</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="/lib/jquery/jquery.min.js"></script>
  <script src="/lib/jquery/jquery-migrate.min.js"></script>
  <script src="/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/lib/easing/easing.min.js"></script>
  <script src="/lib/mobile-nav/mobile-nav.js"></script>
  <script src="/lib/wow/wow.min.js"></script>
  <script src="/lib/waypoints/waypoints.min.js"></script>
  <script src="/lib/counterup/counterup.min.js"></script>
  <script src="/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="/cpdi/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="/cpdi/js/main.js"></script>


  <script src="/da/assets/plugins/sweetalert/js/sweetalert.min.js"></script>

  <?php if(isset($_SESSION['success'])): ?>
    <script type="text/javascript">
    swal("Done!", '<?php echo $_SESSION['success'] ?>', "success");
    </script>
  <?php   unset($_SESSION['success']); endif; ?>

  <?php if (isset($_SESSION['failed'])): ?>
    <script type="text/javascript">
   swal("Failed!", "<?php echo $_SESSION['failed'] ?>", "error");
    </script>
  <?php  unset($_SESSION['failed']); endif; ?>

  <?php if(isset($_SESSION['info'])): ?>
    <script type="text/javascript">
    swal("Notice!", '<?php echo $_SESSION['info'] ?>', "info");
    </script>
  <?php   unset($_SESSION['info']); endif; ?>

  <?php if(isset($_SESSION['warning'])): ?>
    <script type="text/javascript">
    swal("Notice!", '<?php echo $_SESSION['warning'] ?>', "warning");
    </script>
  <?php   unset($_SESSION['warning']); endif; ?>

</body>
</html>
