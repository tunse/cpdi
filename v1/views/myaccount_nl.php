<?php 

	session_start();
	include('output_fns.php'); 
	require_once('min_auto_fns.php');


	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['userId'];
					$fname = $row['fname'];
					$lname = $row['lname'];
					
				}
			 }
		}
	}
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		header("Location: login_form.php");
	}
	




	
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>African Centered Architecture</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
  
  
  
  <style type="text/css">
      
      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index_nl.php" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_with_links_nl(); ?>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <br /><br /><h3>Geregistreerde Clubs</h3>
        </header>

        <div class="row">

          
		 
<?php
					
					if ($username && $passwd)
					// they have just tried logging in
					{
					  try
					  {
						login($username, $passwd);
						// if they are in the database register the user id
						$_SESSION['valid_user'] = $username;
						
						//Display the booking details
						
						$result4 = selectUserByEmail($username);

						if($result4)
						{
							$num_result = $result4->num_rows;
							if($num_result > 0)
							{
								for ($i=0; $i<$num_result; $i++)
								{
									$row = $result4->fetch_assoc();
									$userId = $row['userId'];
									$fname = $row['fname'];
									$lname = $row['lname'];
									
								}
							 }
						}
						
						
						
						$result1 = selectUserClubs($userId);

						if($result1)
						{
							$num_result1 = $result1->num_rows;
							if($num_result1 > 0)
							{
								echo "<table cellpadding='10'><tr><td><b>Club</b></td><td><b>Beschrijving</b></td><td><b>Bedrag</b></td></tr>";
								for ($i=0; $i<$num_result1; $i++)
								{
										$row1 = $result1->fetch_assoc();
										$order_id = $row1['order_id'];
										$product_id = $row1['product_id'];
										$amount = $row1['amount'];
										$status = $row1['status'];
										
									
										$result2 = selectProductById($product_id);

										if($result2)
										{
											$num_result2 = $result2->num_rows;
											if($num_result2 > 0)
											{
												
												for ($c=0; $c<$num_result2; $c++)
												{
													$bg_color = $i % 2 === 0 ? "#e6ffff" : "#b3ffff";
													
													$row2 = $result2->fetch_assoc();
													$name = $row2['name_nl'];
													$product_name = $row2['product_name_nl'];
													$briefDescription = $row2['briefDescription_nl'];
													$product_desc = $row2['product_desc_nl'];
													$product_amount = $row2['product_amount'];
													$period = $row2['period_nl'];
													
													
													$startDate = $row2['startDate'];
													$startDate = date("d-m-Y", strtotime($startDate));
													
													$endDate = $row2['endDate'];
													$endDate = date("d-m-Y", strtotime($endDate));
													
													$time = $row2['time'];
													
													$secondDate = $row2['secondDate'];
													$secondDate = date("d-m-Y", strtotime($secondDate));
													
													$thirdDate = $row2['thirdDate'];
													$thirdDate = date("d-m-Y", strtotime($thirdDate));
													
													$forthDate = $row2['forthDate'];
													$forthDate = date("d-m-Y", strtotime($forthDate));
													
													$fifthDate = $row2['fifthDate'];
													if (!empty($fifthDate)) 
													{
														$fifthDate = date("d-m-Y", strtotime($fifthDate));
													}
													
													
													$sixthDate = $row2['sixthDate'];
													if (!empty($sixthDate)) 
													{
														$sixthDate = date("d-m-Y", strtotime($sixthDate));
													}
													
													
													$seventhDate = $row2['seventhDate'];
													if (!empty($seventhDate)) 
													{
														$seventhDate = date("d-m-Y", strtotime($seventhDate));
													}
													
													
													$eighthDate = $row2['eighthDate'];
													if (!empty($eighthDate)) 
													{
														$eighthDate = date("d-m-Y", strtotime($eighthDate));
													}
															
													
													$ninthDate = $row2['ninthDate'];
													if (!empty($ninthDate)) 
													{
														$ninthDate = date("d-m-Y", strtotime($ninthDate));
													}
													
													
													$tenthDate = $row2['tenthDate'];
													if (!empty($tenthDate)) 
													{
														$tenthDate = date("d-m-Y", strtotime($tenthDate));
													}
													
													
													$eleventhDate = $row2['eleventhDate'];
													if (!empty($eleventhDate)) 
													{
														$eleventhDate = date("d-m-Y", strtotime($eleventhDate));
													}
													
													
													$twelfthDate = $row2['twelfthDate'];
													if (!empty($twelfthDate)) 
													{
														$twelfthDate = date("d-m-Y", strtotime($twelfthDate));
													}
													
													echo "<tr style='background-color: ". $bg_color .";'><td>$name</td><td> $product_name <br /><br /><u>Date & Time </u><br />$startDate @ $time <br/>$secondDate @ $time <br/>$thirdDate @ $time <br/>$forthDate @ $time <br/>$fifthDate @ $time <br/>$sixthDate @ $time <br/>$seventhDate @ $time <br/>$eighthDate @ $time <br/>$ninthDate @ $time <br/>$tenthDate @ $time <br/>$eleventhDate @ $time <br/>$twelfthDate @ $time<br/>$endDate @ $time <br/></td><td>&euro; $amount</td></tr>";
												}
												
												
											 }
											 
										}
										
							
								}
								echo "</table>";
							 }
							 else
							 {
								 echo "You have not booked any clubs yet. <br /><br /> <a href='index.php'> Book a clode club </a>";
							 }
						}
						
						
						
						
						
						
						
					  }
					  catch(Exception $e)
					  {
						
						echo 'You could not be logged in. 
							  You must be logged in to view this page.';
						login_form();
						
					  }      
					}
					
						//user is logged in
				if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
				{
						$result4 = selectUserByEmail($_SESSION['valid_user']);

						if($result4)
						{
							$num_result = $result4->num_rows;
							if($num_result > 0)
							{
								for ($i=0; $i<$num_result; $i++)
								{
									$row = $result4->fetch_assoc();
									$userId = $row['userId'];
									$fname = $row['fname'];
									$lname = $row['lname'];
									
								}
							 }
						}
						
						
						
						$result1 = selectUserClubs($userId);

						if($result1)
						{
							$num_result1 = $result1->num_rows;
							if($num_result1 > 0)
							{
								echo "<table cellpadding='10'><tr><td><b>Club</b></td><td><b>Beschrijving</b></td><td><b>Bedrag</b></td></tr>";
								for ($i=0; $i<$num_result1; $i++)
								{
										$row1 = $result1->fetch_assoc();
										$order_id = $row1['order_id'];
										$product_id = $row1['product_id'];
										$amount = $row1['amount'];
										$status = $row1['status'];

																			
									
										$result2 = selectProductById($product_id);

										if($result2)
										{
											$num_result2 = $result2->num_rows;
											if($num_result2 > 0)
											{
												
												for ($c=0; $c<$num_result2; $c++)
												{
													$bg_color = $i % 2 === 0 ? "#e6ffff" : "#b3ffff";
													
													$row2 = $result2->fetch_assoc();
													$name = $row2['name_nl'];
													$product_name = $row2['product_name_nl'];
													$briefDescription = $row2['briefDescription_nl'];
													$product_desc = $row2['product_desc_nl'];
													$product_amount = $row2['product_amount'];
													$period = $row2['period_nl'];
													$startDate = $row2['startDate'];
													$startDate = date("d-m-Y", strtotime($startDate));
													
													$endDate = $row2['endDate'];
													$endDate = date("d-m-Y", strtotime($endDate));
													
													$time = $row2['time'];
													
													$secondDate = $row2['secondDate'];
													$secondDate = date("d-m-Y", strtotime($secondDate));
													
													$thirdDate = $row2['thirdDate'];
													$thirdDate = date("d-m-Y", strtotime($thirdDate));
													
													$forthDate = $row2['forthDate'];
													$forthDate = date("d-m-Y", strtotime($forthDate));
													
													$fifthDate = $row2['fifthDate'];
													if (!empty($fifthDate)) 
													{
														$fifthDate = date("d-m-Y", strtotime($fifthDate));
													}
													
													
													$sixthDate = $row2['sixthDate'];
													if (!empty($sixthDate)) 
													{
														$sixthDate = date("d-m-Y", strtotime($sixthDate));
													}
													
													
													$seventhDate = $row2['seventhDate'];
													if (!empty($seventhDate)) 
													{
														$seventhDate = date("d-m-Y", strtotime($seventhDate));
													}
													
													
													$eighthDate = $row2['eighthDate'];
													if (!empty($eighthDate)) 
													{
														$eighthDate = date("d-m-Y", strtotime($eighthDate));
													}
															
													
													$ninthDate = $row2['ninthDate'];
													if (!empty($ninthDate)) 
													{
														$ninthDate = date("d-m-Y", strtotime($ninthDate));
													}
													
													
													$tenthDate = $row2['tenthDate'];
													if (!empty($tenthDate)) 
													{
														$tenthDate = date("d-m-Y", strtotime($tenthDate));
													}
													
													
													$eleventhDate = $row2['eleventhDate'];
													if (!empty($eleventhDate)) 
													{
														$eleventhDate = date("d-m-Y", strtotime($eleventhDate));
													}
													
													
													$twelfthDate = $row2['twelfthDate'];
													if (!empty($twelfthDate)) 
													{
														$twelfthDate = date("d-m-Y", strtotime($twelfthDate));
													}
													
													echo "<tr style='background-color: ". $bg_color .";'><td>$name</td><td> $product_name <br /><br /><u>Date & Time </u><br />$startDate @ $time <br/>$secondDate @ $time <br/>$thirdDate @ $time <br/>$forthDate @ $time <br/>$fifthDate @ $time <br/>$sixthDate @ $time <br/>$seventhDate @ $time <br/>$eighthDate @ $time <br/>$ninthDate @ $time <br/>$tenthDate @ $time <br/>$eleventhDate @ $time <br/>$twelfthDate @ $time<br/>$endDate @ $time <br/></td><td>&euro; $amount</td></tr>";
												}
												
												
											 }
											 
										}
										
							
								}
								echo "</table>";
							 }
							 else
							 {
								 echo "Je hebt geen geregistreerd clubs. <br /><br /> <a href='clubs_nl.php'> Book a clode club </a>";
							 }
						}
				}
?>
          
		

      </div>
    </section><!-- #services -->

    
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>African Centered Architecture</h3>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">About us</a></li>
              <li><a href="#">Services</a></li>
              <li><a href="#">Terms of service</a></li>
              <li><a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
            <?php display_footer_address(); ?>
			<br />
			<?php display_footer_email(); ?>
			<br />
			<?php display_footer_phone(); ?>
			 
            </p>
<!--
            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
-->
          </div>
<!--
          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>
-->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>African Centered Architecture</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
