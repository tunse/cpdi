<?php
	include('output_fns.php');
    require_once('min_auto_fns.php');
    $searchterm=@$_POST['searchterm'];
    $value=@$_POST['value'];
?>

<?php include "includes/header.php" ?>

<section id="services" class="section-bg">
    <div class="container">

      <header class="section-header">


      </header>

      <div class="row">
      <div class="container mt-4"><br /><br />




                  <div class="container-fluid">
                      <div class="row mt-3" style="color:#495057;">
                      <div class="col-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                          <div class="col-9">
                              <p class="h3">Compendium</p>
      <?php

                                  echo "<p>Winning design search results for <b>$searchterm</b> $value</p>";

                                  if ($searchterm=="Country")
                                  {
                                      $result2 = selectApprovedCompetetionsByCountry($value);

                                  }
                                  elseif ($searchterm=="Ethnic")
                                  {
                                      $result2 = selectApprovedCompetetionsByEthnicGroup($value);
                                  }
                                  elseif ($searchterm=="Architect")
                                  {
                                      $result2 = selectApprovedCompetetionsByLastName($value);

                                      if($result2)
                                      {
                                          $num_result2 = $result2->num_rows;
                                          if($num_result2 < 0)
                                          {
                                              $result2 = selectApprovedCompetetionsByFirstName($value);
                                          }
                                      }


                                  }
      ?>



                              <!-- Grid row -->
                              <div class="row">

                              <?php



                                      if($result2)
                                      {
                                          $num_result2 = $result2->num_rows;
                                          if($num_result2 > 0)
                                          {
                                              for ($c=0; $c<$num_result2; $c++)
                                              {
                                                  $row2 = $result2->fetch_assoc();
                                                  $competitionDesign = $row2['competition_design'];
                                                  $fname = $row2['input_first_name'];
                                                  $lname = $row2['input_last_name'];


                              ?>
                                                  <!-- Grid column -->
                                                  <div class="col-lg-4 col-md-6 mb-4">

                                                  <!--Modal: Name-->
                                                  <div class="modal fade" id="modal<?php echo "$c"; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog modal-lg" role="document">

                                                      <!--Content-->
                                                      <div class="modal-content">

                                                      <!--Body-->
                                                      <div class="modal-body mb-0 p-0">

                                                      <img class="img-fluid z-depth-1" src="./studentDesignMaterials/<?php echo $competitionDesign; ?>" alt="Picture" data-toggle="modal" data-target="#<?php echo $c; ?>">

                                                      </div>

                                                      <!--Footer-->
                                                      <div class="modal-footer justify-content-center">

                                                      <?php echo "$fname $lname"; ?> <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                                      </div>

                                                      </div>
                                                      <!--/.Content-->

                                                  </div>
                                                  </div>
                                                  <!--Modal: Name-->

                                                  <a><img class="img-fluid z-depth-1" src="./studentDesignMaterials/<?php echo $competitionDesign; ?>" alt="video"
                                                      data-toggle="modal" data-target="#modal<?php echo "$c"; ?>"></a>

                                                  </div>
                                                  <!-- Grid column -->

                              <?php

                                              }
                                          }
                                          else
                                          {
                                              echo "There are no search results";
                                          }
                                      }
                                      else
                                      {
                                          echo "Poor";
                                      }


                              ?>




                              </div>
                              <!-- Grid row -->

                              <b>New search</b>
                                  <form action="compendiumsearch" method="post">
                                      <div class="form-group">
                                          <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                              <label class="btn btn-secondary">
                                                  <input type="radio" name="searchterm" autocomplete="off" value="Country"> Country
                                              </label>
                                              <label class="btn btn-secondary active">
                                                  <input type="radio" name="searchterm" autocomplete="off" checked value="Ethnic"> Ethnic Group
                                              </label>
                                              <label class="btn btn-secondary">
                                                  <input type="radio" name="searchterm" autocomplete="off" value="Architect"> Name of Architect
                                              </label>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <input type="text" class="form-control" id="url" name="value" placeholder="" required="required">
                                      </div>
                                      <div class="form-group">
                                      <button type="submit" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;">Search</button>
                                      </div>

                                  </form>


                          </div>


                      </div>
                  </div>




          </div>










      </div>

    </div>
  </section><!-- #services -->
<?php include "includes/footer.php" ?>
