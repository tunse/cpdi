<?php
include('output_fns.php');
    require_once('min_auto_fns.php');

    if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		// $result4 = selectUserByEmail($_SESSION['valid_user']);
    //
		// if($result4)
		// {
		// 	$num_result = $result4->num_rows;
		// 	if($num_result > 0)
		// 	{
		// 		for ($i=0; $i<$num_result; $i++)
		// 		{
		// 			$row = $result4->fetch_assoc();
		// 			$userId = $row['id'];
		// 			$fname = $row['input_first_name'];
		// 			$lname = $row['input_last_name'];
    //
		// 		}
		// 	 }
		// }
    $result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
    $userId = $result4[0]['id'];
    $fname = $result4[0]['input_first_name'];
    $lname = $result4[0]['input_last_name'];
	}
	else
	{
		header("Location: /signin");
    exit();
	}

include "includes/header.php" ?>

<section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"><br /><br />

            <ul class="nav nav-tabs responsive " role="tablist" style="background-color:#970C7B; border-color: #970C7B;">
                <li class="nav-item">
                    <a class="nav-link active" style="color:#fff;" data-toggle="tab" href="#manifesto" role="tab">My Courses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" style="color:#fff;" href="#founder" role="tab">My Architecture Competitions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" style="color:#fff;" href="#council" role="tab">My Subscription</a>
                </li>

            </ul><!-- Tab panes -->
            <div class="tab-content responsive">
                <div class="tab-pane active" id="manifesto" role="tabpanel">
                    <div class="container-fluid">
                        <!-- <div class="row mt-3" style="color:#495057;"> -->
                            <div  style="color:#495057">
                                <p class="h3">My Courses</p>

                                            <?php
                                        //user is logged in
                                if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
                                {
                                        // $result4 = selectUserByEmail($_SESSION['valid_user']);

                                        // if($result4)
                                        // {
                                        //     $num_result = $result4->num_rows;
                                        //     if($num_result > 0)
                                        //     {
                                        //         for ($i=0; $i<$num_result; $i++)
                                        //         {
                                        //             $row = $result4->fetch_assoc();
                                        //             $userId = $row['id'];
                                        //             $fname = $row['input_first_name'];
                                        //             $lname = $row['input_last_name'];
                                        //
                                        //         }
                                        //     }
                                        // }

                                        $result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
                                        $userId = $result4[0]['id'];
                                        $fname = $result4[0]['input_first_name'];
                                        $lname = $result4[0]['input_last_name'];

                                        $result1 = selectUserClubs($userId);

                                        if($result1)
                                        {
                                            $num_result1 = $result1->num_rows;
                                            if($num_result1 > 0)
                                            {
                                                echo "<table cellpadding='10'><tr><td><b>Course</b></td><td><b>Description</b></td><td><b>Amount</b></td><td><b>My uploaded Course Material</b></td><td><b>Upload Course Material</b></td><td><b>View sent Feedback</b></td><td><b>Send Feedback</b></td></tr>";
                                                for ($i=0; $i<$num_result1; $i++)
                                                {
                                                        $row1 = $result1->fetch_assoc();
                                                        $order_id = $row1['id'];
                                                        $course_id = $row1['course_id'];
                                                        $amount = $row1['input_amount'];
                                                        $status = $row1['bool_status'];



                                                        $result2 = selectCourse($course_id);

                                                        if($result2)
                                                        {
                                                            $num_result2 = $result2->num_rows;
                                                            if($num_result2 > 0)
                                                            {

                                                                for ($c=0; $c<$num_result2; $c++)
                                                                {
                                                                    $bg_color = $i % 2 === 0 ? "#e6ffff" : "#b3ffff";

                                                                    $row2 = $result2->fetch_assoc();

                                                                    $courseName = $row2['input_course_name'];
                                                                    $course_id = $row2['id'];
                                                                    $courseDescription = $row2['text_course_description'];
                                                                    $courseDetails = $row2['text_course_details'];
                                                                    $courseAmount = $row2['input_course_amount'];
                                                                    $coursePromotion = $row2['input_course_promotion'];
                                                                    $conferenceLink =  $row2['input_conference_link'];


                                                                    echo "<tr style='background-color: ". $bg_color .";'><td>$courseName<br /><br /><a href='course-materials?course=$course_id' style='color:#6666FF;'>View supporting documents</a><br /><br /><p>$conferenceLink </p></td><td> $courseDescription</td><td>$amount</td>";
                    ?>
                                                                    <td>
                                                                    <a href="student-course-materials?course=<?php echo "$course_id" ?>" class="btn-primary btn-block btn-sm" style='background-color:#970C7B; border-color: #970C7B;'><b>View uploaded Course Materials</b></a>
                                                                    </td>
                                                                    <td>
                                                                    <a href="upload-student-course-material?course=<?php echo "$course_id" ?>" class="btn-primary btn-block btn-sm" style='background-color:#970C7B; border-color: #970C7B;'><b>Upload Course Material</b></a>
                                                                    </td>
                                                                    <td>
                                                                    <a href="student-course-feedback?course=<?php echo "$course_id" ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true" style='background-color:#970C7B; border-color: #970C7B;'>View sent feedback</a></td>
                                                                    <td>
                                                                    <a href="create-feedback-form?course=<?php echo "$course_id" ?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true" style='background-color:#970C7B; border-color: #970C7B;'>Send Feedback to Professor</a></td>
                                                                    </tr>
                    <?php
                                                                }


                                                            }

                                                        }


                                                }
                                                echo "</table>";
                                            }
                                            else
                                            {
                                                echo "You have not booked any course yet. <br /><br /> <a href='/courses?active=yes' class='btn btn-success btn-sm' style='background-color:#970C7B; border-color: #970C7B;'> Book a course </a>";
                                            }
                                        }
                                }
                ?>



                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="founder" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">

                            <div class="col-12" style="color:#495057;">
                                <p class="h3">My Competitions</p>
                                            <?php

                                            //user is logged in
                                    if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
                                    {
                                            // $result4 = selectUserByEmail($_SESSION['valid_user']);
                                            //
                                            // if($result4)
                                            // {
                                            //     $num_result = $result4->num_rows;
                                            //     if($num_result > 0)
                                            //     {
                                            //         for ($i=0; $i<$num_result; $i++)
                                            //         {
                                            //             $row = $result4->fetch_assoc();
                                            //             $userId = $row['id'];
                                            //             $fname = $row['input_first_name'];
                                            //             $lname = $row['input_last_name'];
                                            //
                                            //         }
                                            //     }
                                            // }
                                            $result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
                                            $userId = $result4[0]['id'];
                                            $fname = $result4[0]['input_first_name'];
                                            $lname = $result4[0]['input_last_name'];



                                            $result1 = selectUserCompetitions($userId);

                                            if($result1)
                                            {
                                                $num_result1 = $result1->num_rows;
                                                if($num_result1 > 0)
                                                {
                                                    echo "<table cellpadding='10'><tr><td><b>Competition</b></td><td><b>Description</b></td><td><b>Amount</b></td><td><b>Design brief</b></td><td><b>Template</b></td><td><b>Upload your design</b></td></tr>";
                                                    for ($i=0; $i<$num_result1; $i++)
                                                    {
                                                            $row1 = $result1->fetch_assoc();
                                                            $order_id = $row1['id'];
                                                            $course_id = $row1['competition_id'];
                                                            $competitionamount = $row1['input_amount'];
                                                            $status = $row1['bool_status'];



                                                            $result2 = selectCompetition($course_id);

                                                            if($result2)
                                                            {
                                                                $num_result2 = $result2->num_rows;
                                                                if($num_result2 > 0)
                                                                {

                                                                    for ($c=0; $c<$num_result2; $c++)
                                                                    {
                                                                        $bg_color = $i % 2 === 0 ? "#e6ffff" : "#b3ffff";

                                                                        $row2 = $result2->fetch_assoc();

                                                                        $courseName = $row2['input_name'];
                                                                        $course_id = $row2['id'];
                                                                        $courseDescription = $row2['text_description'];
                                                                        $year = $row2['input_year'];

                                                                        $coursePromotion = $row2['input_amount'];


                                                                        echo "<tr style='background-color: ". $bg_color .";'><td>$courseName $year</td><td> $courseDescription</td><td>$competitionamount</td>";
                              ?>
                                                                        <td>
                                                                        <a href="https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;" target="_blank"><b>View design brief</b></a>
                                                                        </td>
                                                                        <td>
                                                                        <a href="/cpdi/CompetitionSubmissionTemplate.PNG" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;" target="_blank"><b>Download template</b></a>
                                                                        </td>
                                                                        <td>
                                                                        <a href="student-design-materials?course=<?php echo "$course_id" ?>" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Upload your design</b></a>
                                                                        </td>

                                                                        </tr>
                              <?php
                                                                    }


                                                                }

                                                            }


                                                    }
                                                    echo "</table>";
                                                }
                                                else
                                                {
                                                    echo "You have not booked any competition yet. <br /><br /> <a href='competition-registration-form?competition=21' class='btn btn-success btn-sm' style='background-color:#970C7B; border-color: #970C7B;'> Register for competition </a>";
                                                }
                                            }
                                    }
                              ?>






                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="council" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3">

                            <div class="col-9" style="color:#495057;">
                            <p class="h3">My Subscriptions</p>
















                            </div>



                        </div>



                    </div>
                </div>














            </div>
            </div>










        </div>

      </div>
    </section><!-- #services -->
<?php include "includes/footer.php" ?>
