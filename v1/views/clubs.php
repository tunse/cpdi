<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');



?>

<?php include "includes/header.php" ?>
    <!--==========================
      Services Section
    ============================-->
<?php
				$result1 = selectProducts('SCRATCH');

				if($result1)
				{
					$num_result = $result1->num_rows;
					if($num_result > 0)
					{
						for ($i=0; $i<$num_result; $i++)
						{
							$row = $result1->fetch_assoc();
							$scratch_name = $row['name'];
							$scratch_product_name = $row['product_name'];
							$scratch_briefDescription = $row['briefDescription'];
							$scratch_product_desc = $row['product_desc'];
							$scratch_product_amount = $row['product_amount'];
							$scratch_period = $row['period'];
							$scratch_startDate = $row['startDate'];
							$scratch_endDate = $row['endDate'];
							$scratch_time = $row['time'];
						}
					 }
				}


				$result1 = selectProducts('PYTHON');

				if($result1)
				{
					$num_result = $result1->num_rows;
					if($num_result > 0)
					{
						for ($i=0; $i<$num_result; $i++)
						{
							$row = $result1->fetch_assoc();
							$python_name = $row['name'];
							$python_product_name = $row['product_name'];
							$python_briefDescription = $row['briefDescription'];
							$python_product_desc = $row['product_desc'];
							$python_product_amount = $row['product_amount'];
							$python_period = $row['period'];
							$python_startDate = $row['startDate'];
							$python_endDate = $row['endDate'];
							$python_time = $row['time'];
						}
					 }
				}


				$result1 = selectProducts('HTML & CSS');

				if($result1)
				{
					$num_result = $result1->num_rows;
					if($num_result > 0)
					{
						for ($i=0; $i<$num_result; $i++)
						{
							$row = $result1->fetch_assoc();
							$html_name = $row['name'];
							$html_product_name = $row['product_name'];
							$html_briefDescription = $row['briefDescription'];
							$html_product_desc = $row['product_desc'];
							$html_product_amount = $row['product_amount'];
							$html_period = $row['period'];
							$html_startDate = $row['startDate'];
							$html_endDate = $row['endDate'];
							$html_time = $row['time'];
						}
					 }
        }



		$result1 = selectProducts('TYPING CLASS');

		if($result1)
		{
			$num_result = $result1->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result1->fetch_assoc();
					$typing_name = $row['name'];
					$typing_product_name = $row['product_name'];
					$typing_briefDescription = $row['briefDescription'];
					$typing_product_desc = $row['product_desc'];
					$typing_product_amount = $row['product_amount'];
					$typing_period = $row['period'];
					$typing_startDate = $row['startDate'];
					$typing_startDate = date("d-m-Y", strtotime($html_startDate));
					$typing_endDate = $row['endDate'];
					$typing_endDate = date("d-m-Y", strtotime($html_endDate));
					$typing_time = $row['time'];
				}
			 }
		}

?>



    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3><br />Code Clubs</h3>
          <p>Please choose your Code Club</p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/scratchsmall.svg" class="img-fluid" alt="Scratch"></div>
              <h4 class="title"><?php echo "$scratch_name" ?></h4>
              <p class="description"><?php echo "$scratch_briefDescription"; ?> <br /><a href="book_now.php?course=<?php echo "$scratch_name" ?>" class="btn btn-success"><b>Book code club</b></a></p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">

			<div class="box">

              <div class="icon"><img src="img/pythonsmall.svg" class="img-fluid" alt="Python"></div>
              <h4 class="title"><?php echo "$python_name" ?></h4>
              <p class="description"><?php echo "$python_briefDescription"; ?> <br /><a href="book_now.php?course=<?php echo "$python_name" ?>" class="btn btn-success"><b>Book code club</b></a></p>

            </div>

          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/htmlcsssmall.svg" class="img-fluid" alt="Html & CSS"></div>


              <h4 class="title"><?php echo "$html_name" ?></h4>
			  <?php $html_name = urlencode($html_name); ?>
              <p class="description"><?php echo "$html_briefDescription"; ?> <br /><a href="book_now.php?course=<?php echo "$html_name" ?>" class="btn btn-success"><b>Book code club</b></a></p>
            </div>
          </div>
         <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/typeless.svg" class="img-fluid" alt="Typing Class"></div>
             <h4 class="title"><?php echo "$typing_name" ?></h4>
              <p class="description"><?php echo "$typing_briefDescription"; ?> <br /><a href="book_now.php?course=<?php echo "$typing_name" ?>" class="btn btn-success"><b>Book typing class</b></a></p>
            </div>
          </div>



        </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
