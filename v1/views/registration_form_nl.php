<?php 

	session_start();
	include('output_fns.php'); 
	require_once('min_auto_fns.php'); 
	
	$product = $_GET['product'];
	
	
	//User logged in proceed 
	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		header("Location: registerexisting.php?product=$product");
	}
	
	
	if ($product==""){
		header("Location: index_nl.php");
		
	}
	
	
	$result1 = selectProductById($product);

	if($result1)
	{
		$num_result = $result1->num_rows;
		if($num_result > 0)
		{
			for ($i=0; $i<$num_result; $i++)
			{
				$row = $result1->fetch_assoc();
				$name = $row['name_nl'];
				$product_name = $row['product_name_nl'];
				$briefDescription = $row['briefDescription_nl'];
				$product_desc = $row['product_desc_nl'];
				$product_amount = $row['product_amount'];
				$period = $row['period_nl'];
				
				
				$startDate = $row['startDate'];
				$startDate = date("d-m-Y", strtotime($startDate));
				
				$endDate = $row['endDate'];
				$endDate = date("d-m-Y", strtotime($endDate));
				
				$secondDate = $row['secondDate'];
				$secondDate = date("d-m-Y", strtotime($secondDate));
				
				$thirdDate = $row['thirdDate'];
				$thirdDate = date("d-m-Y", strtotime($thirdDate));
				
				$forthDate = $row['forthDate'];
				$forthDate = date("d-m-Y", strtotime($forthDate));
				
        $time = $row['time'];
        
        $fifthDate = $row['fifthDate'];
        if (!empty($fifthDate)) 
        {
          $fifthDate = date("d-m-Y", strtotime($fifthDate));
        }
       
        
        $sixthDate = $row['sixthDate'];
        if (!empty($sixthDate)) 
        {
          $sixthDate = date("d-m-Y", strtotime($sixthDate));
        }
       
        
        $seventhDate = $row['seventhDate'];
        if (!empty($seventhDate)) 
        {
          $seventhDate = date("d-m-Y", strtotime($seventhDate));
        }
       
        
        $eighthDate = $row['eighthDate'];
        if (!empty($eighthDate)) 
        {
          $eighthDate = date("d-m-Y", strtotime($eighthDate));
        }
                 
        
        $ninthDate = $row['ninthDate'];
        if (!empty($ninthDate)) 
        {
          $ninthDate = date("d-m-Y", strtotime($ninthDate));
        }
       
        
        $tenthDate = $row['tenthDate'];
        if (!empty($tenthDate)) 
        {
          $tenthDate = date("d-m-Y", strtotime($tenthDate));
        }
       
        
        $eleventhDate = $row['eleventhDate'];
        if (!empty($eleventhDate)) 
        {
          $eleventhDate = date("d-m-Y", strtotime($eleventhDate));
        }
       
        
        $twelfthDate = $row['twelfthDate'];
        if (!empty($twelfthDate)) 
        {
          $twelfthDate = date("d-m-Y", strtotime($twelfthDate));
        }



			}
		 }
		 else{
			 header("Location: index_nl.php");
		 }
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>African Centered Architecture</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
  
  
  
  <style type="text/css">
      
      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_with_links_nl(); ?>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <br /><br /><h3>Code Club Registratie</h3>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
		  
  	  
            <div class="box">
              <div class="icon">
<?php if ($name == "SCRATCH")
	  {
		  echo '<img src="img/scratchsmall.svg" class="img-fluid" alt="Scratch">';
	  }
	  elseif ($name == "PYTHON"){
		  echo '<img src="img/pythonsmall.svg" class="img-fluid" alt="Python">';
	  }
	  elseif ($name == "HTML & CSS"){
		  echo '<img src="img/htmlcsssmall.svg" class="img-fluid" alt="HTML & CSS">';
	  }
	  elseif ($name == "TYPECURSUS"){
		  echo '<img src="img/typeless.svg" class="img-fluid" alt="3D Printer">';
	  }
?>
			  </div>
              <h4 class="title"><?php echo "$name ($period)"; ?></h4>
              <p class="description"><?php echo "$briefDescription"; ?><br /><br /> <b>Bedrag:</b> €<?php echo $product_amount; ?> <br /> <b>Tijd:</b> <?php echo $time; ?><br /><b>Duur:</b> 1 Uur (Per sessie)<br /><b>Dagen:</b><br /><?php echo $startDate; ?> <br /><?php echo $secondDate; ?> <br /><?php echo $thirdDate; ?> <br /><?php echo $forthDate; ?> <br />
              
              <?php
            if (!empty($fifthDate)) 
            {
              echo "$fifthDate <br />";
            }

            if (!empty($sixthDate)) 
            {
              echo "$sixthDate <br />";
            }

            if (!empty($seventhDate)) 
            {
              echo "$seventhDate <br />";
            }

            if (!empty($eighthDate)) 
            {
              echo "$eighthDate <br />";
            }

            if (!empty($ninthDate)) 
            {
              echo "$ninthDate <br />";
            }

            if (!empty($tenthDate)) 
            {
              echo "$tenthDate <br />";
            }

            if (!empty($eleventhDate)) 
            {
              echo "$eleventhDate <br />";
            }

            if (!empty($twelfthDate)) 
            {
              echo "$twelfthDate <br />";
            }
          ?>
              
              <?php echo $endDate; ?> <br /><br /></p>
            </div>
          </div>
		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
					  <div class="register-form">
						<form action="register_new_nl.php" method="post">
						
						  <input id="prodId" name="prodId" type="hidden" value="<?php echo $product; ?>">
						  
						  <h2 class="text-center">Registratie</h2>
						  <p class="hint-text">Vul in jouw persoonlijke gegevens.</p>
						  <div class="row">
							<div class="col-md-6 col-xs-12">
							  <div class="form-group">
								<input type="text" name="first_name" class="form-control" placeholder="Voornaam" required="required">
							  </div>
							</div>
							<div class="col-md-6 col-xs-12">
							  <div class="form-group">
								<input type="text" name="last_name" class="form-control" placeholder="Achternaam" required="required">
							  </div> 
							</div>  
						  </div><br />
						  <div class="form-group">
							<input type="text" name="child" class="form-control" placeholder="Naam van Kind" required="required">
						  </div>
						  <div class="form-group">
							<input type="text" name="tel" class="form-control" placeholder="Mobiel" required="required">
						  </div>
						  <div class="form-group">
							<input type="email" name="email" class="form-control" placeholder="Email" required="required">
						  </div>
						  <div class="form-group">
							<input type="password" name="password" class="form-control" placeholder="Wachtwoord" required="required"> (tussen 6 en 16 tekens)
						  </div>
						  <div class="form-group">
							<input type="password" name="confirm_password" class="form-control" placeholder="Bevestig Wachtwoord" required="required">
						  </div>        
						  <div class="form-group">
							<label class="checkbox-inline">
							  <input type="checkbox" required="required"> Je gaat akoord met de  <a href="#">Algemene</a> & <a href="#">Voowaarden</a>.
							</label>
						  </div>
						  <div class="row">
							<div class="col-md-6 col-xs-12">
							  <div class="form-group">
								<input type="submit" value="Registreer" class="btn btn-primary btn-block btn-lg" tabindex="7">
							  </div>
							</div>
							
						  </div>
						</form>
			</div>
        </div>
		

      </div>
    </section><!-- #services -->

    
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>African Centered Architecture</h3>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">About us</a></li>
              <li><a href="#">Services</a></li>
              <li><a href="#">Terms of service</a></li>
              <li><a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
            <?php display_footer_address(); ?>
			<br />
			<?php display_footer_email(); ?>
			<br />
			<?php display_footer_phone(); ?>
			 
            </p>
<!--
            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
-->
          </div>
<!--
          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>
-->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>African Centered Architecture</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
