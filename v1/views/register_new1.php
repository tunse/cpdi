<?php

$paypal_url='https://www.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id='design@southernsahara.com'; // Business email ID

$course_id = $_GET['course'];

   $fetchcourse = selectContent($conn, "panel_courses", ['id' => $course_id]);

   $course_name = $fetchcourse[0]['input_course_name'];
   $course_description = $fetchcourse[0]['text_course_description'];
   $course_promotion = $fetchcourse[0]['input_course_promotion'];
   $course_datails = $fetchcourse[0]['text_course_details'];


   $paypalName = $course_name;


 ?>

<?php include "includes/header.php" ?>

<style type="text/css">

    .register-form{
      margin: 0px auto;
      padding: 25px 20px;
      background: #3a1975;
      box-shadow: 2px 2px 4px #ab8de0;
      border-radius: 5px;
      color: #fff;
    }
    .register-form h2{
      margin-top: 0px;
      margin-bottom: 15px;
      padding-bottom: 5px;
      border-radius: 10px;
      border: 1px solid #25055f;
    }
  </style>

  <section id="services" class="section-bg">
        <div class="container"><br />

          <header class="section-header">
            <br /><br /><h3>Proceed to Payment</h3>
          </header>

          <div class="row">

            <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
              <div class="box">

  			  <h4 class="title"><?php echo $course_name; ?></h4>
                <p class="description"><?php echo $course_description; ?></p><br />
                <p class="description"><b>Amount </b> &#36;<?php echo $course_promotion; ?></p>

              </div>
            </div>

  		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2" style="color:#495057;">



        				<br /><br />
        				<form action='<?php echo $paypal_url; ?>' method='post'>
        				<input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
        				<input type='hidden' name='cmd' value='_xclick'>
        				<input type='hidden' name='item_name' value='<?php echo "$paypalName"; ?>'>
        				<input type='hidden' name='item_number' value='<?php echo "$order_id"; ?>'>
        				<input type='hidden' name='amount' value='<?php echo "$course_promotion"; ?>'>
        				<input type='hidden' name='no_shipping' value='1'>
        				<input type='hidden' name='currency_code' value='USD'>
        				<input type='hidden' name='cancel_return' value='http://www.cpdiafrica.org/cancel'>
        				<input type='hidden' name='return' value='https://www.cpdiafrica.org/success'>
        				<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        				</form> <br />
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                    <a href="/banktransfer?order=<?php echo $order_id; ?>" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Pay by bank transfer or PayStack</b></a>
                    </div>
                </div>
                <br />



                <?php
          $urlEncodedText = "I'm interested in the course: $course_name";
          $urlEncodedText = urlencode($urlEncodedText);
          echo "<a href='https://wa.me/16786509145?text=$urlEncodedText' target='_blank' style='color:#008000;'>Send a WhatsApp message <img src='./cpdi/img/whatsapp.png'></a>";
  ?>










    </div>

    </div>
  </section><!-- #services -->


</main>


<?php include "includes/footer.php" ?>
