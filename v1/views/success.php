<?php
session_start();
include('output_fns.php');
require_once('min_auto_fns.php');

$item_no = $_GET['item_number'];

$item_transaction = $_GET['tx'];

$item_price = $_GET['amt'];

$item_currency = $_GET['cc'];

$conn1 = db_connect();


?>


<?php include "includes/header.php" ?>

    <!--==========================
      Services Section
    ============================-->



    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"> <br /><br />




                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                        <div class="col-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                            <div class="col-9">
                                <p class="h3">Payment</p>


                                <?php

                                    if ($item_no != "" && $item_price !="")
                                    {

                                              if ($item_no >= 2000000)
                                              {
                                                      //select competition


                                                      $result1 = selectCompetitionOrder($item_no);

                                                      if($result1)
                                                      {
                                                        $num_result = $result1->num_rows;
                                                        if($num_result > 0)
                                                        {
                                                          for ($i=0; $i<$num_result; $i++)
                                                          {
                                                            $row = $result1->fetch_assoc();
                                                            $product_id = $row['competition_id'];
                                                            $amount = $row['input_amount'];
                                                            $user_id = $row['user_id'];

                                                          }
                                                        }

                                                      }

                                                      $result4 = selectUserById($user_id);

                                                      if($result4)
                                                      {
                                                        $num_result = $result4->num_rows;
                                                        if($num_result > 0)
                                                        {
                                                          for ($i=0; $i<$num_result; $i++)
                                                          {
                                                            $row = $result4->fetch_assoc();
                                                            $userId = $row['id'];
                                                            $fname = $row['input_first_name'];
                                                            $lname = $row['input_last_name'];
                                                            $email = $row['input_email'];

                                                          }
                                                        }
                                                      }

                                                      $result1 = selectCompetition($product_id);

                                                      if($result1)
                                                      {
                                                          $num_result = $result1->num_rows;
                                                          if($num_result > 0)
                                                          {
                                                            for ($i=0; $i<$num_result; $i++)
                                                            {
                                                              $row = $result1->fetch_assoc();
                                                              $name = $row['input_name'];
                                                              $briefDescription = $row['text_description'];


                                                            }
                                                          }
                                                      }

                                                      if ($amount == $item_price)
                                                      {
                                                        $sql = "UPDATE read_competition_order SET input_status = 'CONFIRMED', pay_pal_id = '$item_transaction' WHERE order_id=$item_no";
                                                        $result = $conn1->query($sql);



                                                        $to = $email;
                                                        $subject = 'African Centered Architecture';
                                                        $message = "Dear $fname $lname, \r\n \r\n Thank you for your payment for $name. \r\n \r\n Your registration is now confirmed! . \r\n \r\n $name  \r\n $briefDescription \r\n Kind regards, \r\n African Centered Architecture";

                                                        $headers = 'From: design@cpdiafrica.org' . "\r\n" .
                                                          'Reply-To: design@cpdiafrica.org' . "\r\n" .
                                                          'X-Mailer: PHP/' . phpversion();

                                                        mail($to, $subject, $message, $headers);

                                                        echo "Thank you for your payment. <br /><br />Your registration for the competition is now confirmed<br /><br />";
                                                        echo "$name  <br /> $briefDescription <br />";

                                                      }
                                              }
                                              elseif ($item_no < 2000000)
                                              {

                                                        $result1 = selectCourseOrder($item_no);

                                                        if($result1)
                                                        {
                                                          $num_result = $result1->num_rows;
                                                          if($num_result > 0)
                                                          {
                                                            for ($i=0; $i<$num_result; $i++)
                                                            {
                                                              $row = $result1->fetch_assoc();
                                                              $product_id = $row['course_id'];
                                                              $amount = $row['input_amount'];
                                                              $user_id = $row['user_id'];

                                                            }
                                                          }

                                                        }

                                                        $result4 = selectUserById($user_id);

                                                        if($result4)
                                                        {
                                                          $num_result = $result4->num_rows;
                                                          if($num_result > 0)
                                                          {
                                                            for ($i=0; $i<$num_result; $i++)
                                                            {
                                                              $row = $result4->fetch_assoc();
                                                              $userId = $row['id'];
                                                              $fname = $row['input_first_name'];
                                                              $lname = $row['input_last_name'];
                                                              $email = $row['input_email'];

                                                            }
                                                          }
                                                        }

                                                        $result1 = selectCourse($product_id);

                                                        if($result1)
                                                        {
                                                            $num_result = $result1->num_rows;
                                                            if($num_result > 0)
                                                            {
                                                              for ($i=0; $i<$num_result; $i++)
                                                              {
                                                                $row = $result1->fetch_assoc();
                                                                $name = $row['input_course_name'];
                                                                $briefDescription = $row['text_course_description'];


                                                              }
                                                            }
                                                        }

                                                        if ($amount == $item_price)
                                                        {
                                                          $sql = "UPDATE read_product_order SET input_status = 'CONFIRMED', pay_pal_id = '$item_transaction' WHERE order_id=$item_no";
                                                          $result = $conn1->query($sql);



                                                          $to = $email;
                                                          $subject = 'African Centered Architecture';
                                                          $message = "Dear $fname $lname, \r\n \r\n Thank you for your payment for $name. \r\n \r\n Your registration is now confirmed! . \r\n \r\n $name  \r\n $briefDescription \r\n Kind regards, \r\n African Centered Architecture";

                                                          $headers = 'From: design@cpdiafrica.org' . "\r\n" .
                                                            'Reply-To: design@cpdiafrica.org' . "\r\n" .
                                                            'X-Mailer: PHP/' . phpversion();

                                                          mail($to, $subject, $message, $headers);

                                                          echo "Thank you for your payment. <br /><br />Your registration for the course is now confirmed <br /><br />";
                                                          echo "$name  <br /> $briefDescription <br />";

                                                        }


                                              }

                                    }






                                ?>


                            </div>
                        </div>
                    </div>




            </div>










        </div>

      </div>
    </section><!-- #services -->


  </main>


	<?php include "includes/footer.php" ?>
