<?php
include('output_fns.php');
require_once('min_auto_fns.php');



    if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		// $result4 = selectUserByEmail($_SESSION['valid_user']);
    //
		// if($result4)
		// {
		// 	$num_result = $result4->num_rows;
		// 	if($num_result > 0)
		// 	{
		// 		for ($i=0; $i<$num_result; $i++)
		// 		{
		// 			$row = $result4->fetch_assoc();
		// 			$userId = $row['id'];
		// 			$fname = $row['input_first_name'];
		// 			$lname = $row['input_last_name'];
    //
		// 		}
		// 	 }
		// }
    $result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
    $userId = $result4[0]['id'];
    $fname = $result4[0]['input_first_name'];
    $lname = $result4[0]['input_last_name'];
	}
	else
	{
		header("Location: /signin");
    exit();
	}

include "includes/header.php" ?>

<section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">

          <?php

          if(isset($_SESSION['msg'])){
          ?>
            <div style="color:green; margin-top:100px;">
              <?php
                foreach ($_SESSION['msg'] as  $value) {
                  echo $value ."<br>";
                }
              ?>
            </div>

        <?php }
          unset($_SESSION['msg'])
        // var_dump($_SESSION['msg']);
        ?>


        <div class="container mt-4"><br /><br />

            <ul class="nav nav-tabs responsive " role="tablist" style="background-color:#970C7B; border-color: #970C7B;">
                <li class="nav-item">
                    <a class="nav-link active" style="color:#fff;background-color:none" data-toggle="tab" href="#manifesto" role="tab">My Courses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" style="color:#fff;background-color:none" href="#founder" role="tab">My Architecture Competitions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" style="color:#fff;background-color:none" href="#council" role="tab">My Subscription</a>
                </li>

            </ul><!-- Tab panes -->
            <div class="tab-content responsive">
                <div class="tab-pane active" id="manifesto" role="tabpanel">
                    <div class="container-fluid">
                        <!-- <div class="row mt-3" style="color:#495057;"> -->
                            <div  style="color:#495057">
                                <p class="h3 text-center">My Courses</p>

                                            <?php
                                        //user is logged in
                                if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
                                {
                                        // $result4 = selectUserByEmail($_SESSION['valid_user']);

                                        // if($result4)
                                        // {
                                        //     $num_result = $result4->num_rows;
                                        //     if($num_result > 0)
                                        //     {
                                        //         for ($i=0; $i<$num_result; $i++)
                                        //         {
                                        //             $row = $result4->fetch_assoc();
                                        //             $userId = $row['id'];
                                        //             $fname = $row['input_first_name'];
                                        //             $lname = $row['input_last_name'];
                                        //
                                        //         }
                                        //     }
                                        // }

                                        $result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
                                        $userId = $result4[0]['id'];
                                        $fname = $result4[0]['input_first_name'];
                                        $lname = $result4[0]['input_last_name'];
                                        // $usersCourses = selectContent($conn,"read_product_order",['user_id'=>$_SESSION['valid_user'],"bool_status"=>1]);
                                        $usersCourses = selectContent($conn,"read_product_order",['user_id'=>$_SESSION['valid_user']]);

?>

<div class="row d-flex justify-content-center">


<?php foreach ($usersCourses as $key => $value): ?>
  <?php $course = selectContent($conn,"panel_courses",['id'=>$value['course_id']]); ?>
  <div class="col-md-4 card " style="margin:5px">
  <div class="card-header">
<h6><?php echo $value['input_course_name'] ?></h6>
<!-- <h6><?php  // echo $course[0]['id'] ?></h6> -->
  </div>
  <?php if ($value['bool_status'] != 1){ ?>
    <p style="color:red">You haven't paid for this Course</p>
    <a style='color:blue' href='registerexisting?product=<?=$value['course_id']?>'>Click Here To Pay</a>
  <?php }else{?>
  <div class="card-body">
    <a class="btn btn-primary btn-sm" target="_blank" href="<?php echo $course[0]['input_conference_link']; ?>">Zoom Link</a>
    <a class="btn btn-primary btn-sm" target="_blank" href="course-materials?course=<?php echo $course[0]['id']; ?>">Course Material</a>
    <a class="btn btn-primary btn-sm" target="_blank" href="/create-feedback-form?course=<?php echo $course[0]['id']; ?>">Send Feedback</a>

  </div>


<?php } ?>
  </div>
<?php endforeach; ?>

<?php if (empty($usersCourses)): ?>
  <div class="col-md-4  " style="margin:5px">
    <p>You don't have an active course yet, kindly register for a course <a href="/courses?active=yes" style="color:blue">here</a> </p>
  </div>
<?php endif; ?>



</div>


                            <?php    }
                ?>



                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="founder" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">

                            <div class="col-12 text-center" style="color:#495057;">


<!--
                                  <?php
                                  $validUserComp = $conn->prepare("SELECT * FROM read_competition_order WHERE NOT bool_payment_status=0 AND user_id = '$userId'");
                                  $validUserComp->execute();

                                  $validUserCompRows = $validUserComp->fetch(PDO::FETCH_BOTH);

                                  $validUserCompRow = [];

                                  $validUserCompRow[] = $validUserCompRows;

                                  // var_dump($validUserCompRow);



                                  // var_dump($validUserCompRow);

                                  if($validUserCompRow){
                                    // nselectContent($conn, 'read_competition_order', ['user_id' => $userId, 'bool_payment_status' => ]);

                                   ?>
                                  <p>You haven't paid the competitions below :</p>
                                  <?php
                                    foreach ($validUserCompRow as $value) {
                                      echo "<a style='color:blue' href='registerexistingcompetition?product=".$value['competition_id']."'>Click Here To Pay</a>";
                                    }
                                   ?>

                                 <?php }?> -->

                                <p class="h3">My Competitions</p>
                                            <?php

                                            //user is logged in
                                    if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
                                    {
                                            // $result4 = selectUserByEmail($_SESSION['valid_user']);
                                            //
                                            // if($result4)
                                            // {
                                            //     $num_result = $result4->num_rows;
                                            //     if($num_result > 0)
                                            //     {
                                            //         for ($i=0; $i<$num_result; $i++)
                                            //         {
                                            //             $row = $result4->fetch_assoc();
                                            //             $userId = $row['id'];
                                            //             $fname = $row['input_first_name'];
                                            //             $lname = $row['input_last_name'];
                                            //
                                            //         }
                                            //     }
                                            // }
                                            $result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);


                                            $userId = $result4[0]['id'];
                                            $fname = $result4[0]['input_first_name'];
                                            $lname = $result4[0]['input_last_name'];

                                            $usersCompetition = selectContent($conn,"read_competition_order",['user_id'=>$_SESSION['valid_user']]);

                                            // $result1 = selectUserCompetitions($userId);
                                            if(count($usersCompetition) > 0){
                                              ?>

                                              <div class="row d-flex justify-content-center">


                                              <?php foreach ($usersCompetition as $key => $value): ?>
                                                <?php $competition = selectContent($conn,"panel_competitions",['id'=>$value['competition_id']]);

                                                  // die(var_dump($competition));

                                                 ?>
                                                <div class="col-md-6 card " style="margin:5px">
                                                <div class="card-header">
                                              <h6><?php echo $competition[0]['input_name'] ?></h6>
                                              <p style="color:red"> <b>Submission Deadline: October 31, 2021</b></p>




                                              <!-- <h6><?php  // echo $course[0]['id'] ?></h6> -->
                                                </div>
                                                <div class="card-body">

                                                  <a href="/CPDI2021Competitionbrief.pdf" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;" target="_blank"><b>View Design Brief</b></a>

                                                  <a href="/cpdi/CompetitionSubmissionTemplate.PNG" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;" target="_blank"><b>Download Exhibition Board Template</b></a>

                                                <a href="student-design-materials?course=<?php echo $value['competition_id'] ?>" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Upload Design & Links</b></a>


                                                </div>
                                                <?php if ($value['bool_payment_status'] != 0): ?>


                                                    <p class="text-center" style="color:red">You haven't paid for this competition</p>
                                                    <a class="text-center" style='color:blue' href='registerexistingcompetition?product=<?=$value['competition_id']?>'>Click Here To Pay</a>

                                                <?php endif; ?>
                                                </div>
                                              <?php endforeach; ?>

                                              </div>
                                          <?php }else{
                                                echo "You have not booked any competition yet. <br /><br /> <a href='/competition-registration-form?competition=21' class='btn btn-success btn-sm' style='background-color:#970C7B; border-color: #970C7B;'> Register for competition </a>";
                                            }


                                    }
                              ?>






                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="council" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3">

                            <div class="col-9" style="color:#495057;">
                            <p class="h3">My Subscriptions</p>
















                            </div>



                        </div>



                    </div>
                </div>














            </div>
            </div>










        </div>

      </div>
    </section><!-- #services -->
<?php include "includes/footer.php" ?>
