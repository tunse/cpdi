<?php

	  // session_start();

	  // include function files for this application
	  require_once('db_fns.php');
	  include('output_fns.php');
	  include('min_auto_fns.php');
	  $conn1 = db_connect();






	  //create short variable names
	  $email=@$_POST['email'];
	  $first_name=@$_POST['first_name'];
	  $last_name=@$_POST['last_name'];

	  $add1=@$_POST['add1'];
	  $add2=@$_POST['add2'];
	  $country=@$_POST['country'];
	  $postcode=@$_POST['postcode'];
	  $linkedin=@$_POST['linkedin'];
	  $profession=@$_POST['profession'];


	  $tel=@$_POST['tel'];
	  $passwd=@$_POST['password'];
	  $passwd2=@$_POST['confirm_password'];
    $courseId=@$_POST['courseId'];
    // $courseId = 21;



      $ethnic=@$_POST['ethnic'];
      $study=@$_POST['study'];
      $status=@$_POST['status'];
      $fbook=@$_POST['fbook'];
      $issu=@$_POST['issu'];
      $materialFile=@$_POST['materialFile'];




?>

<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <br /><br /><h3>Register Administrator</h3>
        </header>

        <div class="row">



		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2" style="color:#495057;">
<?php

			try
			  {
				// check forms filled in
				if (!filled_out($_POST))
				{
				  throw new Exception('You have not filled the form out correctly - please go back'
					  .' and try again.');
				}

				// email address not valid
				if (!valid_email($email))
				{
				  throw new Exception('That is not a valid email address.  Please go back '
									  .' and try again.');
				}

				// passwords not the same
				if ($passwd != $passwd2)
				{
				  throw new Exception('The passwords you entered do not match - please go back'
									   .' and try again.');
				}

				// check password length is ok
				// ok if username truncates, but passwords will get
				// munged if they are too long.
				if (strlen($passwd)<6 || strlen($passwd) >16)
				{
				  throw new Exception('Your password must be between 6 and 16 characters.'
									   .'Please go back and try again.');
				}


				// attempt to register
				// this function can also throw an exception for duplicate emails so it is disabled

				registerprofessor($email, $courseId);




				$rnd = rand(0000000000,9999999999);
				$split = explode(" ",$_POST['first_name']);
				$id = $rnd.cleans($split['0']);
				$hash_id = str_shuffle($id.'cpdi');

				// if ok, put in db
        $sql = "insert into read_users values (null,'$hash_id', '$first_name', '$last_name', '$add1', '$add2', '$postcode', '$country', '$tel', '$email', sha1('$passwd'), '$linkedin', 'NO', 'YES', 'None', '$profession', '$ethnic', '$study', '$status', '$fbook', '$issu', '$materialFile','show',NOW(),NOW())";




				if (mysqli_query($conn1, $sql)) {


          echo 'Administrator was created successful. <br /><br />';

          $user_id =  mysqli_insert_id($conn1);



            $filename = $_FILES["materialFile"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext = substr($filename, strripos($filename, '.')); // get file name
            $filesize = $_FILES["materialFile"]["size"];
            $allowed_file_types = array('.doc','.docx','.rtf','.pdf');

            //if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000))
            //{
                // Rename file
                $newfilename = md5($user_id) . $file_ext;
                if (file_exists("userPhotos/" . $newfilename))
                {
                    // file already exists error
                    echo " ";
                }
                else
                {
                    move_uploaded_file($_FILES["materialFile"]["tmp_name"], "userPhotos/" . $newfilename);
                    echo " ";

                    $sql = " UPDATE read_users SET photo='$newfilename' WHERE id=$user_id ";
                    if (mysqli_query($conn1, $sql)) {


                        echo '<br /> ';

                    }

                }

            //}
           // elseif (empty($file_basename))
           // {
                // file selection error
            //    echo "Please select a file to upload.";
           // }
           // elseif ($filesize > 200000)
           // {
                // file size error
           //     echo "The file you are trying to upload is too large.";
          //  }
          //  else
          //  {
                // file type error
          //      echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
          //      unlink($_FILES["materialFile"]["tmp_name"]);
          //  }


				} else {
           echo "Error: " . $sql . "" . mysqli_error($conn1);

				}


				$to      = $email;
				$subject = 'CPDI Africa';
				$message = "Dear $first_name $last_name, \r\n \r\n You have been registered as a professor. \r\n \r\n \r\n \r\n Kind regards, \r\n CPDI Africa";
				$headers = 'From: design@cpdiafrica.org' . "\r\n" .
					'Reply-To: design@cpdiafrica.org' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();

				mail($to, $subject, $message, $headers);

				$conn1->close();

?>

        <a href="admin_menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>

<?php
			  }
			  catch (Exception $e)
			  {
         echo $e->getMessage();
?>
                <a href="admin_menu" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Admin Menu</b></a>

<?php

			  }
?>


		  </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
