<?php $page_title = "Resources" ?>
<?php include "includes/header.php" ?>

<section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"><br /><br />




                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">

                            <div class="col-md-4">

                                <p class="h3" style="text-align:center">Research</p>

                                <p>
                                <b>Welcome to the African Architecture Studies research database search engine</b>. Our database of Afrocentric scholarship provides students and professionals access to publications, journals, videos, interviews, podcasts, and all content critical to designers who seek to translate and incorporate African architectural design philosophies into their academic and professional work.
                                </p>
                                <p>
                                Our African Centered Masterpieces are created to inspire conceptual designs for students and professionals who seek to translate and incorporate African architectural design philosophies into their projects.

                                </p>
                                <a href=" https://cpdiafrica.blogspot.com/" class="btn btn-danger btn-sm active btn-block btn-sm" target="_blank"><b>Click to Research</b></a>

                            </div>

                            <div class="col-md-4">
                                  <p class="h3" style="text-align:center">JOURNAL</p>
                                  <p><b>CPDI Africa peer review journal</b>. The Special Edition CPDI Africa 2015 Design Competition winning designs publication, is a feature collaboration with Digital Commons / ATL at Kennesaw State University, Georgia!  Our further collaboration with the KSU Division of  Global Affairs, Center for African and African Diaspora Studies perfectly integrated the CPDI Africa - Art of African Architecture Travelling Exhibition, a celebration of the 25 Winning Designers! This project is Volume One of our annual collaboration with global Afrocentric scholars contributing Africa's voice to the discourse on contemporary world architecture.</p>

                                  <a href="https://digitalcommons.kennesaw.edu/atl/" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;" target="_blank"><b>View Volume 1 Journal</b></a>

                                  <a href="https://digitalcommons.kennesaw.edu/atl/" class="btn btn-danger btn-sm active btn-block btn-sm" target="_blank" ><b>Volume 2 Coming Soon</b></a>
                            </div>

                            <div class="col-md-4">
                                  <p class="h3" style="text-align:center">BOOKSTORE</p>
                                  <p><b>CPDI Africa Merchandise & Bookstore.</b> We sell hard to find books on African architecture and urban planning from authors and design practitioners throughout Africa. Looking for research published by continental architects and academics, then look no further than your online CPDI Africa Bookstore!</p>
                                  <p><b>Our Specialty Ecommerce includes:</b></p>
                                  <p>
                                    <ul>

                                        <li>Academic publications and rare texts</li>
                                        <li> African home design magazines</li>
                                        <li> Art and signature aesthetics</li>
                                        <li> Branded gifts and more!</li>

                                    </ul>
                                  </p>



                                  <a href="https://flutterwave.com/store/cpdiafrica?_ga=2.121013832.158656203.1614146625-681761871.1614022475" class="btn btn-danger btn-sm active btn-block btn-sm" target="_blank"><b>Click to Shop</b></a>
                            </div>


                        </div>
                    </div>




            </div>










        </div>

      </div>
    </section><!-- #services -->
<?php include "includes/footer.php" ?>
