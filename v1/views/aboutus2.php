<?php 
	session_start();
	include('output_fns.php'); 
	require_once('min_auto_fns.php'); 
	
	
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>African Centered Architecture</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index.php" class="scrollto">African Centered Architecture</a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_with_links(); ?>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  

    <!--==========================
      Services Section
    ============================-->

	
	
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          
          
        </header>

        <div class="row">    
        <div class="container mt-4">

            <ul class="nav nav-tabs responsive " role="tablist" style="color:#fff; background-color:#73276F; border-color: #73276F;">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#manifesto" role="tab" style="color:#fff;">CPDI Africa 2021</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#founder" role="tab" style="color:#fff;">CPDI Africa 2020</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#council" role="tab" style="color:#fff;">CPDI Africa 2017</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#pioneers" role="tab" style="color:#fff;">CPDI Africa 2015</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#affiliates" role="tab" style="color:#fff;">Affiliates</a>
                </li>
            </ul><!-- Tab panes -->
            <div class="tab-content responsive">
                <div class="tab-pane active" id="manifesto" role="tabpanel">
                    
                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                        <div class="col-5"><img alt="CPDI" src="Slide1.JPG" class="w-100"></div>
                            <div class="col-7">
                                
                                <p>
                                <b>The African Heritage Architecture 2021 DESIGN BRIEF </b> is created to celebrate and preserve the architectural identity of the Cultures of Africa and the Diaspora. Designed as the quintessential <b>Cultural Community Center</b>, the Competition Project will serve as the ultimate example of sustainable design in any community in which it is built. The Objective is to design a space where guests fall in love with the cultures and architectural traditions of the African Diaspora, just by experiencing the beauty, comfort, hospitality and heritage embedded in the Masterpiece!  Building upon the elements of African traditional architecture highlighted in the Design Brief, the knowledge of contemporary character and lifestyle of Africa's diverse continental and diaspora Ethnic Groups, participating architects and collaborating design teams will embark on a brave exercise in  the ‘evolution and transformation’ of African architecture into a Signature Style for our global audience!
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                    <a href="competition_registration_form.php?competition=21" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Register</b></a>
                                    </div>
                                </div>
                                </p>

                            </div>

                          
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="founder" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3">
                        <div class="col-5"><img alt="CPDI" src="Slide4.JPG" class="w-100"></div>
                            <div class="col-7" style="color:#495057;">
                                <p>The CPDI AFRICA <b>DESIGN BRIEF 2017: Option One & Option Two</b></p>
                                <p>
                                <b>The New African House: Option One </b><br />To design an innovative African home that successfully translates traditional residential architecture, in an urban setting; celebrates African inspired architectural aesthetes, in façade and form: and is built with sustainable, modernized, local materials.  
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="council" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3">
                        <div class="col-3"><img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100"></div>
                            

                           
                        </div>


                        
                    </div>
                </div>



                <div class="tab-pane" id="pioneers" role="tabpanel">
                    <div class="container-fluid" style="color:#495057;">
                        <div class="row mt-3">
                        
                            
                        </div>
                    </div>
                </div>



                <div class="tab-pane" id="affiliates" role="tabpanel">
                    <div class="container-fluid" style="color:#495057;">
                        <div class="row mt-3">
                                                
                            
                        </div>
                    </div>
                </div>






            </div>
            </div>
        







         

        </div>

      </div>
    </section><!-- #services -->

    
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>African Centered Architecture</h3>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
           <?php display_useful_links(); ?>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
             <p>
				<?php display_footer_address(); ?>
				<br />
				<?php display_footer_email(); ?>
				<br />
        <?php display_footer_phone(); ?>
        <?php display_social_links(); ?>
			 
            </p>
<!--
            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
-->
          </div>
<!--
          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>
-->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>African Centered Architecture</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
