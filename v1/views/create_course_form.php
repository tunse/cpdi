<?php

	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');


	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
	}
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		//header("Location: login_form.php");
	}






?>
<?php include "includes/header.php" ?>

  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>
    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">
<br><br>
        <header class="section-header">
          <br /><br /><h3>Create Course</h3>
        </header>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                        <form action="create-course" method="post">

                                <div class="form-group">
                                    <label for="courseName">Course Name</label>
                                    <input type="text" class="form-control" id="courseName" name="courseName" placeholder="Course Name" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseDescription">Course Description</label>
                                    <input type="text" class="form-control" id="courseDescription" name="courseDescription" placeholder="Course Description" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseSession">Course Session</label>
																		<select class="form-control" id="courseSession" name="courseSession">
																			<?php $session = selectContent($conn, "selection_session", []); ?>
																			<option value="">--Select course session--</option>
																			<?php foreach ($session as $key => $value): ?>
																				<option value="<?php echo $value['id'] ?>"><?php echo $value['input_name'] ?></option>
																			<?php endforeach; ?>
																		</select>
                                </div>

                                <div class="form-group">
                                    <label for="courseAmount">Course Amount</label>
                                    <input type="text" class="form-control" id="courseAmount" name="courseAmount" placeholder="Course Amount" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="coursePromotionAmount">Course Promotion Amount</label>
                                    <input type="text" class="form-control" id="coursePromotionAmount" name="coursePromotionAmount" placeholder="Course Promotion Amount shown to users" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseDetails">Course Details and Overview</label>
                                    <textarea id="courseDetails" name="courseDetails" class="widgEditor nothing"  required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="live">Is the course LIVE</label>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" name="live" id="gridRadios1" value="Yes" required="required">
                                        <label class="form-check-label" for="gridRadios1">
                                          Yes
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" name="live" id="gridRadios2" value="No" required="required">
                                        <label class="form-check-label" for="gridRadios2">
                                          No
                                        </label>
                                      </div>
                                </div>
                                <div class="form-group">
                                    <label for="conferenceLink">Conference Link</label>
                                    <input type="text" class="form-control" id="conferenceLink" name="conferenceLink" placeholder="Conference Link" required="required">
                                </div>
                                <button type="submit" class="btn btn-success btn-sm">Create Course</button>
                                <a href="admin_menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>

                        </form>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
