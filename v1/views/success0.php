<?php 
session_start();
include('output_fns.php'); 
require_once('min_auto_fns.php');

$item_no = $_GET['item_number'];

$item_transaction = $_GET['tx'];

$item_price = $_GET['amt'];

$item_currency = $_GET['cc'];

$conn = db_connect();
	
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>African Centered Architecture</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index.php" class="scrollto">African Centered Architecture</a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_with_links(); ?>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  

    <!--==========================
      Services Section
    ============================-->

	
	
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          
          
        </header>

        <div class="row">    
        <div class="container mt-4"> <br /><br />

           
            
               
                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                        <div class="col-3"><img alt="CPDI" src="services.png" class="w-100"></div>
                            <div class="col-9">
                                <p class="h3">Payment</p>
                                

                                <?php
                    
                                    if ($item_no != "" && $item_price !="")
                                    {
     
                                              if ($item_no >= 2000000)
                                              {
                                                      //select competition

                                                      
                                                      $result1 = selectCompetitionOrder($item_no);

                                                      if($result1)
                                                      {
                                                        $num_result = $result1->num_rows;
                                                        if($num_result > 0)
                                                        {
                                                          for ($i=0; $i<$num_result; $i++)
                                                          {
                                                            $row = $result1->fetch_assoc();
                                                            $product_id = $row['competition_id'];
                                                            $amount = $row['amount'];
                                                            $user_id = $row['user_id'];
                                                            
                                                          }
                                                        }
                                                        
                                                      }

                                                      $result4 = selectUserById($user_id);

                                                      if($result4)
                                                      {
                                                        $num_result = $result4->num_rows;
                                                        if($num_result > 0)
                                                        {
                                                          for ($i=0; $i<$num_result; $i++)
                                                          {
                                                            $row = $result4->fetch_assoc();
                                                            $userId = $row['userId'];
                                                            $fname = $row['fname'];
                                                            $lname = $row['lname'];
                                                            $email = $row['email'];
                                                            
                                                          }
                                                        }
                                                      }
        
                                                      $result1 = selectCompetition($product_id);
        
                                                      if($result1)
                                                      {
                                                          $num_result = $result1->num_rows;
                                                          if($num_result > 0)
                                                          {
                                                            for ($i=0; $i<$num_result; $i++)
                                                            {
                                                              $row = $result1->fetch_assoc();
                                                              $name = $row['name'];
                                                              $briefDescription = $row['description'];
                                                              
        
                                                            }
                                                          }
                                                      }

                                                      if ($amount == $item_price)
                                                      {
                                                        $sql = "UPDATE competition_order SET status = 'CONFIRMED', pay_pal_id = '$item_transaction' WHERE order_id=$item_no";
                                                        $result = $conn->query($sql);
                                                        
                                                        

                                                        $to = $email;
                                                        $subject = 'African Centered Architecture';
                                                        $message = "Dear $fname $lname, \r\n \r\n Thank you for your payment for $name. \r\n \r\n Your registration is now confirmed! . \r\n \r\n $name  \r\n $briefDescription \r\n Kind regards, \r\n African Centered Architecture";
                                                        
                                                        $headers = 'From: design@cpdiafrica.org' . "\r\n" .
                                                          'Reply-To: design@cpdiafrica.org' . "\r\n" .
                                                          'X-Mailer: PHP/' . phpversion();

                                                        mail($to, $subject, $message, $headers);
                                                        
                                                        echo "Thank you for your payment. <br /><br />Your registration for the competition is now confirmed<br /><br />";
                                                        echo "$name  <br /> $briefDescription <br />";
                                                    
                                                      }    
                                              }
                                              elseif ($item_no < 2000000)
                                              {

                                                        $result1 = selectCourseOrder($item_no);

                                                        if($result1)
                                                        {
                                                          $num_result = $result1->num_rows;
                                                          if($num_result > 0)
                                                          {
                                                            for ($i=0; $i<$num_result; $i++)
                                                            {
                                                              $row = $result1->fetch_assoc();
                                                              $product_id = $row['course_id'];
                                                              $amount = $row['amount'];
                                                              $user_id = $row['user_id'];
                                                              
                                                            }
                                                          }
                                                          
                                                        }

                                                        $result4 = selectUserById($user_id);

                                                        if($result4)
                                                        {
                                                          $num_result = $result4->num_rows;
                                                          if($num_result > 0)
                                                          {
                                                            for ($i=0; $i<$num_result; $i++)
                                                            {
                                                              $row = $result4->fetch_assoc();
                                                              $userId = $row['userId'];
                                                              $fname = $row['fname'];
                                                              $lname = $row['lname'];
                                                              $email = $row['email'];
                                                              
                                                            }
                                                          }
                                                        }
          
                                                        $result1 = selectCourse($product_id);
          
                                                        if($result1)
                                                        {
                                                            $num_result = $result1->num_rows;
                                                            if($num_result > 0)
                                                            {
                                                              for ($i=0; $i<$num_result; $i++)
                                                              {
                                                                $row = $result1->fetch_assoc();
                                                                $name = $row['courseName'];
                                                                $briefDescription = $row['courseDescription'];
                                                                
          
                                                              }
                                                            }
                                                        }
                                                          
                                                        if ($amount == $item_price)
                                                        {
                                                          $sql = "UPDATE product_order SET status = 'CONFIRMED', pay_pal_id = '$item_transaction' WHERE order_id=$item_no";
                                                          $result = $conn->query($sql);
                                                          
                                                          

                                                          $to = $email;
                                                          $subject = 'African Centered Architecture';
                                                          $message = "Dear $fname $lname, \r\n \r\n Thank you for your payment for $name. \r\n \r\n Your registration is now confirmed! . \r\n \r\n $name  \r\n $briefDescription \r\n Kind regards, \r\n African Centered Architecture";
                                                          
                                                          $headers = 'From: design@cpdiafrica.org' . "\r\n" .
                                                            'Reply-To: design@cpdiafrica.org' . "\r\n" .
                                                            'X-Mailer: PHP/' . phpversion();

                                                          mail($to, $subject, $message, $headers);
                                                          
                                                          echo "Thank you for your payment. <br /><br />Your registration for the course is now confirmed <br /><br />";
                                                          echo "$name  <br /> $briefDescription <br />";
                                                      
                                                        }

                                                  
                                              }

                                    }
          





                                ?>
                                
                                
                            </div>
                        </div>
                    </div>
                
                

            
            </div>
        







         

        </div>

      </div>
    </section><!-- #services -->

    
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>African Centered Architecture</h3>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
           <?php display_useful_links(); ?>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
             <p>
				<?php display_footer_address(); ?>
				<br />
				<?php display_footer_email(); ?>
				<br />
                <?php display_footer_phone(); ?>
                <?php display_social_links(); ?>
			 
            </p>
<!--
            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
-->
          </div>
<!--
          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>
-->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>African Centered Architecture</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
