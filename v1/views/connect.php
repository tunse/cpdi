<?php include "includes/header.php" ?>

<section id="services" class="section-bg">
    <div class="container">

      <header class="section-header">


      </header>

      <div class="row">
      <div class="container mt-4"><br /><br />




                  <div class="container-fluid">
                      <div class="row mt-3" style="color:#495057;">
                      <div class="col-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                          <div class="col-9">
                              <p class="h3">Receive our Newsletter <a href="https://us15.campaign-archive.com/home/?u=82858656ff28d4df9d68eeeb0&id=d3cb2d4ce8" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;" target="_blank"><b>Click to sign up</b></a></p>
                              <p>To learn more about CPDI Africa visit us online

                              </p>

                                      <a href="http://www.cpdiafrica.org" class="btn btn-icon btn-rounded btn-secondary " target="_blank"><i class="fa fa-dribbble"></i> </a>

                                      <a  href="https://www.linkedin.com/company/cpdi-africa-global-studio/" class="btn btn-icon btn-rounded btn-info " target="_blank"> <i class="fa fa-linkedin"></i> </a>

                                      <a href="https://www.facebook.com/cpdiafrica" class="btn btn-icon btn-rounded btn-primary " target="_blank"> <i class="fa fa-facebook"></i> </a>

                                      <a href="https://www.instagram.com/cpdi_africa/" class="btn btn-icon btn-rounded btn-warning " target="_blank"> <i class="fa fa-instagram"></i> </a>

                                      <a href = "mailto: design@cpdiafrica.org" class="btn btn-icon btn-rounded btn-success " target="_blank" > <i class="fas fa-envelope"></i> </a>

                                      <a href="https://www.youtube.com/channel/UCfSEFKPbnzHIE4bZieatTgQ" class="btn btn-icon btn-rounded btn-danger " target="_blank"> <i class="fa fa-youtube"></i> </a>

                                      <a href="https://twitter.com/cpdi_africa" class="btn btn-icon btn-rounded btn-info " target="_blank"> <i class="fa fa-twitter"></i> </a>

                                      <a href="https://cpdiafrica.blogspot.com/" class="btn btn-icon btn-rounded btn-dark " target="_blank" > <i class="fab fa-blogger-b"></i> </a>

                                      <a href="https://www.pinterest.ca/cpdiafrica" class="btn btn-icon btn-rounded btn-danger" target="_blank"> <i class="fa fa-pinterest"></i> </a>





                          </div>
                      </div>
                  </div>




          </div>










      </div>

    </div>
  </section><!-- #services -->
<?php include "includes/footer.php" ?>
