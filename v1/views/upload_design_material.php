<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $course=@$_GET['competition'];

    //get the course details from ID
    $result2 = selectCompetition($course);
    if($result2)
    {
        $num_result2 = $result2->num_rows;
        if($num_result2 > 0)
        {
            for ($c=0; $c<$num_result2; $c++)
            {
                $row2 = $result2->fetch_assoc();
                $courseName = $row2['input_name'];
                $courseDescription = $row2['text_description'];
            }
        }



		}

		// $result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
    // $userId = $result4[0]['id'];
    // $fname = $result4[0]['input_first_name'];
    // $lname = $result4[0]['input_last_name'];


	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		// $result4 = selectUserByEmail($_SESSION['valid_user']);
		// 	$userRow = $result4->fetch_assoc();
		$result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
		$userId = $result4[0]['id'];
		$fname = $result4[0]['input_first_name'];
		$lname = $result4[0]['input_last_name'];

		// if($result4)
		// {
		// 	$num_result = $result4->num_rows;
		// 	if($num_result > 0)
		// 	{
		// 		for ($i=0; $i<$num_result; $i++)
		// 		{
		// 			$row = $result4->fetch_assoc();
		// 			$userId = $row['id'];
		// 			$fname = $row['input_first_name'];
		// 			$lname = $row['input_last_name'];
		//
		// 			// var_dump($row);
		// 		}
		// 	 }
		// }
	}
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		//header("Location: login_form.php");
	}






?>

<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Upload Your Design & Content Folder</h3>
        </header>
				<br>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                        <form action="create-design-material" method="post"  enctype="multipart/form-data">
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
															<label for="materialFile">Upload Your Exhibition Board</label>

															<input type="file" class="form-control-file" id="materialFile" name="materialFile">
													</div>
												</div>
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
                                    <label for="courseName">Name of your submitted design </label>
                                    <input type="text" class="form-control" id="courseName" name="courseName" placeholder="Name of your submitted design" required="required">
                                </div>
															</div>
																<br>

																<style media="screen">

																				.help-tip{
																				position: absolute;
																				/* top: 30px; */
																				/* right: 18px; */
																				text-align: center;
																				background-color: #BCDBEA;
																				border-radius: 50%;
																				width: 20px;
																				height: 20px;
																				font-size: 14px;
																				line-height: 20px;
																				cursor: default;
																				margin-left: 10px;
																				}

																				.help-tip:before{
																				content:'?';
																				font-weight: bold;
																				color:#fff;
																				}

																				.help-tip:hover p{
																				display:block;
																				transform-origin: 100% 0%;

																				-webkit-animation: fadeIn 0.3s ease-in-out;
																				animation: fadeIn 0.3s ease-in-out;

																				}

																				.help-tip p{
																				display: none;
																				text-align: left;
																				background-color: #1E2021;
																				padding: 10px;
																				width: 200px;
																				height: auto;
																				position: absolute;
																				border-radius: 3px;
																				box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
																				/* right: -4px; */
																				right: -4px;
																				color: #FFF;
																				font-size: 13px;
																				line-height: 1.4;
																				z-index: 1;
																				}

																				.help-tip p:before{
																				position: absolute;
																				content: '';
																				width:0;
																				height: 0;
																				border:6px solid transparent;
																				border-bottom-color:#1E2021;
																				right:10px;
																				top:-12px;
																				}

																				.help-tip p:after{
																				width:100%;
																				height:40px;
																				content:'';
																				position: absolute;
																				top:-40px;
																				left:0;
																				}

																				@-webkit-keyframes fadeIn {
																				0% {
																					opacity:0;
																					transform: scale(0.6);
																				}

																				100% {
																					opacity:100%;
																					transform: scale(1);
																				}
																				}

																				@keyframes fadeIn {
																				0% { opacity:0; }
																				100% { opacity:100%; }
																				}


																</style>
																<div class="row">
																	<div class="col-md-6 col-xs-12">
																		<div class="form-group">
                                    <label for="courseDescription">Link to Google/Online drive</label>
																		<!-- <span class="help-tip"> -->
																			<p>
																				<b><u>Content Should Include:</u></b> <br><br>
																				<b>
																				- 4 High Resolution 3D Renderings
																				</b><br>
																				<b>
																				- Rendered Floor Plan(s)
																				</b><br>
																				<b>
																				- Project Research Narrative (Word Document)
																				</b><br>
																				<b>
																				- Exhibition Board (provided template as your guide)
																				</b><br>
																				<b>
																				- African Architecture Inspirational Images
																				</b><br>
																				<b>
																				- Contemporary Inspirational Images
																				</b><br>
																				<b>
																				- Cited Bibliography
																				</b><br>
																				<b>
																				- Mood & Material Board (optional)
																				</b><br>
																				<b>
																				- Your Professional Head Shot Photo
																				</b><br>
																				<b>
																				- Your Most Recent CV/Resume
																				</b><br>
																				<b>
																				- Portfolio (optional)
																				</b><br>
																				<b>
																				- All other Items Required as listed in the design brief.
																				</b><br>







																			</p>
																		<!-- </span> -->
																		<br>
																		<br>
                                    <input type="text" class="form-control" id="courseDescription" name="courseDescription" placeholder="Link to Google/Online drive" required="required">
                                </div>
															</div>
																<br>
																<?php

																// $userRow = $result4->fetch_assoc();
																// var_dump($result4[0])

																?>


															<div class="col-md-6 col-lg-6">

																<div class="col-md-6 col-xs-12">
																	<div class="form-group">
                                    <label for="courseName">LinkedIn handle</label>
                                    <input type="text" class="form-control" id="courseName" name="linkedin" placeholder="LinkedIn handle" required="required"
																		<?php if(isset($result4[0]['input_linkedIn']) && !empty($result4[0]['input_linkedIn'])){ ?>
																			value="<?=$result4[0]['input_linkedIn']?>"

																		<?php } ?>
																		>
                                </div>
															</div>
																<br>
																<div class="col-md-6 col-xs-12">
																	<div class="form-group">
                                    <label for="courseName">Facebook handle </label>
                                    <input type="text" class="form-control" id="courseName" name="facebook" placeholder="Facebook handle" required="required"

																		<?php if(isset($result4[0]['input_facebook']) && !empty($result4[0]['input_facebook'])){ ?>
																			value="<?=$result4[0]['input_facebook']?>"

																		<?php } ?>

																		>
                                </div>
															</div>
																<br>
																<div class="col-md-6 col-xs-12">
																	<div class="form-group">
                                    <label for="courseName">Instagram handle</label>
                                    <input type="text" class="form-control" id="courseName" name="instagram" placeholder="Instagram handle" required="required">
                                </div>
															</div>
														</div>
																<br>


                                <input type='hidden' name='course' value='<?php echo $course; ?>'>

                                <!-- <div class="form-group">
                                    <label for="courseName">Competition</label>
                                    <input type="text" readonly class="form-control" value='<?php echo "$courseName $courseDescription"; ?> '>
                                </div> -->

                                <button type="submit" class="btn btn-success btn-sm">Create new design material</button>
                                <a href="myaccount" class="btn btn-light btn-sm"><b>Back to my account</b></a>

                        </form>
												<br><br><br>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
