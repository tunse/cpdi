<?php $page_title = "Resource Center" ?>
<?php include "includes/header.php" ?>


<style media="screen">
body {
  overflow-x: hidden;
}

.wrapper {
  display: flex;
  align-items: stretch;
}

.left-section {
  flex: 0 0 auto;
  width: fit-content;
  padding: 0px 30px;
}

.logo {
  margin-bottom: 20px;
}

.navigation {
  list-style-type: none;
  padding: 0;
  position: relative;
}

.navigation {
  border-right: 2px solid #EC1F25;
}

.navigation li {
  margin-bottom: 15px;
  font-weight: bold;
  display: block;
  max-width: 120px;
  overflow: hidden;
  text-overflow: ellipsis;
}

.navigation li a span {
  display: inline;
}

.navigation li a {
  text-decoration: none;
  color: #000;
  font-weight: bold;
}

.navigation li.active a {
  color: #F95D09;
  font-weight: bolder;
}

.content {
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  padding: 20px;
}

.tab-content {
  flex: 0 0 100%;
  display: none;
  box-shadow: none;
}

.tab-content.active {
  display: block;
  color: #000;
  overflow-y: auto;
  max-height: 600px;
}

.tab-content.active::-webkit-scrollbar {
  display: none;
}

.logo-content img {
  width: 100%;
  display: block;
  height: auto;
  margin: 0 auto;
  margin-bottom: 20px;
}

.logo-content h2 {
  font-size: 20px;
  margin-bottom: 5px;
  font-weight: bold;
}

.logo-content h3 {
  font-size: 18px;
  margin-bottom: 5px;
}

.logo-content h4 {
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 10px;
}

.logo-content p {
  font-size: 14px;
  line-height: 1.6;
  margin-bottom: 10px;
}

.image-details {
  display: flex;
  align-items: flex-start;
  margin-top: 20px;
}

.details {
  width: 70%;
  padding-left: 20px;
  flex-grow: 1;
}

.menu-button {
  display: none;
  outline: none;
}

.menu-button:focus {
  outline: none;
}

.overlay {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 999;
  display: none;
}

.left-section.active ~ .overlay {
  display: block;
}

.section1-content {
  display: flex;
}

.left-part {
  width: 70%;
  padding-right: 20px;
}

.right-part {
  width: 30%;
  border: 1px solid black;
  background-color: #F95D09;
  min-height: 400px;
}

.right-part-inner {
  padding: 20px;
}

.right-part img {
  max-width: 100%;
  height: auto;
}

.text-under-image {
  margin-top: 20px;
}

.text-under-image h3 {
  color: white;
}

.text-under-image p {
  color: white;
}

.tab-content {
  min-height: 100vh;
}

#section2 p {
  text-align: justify;
}

.section3-content {
  display: flex;
  flex-wrap: wrap;
}

/* section 3 */
.section3-left {
  flex: 1;
  padding: 0 10px;
  margin-bottom: 20px;
  margin-right: 80px;
  background: white;
  border: none;
}

.section3-right {
  flex: 1;
  padding: 0 10px;
  margin-bottom: 20px;
  background: white;
  border: none;
  display: flex;
  flex-direction: column;
}

.section3-right .section-header {
  background-color: #f95d09;
  padding: 5px 70px 5px 10px;
  margin-bottom: 10px;
  width: fit-content;
}

.right-content {
  flex-grow: 1;
  text-align: justify;
  margin-left: 15px;
}

.section-header {
  background-color: #f95d09;
  padding: 5px 70px 5px 10px;
  width: fit-content;
}

.header-title {
  margin: 0;
  color: white;
  font-size: 22px;
  font-weight: bold;
}

.left-content img {
  width: 50%;
  margin-top: 15px;
  margin-left: 15px;
}

/* section 4 */
.section4-content {
  display: flex;
}

.section4-left {
  flex: 1;
  margin-right: 10px;
}

.section4-left img {
  width: 100%;
}

.section4-right {
  flex: 1;
  padding: 30px;
}

.section4-background {
  background-color: #f95d09;
  padding: 10px;
  color: white;
}

.section4-background p {
  margin: 0;
}

.section4-background p:first-child {
  font-weight: bold;
  margin-bottom: 10px;
}

.section4-background p:nth-child(2) {
  font-weight: bolder;
  margin-top: 10px;
}

.section4-background p:nth-child(n+3) {
  margin-top: 10px;
}

/* CSS for section 6 layout */
#section6 {
  display: none;
  flex-direction: column;
}

#section6.active {
  display: flex;
}

.top-section {
  margin-bottom: 20px;
}

.bottom-section {
  display: flex;
  flex-wrap: wrap;
}

.left-info {
  flex: 1;
  width: 50%;
}

.right-image {
  flex: 1;
  width: 50%;
}

.left-info p {
  margin: 5px 0;
}

.right-image img {
  max-width: 60%;
  height: auto;
}

@media screen and (max-width: 768px) {
  .content {
    padding: 0px;
  }

  .left-section {
    display:none;
  }

  .menu-button {
    display: block;
    position: absolute;
    left: 20px;
    z-index: 1001;
    background-color: transparent;
    border: none;
    font-size: 24px;
    cursor: pointer;
    outline: none
  }

  .tab-content.active {
    overflow-y: visible;
    max-height: none;
  }

  .left-section.active {
    display: block;
    position: absolute;
    top: 60px;
    left: 0;
    background-color: #fff;
    border: 1px solid #ccc;
    padding: 10px;
    z-index: 1002;
    width: 60%;
    height: 100%;
  }

  .navigation {
    border-right: none;
  }

  .navigation li {
    margin-bottom: 15px;
    font-weight: bold;
    display: block;
    max-width: 200px;
    overflow: none;
  }

  .section1-content {
    flex-direction: column-reverse;
  }

  .left-part {
    width: 100%;
    padding-right: 0;
    margin-bottom: 20px;
  }

  .right-part {
    width: 100%;
    margin-bottom: 20px;
  }

  .right-part img {
    width: 100%;
  }

  .text-under-image p {
    margin-bottom: 5px;
  }

  .section3-left {
    margin-right: 25px;
  }

  .section4-content {
    flex-direction: column;
  }

  .section4-left,
  .section4-right {
    width: 100%;
    margin-right: 0;
    margin-bottom: 20px;
  }

  .bottom-section {
    flex-direction: column;
  }

  .left-info {
    width: 100%;
    margin-bottom: 20px;
  }

  .right-image {
    width: 100%;
  }
}

@media screen and (min-width: 769px) {
  .section4-content {
    flex-direction: row;
  }

  .section4-left {
    width: 50%;
    margin-right: 10px;
    margin-bottom: 0;
  }

  .section4-right {
    width: 50%;
    margin-bottom: 0;
  }
}
</style>


<section>
  <div>
    <header class="section-header"></header>
    <div class="row">
      <div class="mt-4"><br/><br/>
        <div class="container-fluid">
          <div class="wrapper">
            <div class="left-section">
              <div class="logo tab">
                <img style="width:150px;" src="/assets/img/resource_center/CPDI-LOGO2-removebg-preview.png" alt="Your Logo">
              </div>
              <nav>
                <ul class="navigation">
                  <li><a href="#section2"><span>About Us</span></a></li>
                  <li><a href="#section3"><span>Events</span></a></li>
                  <li><a href="#section4"><span>Courses</span></a></li>
                  <li><a href="#section5"><span>Resource Center</span></a></li>
                  <li><a href="#section6"><span>Contact Us</span></a></li>
                </ul>
              </nav>
            </div>

            <div class="overlay"></div>
            <button style="color:#000" class="menu-button">&#9776;</button>

            <div class="content">

              <div id="section1" class="tab-content active logo-content">
                <div class="logo-contentsddd">
                  <img src="/assets/img/resource_center/slide1.jpg" alt="Logo Image">
                  <h2>CPDI AFRICA</h2>
                  <h3>Center for Afrocentric Design Advancement (CADA)</h3>
                  <h4>At African University of Science & Technology, Abuja, Nigeria.</h4>
                  <p>The CPDI Africa Center for Afrocentric Design Advancement (CADA) at African University of Science & Technology is an education and materials Resource Hub for built environment academics, professionals, students and policy makers.</p>
                  <p>CADA is the first of its kind in Nigeria – a center designed to research and develop African centered academic and professional knowledge, for solving challenges specific to Africa’s’ built environment. Our courses, workshops, excursions, international exchange programs, architecture exhibitions, green building materials and publications resource Hub are available to all members of the allied professions, and are multi and interdisciplinary by design. We welcome urban planners, architects, engineers, artists, policy makers and environmentalists to the center for dialogue and collaboration.</p>
                  <p style="font-weight:600">We look forward to welcoming individuals and members of African and international organizations to CADA, in the quest for knowledge, empowerment and the manifestation of sustainable African cities, towns and villages.</p>
                  <div class="image-details">
                    <img style="width:auto; margin:0px" src="/assets/img/resource_center/image.jpg" alt="Image">
                    <div class="details">
                      <p style="margin-bottom:15px;">Best Regards,</p>
                      <p style="margin-bottom:0px;"><strong>Nmadili N. Okwumabua</strong></p>
                      <p style="margin-bottom:0px;">CEO / Founder</p>
                      <p style="margin-bottom:0px;"><strong><em>Community Planning & Design Intuitive (CPDI) Africa</em></strong></p>
                      <p style="margin-bottom:0px;">Managing Director,</p>
                      <p style="margin-bottom:0px;"><strong><em>Center for Afrocentric Design Advancement (CADA)</em></strong></p>
                      <p style="margin-bottom:15px;">at Africa University of Science & Technology</p>
                      <p><strong>WWW.CPDIAFRICA.ORG</strong></p>
                    </div>
                  </div>
                </div>
              </div>

              <div id="section2" class="tab-content">
                <div class="section1-content">
                  <div class="left-part">
                    <p><strong>The Community Planning & Design Initiative Africa (CPDI Africa)</strong> is a culture-inspired, research-based organization, committed to the development of new African and Diaspora architectural languages, that are culturally and environmentally sustainable.</p>
                    <p><strong>Our Global Studio for African Centered Architecture (GSACA)</strong>, is the premiere academy for teaching a new pedagogy in Afrocentric architecture. We educate built environment professionals on Africa’s contribution to world architecture, for the purpose of preserving heritage, culture, science and technology, and sourcing solutions to contemporary built environment challenges, from these African design philosophies.</p>
                  </div>
                  <div class="right-part">
                    <div class="right-part-inner">
                      <img src="/assets/img/resource_center/newsletterlogo.jpg" alt="Image">
                      <div class="text-under-image">
                        <p style="margin-bottom:5px;">CLICK TO VISIT OUR HOME WEBSITE @</p>
                        <a href="https://www.cpdia.org" target="_blank"><p style="margin-bottom:5px;"><strong>WWW.CPDIA.ORG</strong></p></a>
                        <p style="margin-bottom:0px;">TO LEARN MORE ON OUR AFROCENTRIC PEDAGOGY</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div id="section3" class="tab-content">
                <div class="section3-content">
                  <div class="section3-left">
                    <div class="section-header">
                      <h2 class="header-title">Upcoming Event!</h2>
                    </div>
                    <div class="left-content">
                      <img src="/assets/img/resource_center/d1fe173d08e959397adf34b1d77e88d7.JPG" alt="Image 1">
                      <img src="/assets/img/resource_center/d1fe173d08e959397adf34b1d77e88d7.JPG" alt="Image 2">
                    </div>
                  </div>
                  <div class="section3-right">
                    <div class="section-header">
                      <h2 class="header-title">Past Events Links!</h2>
                    </div>
                    <div class="right-content">
                      <p>1. Link to ASUT coverage of OPEN House event, February 23rd 2024</p>
                      <p>2. AUST coverage of the Presidents student welcome, February 2024</p>
                      <p>3. Link to ASUT coverage of the Hub Walk through in December 2023</p>
                      <p>4. Link to AUST coverage of the MOU Signing, February 2023</p>
                    </div>
                  </div>
                </div>
              </div>

              <div id="section4" class="tab-content">
                <div class="section4-content">
                  <div class="section4-left">
                    <img src="/assets/img/resource_center/CPDs CPDIA AUST POSTER.jpg" alt="Left Part Image">
                  </div>
                  <div class="section4-right">
                    <h2 style="font-size: 20px; margin-bottom: 5px; font-weight: bold;">CPDI AFRICA</h2>
                    <h3 style="font-size: 18px; margin-bottom: 5px;">Center for Afrocentric Design Advancement (CADA) </h3>
                    <p style="font-size: 16px; font-weight: bold; margin-bottom: 10px;">At African University of Science & Technology, Abuja, Nigeria.</p>
                    <div class="section4-background">
                      <p style="font-size:17px"><strong>CPDS OFFERS:</strong></p>
                      <p style="margin-top: 25px; font-size:24px"><strong>Continuing Professional Development Courses (CPDs) at CADA</strong></p>
                      <p style="margin-top: 25px; font-weight:bold; font-size:15px;">Click to Register!</p>
                      <p style="margin-top: 0px; font-weight:bold; font-size:15px;">Enrollment and Payment Portal</p>
                    </div>
                  </div>
                </div>
              </div>

              <div id="section5" class="tab-content">
                <div class="section4-content">
                  <div class="section4-left">
                    <img src="/assets/img/resource_center/CADA NOW OPEN.jpg" alt="Left Part Image">
                  </div>
                  <div class="section4-right">
                    <h2 style="font-size: 20px; margin-bottom: 5px; font-weight: bold;">CPDI AFRICA</h2>
                    <h3 style="font-size: 18px; margin-bottom: 5px;">Center for Afrocentric Design Advancement (CADA) </h3>
                    <p style="font-size: 16px; font-weight: bold; margin-bottom: 10px;">At African University of Science & Technology, Abuja, Nigeria.</p>
                    <div class="section4-background">
                      <p style="font-size:17px"><strong>OPEN HOUSE MIXERS</strong></p>
                      <p style="margin-top: 25px; font-size:24px"><strong>Every Thursday 12 – 5pm</strong></p>
                      <p style="margin-top: 25px; font-weight:bold; font-size:15px;">Join Us!</p>
                      <p style="margin-top: 0px; font-weight:bold; font-size:15px;">Tour our Center and Engage with our Design Community of Experts!</p>
                    </div>
                  </div>
                </div>
              </div>

              <div id="section6" class="tab-content">
                <div class="top-section">
                  <h2 style="font-size: 20px; margin-bottom: 5px; font-weight: bold;">CPDI AFRICA</h2>
                  <h3 style="font-size: 18px; margin-bottom: 5px;">Center for Afrocentric Design Advancement (CADA)</h3>
                  <p style="font-size: 16px; font-weight: bold; margin-bottom: 10px;">At African University of Science & Technology, Abuja, Nigeria.</p>
                </div>
                <div class="bottom-section">
                  <div class="left-info">
                    <p style="font-weight: bold; margin-bottom: 15px; text-decoration: underline;">CONTACT US</p>
                    <p><span style="font-weight: bold;">Tel:</span> <a style="font-weight: bold; color: black;" href="tel:+2348091556480">+234 809 155 6480 (Nigeria)</a></p>
                    <p style="margin-bottom: 15px;"><span style="font-weight: bold;">Tel:</span> <a style="font-weight: bold; color: black;" href="tel:+14046413557">+1 404 641 3557 (USA International)</a></p>
                    <p><span style="font-weight: bold;">Email:</span> <a href="mailto:design@cpdiafrica.org" style="font-weight: bold; color: blue; text-decoration: underline;">design@cpdiafrica.org</a></p>
                    <p style="margin-bottom: 15px;"><span style="font-weight: bold;">Email:</span> <a href="mailto:cpdiafrica@aust.edu.ng" style="font-weight: bold; color: blue; text-decoration: underline;">cpdiafrica@aust.edu.ng</a></p>
                    <p style="font-weight: bold; margin-bottom: 15px; text-decoration: underline;">LOCATION:</p>
                    <p style="font-weight: bold;">CADA, Mandela House,</p>
                    <p style="font-weight: bold;">AUST Campus, #10 Umar Yar’Adua Road</p>
                    <p style="font-weight: bold;">Galadimawa, Abuja FCT, Nigeria</p>
                  </div>
                  <div class="right-image">
                    <img src="/assets/img/resource_center/CPDI Social Media Contacts Sankofa.jpg" alt="Image">
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php include "includes/footer.php" ?>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
  const menuButton = document.querySelector('.menu-button');
  const leftSection = document.querySelector('.left-section');
  const body = document.querySelector('body');
  const overlay = document.querySelector('.overlay');
  const logoTab = document.querySelector('.logo.tab');

  function closeMenu() {
    leftSection.classList.remove('active');
    body.style.overflow = 'auto';
    overlay.style.display = 'none';
  }

  function toggleMenu() {
    leftSection.classList.toggle('active');
    if (leftSection.classList.contains('active')) {
      body.style.overflow = 'hidden';
      overlay.style.display = 'block';
    } else {
      body.style.overflow = 'auto';
      overlay.style.display = 'none';
    }
  }

  menuButton.addEventListener('click', function() {
    toggleMenu();
  });

  document.addEventListener('click', function(event) {
    const isClickOnOverlay = event.target.classList.contains('overlay');

    if (isClickOnOverlay) {
      closeMenu();
    }
  });

  logoTab.addEventListener('click', function() {
    closeMenu();
  });

  const tabs = document.querySelectorAll('.navigation li a');
  tabs.forEach(tab => {
    tab.addEventListener('click', function(event) {
      closeMenu();
    });
  });
});
</script>



<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
const tabs = document.querySelectorAll('.navigation li a');
const tabContents = document.querySelectorAll('.tab-content');
const logoImg = document.querySelector('.logo.tab img');
const content = document.querySelector('.content');

function toggleLogoContent() {
  const logoContent = document.querySelector('.logo-content');
  logoContent.classList.toggle('active');
}

tabs.forEach(tab => {
  tab.addEventListener('click', function(event) {
    event.preventDefault();

    tabs.forEach(t => t.parentElement.classList.remove('active'));
    tabContents.forEach(content => content.classList.remove('active'));

    tab.parentElement.classList.add('active');
    const targetTabId = tab.getAttribute('href').substring(1);
    const targetTab = document.getElementById(targetTabId);
    targetTab.classList.add('active');

    if (tab.parentElement.classList.contains('logo')) {
      toggleLogoContent();
    } else {
      document.querySelector('.logo-content').classList.remove('active');
    }

    scrollToSection(targetTab);
  });
});

logoImg.addEventListener('click', function() {
  tabs.forEach(tab => tab.parentElement.classList.remove('active'));
  tabContents.forEach(content => content.classList.remove('active'));

  document.querySelector('.logo.tab').classList.add('active');

  toggleLogoContent();
});
});

</script>
