<?php $page_title = "Resource Center" ?>
<?php $page_title = "CADA Hub" ?>
<?php include "includes/header.php" ?>


<style media="screen">
body {
  overflow-x: hidden;
}

.wrapper {
  position: relative;
  display: flex;
  align-items: stretch;
}

.left-section {
  flex: 0 0 auto;
  width: fit-content;
  padding: 0px 30px;
  position: sticky;
  top: 0;
  height: 100vh;
}

.logo {
  margin-bottom: 20px;
}

.navigation {
  list-style-type: none;
  padding: 0;
  position: relative;
}

.navigation {
  border-right: 2px solid #EC1F25;
}

.navigation li {
  margin-bottom: 15px;
  font-weight: bold;
  display: block;
  max-width: 120px;
  overflow: hidden;
  text-overflow: ellipsis;
}

.navigation li a span {
  display: inline;
}

.navigation li a {
  text-decoration: none;
  color: #000;
  font-weight: bold;
}

.navigation li.active a {
  color: #F95D09;
  font-weight: bolder;
}

.content {
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  padding: 20px 50px;
}

.tab-content {
  flex: 0 0 100%;
  display: none;
  box-shadow: none;
  min-height: 100vh;
}

.tab-content.active {
  display: block;
  color: #000;
}

.tab-content.active::-webkit-scrollbar {
  display: none;
}

.logo-content img {
  width: 100%;
  display: block;
  height: auto;
  margin: 0 auto;
}

.logo-content h2 {
  font-size: 20px;
  margin-bottom: 5px;
  font-weight: bold;
}

.logo-content h3 {
  font-size: 18px;
  margin-bottom: 5px;
}

.logo-content h4 {
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 10px;
}

.logo-content p {
  font-size: 14px;
  line-height: 1.6;
  margin-bottom: 10px;
}

.service-list {
  list-style-type: none;
  padding: 0;
}

.service-list li {
  margin-bottom: 10px;
  padding-left: 30px;
  position: relative;
  font-size: 14px;
  line-height: 1.6;
}

.service-list li:before {
  content: "";
  width: 12px;
  height: 12px;
  background-color: white;
  border: 1px solid black;
  border-radius: 2px;
  box-shadow: 3px 3px 8px rgba(0, 0, 0, 0.5);
  display: inline-block;
  margin-right: 15px;
}

.normal-circle, .bold-circle {
  list-style-type: none;
  padding-left: 20px;
}

.normal-circle li, .bold-circle li {
  font-size: 14px;
  line-height: 1.6;
}

.normal-circle li:before, .bold-circle li:before {
  content: "•";
  margin-right: 15px;
}

.bold-circle li:before {
  font-weight: bold;
}

.image-details {
  display: flex;
  align-items: flex-start;
  margin-top: 20px;
}

.details {
  width: 70%;
  padding-left: 20px;
  flex-grow: 1;
}

.menu-button {
  display: none;
  outline: none;
}

.menu-button:focus {
  outline: none;
}

.overlay {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 999;
  display: none;
}

.left-section.active ~ .overlay {
  display: block;
}

.section1-content {
  display: flex;
}

.left-part {
  width: 70%;
  padding-right: 20px;
}

.right-part {
  width: 30%;
  border: 1px solid black;
  background-color: #F95D09;
  min-height: 400px;
}

.right-part-inner {
  padding: 20px;
}

.right-part img {
  max-width: 100%;
  height: auto;
}

.text-under-image {
  margin-top: 20px;
}

.text-under-image h3 {
  color: white;
}

.text-under-image p {
  color: white;
}

#section2 p {
  text-align: justify;
}

.section3-content {
  display: flex;
  flex-wrap: wrap;
}

/* section 3 */
.section3-left {
  flex: 1;
  padding: 0 10px;
  margin-bottom: 20px;
  margin-right: 80px;
  background: white;
  border: none;
}

.section3-right {
  flex: 1;
  padding: 0 10px;
  margin-bottom: 20px;
  background: white;
  border: none;
  display: flex;
  flex-direction: column;
}

.section3-right .section-header {
  background-color: #f95d09;
  padding: 5px 70px 5px 10px;
  margin-bottom: 10px;
  width: fit-content;
}

.right-content {
  flex-grow: 1;
  text-align: justify;
  margin-left: 15px;
}

.section-header {
  background-color: #f95d09;
  padding: 5px 70px 5px 10px;
  width: fit-content;
}

.header-title {
  margin: 0;
  color: white;
  font-size: 22px;
  font-weight: bold;
}

.left-content img {
  width: 50%;
  margin-top: 15px;
  margin-left: 15px;
}

/* section 4 */
.section4-content {
  display: flex;
}

.section4-left {
  flex: 1;
  margin-right: 10px;
}

.section4-left img {
  width: 100%;
}

.section4-right {
  flex: 1;
  padding: 30px;
}

.section4-background {
  background-color: #f95d09;
  padding: 10px;
  color: white;
}

.section4-background p {
  margin: 0;
}

.section4-background p:first-child {
  font-weight: bold;
  margin-bottom: 10px;
}

.section4-background p:nth-child(2) {
  font-weight: bolder;
  margin-top: 10px;
}

.section4-background p:nth-child(n+3) {
  margin-top: 10px;
}

/* CSS for section 6 layout */
#section6 {
  display: none;
  flex-direction: column;
}

#section6.active {
  display: flex;
}

.top-section {
  margin-bottom: 20px;
}

.bottom-section {
  display: flex;
  flex-wrap: wrap;
}

.left-info {
  flex: 1;
  width: 50%;
}

.right-image {
  flex: 1;
  width: 50%;
}

.left-info p {
  margin: 5px 0;
}

.right-image img {
  max-width: 60%;
  height: auto;
}

.sideText {
  flex: 0 0 70%;
  max-width: 70%;
}

.sideImg {
  style=flex: 0 0 30%;
  max-width: 30%;
  text-align: right;
}

@media screen and (max-width: 768px) {
  .wrapper {
    position: unset;
  }

  .content {
    padding: 0px;
  }

  .left-section {
    display:none;
  }

  .menu-button {
    display: block;
    position: absolute;
    left: 20px;
    z-index: 1001;
    background-color: transparent;
    border: none;
    font-size: 24px;
    cursor: pointer;
    outline: none
  }

  .left-section.active {
    display: block;
    position: absolute;
    top: 60px;
    left: 0;
    background-color: #fff;
    border: 1px solid #ccc;
    padding: 10px;
    z-index: 1002;
    width: 60%;
    height: 100%;
  }

  .navigation {
    border-right: none;
  }

  .navigation li {
    margin-bottom: 15px;
    font-weight: bold;
    display: block;
    max-width: 200px;
    overflow: none;
  }

  .section1-content {
    flex-direction: column-reverse;
  }

  .left-part {
    width: 100%;
    padding-right: 0;
    margin-bottom: 20px;
  }

  .right-part {
    width: 100%;
    margin-bottom: 20px;
  }

  .right-part img {
    width: 100%;
  }

  .text-under-image p {
    margin-bottom: 5px;
  }

  .section3-left {
    margin-right: 25px;
  }

  .section4-content {
    flex-direction: column;
  }

  .section4-left,
  .section4-right {
    width: 100%;
    margin-right: 0;
    margin-bottom: 20px;
  }

  .bottom-section {
    flex-direction: column;
  }

  .left-info {
    width: 100%;
    margin-bottom: 20px;
  }

  .right-image {
    width: 100%;
  }

  .sideText {
    flex: 0 0 100%;
    max-width: 100%;
  }

  .sideImg {
    display: none;
  }
}

@media screen and (min-width: 769px) {
  .section4-content {
    flex-direction: row;
  }

  .section4-left {
    width: 50%;
    margin-right: 10px;
    margin-bottom: 0;
  }

  .section4-right {
    width: 50%;
    margin-bottom: 0;
  }
}

@media (min-width: 1132px) {
  .sideImg img {
    height: 75px;
  }
}
</style>


<section>
  <div>
    <header class="section-header"></header>
    <div style="margin: 0px; display: block" class="row">
      <div class="mt-4"><br/><br/>
        <div class="container-fluid">
          <div class="wrapper">
            <div class="left-section">
              <div class="logo tab">
                <a href="/resource-center">
                  <img style="width:150px;" src="/assets/img/resource_center/CPDI-LOGO2-removebg-preview.png" alt="Your Logo">
                </a>
              </div>
              <nav>
                <ul class="navigation">
                  <li><a href="#section2"><span>About Us</span></a></li>
                  <li><a href="#section3"><span>Courses</span></a></li>
                  <li><a href="#section4"><span>Contact Us</span></a></li>
                </ul>
              </nav>
              <img style="width:150px; margin-top:20px" src="/assets/img/resource_center/whiteIcon.jpeg" alt="Your Logo">
            </div>

            <div class="overlay"></div>
            <button style="color:#000" class="menu-button">&#9776;</button>

            <div class="content">

              <div id="section1" class="tab-content active logo-content">
                <div style="margin-bottom:75px">

                  <div style="display: flex;">
                    <div class="sideText">
                      <h3>CPDI AFRICA</h3>
                      <h2>Center for Afrocentric Design Advancement (CADA)</h2>
                      <h3 style="margin-bottom: 30px;">At African University of Science & Technology, Abuja, Nigeria.</h3>
                    </div>
                    <div class="sideImg">
                      <!-- <img src="/assets/img/resource_center/icon.jpeg" alt="Logo" style="max-width: 100px;"> -->
                    </div>
                  </div>

                  <p style="margin-bottom:20px">The CPDI Africa Center for Afrocentric Design Advancement (CADA) at African University of Science & Technology is an education and materials Resource Hub for built environment academics, professionals, students and policy makers.</p>
                  <p style="margin-bottom:20px">CADA is the first of its kind in Nigeria – a center designed to research and develop African centered academic and professional knowledge, for solving challenges specific to Africa’s’ built environment. Our courses, workshops, excursions, international exchange programs, architecture exhibitions, green building materials and publications resource Hub are available to all members of the allied professions, and are multi and interdisciplinary by design. We welcome urban planners, architects, engineers, artists, policy makers and environmentalists to the center for dialogue and collaboration.</p>
                  <p style="margin-bottom:20px">We look forward to welcoming individuals and members of African and international organizations to CADA, in the quest for knowledge, empowerment and the manifestation of sustainable African cities, towns and villages.</p>
                  <div class="image-details">
                    <img style="width:auto; margin:0px" src="/assets/img/resource_center/image.jpg" alt="Image">
                    <div class="details">
                      <p style="margin-bottom:0px;"><strong>Nmadili N. Okwumabua</strong><a href="/assets/img/resource_center/Nmadili Okwumabua BIO_page-0001.jpg" target="_blank" style="color: #EC1A23;"> (Click for Bio)</a></p>
                      <p style="margin-bottom:0px;">CEO / Founder</p>
                      <p style="margin-bottom:0px;"><strong><em>Community Planning & Design Intuitive (CPDI) Africa</em></strong></p>
                      <p style="margin-bottom:0px;">Managing Director,</p>
                      <p style="margin-bottom:0px;"><strong><em>Center for Afrocentric Design Advancement (CADA)</em></strong></p>
                      <p style="margin-bottom:15px;">at Africa University of Science & Technology</p>
                      <p><strong>WWW.CPDIAFRICA.ORG</strong></p>
                    </div>
                  </div>
                  <img style="margin-top:50px" src="/assets/img/resource_center/CADA NOW OPEN.jpg" alt="">
                  <div id="section2"></div>
                </div>

                <div style="margin-bottom:75px">
                  <h2 style="margin-bottom:20px;"><strong>ABOUT CPDI AFRICA</strong></h2>
                  <p style="margin-bottom:20px"><strong>The Community Planning & Design Initiative Africa (CPDI Africa)</strong> is a culture-inspired, research-based organization, committed to the development of new African and Diaspora architectural languages, that are culturally and environmentally sustainable.</p>
                  <p style="margin-bottom:20px"><strong>Our Global Studio for African Centered Architecture (GSACA),</strong> is the premiere academy for teaching a new pedagogy in Afrocentric architecture. We educate built environment professionals on Africa’s contribution to world architecture, for the purpose of preserving heritage, culture, science and technology, and sourcing solutions to contemporary built environment challenges, from these African design philosophies.</p>
                  <h2 style="margin-bottom:20px">Our Services</h2>
                  <ul class="service-list">
                    <li>Afrocentric Design curriculums, courses, workshops & exchange programs.</li>
                    <li>Art and architecture excursions, field trips, travel and tourism expositions.</li>
                    <li>African architecture competitions and curated winning design exhibitions.</li>
                    <li>Resource for local and standardized green building materials.</li>
                    <li>Resource for African architecture textbooks, journals & documentary films.</li>
                    <li>A collaborative hub for Afrocentric designers, artists, and creative partnerships.</li>
                  </ul>
                  <img style="margin-top:50px" src="/assets/img/resource_center/CPDIA Expo 2022 Mambah Abuja.jpg" alt="">
                  <img style="margin-top:50px" src="/assets/img/resource_center/picture4.png" alt="">
                  <div id="section3"></div>
                </div>

                <div style="margin-bottom:75px">
                  <h2 style="margin-bottom:20px">CPDS – CONTINUING PROFESSIONAL DEVELOPMENT COURSES</h2>
                  <h3 style="margin-bottom:20px"><strong>About The CPDI Africa Training and Certification Courses</strong></h3>
                  <p style="margin-bottom:20px">The Certificate courses are designed to train architects, urban planners, policy makers, and real estate developers on how to successfully deliver and respond to cultural and political factors that shape value chain and Eco systems in Africa's built environment. The four-day courses expose participants to theoretical frameworks for developing new responsive design languages via research, discussion and live interaction with CPDI Africa keynote speakers, registered professionals, professors and facilitators from CPDIA local and international institutions. Courses are multidisciplinary and interdisciplinary in structure and addresses 7 of the 17 the United Nations Sustainable Development Goals, defined as a blueprint for achieving a better and more sustainable future for the global citizen. In addition, the content responds to the recent Nigerian Universities Commission endorsement to introduce new courses in African design.</p>
                  <h3 style="margin-bottom:20px"><strong>Dual Certificates with the African University of Science & Technology, Nigeria</strong></h3>
                  <p style="margin-bottom:20px">The African University of Science and Technology (AUST) is a regional higher education and research initiative for Sub-Saharan Africa. Through its capacity building platform AUSTInspire, the AUST partnership with CPDI Africa organizes trainings and certification workshops. CPDI Africa Global Studio is the premier advocacy consortium for the advancement of Afrocentric design and innovation on the African continent. Research, development and education in local materials and design philosophy’s are is promoted at the CPDI Africa Center for Afrocentric Design Advancement (ACADA) at the AUST Campus.</p>
                  <h2>Registration Link at</h2>
                  <h2 style="margin-bottom:20px"><a style="color:red" href="https://cpd.aust.edu.ng/">https://cpd.aust.edu.ng/</a></h2>
                  <h4>2024 PROGRAMS QUARTERLY SCHEDULE:</h4>
                  <h4>Session One: April 2024</h4>
                  <h4>Session Two: August 2024</h4>
                  <h4>Session Three: October 2024</h4>
                  <div style="margin-top:20px" class="benefits">
                    <h2>Benefits of the Course to Participants</h2>
                    <ul class="bold-circle">
                      <li>Certificate in African Centered Design and Development</li>
                      <li>CPDs (Continuing Professional Development) Credits for Built Environment Professionals</li>
                      <li>Firsthand Experience & Skills Training from Experts in the Profession</li>
                      <li>Exposure to International Professionals Operating in the Afrocentric Design Spaces</li>
                      <li>Opportunity to feature Sustainable Design Projects of Registrants on International Platforms</li>
                    </ul>

                    <h2 style="margin-top:20px">Benefits to the Built Environment Profession</h2>
                    <ul class="bold-circle">
                      <li>Corroborates Public Policy for Urban and Rural Urban Development</li>
                      <li>Addresses Disconnect between the Academy and Professional Practice</li>
                      <li>Connects Value Chain from Architects, Planners to Developers</li>
                      <li>Addresses Issues in Real Estate Delivery and Housing Deficit</li>
                      <li>Standardizes Local Materials for Regional Use and Affordability</li>
                      <li>Addresses Climate Change and Global Warming from Local Perspectives</li>
                      <li>Develops Process for Nigerian Centered Pedagogy and Curriculums</li>
                      <li>Establishes Theoretical Frameworks and History for Researchers</li>
                      <li>Fosters Critical Assessment of Nigeria’s Contribution to Global Discourses</li>
                    </ul>

                    <h2 style="margin-top:20px">Who Should Attend?</h2>
                    <ul class="bold-circle">
                      <li>Architects</li>
                      <li>Urban planners</li>
                      <li>Real Estate developers</li>
                      <li>Policy makers</li>
                      <li>Interior designers</li>
                      <li>Artists and artisans</li>
                      <li>Manufacturers</li>
                      <li>Museum curators</li>
                      <li>Heritage preservationists</li>
                      <li>Allied Built Environment Professionals</li>
                    </ul>
                  </div>
                  <img style="margin-top:50px" src="/assets/img/resource_center/CPDI CPDs AUST.JPG" alt="">
                  <div id="section4"></div>
                </div>

                <div>
                  <div style="text-align: center;" class="contact-info">
                    <h2 style="margin-bottom:20px">Contact Us</h2>
                    <h4><a href="tel:+234 809 155 6480" style="color:black;">Tel: +234 809 155 6480 (Nigeria)</a></h4>
                    <h4 style="margin-bottom:20px"><a href="tel:+1 404 641 3557" style="color:black;">Tel: +1 404 641 3557 (USA International)</a></h4>
                    <h4>Email: <a href="mailto:design@cpdiafrica.org" style="text-decoration: underline; color:#0563C1;">design@cpdiafrica.org</a></h4>
                    <h4 style="margin-bottom:20px">Email: <a href="mailto:cpdiafrica@aust.edu.ng" style="text-decoration: underline; color:#0563C1;">cpdiafrica@aust.edu.ng</a></h4>
                    <h2 style="margin-bottom:20px">LOCATION</h2>
                    <h4>Center for Afrocentric Design Advancement - CADA</h4>
                    <h4>@ Mandela House,</h4>
                    <h4>AUST Campus, #10 Umar Yar’Adua Road</h4>
                    <h4>Galadimawa, Abuja FCT, Nigeria</h4>
                    <img style="margin-top:50px; width:65%;" src="/assets/img/resource_center/CPDI Social Media Contacts Sankofa.jpg" alt="">
                    <img src="/assets/img/resource_center/cpdi africa portrait i-1.jpg" alt="">
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php include "includes/footer.php" ?>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
  const menuButton = document.querySelector('.menu-button');
  const leftSection = document.querySelector('.left-section');
  const body = document.querySelector('body');
  const overlay = document.querySelector('.overlay');
  const logoTab = document.querySelector('.logo.tab');

  function closeMenu() {
    leftSection.classList.remove('active');
    body.style.overflow = 'auto';
    overlay.style.display = 'none';
  }

  function toggleMenu() {
    leftSection.classList.toggle('active');
    if (leftSection.classList.contains('active')) {
      body.style.overflow = 'hidden';
      overlay.style.display = 'block';
    } else {
      body.style.overflow = 'auto';
      overlay.style.display = 'none';
    }
  }

  menuButton.addEventListener('click', function() {
    toggleMenu();
  });

  document.addEventListener('click', function(event) {
    const isClickOnOverlay = event.target.classList.contains('overlay');

    if (isClickOnOverlay) {
      closeMenu();
    }
  });

  logoTab.addEventListener('click', function() {
    closeMenu();
  });

  const tabs = document.querySelectorAll('.navigation li a');
  tabs.forEach(tab => {
    tab.addEventListener('click', function(event) {
      closeMenu();
    });
  });
});
</script>
