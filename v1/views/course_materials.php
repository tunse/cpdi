<?php
	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $course=@$_GET['course'];

    if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
	}



?>

<?php include "includes/header.php" ?>
    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">
<br><br>
        <header class="section-header">
          <h3><br />Course Materials</h3>
        </header>

        <div class="row">

            <?php
                $result1 = selectCourseMaterials($course);
                if($result1)
                {
                    $num_result = $result1->num_rows;
                    if($num_result > 0)
                    {
                        for ($i=0; $i<$num_result; $i++)
                        {
                            $row = $result1->fetch_assoc();
                            $matID = $row['id'];
                            $name = $row['input_name'];
                            $description = $row['text_description'];
                            $courseDocument = $row['input_course_document'];



                            $result2 = selectCourse($course);
                            if($result2)
                            {
                                $num_result2 = $result2->num_rows;
                                if($num_result2 > 0)
                                {
                                    for ($c=0; $c<$num_result2; $c++)
                                    {
                                        $row2 = $result2->fetch_assoc();
                                        $courseName = $row2['input_course_name'];
                                    }
                                }
                            }




?>

                            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                                <div class="box">

                                <h4 class="title"><?php echo "$courseName" ?></h4>
                                <p class="description"><?php echo "$name"; ?> <br />

                                  <?php echo "$description"; ?><br /><br />
                                  <a href="downloadcoursefile?file=<?php echo $courseDocument; ?>" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Download File</a>
                                  <a href="myaccount" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to my account</a><br /><br />


                                </p>
                                </div>
                            </div>
<?php
                        }
                    }
                    else
                    {   ?>
            <div class="card card-body col-md-6 mx-auto ">
							<p class="text-dark text-center">  There are no course materials available<br />
								<a href="myaccount" class="btn btn-light btn-sm"><b>Back to my account</b></a></p>
            </div>

                  <?php  }
                }
            ?>
        </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
