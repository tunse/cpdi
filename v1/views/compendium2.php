<?php include "includes/header.php" ?>


<section id="services" class="section-bg">
    <div class="container">

      <header class="section-header">


      </header>

      <div class="row">
      <div class="container mt-4"><br /><br />




                  <div class="container-fluid">
                      <div class="row mt-3" style="color:#495057;">
                      <div class="col-md-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                          <div class="col-md-9">
                              <p class="h3"><b>African Heritage Architecture Compendium</b> <br /> Winning Designs from our CPDI Africa Competitions</p>

                              <p>
                              Welcome to the African Heritage Architecture Compendium, the celebration of contemporary vernacular inspired architecture! Our database comprises of architecture that preserves traditional design philosophies, translates these indigenous philosophies into new architectural languages, and spotlights the architects that created our competitions winning masterpieces!
                              </p>
                              <p>
                              Our African Centered Masterpieces are created to inspire conceptual designs for students and professionals who seek to translate and incorporate African architectural design philosophies into their projects.

                              </p>
                              <p>
                                  <b>Search our Compendium by Country, Ethnic Group or Name of the Architect.</b>
                              </p>
                              <p>
      <div class="row" id="filter_cont" style="display:none">
        <div class="col-md-3 btn btn-secondary" id="searchC">
          Country
        </div>
        <div class="col-md-3 btn btn-secondary" id="searchE">
          Ethnic Group
        </div>
        <!-- <div class="col-md-3 btn btn-secondary" onclick="searchName()" id="searchN">
          Name of Architect
        </div> -->
        <p id="alerts" style="margin-top:15px"></p>
      </div>
      <div class="row">
        <div class="col-md-3" id="country" style="display:none">
          <label for="">Filter by Country</label>
            <select class="form-control" name="" id="selectcountry">
            <?php $stmt = $conn->prepare("SELECT DISTINCT input_country FROM read_registered_competition_status");
                $stmt->execute();
                while($row=$stmt->fetch(PDO::FETCH_BOTH)){
                  $fetchcountry[] = $row;
                }; ?>
            <option value="">--Select Country--</option>
                <?php foreach ($fetchcountry as $key => $value): ?>
                  <option value="<?php echo $value['input_country'] ?>" ><?php echo $value['input_country'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-3" id="ethnic" style="display:none">
          <label for="">Filter by Ethnic Group</label>
          <?php $stmt = $conn->prepare("SELECT DISTINCT input_ethnic_group FROM read_registered_competition_status");
              $stmt->execute();
              while($row=$stmt->fetch(PDO::FETCH_BOTH)){
                $fetchethnicgroup[] = $row;
              }; ?>
            <select class="form-control" name="" id="selectethnic">
              <option value="">--Select Ethnic group--</option>
                <?php foreach ($fetchethnicgroup as $key => $value): ?>
                  <option value="<?php echo $value['input_ethnic_group'] ?>" ><?php echo $value['input_ethnic_group'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
      </div><br>
                                  <form action="" method="post">
                                      <!-- <div class="form-group">
                                          <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                              <label class="btn btn-secondary">
                                                  <input type="radio" name="searchterm" autocomplete="off" value="Country"> Country
                                              </label>
                                              <label class="btn btn-secondary active">
                                                  <input type="radio" name="searchterm" autocomplete="off" checked value="Ethnic"> Ethnic Group
                                              </label>
                                              <label class="btn btn-secondary">
                                                  <input type="radio" name="searchterm" autocomplete="off" value="Architect"> Name of Architect
                                              </label>
                                          </div>
                                      </div> -->
                                      <div class="form-group">
                                        <input type="text" name="searchW" onkeyup="searchWord()" id="searchW" placeholder="Type a name to begin search" class="form-control col-md-6" value="">
                                          <!-- <input type="text" class="form-control" name="value" id="url" placeholder="" required="required"> -->
                                      </div>
                                      <!-- <div class="form-group">
                                      <button type="submit" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;">Search</button>
                                      </div> -->

                                  </form>
                              </p>
                              <div id="spinner" class="text-center col-md-6" style="display: none">
                                <img src="/spinner.gif" height=40 width="40" alt="">
                              </div>
                              <p id="board" style="padding: 5px"></p>
                          </div>


<script type="text/javascript">
let searchW = document.querySelector('#searchW');
let searchC = document.querySelector('#searchC');
let searchE = document.querySelector('#searchE');
let country = document.querySelector('#country');
let selectcountry = document.querySelector('#selectcountry');
let ethnic = document.querySelector('#ethnic');
let selectethnic = document.querySelector('#selectethnic');
// let searchN = document.querySelector('#searchN');
searchW.addEventListener("keyup",function(){
  document.querySelector('#filter_cont').style.display = "block";
  document.querySelector('#searchC').style.display = "";
  document.querySelector('#searchE').style.display = "";
});

searchC.addEventListener("click",function(){
  // document.querySelector('#filter_cont').style.display = "block";
  document.querySelector('#country').style.display = "";
  document.querySelector('#ethnic').style.display = "none";
});

searchE.addEventListener("click",function(){
  // document.querySelector('#filter_cont').style.display = "block";
  document.querySelector('#country').style.display = "none";
  document.querySelector('#ethnic').style.display = "";
});

selectcountry.addEventListener("change",function(){
  // console.log("clicked");
  var x = window.localStorage.getItem('result');
  var ddata = JSON.parse(x);
  var newRecord = [];
  // console.log(ddata);
    ddata.data.forEach(function(value, key) {
      if (selectcountry.value == value.input_country) {
        newRecord.push(value);
      }
      // console.log(newRecord);
    });
    var html = "";
    html += '<div class="row">';
    newRecord.forEach(function(value, key){
      html += '<div class="col-md-3"><img class="img-fluid z-depth-1" src="/studentDesignMaterials/'+value.competition_design+'"data-toggle="modal" data-target="#modal'+value.id+'"><br><br></div>';
      html += '<span class="">'+value.input_first_name+" "+value.input_last_name+" -"+value.input_country+" -"+value.input_ethnic_group+'<span><br>';

    html +=  '<div class="modal fade" id="modal'+value.id+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    html +=  '<div class="modal-dialog modal-lg" role="document">';


      html +=    '<div class="modal-content">';


      html +=    '<div class="modal-body mb-0 p-0">';

      html +=    '<img class="img-fluid z-depth-1" src="./studentDesignMaterials/'+value.competition_design+'" alt="Picture" data-toggle="modal" data-target="#'+value.id+'">';

      html +=    '</div>';

      html +=    '<div class="modal-footer justify-content-center">';

      html +=     '<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>';

      html +=    '</div>';

      html +=    '</div>';


    html +=  '</div>';
    html +=  '</div>';
    });
    html += '</div>';
    document.querySelector("#board").innerHTML = html;
      document.querySelector("#alerts").style.display = "none";
});

selectethnic.addEventListener("change",function(){
  // console.log("clicked");
  var x = window.localStorage.getItem('result');
  var ddata = JSON.parse(x);
  var newRecord = [];
  // console.log(ddata);
    ddata.data.forEach(function(value, key) {
      if (selectethnic.value == value.input_ethnic_group) {
        newRecord.push(value);
      }
      // console.log(newRecord);
    });
    var html = "";
    html += '<div class="row">';
    newRecord.forEach(function(value, key){
      html += '<div class="col-md-3"><img class="img-fluid z-depth-1" src="/studentDesignMaterials/'+value.competition_design+'"data-toggle="modal" data-target="#modal'+value.id+'"><br><br></div>';
      html += '<span class="">'+value.input_first_name+" "+value.input_last_name+" -"+value.input_country+" -"+value.input_ethnic_group+'<span><br>';

    html +=  '<div class="modal fade" id="modal'+value.id+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
    html +=  '<div class="modal-dialog modal-lg" role="document">';


      html +=    '<div class="modal-content">';


      html +=    '<div class="modal-body mb-0 p-0">';

      html +=    '<img class="img-fluid z-depth-1" src="./studentDesignMaterials/'+value.competition_design+'" alt="Picture" data-toggle="modal" data-target="#'+value.id+'">';

      html +=    '</div>';

      html +=    '<div class="modal-footer justify-content-center">';

      html +=     '<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>';

      html +=    '</div>';

      html +=    '</div>';


    html +=  '</div>';
    html +=  '</div>';
    });
    html += '</div>';
    document.querySelector("#board").innerHTML = html;
      document.querySelector("#alerts").style.display = "none";
});


function searchWord(){
  if (searchW.value.length == 0) {
    document.querySelector("#board").innerHTML = "";
      document.querySelector("#alerts").style.display = "none";
      document.querySelector('#filter_cont').style.display = "none";
      document.querySelector('#ethnic').style.display = "none";
      document.querySelector('#country').style.display = "none";
  }
  if (searchW.value.length < 3) {
    document.querySelector('#spinner').style.display = "";
      document.querySelector("#alerts").style.display = "none";
      document.querySelector('#filter_cont').style.display = "none";
      document.querySelector('#ethnic').style.display = "none";
      document.querySelector('#country').style.display = "none";
  }else{
    document.querySelector('#spinner').style.display = "none";
  var method = "POST";
  var url = "compendiumsearch";


  // if (filter = true) {
  //   var data = {
  //     "search":searchW.value,
  //     "filter":filter
  //   }
  // }else{
    var data = {
      "search":searchW.value
    }
  // }



  var json = JSON.stringify(data);
  var xhr = new XMLHttpRequest;
  xhr.onreadystatechange = function(){
    if (xhr.status == 200 && xhr.readyState == 4) {
      // console.log(xhr.responseText);

      var res = JSON.parse(xhr.responseText);
      if (res.success) {
        // console.log(res.success);
        var html = "";
        // if (filter = "country") {
        //   res.data.forEach(function(value, key) {
        //     html += '<span>'+value.input_country+'<span><br>';
        //     console.log(value);
        //   });
        // }else{

        // var jsonnn = JSON.stringify(xhr.responseText);

        window.localStorage.setItem('result',xhr.responseText);
        // var x = window.localStorage.getItem('result');
        // console.log(x);
        html += '<div class="row">';
          res.data.forEach(function(value, key) {
            html += '<div class="col-md-3"><img class="img-fluid z-depth-1" src="/studentDesignMaterials/'+value.competition_design+'"data-toggle="modal" data-target="#modal'+value.id+'"><br><br></div>';
            html += '<div class="col-md-3"><span>'+value.input_first_name+" "+value.input_last_name+" -"+value.input_country+" -"+value.input_ethnic_group+'<span></div>';
            // }
            // console.log(value);

          html +=  '<div class="modal fade" id="modal'+value.id+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
          html +=  '<div class="modal-dialog modal-lg" role="document">';


            html +=    '<div class="modal-content">';


            html +=    '<div class="modal-body mb-0 p-0">';

            html +=    '<img class="img-fluid z-depth-1" src="./studentDesignMaterials/'+value.competition_design+'" alt="Picture" data-toggle="modal" data-target="#'+value.id+'">';

            html +=    '</div>';

            html +=    '<div class="modal-footer justify-content-center">';

            html +=     '<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>';

            html +=    '</div>';

            html +=    '</div>';


          html +=  '</div>';
          html +=  '</div>';
          });
          html += '</div>';
        // }


        document.querySelector("#board").innerHTML = html;
          document.querySelector("#alerts").style.display = "none";
      }

      if (res.empty) {
        document.querySelector("#alerts").innerHTML = res.empty;
        document.querySelector("#alerts").style.display = "block";
        document.querySelector('#alerts').setAttribute("class","alert alert-info");
      }

      if (res.not_found) {
        document.querySelector("#alerts").innerHTML = res.not_found;
        document.querySelector("#alerts").style.display = "block";
        document.querySelector('#alerts').setAttribute("class","alert alert-danger");
      }
    }
  }
  xhr.open(method,url,true);
  xhr.setRequestHeader("Content-type","application/json");
  xhr.send(json);
}
}

</script>
                      </div>
                  </div>




          </div>










      </div>

    </div>
  </section><!-- #services -->
<?php include "includes/footer.php" ?>
