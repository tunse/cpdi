<?php

	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');




	//check if use is admin



?>
<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>



    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <br /><br /><h3>Administration Menu</h3>
        </header>

        <div class="row">

        </div>
      </div>
		  <div class="col-md-6 col-lg-6">
					  <div style="color:#495057;">

            <form action="register_professor" method="post" enctype="multipart/form-data">

						  <input id="courseId" name="courseId" type="hidden" value="2">


						  <p class="hint-text" style="color:#495057;">Please enter your personal details.</p>
						  <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                  <input type="text" name="first_name" class="form-control" placeholder="First Name" required="required">
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                  <input type="text" name="last_name" class="form-control" placeholder="Last Name" required="required">
                  </div>
                </div>

                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                  <input type="text" name="add1" class="form-control" placeholder="Address Line 1" required="required">
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                  <input type="text" name="add2" class="form-control" placeholder="Address Line 2" required="required">
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                  <input type="text" name="postcode" class="form-control" placeholder="Post Code" required="required">
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                  <input type="text" name="country" class="form-control" placeholder="Country" required="required">
                  </div>
                </div>

                <div class="col-md-6 col-xs-12">

                        <div class="form-group">
                        <input type="text" name="ethnic" class="form-control" placeholder="Ethnic Group" required="required">
                        </div>
                </div>

                <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                        <input type="text" name="tel" class="form-control" placeholder="Mobile" required="required">
                        </div>
                </div>
                <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                        <input type="text" name="study" class="form-control" placeholder="Academic study" required="required">
                        </div>
                </div>

                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label for="status" style="color:#495057;">Status</label>
                        <select class="form-control" id="status" name="status">
                            <option value="Student">Student</option>
                            <option value="Graduate">Graduate</option>
                            <option value="Intern">Intern</option>
                        </select>


                    </div>
                </div>

              <div class="col-md-6 col-xs-12">

                    <div class="form-group">
                                    <input type="text" name="linkedin" class="form-control" placeholder="LinkedIn Profile">
                    </div>
              </div>
              <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                                    <input type="text" name="fbook" class="form-control" placeholder="FaceBook Profile">
                    </div>
              </div>
              <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                                    <input type="text" name="issu" class="form-control" placeholder="ISSU Profile">
                    </div>
               </div>
               <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                                    <input type="text" name="profession" class="form-control" placeholder="Profession">
                    </div>
                </div>

                <div class="col-md-6 col-xs-12">
                    <div class="form-group" style="color:#495057;">
                    <label for="materialFile">Upload photo</label>
                    <input type="file" class="form-control-file" id="materialFile" name="materialFile">
                    </div>
                </div>

                <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                                </div>
                </div>
                <div class="col-md-6 col-xs-12">
                                <div class="form-group" style="color:#495057;">
                                    <input type="password" name="password" class="form-control" placeholder="Password" required="required"> (between 6 and 16 chars)
                                </div>
                </div>
                <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" required="required">
                                </div>
                </div>
                <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label class="checkbox-inline" style="color:#495057;">
                                    <input type="checkbox" required="required"> You agree to the <a href="#" style="color:#495057;">Terms</a> & <a href="#" style="color:#495057;">Conditions</a>.
                                    </label>
                                </div>
                </div>
                <div class="col-md-6 col-xs-12">

                                    <div class="form-group">
                                        <input type="submit" value="Register" class="btn btn-primary btn-block btn-sm" tabindex="7" style="background-color:#970C7B; border-color: #970C7B;">
                                    </div>


                </div>
                                </form>
			</div>
        </div>


      </div>
    </section><!-- #services -->


  </main>


	<?php include "includes/footer.php" ?>
