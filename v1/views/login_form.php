<?php

	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');

	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		header("Location: /myaccount");
	}

?>
<?php include "includes/header.php" ?>


  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header"><br />
          <br /><br /><h3>Login</h3>
        </header>

        <div class="row">



		      <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">


 					 	<div>
 					 		<form action="/myaccount" method="post">
 					 		  <div class="form-group">
 					 			<input type="email" name="email" class="form-control" placeholder="Email" required="required">
 					 		  </div>
 					 		  <div class="form-group">
 					 			<input type="password" name="password" class="form-control" placeholder="Password" required="required">
 					 		  </div>

 					 		  <div class="row">
 					 			<div class="col-md-6 col-xs-12">
 					 			  <div class="form-group">
 					 				<input type="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7" style="background-color:#970C7B; border-color: #970C7B;">
 					 			  </div>
 					 			</div>

 					 		  </div>
 					 		</form>
 					 	</div>
          </div>


      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
