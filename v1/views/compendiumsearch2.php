<?php
  $json = file_get_contents("php://input");
  $data = json_decode($json,true);
  try {
    $status=[];
    $searchie = strip_tags($data['search']);
    $searchword = removeSpecialChar($data['search']);
    $result = [];
    if ($searchword == "") {
      $status['empty'] = "Type your search below";
    }else{
          $stmt = $conn->prepare("SELECT * FROM read_registered_competition_status WHERE input_first_name LIKE '%".$searchword."%' OR input_last_name LIKE '%".$searchword."%'");
              $stmt->execute();
              while($row=$stmt->fetch(PDO::FETCH_BOTH)){
                $result[] = $row;
              };
            }

          if ($stmt->rowCount() < 1) {
            $status['not_found'] = "Oops! Search word not found";
          }

    $status['data'] = $result;
    $status['success'] = true;


  } catch (Exception $e) {
    // $status['error'] = "Something Went Wrong, contact website owner to fix this issue";
    print_r($e);

  }
  $res = json_encode($status);
  echo $res;
?>
