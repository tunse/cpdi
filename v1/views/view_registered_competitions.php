<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');
    $conn1 = db_connect();


?>












<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>




    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Approve / Reject Registered Competitions</h3>
        </header>

				<?php

				$user = $conn->prepare("SELECT * FROM read_users");
				$user->execute();

				$userArr = [];

				while ($userRow = $user->fetch(PDO::FETCH_BOTH)) {
					$userArr[] = $userRow;
				}

				$users = [];

				foreach ($userArr as $key => $value) {
					$users[$value['id']] = $value;
				}

				// var_dump($users);

				$competition = $conn->prepare("SELECT * FROM panel_competitions");
				$competition->execute();

				$competitionArr = [];

				while ($competitionRow = $competition->fetch(PDO::FETCH_BOTH)) {
					$competitionArr[] = $competitionRow;
				}

				$competitions = [];

				foreach ($competitionArr as $key => $value) {
					$competitions[$value['id']] = $value;
				}

				// var_dump($competitions);




				$datas = $conn->prepare("SELECT * FROM panel_competition_design_materials ORDER BY id DESC");
				$datas->execute();


				$dataArr = [];

				while ($dataRow = $datas->fetch(PDO::FETCH_BOTH)) {
						$dataArr[] = $dataRow;
				}
				// var_dump($dataArr);
				?>

        <div class="row">


					<div class="">
							<table cellspacing="10px" cellpadding="10px">
								<tr><td><b>Full Name</b></td><td><b>Ethnic group</b></td><td><b>Country</b></td><td><b>Competition</b></td><td><b>Design Material Name</b></td><td><b>Design Material</b></td><td><b>Drive Link</b><td><b>Status</b></td><td><b>Approve/Reject</b></td></tr>
								<?php foreach ($dataArr as $key => $value): ?>

									<tr>

										<td><?php echo $users[$value['user_id']]['input_first_name'] ?>  <?php echo $users[$value['user_id']]['input_last_name'] ?></td>
										<td><?php echo $users[$value['user_id']]['input_ethnic'] ?></td>
										<td><?php echo $users[$value['user_id']]['input_country'] ?></td>
										<td><?php echo $competitions[$value['competition_id']]['input_name'] ?> - <?php echo $competitions[$value['competition_id']]['input_year'] ?></td>
										<td><?php echo $value['input_name'] ?></td>
										<td>
											<?php
												 if ($value['image_1']=="")
												 {
													 echo "User has not uploaded any design material";
												 }
												 else
												 {
										 ?>
													 <a href="downloaddesignfile?file=<?php echo $value['image_1']; ?>" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Download File</a>
										 <?php
												 }
										 ?>
										</td>

										<td>
											<?php
												 if ($value['input_online_drive']=="")
												 {
													 echo "User has not added added a drive link";
												 }
												 else
												 {
										 ?>
													 <a href="<?php echo $value['input_online_drive']; ?>" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">View Drive Link</a>
										 <?php
												 }
										 ?>
										</td>

										<td><?php if($value['bool_status'] == 1){ echo "Approved";}else{ echo "Rejected";} ?></td>

										<td>

											<a href="qualifycompetition?user=<?php echo $users[$value['user_id']]['id']."&competition=".$competitions[$value['competition_id']]['id']."&status=1"; ?>" class="btn btn-success btn-sm active" role="button" aria-pressed="true">Approve</a>
											<a href="qualifycompetition?user=<?php echo $users[$value['user_id']]['id']."&competition=".$competitions[$value['competition_id']]['id']."&status=0"; ?>" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">Reject</a>

										</td>



									</tr>

								<?php endforeach; ?>
							</table>

					</div>

<!--

                        <?php
                            $result1 = selectRegisteredCompetitions();
                            if($result1)
                            {
                                $num_result1 = $result1->num_rows;
                                if($num_result1 > 0)
                                {
                                    echo '<div>';

                                    echo '<table cellspacing="10px" cellpadding="10px">';
                                    echo '<tr><td><b>Full Name</b></td><td><b>Ethnic group</b></td><td><b>Country</b></td><td><b>Competition</b></td><td><b>Design Material Name</b></td><td><b>Design Material</b></td><td><b>Drive Link</b><td><b>Status</b></td><td><b>Approve/Reject</b></td></tr>';
                                    for ($i=0; $i<$num_result1; $i++)
                                    {
                                        $row = $result1->fetch_assoc();
                                        $competition_id = $row['competition_id'];
                                        $user_id = $row['user_id'];



                                        //get the course details from ID
                                        $result2 = selectCompetition($competition_id);
                                        if($result2)
                                        {
                                            $num_result2 = $result2->num_rows;
                                            if($num_result2 > 0)
                                            {
                                                for ($c=0; $c<$num_result2; $c++)
                                                {
                                                    $row2 = $result2->fetch_assoc();
                                                    $competitionName = $row2['input_name'];
                                                    $competitionDescription = $row2['text_description'];
                                                    $year = $row2['input_year'];
                                                }
                                            }
                                        }

                                        $result3 = selectUserById($user_id);

                                        if($result3)
                                        {
                                            $num_result3 = $result3->num_rows;
                                            if($num_result3 > 0)
                                            {
                                                for ($k=0; $k<$num_result3; $k++)
                                                {
                                                    $row3 = $result3->fetch_assoc();
                                                    $name = $row3['input_first_name'];
                                                    $lastName = $row3['input_last_name'];
                                                    $country = $row3['input_country'];
                                                    $ethnic = $row3['input_ethnic'];


                                                }
                                            }
                                        }


                                        $result5 = registeredcompetitionstatus($user_id, $competition_id);


                                        if($result5)
                                        {
                                            $num_result5 = $result5->num_rows;
                                            if($num_result5 > 0)
                                            {
                                                for ($t=0; $t<$num_result5; $t++)
                                                {
                                                    $row5 = $result5->fetch_assoc();
                                                    $status = $row5['bool_status'];

                                                }
                                            }
                                            else{
                                              $status="";
                                            }
                                        }

                                        echo "<tr><td>$name $lastName</td><td>$ethnic</td><td>$country</td><td>$competitionName $year</td>";

                                        $result4 = selectStudentcompetitionMaterials($user_id, $competition_id);


                                        if($result4)
                                        {
                                            $num_result4 = $result4->num_rows;
                                            if($num_result4 > 0)
                                            {
                                                for ($z=0; $z<$num_result4; $z++)
                                                {
                                                    $row4 = $result4->fetch_assoc();
                                                    $materialname = $row4['input_name'];
                                                    $courseDocument = $row4['image_1'];
                                                    $student_course_material_id = $row4['id'];
                                                    $description = $row4['input_online_drive'];
                                                    $competitionId = $row4['competition_id'];
                                                }
                                            }
                                            else{
                                                $courseDocument ="";
                                            }
                                        }
                       ?>
																			 <td>
																				 <?php if (isset($materialname)): ?>
																					 <?php echo $materialname ?>
																				 <?php else: ?>
																					 <?php echo "No Design Material Name" ?>
																				 <?php endif; ?>

																			 </td>

                                        <td>


                       <?php
                        	            if ($courseDocument=="")
                                      {
                                        echo "User has not uploaded any design material";
                                      }
                                      else
                                      {
                      ?>
                                        <a href="downloaddesignfile?file=<?php echo $courseDocument; ?>" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Download File</a>
                      <?php
                                      }
                      ?>
																				</td>

																				<td>
											 <?php
																			if ($description=="")
																			{
																				echo "User has not set a drive link for";
																			}
																			else
																			{
											?>
																				<a href="<?php echo $description; ?>" class="btn btn-secondary btn-sm active" aria-pressed="true">Drive Link</a>
											<?php
																			}
											?>
																				</td>

                                        <td><?php if($status == 1){ echo "Approved";}else{ echo "Rejected";} ?></td>
                                        <td>

                                        <a href="qualifycompetition?user=<?php echo "$user_id&competition=$competition_id&status=1"; ?>" class="btn btn-success btn-sm active" role="button" aria-pressed="true">Approve</a>
                                        <a href="qualifycompetition?user=<?php echo "$user_id&competition=$competition_id&status=0"; ?>" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">Reject</a>

                                        </td>
                                        </tr>
                        <?php
                                    }
                                    echo "</table>";

                                    echo '</div>';
                                }
                                else
                                {
                                    echo '<div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">';
                                    echo "There are no registered competitions <br /><br />";
                        ?>

                                    <a href="admin_menu" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to Admin Menu</a><br /><br />
                        <?php
                                    echo '</div>';
                                }
                            }

                        ?> -->



        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
