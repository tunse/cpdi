

<?php $page_title = "About Us" ?>
<?php include "includes/header.php" ?>

<section id="services" class="section-bg">
  <div class="container">

    <header class="section-header">


    </header>

    <div class="row">
    <div class="container mt-4"><br /><br />

        <ul class="nav nav-tabs responsive " role="tablist" style="background-color:#970C7B; border-color: #970C7B;">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#manifesto" role="tab">Our Manifesto</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#founder" role="tab">The Founder</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#council" role="tab">Advisory Council</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#board" role="tab">Our Board</a>
            </li> -->

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#sponsors" role="tab">Our Sponsors</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#our-team" role="tab">Our Team</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#affiliates" role="tab">Our Affiliates</a>
            </li>
            <!--
              <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#pioneers" role="tab" style="color:#fff;">Pioneers</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#affiliates" role="tab" style="color:#fff;">Affiliates</a>
              </li>
            -->
        </ul><!-- Tab panes -->
        <div class="tab-content responsive">
            <div class="tab-pane active" id="manifesto" role="tabpanel">
                <div class="container-fluid">
                    <div class="row mt-3">
                        <div class="col-md-7">


                          <!--Modal: Name-->
                          <div class="modal fade" id="modal2020" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">

                              <!--Content-->

                          <!--Content-->
                          <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                              <img class="img-fluid z-depth-1" src="/cpdi/CPDImanifesto1.jpg" alt="Picture" data-toggle="modal" data-target="#modal2020">

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center">

                              <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                          </div>
                          <!--/.Content-->

                        </div>
                      </div>
                      <!--Modal: Name-->
                      <img class="img-fluid z-depth-1" src="/cpdi/CPDImanifesto1.jpg" alt="Picture" data-toggle="modal" data-target="#modal2020">

                          <!-- <img alt="CPDI" src="/cpdi/CPDImanifesto1.jpg" class="w-100"> -->
                        </div>
                        <div class="col-md-5" style="color:#495057;">
                            <p class="h3"><b>CPDI AFRICA</b> Manifesto</p>
                            <p><b>The Community Planning & Design Initiative Africa (CPDI Africa) Promotes the Development of New Architectural Languages for the African Diaspora that are Culturally and Environmentally Sustainable.
                                    </b><br /> Believing that the development of African and Diaspora built environments should be as
                                    then has always been, built as a collaborative effort between the community members
                                    and designated master builders, CDPI Africa engages participation from the design community
                                    in Africa and the Diaspora at large, for the accomplishment of its vision.
                                    CPDI Africa is culture-inspired, research-based and is committed to an African
                                    Centered pedagogy for teaching architecture and sustainable development philosophies
                            </p>
                            <p>
                            <a href="https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book" class="btn btn-danger btn-sm active btn-block btn-sm" target="_blank" ><b>Read Our Full Manifesto</b></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="founder" role="tabpanel">
                <div class="container-fluid">
                    <div class="row mt-4">
                    <div class="col-5"><img alt="laptop_from_side_925x" src="/cpdi/nnn.jpg" class="w-100">
                    <p style="color:#495057;">
                      <b>Nmadili Okwumabua, MSc, BSc.</b><br>
                      Afrocentric Designer, Developer, Educator.<br>
                      Executive Director & Founder <br>
                      <b>CPDI AFRICA</b> <br>
                      Global Studio for African Centered Architecture</p>

                    </div>
                        <div class="col-7" style="color:#495057;">
                            <p class="h3"><b>CPDI AFRICA</b> Founder</p>

                            <p><strong>Nmadili Okwumabua</strong> is a cultural designer, urbanist, and educator in African architecture and community planning. Her passion for design is rooted in a vision where communities in Africa and the Diaspora are developed with new architectural languages that preserve heritage and are culturally and environmentally sustainable. In 2005, she founded Southern Sahara USA, an international consultancy specializing in the research and development of this new architecture.</p>

                            <p><strong>Nmadili Okwumabua</strong> attended the University of Tennessee and Georgia State University, where she pursued undergraduate studies in architecture and urban studies. She holds a master's degree in African and African American studies from Clark Atlanta University, where her research centered on the evolution of modern vernacular architecture in Nigeria. In 2013, the fruits of these efforts gave birth to the Community Planning & Design Initiative, Africa, (CPDI Africa), a research-based, culture-inspired initiative created to develop new African architectural languages through design competition. Believing that the redevelopment of Africa's built environment should be, as it has always been, built as a collaborative effort between the community members and designated master builders, Ms. Okwumabua engages participation from the design community throughout the African Diaspora, for the accomplishment of the CPDI Africa vision.</p>

                            <p><strong>Nmadili Okwumabua</strong>'s work in African and black culture architecture has been studied worldwide, via her lectures as a lecturer and visiting professor, annual design workshops, and the Art of African Architecture, a curated exhibition of winning designs from CPDI Africa bi-annual design competitions. She has presented research papers in over sixty international conferences, universities, and professional associations. Her work in African American & Diaspora studies builds upon Africanisms, cultural retentions rooted African design philosophies for preserving and developing black communities. In 2021, Okwumabua launched the Global Studio for African Centered Architecture (CPDI Africa GSACA), an academy for teaching this new pedagogy in architecture. Practitioners and students are awarded certificates post-completion of intense research and scholarship led by distinguished, tenured built environment professionals.</p>

                            <p>Ms. Okwumabua's career as a built environment professional spans over 27 years; she is a licensed Realtor in the state of Georgia and Certified Property Manager with Broll CBRE South Africa. Nmadili Okwumabua lives in Atlanta, Georgia, and Abuja, Nigeria, where she offers international consultancy services in urban design, hospitality, and real estate asset management, passionately sharing her vision with the future shapers of African and global built environments.</p>

                            <p style="text-align:center;"><b>Developing New Architectural Languages for Africa and the Diaspora that are Culturally and Environmentally Sustainable!</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="council" role="tabpanel">
                <div class="container-fluid">
                    <div class="row mt-3">
                    <div class="col-5"><img alt="laptop_from_side_925x" src="/cpdi/DemasNwoko.jpg" class="w-100">
                    <p style="color:#495057;"><b> Architect DEMAS NWOKO</b><br />Founder, New Culture Studios<br />Architect, Builder, Artist, Teacher</p>
                    </div>
                        <div class="col-7" style="color:#495057;">
                            <p class="h3"><b>CPDI AFRICA</b> Advisory Council</p>
                            <p>
                                    <b>DEMAS NWOKO</b> is a Nigerian artist, protean designer
                                    and architect. As an artist, he strives to incorporate
                                    modern techniques in architecture and stage design
                                    to enunciate the African subject matters in most of
                                    his works . He was a member of the Mbari club of
                                    Ibadan, a committee of burgeoning Nigerian and
                                    foreign artists. He was also a lecturer at the
                                    University of Ibadan. He was the publisher of the
                                    New Culture magazine. Demas Nwoko is a respected
                                    Nigerian artist, architect and master builder in
                                    Nigeria. Nwoko’s works fuse modern techniques in
                                    architecture and stage design with African tradition.
                                    With works like The Dominican Institute, Ibadan and
                                    The Akenzua Cultural Center, Benin to his credit,
                                    Demas Nwoko is one ‘artist-architect’ who believes
                                    in celebrating the African tradition in his works. In
                                    2007, Farafina published The Architecture of Demas
                                    Nwoko, a study of Nwoko’s work and theories
                                    written by two British Architects, John Godwin OBE
                                    and Gillian Hopwood. He led the way toward a
                                    modern mode of expression in African art, theater,
                                    painting, and architecture. In addition, he is a fine
                                    actor and dancer, having performed in numerous
                                    plays in Ibadan. Nwoko, sees design as an ingenuous
                                    activity that carries with it a focus on social
                                    responsibility for positive influences in the
                                    environment and culture of the society
                            </p>

                        </div>

                        <br /><br />

                        <div class="col-5"><img alt="laptop_from_side_925x" src="/cpdi/DavidHughes.jpg" class="w-100">
                        <p style="color:#495057;"><b> Professor DAVID HUGHES</b><br />NOMAC, FAIA<br />Director, DHC Architects</p>
                        </div>
                        <div class="col-7" style="color:#495057;">

                            <p>
                                    <b>Professor Hughes</b> is an architect, author, and
                                    academic, who is making a significant
                                    contribution to the profession and practice of
                                    architecture and to the advancement of
                                    humanity. As an architect, Professor Hughes is
                                    certified by the National Council of Architectural
                                    Registration Boards and the Council of American
                                    Building Officials. He is a certified as a
                                    Professional Planner, and he is licensed in
                                    eleven states. He has served as an officer in the
                                    National Organization of Minority Architects and
                                    in the American Institute of
                                    Architects-Cleveland. He continues an active
                                    private practice as a consultant in architecture
                                    and urban planning. As an author and academic,
                                    Professor Hughes has logged over 30 years of
                                    international travel on four continents and to
                                    over 50 countries. As a student, academic, and
                                    Fulbright scholar, he has compiled the most
                                    exhaustive slide documentation/archives of
                                    modern contemporary architecture on the
                                    African continent, and on architecture
                                    influenced by African elements, held by any
                                    scholar worldwide. This work is the foundation
                                    of his theory, Afrocentric Architecture, featured
                                    in his seminal work on this body of thought,
                                    Afrocentric Architecture: A design primer.
                            </p>

                        </div>

                        <img src="https://imgpile.com/images/NhAEur.jpg" alt="NhAEur.jpg" border="0" width="100%">
                    </div>



                </div>
            </div>

            <div class="tab-pane" id="our-team" role="tabpanel">
                <div class="container-fluid">

                  <img src="/uploads/Team CPDIA Poster.jpg" alt="NhA3BE.jpg" border="0" width="100%">


                </div>
            </div>


            <div class="tab-pane" id="pioneers" role="tabpanel">
                <div class="container-fluid" style="color:#495057;">
                    <div class="row mt-3">
                    <div class="col-3" style="color:#495057;">
                        <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                        <p> Text here </p>
                    </div>
                    <div class="col-3" >
                        <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                        <p> Text here </p>
                    </div>
                    <div class="col-3">
                        <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                        <p> Text here </p>
                    </div>
                    <div class="col-3">
                        <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                        <p> Text here </p>
                    </div>
                    <div class="col-3">
                        <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                        <p> Text here </p>
                    </div>
                    <div class="col-3">
                        <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                        <p> Text here </p>
                    </div>

                    </div>
                </div>
            </div>



                        <div class="tab-pane" id="board" role="tabpanel">
                            <div class="container-fluid" style="color:#495057;">
                              <p class="h3">Our Boards</p>
                                <div class="row mt-3">
                                <div class="col-3">
                                    <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                                    <p> Text here </p>
                                </div>
                                <div class="col-3">
                                    <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                                    <p> Text here </p>
                                </div>
                                <div class="col-3">
                                    <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                                    <p> Text here </p>
                                </div>
                                <div class="col-3">
                                    <img alt="laptop_from_side_925x" src="https://preview.ibb.co/jjt8iF/laptop_from_side_925x.jpg" class="w-100">
                                    <p> Text here </p>
                                </div>


                                </div>
                            </div>
                        </div>



            <div class="tab-pane" id="affiliates" role="tabpanel">
              <div class="container-fluid">
                  <div class="row mt-4">
                  <div class="col-5"><img alt="laptop_from_side_925x" src="/uploads/N Okwumabua 2019.jpg" class="w-100">
                    <p style="color:#495057;">
                      <b>Nmadili Okwumabua, MSc, BSc.</b><br>
                      Afrocentric Designer, Developer, Educator.<br>
                      Executive Director & Founder <br>
                      <b>CPDI AFRICA</b> <br>
                      Global Studio for African Centered Architecture</p>
                    <!-- <p style="color:#495057;"><b>Nmadili Okwumabua </b> <br> CPDI Africa Founder<br />The Global Studio for African Centered Architecture</b></p> -->

                  </div>
                      <div class="col-7" style="color:#495057;">
                          <p class="h3"><b>CPDI AFRICA</b> Affiliates</p>

                          <p>
                            We wish to extend a special thank you to our long-standing partners and collaborators! This journey would not be complete without your commitment to the vision. Vote of thanks to our highlighted affiliates, and many more celebrated in our chronicles of events, speaking engagements and African architecture exhibitions:
                          </p>
                            <p style="font-weight:bold; text-decoration:italic">

                            New Culture Studios <br>

                            DHC Architects <br>

                            EXPOReal Munich <br>

                            National Organization of Minority Architects <br>

                            American Institute of Architects <br>

                            Nigerian Institute of Architects <br>

                            Nigerian Institute of Town Planners <br>

                            One Environment Abuja <br>

                            Athabasca University Canada - RAIC <br>

                            Morehouse College <br>

                             BlackspaceATL and many more! <br>

                           </p>

                            <p><b>Click to enjoy a chronology of Events, Presentations and Collaborators of  CPDI Africa around the Globe!</b> <br><br>
                              <a href="/uploads/CPDI Africa Presentations & Exhibitions.pdf" style="color:blue"> Click for Presentations & Exhibitions</a> <br>
                              <a href="/uploads/CPDI Africa EventPosters&Itineraries.pdf" style="color:blue"> Click for Event Posters & Itineraries </a>

</p>

                          <!-- <p>
                              <b>Nmadili Okwumabua </b> is a cultural designer, urbanist and educator in African architecture and community planning. Her passion for design is rooted in a vision where communities in Africa and the Diaspora are developed with new architectural languages that preserve heritage, and are culturally and environmentally sustainable.  In 2005, she founded Southern Sahara USA, an international consultancy specializing in the research and development of this new architecture.
                              <br /><br />
                              Nmadili Okwumabua attended the University of Tennessee and Georgia State University, where she pursued undergraduate studies in architecture and urban studies. She holds a masters degree in African and African American studies from Clark Atlanta University, where her research centred on the evolution of modern vernacular architecture in Nigeria. In 2013, the fruits of these efforts gave birth to the <b> <i> Community Planning & Design Initiative, Africa </i> </b> , (CPDI Africa), a research-based, culture-inspired initiative created to develop new African architectural languages though design competition. Believing that the redevelopment of Africa's built environment should be as it has always been, built as a collaborative effort between the community members and designated master builders, Ms. Okwumabua engages participation from the design community throughout the African Diaspora, for the accomplishment of the CPDI Africa vision.

                              <br><br>

                              Nmadili Okwumabuas work in African and black culture architecture has been studied worldwide, via her lectures as a lecturer and visiting professor, annual design workshops and the <b> <i> Art of African Architecture </i> </b> , a curated exhibition of  winning designs from CPDI Africa bi annual design competitions. She has presented research papers in over sixty international conferences, universities and professional associations. Her work in African American & Diaspora studies builds upon Africanisms, cultural retentions rooted African design philosophies for preserving and developing black communities. In 2021 Okwumabua launched the <b> <i> Global Studio for African Centered Architecture </i> </b>  (CPDI Africa GSACA), an academy for teaching this new pedagogy in architecture. Practitioners and students are awarded certificates post completion of intense research and scholarship led by distinguished, tenured built environment professionals.

                              <br><br>
                              Ms. Okwumabuas career as a built environment professional spans over 27 years, she is a licensed Realtor in the state of Georgia and Certified Property Manager with Broll CBRE South Africa. <b> <i> Nmadili Okwumabua lives </i> </b> in Atlanta Georgia and Abuja Nigeria, where she offers international consultancy services in urban design, hospitality, and real estate asset management, passionately sharing her vision with the future shapers of African and global built environments.


                          </p> -->
                          <!-- <p style="text-align:center;"><b>Developing New Architectural Languages for African and the Diaspora that are Culturally and Environmentally Sustainable!</b></p> -->
                      </div>
                  </div>
              </div>
            </div>



            <div class="tab-pane" id="sponsors" role="tabpanel">
              <div class="container-fluid">
                <div class="row mt-4">
                  <div class="col-5"><img alt="laptop_from_side_925x" src="/uploads/N Okwumabua 2019.jpg" class="w-100">
                    <p style="color:#495057;">
                      <b>Nmadili Okwumabua, MSc, BSc.</b><br>
                      Afrocentric Designer, Developer, Educator.<br>
                      Executive Director & Founder <br>
                      <b>CPDI AFRICA</b> <br>
                      Global Studio for African Centered Architecture</p>
                      <!-- <p style="color:#495057;"><b>Nmadili Okwumabua </b> <br> CPDI Africa Founder<br />The Global Studio for African Centered Architecture</b></p> -->

                    </div>
                    <div class="col-7" style="color:#495057;">
                      <p class="h3"><b>CPDI AFRICA</b> Sponsors</p>

                      <p>
                        We wish to extend a special thank you to our long-standing partners and collaborators! This journey would not be complete without your commitment to the vision. Vote of thanks to our highlighted affiliates, and many more celebrated in our chronicles of events, speaking engagements and African architecture exhibitions:
                      </p>
                      <div class="col-12">
                        <img src="/cpdi/img/CPDI Africa Patrons.JPG" alt="" style="width: 100%;">
                      </div>

                      <!-- <p><b>Click to enjoy a chronology of Events, Presentations and Collaborators of  CPDI Africa around the Globe!</b> <br><br>
                        <a href="/uploads/CPDI Africa Presentations & Exhibitions.pdf" style="color:blue"> Click for Presentations & Exhibitions</a> <br>
                        <a href="/uploads/CPDI Africa EventPosters&Itineraries.pdf" style="color:blue"> Click for Event Posters & Itineraries </a>

                      </p> -->

                      <!-- <p>
                      <b>Nmadili Okwumabua </b> is a cultural designer, urbanist and educator in African architecture and community planning. Her passion for design is rooted in a vision where communities in Africa and the Diaspora are developed with new architectural languages that preserve heritage, and are culturally and environmentally sustainable.  In 2005, she founded Southern Sahara USA, an international consultancy specializing in the research and development of this new architecture.
                      <br /><br />
                      Nmadili Okwumabua attended the University of Tennessee and Georgia State University, where she pursued undergraduate studies in architecture and urban studies. She holds a masters degree in African and African American studies from Clark Atlanta University, where her research centred on the evolution of modern vernacular architecture in Nigeria. In 2013, the fruits of these efforts gave birth to the <b> <i> Community Planning & Design Initiative, Africa </i> </b> , (CPDI Africa), a research-based, culture-inspired initiative created to develop new African architectural languages though design competition. Believing that the redevelopment of Africa's built environment should be as it has always been, built as a collaborative effort between the community members and designated master builders, Ms. Okwumabua engages participation from the design community throughout the African Diaspora, for the accomplishment of the CPDI Africa vision.

                      <br><br>

                      Nmadili Okwumabuas work in African and black culture architecture has been studied worldwide, via her lectures as a lecturer and visiting professor, annual design workshops and the <b> <i> Art of African Architecture </i> </b> , a curated exhibition of  winning designs from CPDI Africa bi annual design competitions. She has presented research papers in over sixty international conferences, universities and professional associations. Her work in African American & Diaspora studies builds upon Africanisms, cultural retentions rooted African design philosophies for preserving and developing black communities. In 2021 Okwumabua launched the <b> <i> Global Studio for African Centered Architecture </i> </b>  (CPDI Africa GSACA), an academy for teaching this new pedagogy in architecture. Practitioners and students are awarded certificates post completion of intense research and scholarship led by distinguished, tenured built environment professionals.

                      <br><br>
                      Ms. Okwumabuas career as a built environment professional spans over 27 years, she is a licensed Realtor in the state of Georgia and Certified Property Manager with Broll CBRE South Africa. <b> <i> Nmadili Okwumabua lives </i> </b> in Atlanta Georgia and Abuja Nigeria, where she offers international consultancy services in urban design, hospitality, and real estate asset management, passionately sharing her vision with the future shapers of African and global built environments.


                    </p> -->
                    <!-- <p style="text-align:center;"><b>Developing New Architectural Languages for African and the Diaspora that are Culturally and Environmentally Sustainable!</b></p> -->
                  </div>
                </div>
              </div>
            </div>



        </div>
        </div>










    </div>

  </div>
</section><!-- #services -->

<?php include "includes/footer.php" ?>
