<?php
	session_start();
	
	// store  to test if they *were* logged in
	unset($_SESSION['valid_user']);
	$result_dest = session_destroy();

	header("Location: index_nl.php");
?>