<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $course=@$_GET['course'];

    //get the course details from ID
  $result2 = selectContent($conn,"panel_courses",['id'=>$_GET['course']]);

// die(var_dump($result2));
		if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
		{
			$result4 = selectContent($conn,"read_users",['id'=>$_SESSION['valid_user']]);

	}






?>
<?php include "includes/header.php" ?>

  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Create Course Feedback</h3>
        </header>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                        <form action="create-feedback" method="post"  enctype="multipart/form-data">

                                <div class="form-group">
                                    <?php echo $result2[0]['input_course_name'] ."<br>".$result2[0]['text_course_description']; ?>
                                </div>
                                <div class="form-group">
                                    <label for="feedback">Feedback</label>
                                    <textarea id="feedback" name="feedback" required></textarea>

                                </div>


                                <input type='hidden' name='course' value='<?php echo $course; ?>'>


                                <button type="submit" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;">Submit Feedback</button>
                                <a href="myaccount" class="btn btn-light btn-sm"><b>Back to my courses</b></a>

                        </form>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
