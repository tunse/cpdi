<?php

	// session_start();
	include('output_fns.php');
  require_once('min_auto_fns.php');



	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		// $result4 = selectUserByEmail($_SESSION['valid_user']);
		//
		// if($result4)
		// {
		// 	$num_result = $result4->num_rows;
		// 	if($num_result > 0)
		// 	{
		// 		for ($i=0; $i<$num_result; $i++)
		// 		{
		// 			$row = $result4->fetch_assoc();
		// 			$userId = $row['id'];
		// 			$fname = $row['input_first_name'];
    //       $lname = $row['input_last_name'];
    //       $professorStatus = $row['input_professor_status'];
    //       $adminStatus = $row['input_admin_status'];
		//
		// 		}
		// 	 }
		// }
		$result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
		$userId = $result4[0]['id'];
		$fname = $result4[0]['input_first_name'];
		$lname = $result4[0]['input_last_name'];
    $professorStatus = $result4[0]['input_professor_status'];
    $adminStatus = $result4[0]['input_admin_status'];

	}
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		//header("Location: login_form.php");
	}






?>
<?php include "includes/header.php" ?>



  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>



    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <br /><br />
          <h3>
<?php
            if ($adminStatus=="YES" )
            {
              echo "Administration Menu";
            }
            else
            {
              echo "Professor Menu";
            }
?>
          </h3>
        </header>

        <div class="row">
<?php
        if ($adminStatus=="YES" )
				{
?>
            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                    <p>
                    <a href="create-course-form">Create Course</a><br>
                    <a href="editcourse">Edit Course</a><br>

                    </p>
              </div>
            </div>
<?php
        }


        if ($adminStatus=="YES" || $professorStatus=="YES")
				{
?>

            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                  <p>
                  <a href="create-material-form">Create Course Material</a><br>
                  <a href="materials">Edit Course Material</a><br /><br />

                  </p>
              </div>
            </div>
<?php
        }
?>


<?php
        if ($adminStatus=="YES" )
				{
?>
            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                    <p>
											<a href="#">Create Competition</a><br />
                      <!-- <a href="create-course-material">Create Competition</a><br /> -->
                      <a href="editcourse">Edit Competition</a><br />

                    </p>
              </div>
            </div>


            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                    <p>
                      <a href="view-registered-competitions">View / Edit Registered competitions</a><br />


                    </p>
              </div>
            </div>
<?php
        }
?>

<?php
        if ($adminStatus=="YES" )
				{
?>
            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                    <p>
                      <a href="administrator-form">Create Administrator</a><br /><br />
                      <a href="professor-form">Create Professor</a><br /><br />
                      <a href="editprofessor">Associate professor to a course / competition</a><br /><br />

                    </p>
              </div>
            </div>
<?php
        }

        if ($adminStatus=="YES" )
				{
?>
            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                    <p>
                      <a href="manual-payment-form">Approve Manual payment</a><br />

                    </p>
              </div>
            </div>
<?php
        }

        if ($adminStatus=="YES" || $professorStatus=="YES")
				{
?>

            <!-- <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="box">
                    <p>
                      <a href="courses">View submitted course material</a><br />
                      <a href="courses">Feedback of Course Material</a><br /><br />
                    </p>
              </div>
            </div> -->
<?php
        }
?>
        </div>
      </div>

    </section><!-- #services -->


  </main>
	<?php include "includes/footer.php" ?>
