<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');
    $conn1 = db_connect();

    $course=@$_GET['course'];



	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
	}
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		//header("Location: login_form.php");
	}


?>


<?php
                $result1 = selectCourse($course);
                if($result1)
                {
                    $num_result = $result1->num_rows;
                    if($num_result > 0)
                    {
                        for ($i=0; $i<$num_result; $i++)
                        {
                            $row = $result1->fetch_assoc();
                            $courseName = $row['input_course_name'];
                            $courseDescription = $row['text_course_description'];
                            $courseDetails = $row['text_course_details'];
                            $courseAmount = $row['input_course_amount'];
                            $coursePromotion = $row['input_course_promotion'];
                            // $courseOverview = $row['input_course_overview'];
                            $live = $row['input_live'];
                            $conferenceLink = $row['input_conference_link'];


                        }
                    }
                }

?>









<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>



    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <br /><br /><h3>Edit Course</h3>
        </header>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                        <form action="edit-course" method="post">

                                <div class="form-group">
                                    <label for="courseName">Course Name</label>
                                    <input type='hidden' name='courseId' value='<?php echo $course; ?>'>
                                    <input type="text" class="form-control" id="courseName" name="courseName" value="<?php echo  $courseName; ?>" placeholder="Course Name" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseDescription">Course Description</label>
                                    <input type="text" class="form-control" id="courseDescription" name="courseDescription" value="<?php echo  $courseDescription; ?>" placeholder="Course Description" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseAmount">Course Amount</label>
                                    <input type="text" class="form-control" id="courseAmount" name="courseAmount" value="<?php echo  $courseAmount; ?>" placeholder="Course Amount" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="coursePromotionAmount">Course Promotion Amount</label>
                                    <input type="text" class="form-control" id="coursePromotionAmount" name="coursePromotionAmount" value="<?php echo  $coursePromotion; ?>" placeholder="Course Promotion Amount shown to users" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseDetails">Course Details and Overview</label>
                                    <textarea id="courseDetails" name="courseDetails" class="widgEditor nothing"  required><?php echo $courseDetails; ?></textarea>
                                </div>


                                <div class="form-group">
                                    <label for="live">Is the course LIVE</label>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" name="live" id="gridRadios1" value="Yes" required="required" <?php if ($live=="Yes") echo "checked"; ?>>
                                        <label class="form-check-label" for="gridRadios1">
                                          Yes
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" name="live" id="gridRadios2" value="No" required="required" <?php if ($live=="No") echo "checked"; ?>>
                                        <label class="form-check-label" for="gridRadios2">
                                          No
                                        </label>
                                      </div>
                                </div>

                                <div class="form-group">
                                    <label for="conferenceLink">Conference Link</label>
                                    <input type="text" class="form-control" id="conferenceLink" name="conferenceLink" placeholder="Conference Link" required="required" value="<?php echo $conferenceLink; ?>">
                                </div>

                                <button type="submit" class="btn btn-success btn-sm">Update Course</button>
                                <a href="admin_menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>

                        </form>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
