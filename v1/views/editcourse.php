<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');



?>

<?php include "includes/header.php" ?>
    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3><br />Courses</h3>
          <p>Please choose your course</p>
        </header>

        <div class="row">

            <?php
                $result1 = selectCourses();
                if($result1)
                {
                    $num_result = $result1->num_rows;
                    if($num_result > 0)
                    {
                        for ($i=0; $i<$num_result; $i++)
                        {
                            $row = $result1->fetch_assoc();
                            $courseName = $row['input_course_name'];
                            $course_id = $row['id'];
                            $courseDescription = $row['text_course_description'];
                            $courseDetails = $row['text_course_details'];
                            $courseAmount = $row['input_course_amount'];
                            $coursePromotion = $row['input_course_promotion'];

?>

                            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                                <div class="box">

                                <h4 class="title"><?php echo "$courseName" ?></h4>
                                <p class="description"><?php echo "$courseDescription"; ?> <br /><a href="view-course?course=<?php echo "$course_id" ?>" class="btn btn-success"><b>Edit course Details</b></a></p>
                                </div>
                            </div>
<?php
                        }
                    }
                }
            ?>
        </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
