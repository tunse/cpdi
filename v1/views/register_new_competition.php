<?php
// die(date('Y'));

if(isset($_GET['competition']) && isset($_GET['email'])){
	$courseId = $_GET['competition'];
}
$getuser = selectContent($conn, "read_users",['input_email' => $_GET['email']]);
$user_id = $getuser[0]['id'];
$first_name = $getuser[0]['input_first_name'];
$last_name = $getuser[0]['input_last_name'];
$name = $first_name." ".$last_name;
$phone = $getuser[0]['input_phone'];
$email = $_GET['email'];

	  // session_start();

	  // include function files for this application
	  require_once('db_fns.php');
	  include('output_fns.php');
	  include('min_auto_fns.php');
	  $conn1 = db_connect();


	  $paypal_url='https://www.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
	  $paypal_id='design@southernsahara.com'; // Business email ID




	  //create short variable names
	  // $email=@$_POST['email'];
	  // $first_name=@$_POST['first_name'];
	  // $last_name=@$_POST['last_name'];
		//
	  // $add1=@$_POST['add1'];
	  // $add2=@$_POST['add2'];
	  // $country=@$_POST['country'];
	  // $postcode=@$_POST['postcode'];
	  // $linkedin=@$_POST['linkedin'];
	  // $profession=@$_POST['profession'];
		//
		//
	  // $tel=@$_POST['tel'];
	  // $passwd=@$_POST['password'];
	  // $passwd2=@$_POST['confirm_password'];
    //   $courseId=@$_POST['courseId'];
    //   //$courseId = 21;
		//
		//
		//
    //   $ethnic=@$_POST['ethnic'];
    //   $study=@$_POST['study'];
    //   $status=@$_POST['status'];
    //   $fbook=@$_POST['fbook'];
    //   $issu=@$_POST['issu'];
    //   $materialFile=@$_POST['materialFile'];


		//
		// $conn1->close();






	  // start session which may be needed later
	  // start it now because it must go before headers


	  $result1 = selectCompetition($courseId);

	  if($result1)
	  {
		  $num_result = $result1->num_rows;
		  if($num_result > 0)
		  {
			  for ($i=0; $i<$num_result; $i++)
			  {
				  $row = $result1->fetch_assoc();
				  $courseName = $row['input_name'];
				  $courseDescription = $row['text_description'];
                  $flyer = $row['image_1'];
                  $coursePromotion = $row['input_amount'];
				  $courseDetails = $row['input_year'];

			  }
		   }
		  // else{
			   //header("Location: index.php");
		   //}
	  }

	  $paypalName = $courseName;

				$date = date('Y');



				$validateUser = selectContent($conn, "read_competition_order",['input_email' => $_GET['email']],['input_competition_year' => $date]);

				if($validateUser){

							$hash_idd = $validateUser[0]['hash_id'];

							header("Location:/registerexistingcompetition?product=$courseId");
				}




				if(!$validateUser){

									$rnd = rand(0000000000,9999999999);
									$split = explode(" ",$_GET['email']);
									$id = $rnd.cleans($split['0']);
									$hash_idd = str_shuffle($id.'cpdi');


									$post['hash_id'] = $hash_idd;
									$post['input_amount'] = $coursePromotion;
									$post['user_id'] = $user_id;
									$post['input_competition_year'] = $courseDetails;
									$post['input_name'] = $name;
									$post['input_phone'] = $phone;
									$post['input_email'] = $email;
									$post['competition_id'] = $courseId;
									$post['input_pay_pal_id'] = "BLANK";
									$post['bool_status'] = 1;
									$post['input_payer_email'] = "BLANK";
									$post['time'] = time();
									$post['date'] = time();
									$post['visibility'] = "show";
									$post["date_created"] = date("Y-m-d");
									$post["time_created"] = date("h:i:s");

									insertSafe($conn, "read_competition_order", $post);



											$to      = $email;
											$subject = 'CPDI Africa';
											$message = '<h3>Welcome to the CPDI Africa HERITAGE Architecture Competition!</h3>
											<p>Thank you for your registration at the CPDI Africa HERITAGE Architecture Competition 2021! You have
												joined an exceptional international body of designers, architects, artists, planners, historians and all
												round built environment professionals who are developing new African inspired design languages that
												are culturally and environmentally sustainable. The Community Planning &amp; Design Initiative (CPDI)
												Africa is the leader in manifesting contemporary African architecture built upon theoretical frameworks
												by leading Afrocentric architects and scholars.</p></br>
											<p>Next Step:</p></br>
											<ol>
												<li>Log back into your My Account at cpdiafrica.org, and complete your payment using our PayPal,
												Flutterwave, or Bank Transfer options.</li>
												<li> In your payment notes, make sure you include the current/active email address we will use to
												communicate with you regarding your participation in the Heritage Competition.</li>
												<li>Log back into your cpdiafrica.org account at any time and upload your African Architecture
												Masterpiece, supporting documentations, and links to your social media handles.</li>
												<li>Make sure you have completed your Payment for the competition before the closing Deadline
												for the Heritage Competition! Your submission will be accepted but only considered for the rewards and prizes with confirmed receipt of payment. Pay at anytime before the closing date.</li>
												<li>You are welcome to engage in CPDI Africa lectures, workshops, exhibitions, and enroll in any of
												our Global Studio courses to master your knowledge of African design philosophy.</li>
												<li>Check our Social Media platforms on a weekly basis for updates on your participation
												announcements and submission deadlines on our Facebook, Instagram and LinkedIn pages!</li>
											</ol><br>
											<p>We wish you the best in your participation CPDI Africa HERITAGE Architecture Competition 2021!</p>
											<p>For further inquiries and information Contact Us at the emails provided below.</p>
											<br><br>
											<p><b>Nmadili Okwumabua<b></p>
											<p>Founder / Executive Director,</p>
											<p>CPDI Africa Global Studio for African Centered Architecture.</p> <br><br>

											<p>Email: <a href="mailto:design@cpdiafrica.org"> design@cpdiafrica.org</a> / <a href="mailto:cpdiafricascholarships@gmail.com">cpdiafricascholarships@gmail.com</a></p>
											<p>Tel: +234 809 155 6480 (NIG) WhatsApp.</p>
											';


											$headers ="MIME-Version :1.0"."\r\n";
											$headers .="Content-type:text/html; charset=iso-8859-1"."\r\n";
											$headers .= 'From: design@cpdiafrica.org' . "\r\n" .
												'Reply-To: design@cpdiafrica.org' . "\r\n" .
												'X-Mailer: PHP/' . phpversion();



											mail($to, $subject, $message, $headers);

									header("refresh: 1");

									exit();



				}


				// var_dump($hash_idd);

				$order = selectContent($conn, "read_competition_order", ['hash_id' => $hash_idd]);
				$order_id = $order[0]['id'];

				$invoice_check = selectContent($conn,"invoice",['event_id'=>$order_id]);

				if($invoice_check){

					$new['hash_id'] = $invoice_check[0]['hash_id'];
				}

				if (count($invoice_check) < 1) {
					$new['phonenumber'] = $phone;
				$new['invoice_id'] = "COMP"."_".rand(10000,99999)."_".str_shuffle("INVOICECODE")."_".time();
					$new['email'] = $email;
					$new['event_id'] = $order_id;

					$new['status'] = "Unpaid";
					$new['title'] = $courseName;
					$new['description'] = "Payment for ".$courseName;
					$new['place'] = "place";
					$new['company'] = ('company');
					$new['time_created'] = date("h:i:s");
					$new['date_created'] = date("Y-m-d");
					$new['hash_id'] = time()."_".rand(1000,9000);
					$new['quantity'] = "quantity";
					$new['amount_due'] = intval($coursePromotion);
					$new['payment_plan'] = $courseName;
					$new['plan_id'] = $courseId;
					$new['name'] = $name;



					insertSafe($conn,'invoice',$new);
						// header("Location:/payment_invoice?id=".$new['hash_id']."&type=EVT");
				}

				updateContent($conn, "read_competition_order",['input_order_id' => $order_id], ['hash_id' => $hash_idd]);

?>

<?php include "includes/header.php" ?>

  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>



    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <br /><br /><h3>Proceed to Payment</h3>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">

			  <h4 class="title"><?php echo $courseName; ?></h4>
              <p class="description"><?php echo $courseDescription; ?></p><br />
              <p class="description"><b>Amount </b> &#36;<?php echo $coursePromotion; ?></p>

            </div>
          </div>

		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2" style="color:#495057;">
<?php

			// try
			//   {
			// 	// check forms filled in
      //     if (!filled_out($_POST))
      //     {
      //       throw new Exception('You have not filled the form out correctly - please go back'
      //         .' and try again.');
      //     }
			//
      //     // email address not valid
      //     if (!valid_email($email))
      //     {
      //       throw new Exception('That is not a valid email address.  Please go back '
      //                 .' and try again.');
      //     }
			//
      //     // passwords not the same
      //     if ($passwd != $passwd2)
      //     {
      //       throw new Exception('The passwords you entered do not match - please go back'
      //                 .' and try again.');
      //     }
			//
			// 	// check password length is ok
			// 	// ok if username truncates, but passwords will get
			// 	// munged if they are too long.
      //     if (strlen($passwd)<6 || strlen($passwd) >16)
      //     {
      //       throw new Exception('Your password must be between 6 and 16 characters.'
      //                 .'Please go back and try again.');
      //     }
			//
			//
			// 	// attempt to register
			// 	// this function can also throw an exception for duplicate emails so it is disabled
			//
			// 	registercompetition($email, $courseId);
			//
			//
			// 	$rnd = rand(0000000000,9999999999);
			// 	$split = explode(" ","cpdi africa");
			// 	$id = $rnd.cleans($split['0']);
			// 	$hash_id = str_shuffle($id.'cpdi');
			//
			// 	// if ok, put in db
			// 	  $sql = "insert into read_users values (null,'$hash_id', '$first_name', '$last_name', '$add1', '$add2', '$postcode', '$country', '$tel', '$email', sha1('$passwd'), '$linkedin', 'NO', 'NO', 'None', '$profession', '$ethnic', '$study', '$status', '$fbook', '$issu', '$materialFile','show',NOW(),NOW())";
			//
			// 	if (mysqli_query($conn1, $sql)) {
			//
			// 	   // register session variable
			// 		$_SESSION['valid_user'] = $email;
			//
      //               echo 'Your registration was successful. <br /><br />';
			//
      //               $user_id =  mysqli_insert_id($conn1);
			//
      //               $time = time();
			//
			//
			// 											$rnds = rand(0000000000,9999999999);
			// 											$splits = explode(" ","cpdi africa");
			// 											$ids = $rnd.cleans($splits['0']);
			// 											$hashed_id = str_shuffle($ids.'cpdi');
      //               // if ok, put in db
      //               $sqlOrder = "insert into read_competition_order values (null,'$hashed_id', '$courseId', '$coursePromotion', '$user_id', '$time', 'BLANK', 'PENDING', 'BLANK','$time','show', NOW(),NOW())";
			//
      //               if (mysqli_query($conn1, $sqlOrder)) {
			//
			//
      //                   echo 'Your order has been created. <br /><br /> Please make a payment to confirm your order for the competition.';
			//
      //               } else {
      //               echo "Error: " . $sql . "" . mysqli_error($conn1);
      //               }
			//
      //               $order_id =  mysqli_insert_id($conn1);
			//
      //               $filename = $_FILES["materialFile"]["name"];
      //               $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
      //               $file_ext = substr($filename, strripos($filename, '.')); // get file name
      //               $filesize = $_FILES["materialFile"]["size"];
      //               $allowed_file_types = array('.doc','.docx','.rtf','.pdf');
			//
      //               //if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000))
      //               //{
      //                   // Rename file
      //                   $newfilename = md5($user_id) . $file_ext;
      //                   if (file_exists("userPhotos/" . $newfilename))
      //                   {
      //                       // file already exists error
      //                       echo " ";
      //                   }
      //                   else
      //                   {
      //                       move_uploaded_file($_FILES["materialFile"]["tmp_name"], "userPhotos/" . $newfilename);
      //                       echo " ";
			//
      //                       $sql = " UPDATE read_users SET image_1='$newfilename' WHERE id=$user_id ";
      //                       if (mysqli_query($conn1, $sql)) {
			//
			//
      //                           echo '<br /> ';
			//
      //                       }
			//
      //                   }
      //               //}
      //              // elseif (empty($file_basename))
      //              // {
      //                   // file selection error
      //               //    echo "Please select a file to upload.";
      //              // }
      //              // elseif ($filesize > 200000)
      //              // {
      //                   // file size error
      //              //     echo "The file you are trying to upload is too large.";
      //             //  }
      //             //  else
      //             //  {
      //                   // file type error
      //             //      echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
      //             //      unlink($_FILES["materialFile"]["tmp_name"]);
      //             //  }
			//
			//
			// 	} else {
			// 	   echo "Error: " . $sql . "" . mysqli_error($conn1);
			// 	}
			//
			//
			//
			//
			//
			//
			//
				// $to      = $email;
				// $subject = 'CPDI Africa';
				// $message = "Dear $first_name $last_name, \r\n \r\n Thank you for your registration for the  $courseName competition. \r\n \r\nYour registration will be confirmed once we receive payment. \r\n \r\n Kind regards, \r\n CPDI Africa";
				// $headers = 'From: design@cpdiafrica.org' . "\r\n" .
				// 	'Reply-To: design@cpdiafrica.org' . "\r\n" .
				// 	'X-Mailer: PHP/' . phpversion();
				//
				// mail($to, $subject, $message, $headers);
			//
			// 	$conn1->close();
			//
			//


?>
				<br /><br />
				<form action='<?php echo $paypal_url; ?>' method='post'>
				<input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
				<input type='hidden' name='cmd' value='_xclick'>
				<input type='hidden' name='item_name' value='<?php echo "$paypalName"; ?>'>
				<input type='hidden' name='item_number' value='<?php echo "$order_id"; ?>'>
				<input type='hidden' name='amount' value='<?php echo "$coursePromotion"; ?>'>
				<input type='hidden' name='no_shipping' value='1'>
				<input type='hidden' name='currency_code' value='USD'>
				<input type='hidden' name='cancel_return' value='http://www.cpdiafrica.org/cancel'>
				<input type='hidden' name='return' value='https://www.cpdiafrica.org/success'>
				<input type="image" src="/unnamedp.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

				</form> <br />
				<div class="form-group">
				<a href="/payment_invoice?id=<?php echo $new['hash_id'] ?>&type=COMP" style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnamedf.png" ></a>
				</div>

				<div class="form-group">
				<a href="banktransfer?order=<?php echo $order_id; ?>"  style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnameb.png" ></a>
				</div>
                <br />

<?php
        $urlEncodedText = "I'm interested in the competition: $courseName";
        $urlEncodedText = urlencode($urlEncodedText);
        // echo "<a href='https://wa.me/16786509145?text=$urlEncodedText' target='_blank' style='color:#008000;'>Send a WhatsApp message <img src='cpdi/img/whatsapp.png'></a>";
?>


<?php
			  // }
			  // catch (Exception $e)
			  // {
				//  echo $e->getMessage();
				//  exit;
			  // }
?>
		  </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
