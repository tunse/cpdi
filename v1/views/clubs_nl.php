<?php 
	session_start();
	include('output_fns.php'); 
	require_once('min_auto_fns.php'); 
	
	
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>African Centered Architecture</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index_nl.php" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_with_links_nl(); ?>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  

    <!--==========================
      Services Section
    ============================-->
<?php		  
				$result1 = selectProducts_nl('SCRATCH');

				if($result1)
				{
					$num_result = $result1->num_rows;
					if($num_result > 0)
					{
						for ($i=0; $i<$num_result; $i++)
						{
							$row = $result1->fetch_assoc();
							$scratch_name = $row['name_nl'];
							$scratch_product_name = $row['product_name_nl'];
							$scratch_briefDescription = $row['briefDescription_nl'];
							$scratch_product_desc = $row['product_desc_nl'];
							$scratch_product_amount = $row['product_amount'];
							$scratch_period = $row['period_nl'];
							$scratch_startDate = $row['startDate'];
							$scratch_endDate = $row['endDate'];
							$scratch_time = $row['time'];
						}
					 }
				}
				
				
				$result1 = selectProducts_nl('PYTHON');

				if($result1)
				{
					$num_result = $result1->num_rows;
					if($num_result > 0)
					{
						for ($i=0; $i<$num_result; $i++)
						{
							$row = $result1->fetch_assoc();
							$python_name = $row['name_nl'];
							$python_product_name = $row['product_name_nl'];
							$python_briefDescription = $row['briefDescription_nl'];
							$python_product_desc = $row['product_desc_nl'];
							$python_product_amount = $row['product_amount'];
							$python_period = $row['period_nl'];
							$python_startDate = $row['startDate'];
							$python_endDate = $row['endDate'];
							$python_time = $row['time'];
						}
					 }
				}
				
				
				$result1 = selectProducts_nl('HTML & CSS');

				if($result1)
				{
					$num_result = $result1->num_rows;
					if($num_result > 0)
					{
						for ($i=0; $i<$num_result; $i++)
						{
							$row = $result1->fetch_assoc();
							$html_name = $row['name_nl'];
							$html_product_name = $row['product_name_nl'];
							$html_briefDescription = $row['briefDescription_nl'];
							$html_product_desc = $row['product_desc_nl'];
							$html_product_amount = $row['product_amount'];
							$html_period = $row['period_nl'];
							$html_startDate = $row['startDate'];
							$html_endDate = $row['endDate'];
							$html_time = $row['time'];
						}
					 }
        }
        

		  
		$result1 = selectProducts_nl('TYPECURSUS');

		if($result1)
		{
			$num_result = $result1->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result1->fetch_assoc();
					$typing_name = $row['name_nl'];
					$typing_product_name = $row['product_name_nl'];
					$typing_briefDescription = $row['briefDescription_nl'];
					$typing_product_desc = $row['product_desc_nl'];
					$typing_product_amount = $row['product_amount'];
					$typing_period = $row['period_nl'];
					$typing_startDate = $row['startDate'];
					$typing_startDate = date("d-m-Y", strtotime($html_startDate));
					$typing_endDate = $row['endDate'];
					$typing_endDate = date("d-m-Y", strtotime($html_endDate));
					$typing_time = $row['time'];
				}
			 }
		}

?>
	
	
	
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3><br />Code Clubs</h3>
          <p>Kies jouw club</p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/scratchsmall.svg" class="img-fluid" alt="Scratch"></div>
              <h4 class="title"><?php echo "$scratch_name" ?></h4>
              <p class="description"><?php echo "$scratch_briefDescription"; ?> <br /><a href="book_now_nl.php?course=<?php echo "$scratch_name" ?>" class="btn btn-success"><b>Boek code club</b></a></p>
            </div>
          </div>
		  
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            
			<div class="box">
			
              <div class="icon"><img src="img/pythonsmall.svg" class="img-fluid" alt="Python"></div>
              <h4 class="title"><?php echo "$python_name" ?></h4>
              <p class="description"><?php echo "$python_briefDescription"; ?> <br /><a href="book_now_nl.php?course=<?php echo "$python_name" ?>" class="btn btn-success"><b>Boek code club</b></a></p>
			
            </div>
			
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/htmlcsssmall.svg" class="img-fluid" alt="Html & CSS"></div>
			  
			  
              <h4 class="title"><?php echo "$html_name" ?></h4>
			  <?php $html_name = urlencode($html_name); ?>
              <p class="description"><?php echo "$html_briefDescription"; ?> <br /><a href="book_now_nl.php?course=<?php echo "$html_name" ?>" class="btn btn-success"><b>Boek code club</b></a></p>
            </div>
          </div>
         <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/typeless.svg" class="img-fluid" alt="Typing Class"></div>
             <h4 class="title"><?php echo "$typing_name" ?></h4>
              <p class="description"><?php echo "$typing_briefDescription"; ?> <br /><a href="book_now_nl.php?course=<?php echo "$typing_name" ?>" class="btn btn-success"><b>Boek typecursus</b></a></p>
            </div>
          </div>

         

        </div>

      </div>
    </section><!-- #services -->

    
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>African Centered Architecture</h3>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Hulpvolle Links</h4>
           <?php display_useful_links_nl(); ?>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
             <p>
				<?php display_footer_address_nl(); ?>
				<br />
				<?php display_footer_email_nl(); ?>
				<br />
				<?php display_footer_phone_nl(); ?>
			 
            </p>
<!--
            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
-->
          </div>
<!--
          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>
-->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>African Centered Architecture</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
