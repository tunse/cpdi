<?php

	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');



    // $course = $_GET['competition'];


	//User logged in proceed
	// if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	// {
	// 	header("Location:/signin");
	// }




		$error = [];
	if(array_key_exists("register", $_POST)){
		if(empty($_POST['input_first_name'])){
			$error['input_first_name'] = "You have not entered your First name";
		}
		if(empty($_POST['input_last_name'])){
			$error['input_last_name'] = "You have not entered your Last name";
		}
		if(empty($_POST['input_email'])){
			$error['input_email'] = "You have not entered your Email";
		}
		if(empty($_POST['input_profession'])){
			$error['input_profession'] = "You have not entered your Profession";
		}
		if(empty($_POST['input_address_line_one'])){
			$error['input_address_line_one'] = "You have not entered your Address";
		}
		if(empty($_POST['input_ethnic'])){
			$error['input_ethnic'] = "You have not entered your Ethnic Group";
		}
		if(empty($_POST['input_linkedIn'])){
			$error['input_linkedIn'] = "You have not entered your LinkedIn";
		}
		if(empty($_POST['input_password'])){
			$error['input_password'] = "You have not entered your Password";
		}
		if(empty($_POST['confirm_password'])){
			$error['confirm_password'] = "You have not Confirmed Your password";
		}
		if($_POST['input_password'] !== $_POST['confirm_password']){
			$error['input_password'] = "Password Mismatch";
		}
		if(empty($_FILES['image_1'])){
			$error['image_1'] = "You have not selected your Profile Image";
		}
		if(strlen($_POST['input_password'])<6 || strlen($_POST['input_password']) >16){
			$error['input_password'] = "Your password must be between 6 to 16 characters";
		}

	$checkemail = selectContent($conn, "read_users", ['input_email' => $_POST['input_email']]);
		  if ($checkemail) {
				$error['input_email'] = "Email already Exists";
			}

		if(empty($error)){

			$password = sha1($_POST['input_password']);
			unset($_POST['confirm_password']);
			unset($_POST['input_password']);
			array_pop($_POST);
			$rnd = rand(0000000000,9999999999);
			$split = explode(" ",$_POST['input_first_name']);
			$id = $rnd.cleans($split['0']);
			$hash_id = str_shuffle($id.'cpdi');

			$clean = array_map('trim', $_POST);
			$img = compressImage2($_FILES,'image_1',90,"userPhotos/");
			$new["hash_id"] = $hash_id;
			$new["input_password"] = $password;
			$new["image_1"] = $img['upload'];
			$new["input_professor_status"] = "YES";
			$new["input_admin_status"] = "NO";
			$new["input_subscription_status"] = "NONE";
			$new["date_created"] = date("Y-m-d");
			$new["time_created"] = date("h:i:s");


			$post = $clean + $new;
			// die(var_dump($post));
			insertSafe($conn, "read_users", $post);

			// header("location:/register-new?course=$course");

	// $email = $_POST['input_email'];


			// $to      = $email;
			// $subject = 'CPDI Africa';
			// $message = "Dear $first_name $last_name, \r\n \r\n Thank you for your registration for the $courseName course. \r\n \r\nYour registration will be confirmed once we receive payment. \r\n \r\n Kind regards, \r\n CPDI Africa";
			// $headers = 'From: design@cpdiafrica.org' . "\r\n" .
			// 	'Reply-To: design@cpdiafrica.org' . "\r\n" .
			// 	'X-Mailer: PHP/' . phpversion();

			// mail($to, $subject, $message, $headers);
			$_SESSION['success'] = "Registration Successful";
			header("location:/admin_menu");
			exit();

		}
	}




?>
<?php include "includes/header.php" ?>

  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">
      <br />

        <header class="section-header">
          <br /><br /><h3>Professor Registration</h3>
        </header>

        <div class="row">


		  <div class="col-md-6 mx-auto">
					  <div>
	          <form action="" method="post" enctype="multipart/form-data">

	            <!-- <input id="courseId" name="course_id" type="hidden" value="<?php echo $course; ?>"> -->


	            <p class="hint-text" style="color:#495057;">Please enter your personal details.</p>
							<?php if(count($error) > 0): ?>
								<p class="text-danger"> Please fill all required fields</p>
							<?php endif; ?>
	            <div class="row">
	              <div class="col-md-6 col-xs-12">
	                <div class="form-group">
											<label for="status" style="color:#495057;">First Name <span class="text-danger">*</span> </label><br>
										<span class="text-danger"><?php if(isset($error['input_first_name'])){ echo $error['input_first_name'];} ?></span>
	                <input type="text" name="input_first_name" class="form-control" placeholder="First Name" value="<?php if(isset($_POST['input_first_name'])){ echo $_POST['input_first_name'];} ?>">
	                </div>
	              </div>
	              <div class="col-md-6 col-xs-12">
	                <div class="form-group">
											<label for="status" style="color:#495057;">Last Name <span class="text-danger">*</span> </label><br>
										<span class="text-danger"><?php if(isset($error['input_last_name'])){ echo $error['input_last_name'];} ?></span>
										<input type="text" name="input_last_name" class="form-control" placeholder="Last Name" value="<?php if(isset($_POST['input_last_name'])){ echo $_POST['input_last_name'];} ?>">
	                </div>
	              </div>

	              <div class="col-md-6 col-xs-12">
	                <div class="form-group">
											<label for="status" style="color:#495057;">Address Line 1 <span class="text-danger">*</span> </label><br>
										<span class="text-danger"><?php if(isset($error['input_address_line_one'])){ echo $error['input_address_line_one'];} ?></span>
	                <input type="text" name="input_address_line_one" class="form-control" placeholder="Address Line 1" value="<?php if(isset($_POST['input_address_line_one'])){ echo $_POST['input_address_line_one'];} ?>">
	                </div>
	              </div>
	              <div class="col-md-6 col-xs-12">
	                <div class="form-group">
											<label for="status" style="color:#495057;">Address Line 2 </label>
	                <input type="text" name="input_address_line_two" class="form-control" placeholder="Address Line 2" value="<?php if(isset($_POST['input_address_line_two'])){ echo $_POST['input_address_line_two'];} ?>">
	                </div>
	              </div>
	              <div class="col-md-6 col-xs-12">
	                <div class="form-group">
											<label for="status" style="color:#495057;">Post Code </label>
	                <input type="text" name="input_post_code" class="form-control" placeholder="Post Code" value="<?php if(isset($_POST['input_post_code'])){ echo $_POST['input_post_code'];} ?>">
	                </div>
	              </div>
	              <div class="col-md-6 col-xs-12">
	                <div class="form-group">
											<label for="status" style="color:#495057;">Country </label>
											<?php $fetchcountry = selectContent($conn, "country", []) ?>
											<select class="form-control" name="input_country">
												<?php foreach ($fetchcountry as $key => $value): ?>
				<?php if (isset($_POST['input_country']) && $_POST['input_country'] == $value['name']): ?>
									<option selected value="<?php echo $value['name'] ?>" ><?php echo $value['name'] ?></option>
					<?php else: ?>
														<option value="<?php echo $value['name'] ?>" ><?php echo $value['name'] ?></option>
				<?php endif; ?>
												<?php endforeach; ?>
											</select>
	                <!-- <input type="text" name="input_country" class="form-control" placeholder="Country" value="<?php if(isset($_POST['input_country'])){ echo $_POST['input_country'];} ?>"> -->
	                </div>
	              </div>

	              <div class="col-md-6 col-xs-12">

	                      <div class="form-group">
														<label for="status" style="color:#495057;"> Ethnic Group <span class="text-danger">*</span> </label><br>
													<span class="text-danger"><?php if(isset($error['input_ethnic'])){ echo $error['input_ethnic'];} ?></span>
	                      <input type="text" name="input_ethnic" class="form-control" placeholder="Ethnic Group" value="<?php if(isset($_POST['input_ethnic'])){ echo $_POST['input_ethnic'];} ?>">
	                      </div>
	              </div>

	              <div class="col-md-6 col-xs-12">
	                      <div class="form-group">
														<label for="status" style="color:#495057;">Mobile </label>
	                      <input type="text" name="input_phone" class="form-control" placeholder="Mobile" value="<?php if(isset($_POST['input_phone'])){ echo $_POST['input_phone'];} ?>">
	                      </div>
	              </div>
	              <div class="col-md-6 col-xs-12">
	                      <div class="form-group">
														<label for="status" style="color:#495057;">Academic Study </label>
	                      <input type="text" name="input_study" class="form-control" placeholder="Academic study" value="<?php if(isset($_POST['input_study'])){ echo $_POST['input_study'];} ?>">
	                      </div>
	              </div>

	              <div class="col-md-6 col-xs-12">
	                  <div class="form-group">
	                      <label for="status" style="color:#495057;">Status</label>
	                      <select class="form-control" id="status" name="input_status">
	                          <option value="Student">Student</option>
	                          <option value="Graduate">Graduate</option>
	                          <option value="Intern">Intern</option>
	                      </select>


	                  </div>
	              </div>

	            <div class="col-md-6 col-xs-12">

	                  <div class="form-group">
												<label for="status" style="color:#495057;">Linked In <span class="text-danger">*</span> </label><br>
											<span class="text-danger"><?php if(isset($error['input_linkedIn'])){ echo $error['input_linkedIn'];} ?></span>
	                                  <input type="text" name="input_linkedIn" class="form-control" placeholder="LinkedIn Profile" value="<?php if(isset($_POST['input_linkedIn'])){ echo $_POST['input_linkedIn'];} ?>">
	                  </div>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                  <div class="form-group">
												<label for="status" style="color:#495057;">Facebook </label>
	                                  <input type="text" name="input_facebook" class="form-control" placeholder="FaceBook Profile" value="<?php if(isset($_POST['input_facebook'])){ echo $_POST['input_facebook'];} ?>">
	                  </div>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                  <div class="form-group">
												<label for="status" style="color:#495057;">Portfolio </label>
	                                  <input type="text" name="input_issu" class="form-control" placeholder="Portfolio" value="<?php if(isset($_POST['input_issu'])){ echo $_POST['input_issu'];} ?>">
	                  </div>
	             </div>
	             <div class="col-md-6 col-xs-12">
	                  <div class="form-group">
												<label for="status" style="color:#495057;">Profession <span class="text-danger">*</span> </label><br>
											<span class="text-danger"><?php if(isset($error['input_profession'])){ echo $error['input_profession'];} ?></span>
	                                  <input type="text" name="input_profession" class="form-control" placeholder="Profession" value="<?php if(isset($_POST['input_profession'])){ echo $_POST['input_profession'];} ?>">
	                  </div>
	              </div>

	              <div class="col-md-6 col-xs-12">
	                  <div class="form-group" style="color:#495057;">
	                  <label for="materialFile">Upload photo <span class="text-danger">*</span> </label><br>
											<span class="text-danger"><?php if(isset($error['image_1'])){ echo $error['image_1'];} ?></span>
	                  <input type="file" class="form-control-file" id="materialFile" name="image_1" value="<?php if(isset($_POST['image_1'])){ echo $_POST['image_1'];} ?>">
	                  </div>
	              </div>

	              <div class="col-md-12 col-xs-12">
	                              <div class="form-group">
																		<label for="status" style="color:#495057;"> Email <span class="text-danger">*</span> </label><br>
																	<span class="text-danger"><?php if(isset($error['input_email'])){ echo $error['input_email'];} ?></span>
	                                  <input type="email" name="input_email" class="form-control" placeholder="Email" value="<?php if(isset($_POST['input_email'])){ echo $_POST['input_email'];} ?>">
	                              </div>
	              </div>
	              <div class="col-md-6 col-xs-12">
	                              <div class="form-group" style="color:#495057;">
																		<label for="status" style="color:#495057;"> Password <span class="text-danger">*</span> </label><br>
																	<span class="text-danger"><?php if(isset($error['input_password'])){ echo $error['input_password'];} ?></span>
	                                  <input type="password" name="input_password" class="form-control" placeholder="Password" value="<?php if(isset($_POST['input_password'])){ echo $_POST['input_password'];} ?>"> (between 6 and 16 chars)
	                              </div>
	              </div>
	              <div class="col-md-6 col-xs-12">
	                              <div class="form-group">
																		<label for="status" style="color:#495057;">Confirm Password <span class="text-danger">*</span> </label><br>
																	<span class="text-danger"><?php if(isset($error['confirm_password'])){ echo $error['confirm_password'];} ?></span>
	                                  <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" value="<?php if(isset($_POST['confirm_password'])){ echo $_POST['confirm_password'];} ?>">
	                              </div>
	              </div>
	              <div class="col-md-12 col-xs-12">
	                              <div class="form-group">
	                                  <label class="checkbox-inline" style="color:#495057;">
	                                  <input type="checkbox" required="required"> You agree to the <a href="#" style="color:#495057;">Terms</a> & <a href="#" style="color:#495057;">Conditions</a>.
	                                  </label>
	                              </div>
	              </div>
	              <div class="col-md-6 col-xs-12">

	                                  <div class="form-group">
	                                      <input type="submit" name="register" value="Register" class="btn btn-primary btn-block btn-sm" tabindex="7" style="background-color:#970C7B; border-color: #970C7B;">
	                                  </div>


	              </div>
	                              </form>
			</div>
        </div>


			</div>
      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
