<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $conn1 = db_connect();



    //create short variable names
    $courseDetails=@$_POST['courseDetails'];
    $courseDetails = addslashes($courseDetails);

    $coursePromotionAmount=@$_POST['coursePromotionAmount'];
    $courseAmount=@$_POST['courseAmount'];
    $courseDescription=@$_POST['courseDescription'];
    $courseName=@$_POST['courseName'];
    $courseId=@$_POST['courseId'];
    $live=@$_POST['live'];
    $conferenceLink=@$_POST['conferenceLink'];




    //session set already
	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
    }
    // user logs in
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		//header("Location: login_form.php");
	}






?>
<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Edit Course Details</h3>
        </header>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <?php


                           // if ok, put in db
                           $courseUpdate = "UPDATE panel_courses SET input_course_name='$courseName', text_course_description='$courseDescription', text_course_details='$courseDetails', input_course_amount=$courseAmount, input_course_promotion=$coursePromotionAmount, input_live='$live', input_conference_link ='$conferenceLink' WHERE id =$courseId";


                            if (mysqli_query($conn1, $courseUpdate)) {


                                echo 'The Course  has been updated. <br /><br /> ';

                            } else {
                            echo "Error: " . $courseUpdate . "" . mysqli_error($conn1);
                            }
                    ?>
                            <a href="view-course?course=<?php echo $courseId; ?>" class="btn btn-success btn-sm"><b>View Course</b></a>

                            <a href="admin_menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
