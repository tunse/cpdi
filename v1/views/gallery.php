<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');



?>
<?php $page_title = "Gallery" ?>

<?php include "includes/header.php" ?>
<!-- <style>
.fa {
  padding: 5px;
  font-size: 20px;
  width: 40px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
  border-radius: 50%;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  background: #00aff0;
  color: white;
}

.fa-android {
  background: #a4c639;
  color: white;
}

.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.fa-vimeo {
  background: #45bbff;
  color: white;
}

.fa-tumblr {
  background: #2c4762;
  color: white;
}

.fa-vine {
  background: #00b489;
  color: white;
}

.fa-foursquare {
  background: #45bbff;
  color: white;
}

.fa-stumbleupon {
  background: #eb4924;
  color: white;
}

.fa-flickr {
  background: #f40083;
  color: white;
}

.fa-yahoo {
  background: #430297;
  color: white;
}

.fa-soundcloud {
  background: #ff5500;
  color: white;
}

.fa-reddit {
  background: #ff5700;
  color: white;
}

.fa-rss {
  background: #ff6600;
  color: white;
}
</style> -->





    <!--==========================
      Services Section
    ============================-->



    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"><br /><br />

                                <div class="container-fluid">
                                    <div class="row mt-12" style="color:#495057;">
                                    <div class="col-12"> <p class="h3"><b>CPDI Africa Chronicles</b> <br>
																			Our Voice and Contribution to the Discourse in Afrocentric Architecture
																		 </p>
                                        <!-- <img alt="CPDI" src="gallery.png" class="w-100"> -->


                                            <p style="margin:0"><br />
                                            <!-- Welcome to the African Architecture Studies search engine. Our database of Afrocentric scholarship provides students and professionals access to publications, journals, videos, interviews, podcasts, and all content critical to designers who seek to translate and incorporate African architectural design philosophies into their academic and professional work. -->
																						The old African proverb teaches us that ‘the journey of a million miles starts with just one step’. These <b>CPDI Africa storyboards </b>chronicle our journey into preserving
African traditional design philosophies, translating these indigenous philosophies into new architectural languages, and spotlighting the architects that inspire our
winning architecture masterpieces! Click on our link and journey with us over the past pioneering and courageous years, enjoy our highlights and memorable
moments
<br>
 </p>
 <p class="text-center" style="font-weight:900;margin:0">Visit our Online Albums at Facebook.com/cpdiafrica <a href="https://facebook.com/cpdiafrica/photos/?tab=album&ref=page_internal" style="color:blue;"> (Click here)</a> </p>


                                            <p>
                                                <!--
                                                <form>
                                                    <div class="form-group">
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" autocomplete="off"> Country
                                                            </label>
                                                            <label class="btn btn-secondary active">
                                                                <input type="radio" name="options" autocomplete="off" checked> Ethnic Group
                                                            </label>
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" autocomplete="off"> Name of Architect
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="url" name="search" placeholder="" required="required">
                                                    </div>
                                                    <a href="registration_form.php?course=<?php echo $course; ?>" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Search</b></a><br /><br />
                                                </form>
                                                -->
                                            </p>
                                        </div>
                                    </div>
                                </div>

        </div>

        </div>



<iframe src="https://albumizr.com/a/P2lD" scrolling="no" frameborder="0" allowfullscreen style="width:100%;height:100vh" ></iframe>

        <!-- Grid row -->

														        <!-- Grid row -->
				</div>
			</section>


<?php
include "includes/footer.php";
 ?>
