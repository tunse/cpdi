<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');



?>

<?php include "includes/header.php" ?>
<!-- <style>
.fa {
  padding: 5px;
  font-size: 20px;
  width: 40px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
  border-radius: 50%;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  background: #00aff0;
  color: white;
}

.fa-android {
  background: #a4c639;
  color: white;
}

.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.fa-vimeo {
  background: #45bbff;
  color: white;
}

.fa-tumblr {
  background: #2c4762;
  color: white;
}

.fa-vine {
  background: #00b489;
  color: white;
}

.fa-foursquare {
  background: #45bbff;
  color: white;
}

.fa-stumbleupon {
  background: #eb4924;
  color: white;
}

.fa-flickr {
  background: #f40083;
  color: white;
}

.fa-yahoo {
  background: #430297;
  color: white;
}

.fa-soundcloud {
  background: #ff5500;
  color: white;
}

.fa-reddit {
  background: #ff5700;
  color: white;
}

.fa-rss {
  background: #ff6600;
  color: white;
}
</style> -->





    <!--==========================
      Services Section
    ============================-->



    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"><br /><br />

                                <div class="container-fluid">
                                    <div class="row mt-12" style="color:#495057;">
                                    <div class="col-12"> <p class="h3"><b>CPDI Africa Chronicles</b> <br>
																			Our Voice and Contribution to the Discourse in Afrocentric Architecture
																		 </p>
                                        <!-- <img alt="CPDI" src="gallery.png" class="w-100"> -->


                                            <p><br />
                                            <!-- Welcome to the African Architecture Studies search engine. Our database of Afrocentric scholarship provides students and professionals access to publications, journals, videos, interviews, podcasts, and all content critical to designers who seek to translate and incorporate African architectural design philosophies into their academic and professional work. -->
																						The old African proverb teaches us that ‘the journey of a million miles starts with just one step’. These <b>CPDI Africa storyboards </b>chronicle our journey into preserving
African traditional design philosophies, translating these indigenous philosophies into new architectural languages, and spotlighting the architects that inspire our
winning architecture masterpieces! Click on our link and journey with us over the past pioneering and courageous years, enjoy our highlights and memorable
moments
<br>
<b class="h6 text-center">Visit our Online Albums at Facebook.com/cpdiafrica <a href="https://facebook.com/cpdiafrica/photos/?tab=album&ref=page_internal" style="color:blue"> (Click here)</a> </b> </p>


                                            <p>
                                                <!--
                                                <form>
                                                    <div class="form-group">
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" autocomplete="off"> Country
                                                            </label>
                                                            <label class="btn btn-secondary active">
                                                                <input type="radio" name="options" autocomplete="off" checked> Ethnic Group
                                                            </label>
                                                            <label class="btn btn-secondary">
                                                                <input type="radio" name="options" autocomplete="off"> Name of Architect
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="url" name="search" placeholder="" required="required">
                                                    </div>
                                                    <a href="registration_form.php?course=<?php echo $course; ?>" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Search</b></a><br /><br />
                                                </form>
                                                -->
                                            </p>
                                        </div>
                                    </div>
                                </div>

        </div>

        </div>





        <!-- Grid row -->
        <div class="row">



        <!-- Grid column -->
        <div class="col-lg-4 col-md-6 mb-4">

          <!--Modal: Name-->
          <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">

              <!--Content-->
              <div class="modal-content">

                <!--Body-->
                <div class="modal-body mb-0 p-0">

                  <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 1.jpg" alt="Picture" data-toggle="modal" data-target="#modal1">

                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                </div>

              </div>
              <!--/.Content-->

            </div>
          </div>
          <!--Modal: Name-->

          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 1.jpg" alt="Picture"
              data-toggle="modal" data-target="#modal1"></a>

        </div>
        <!-- Grid column -->
				<div class="col-lg-4 col-md-6 mb-4">

					<!--Modal: Name-->
					<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">

							<!--Content-->
							<div class="modal-content">

								<!--Body-->
								<div class="modal-body mb-0 p-0">

									<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 2.jpg" alt="Picture" data-toggle="modal" data-target="#modal2">

								</div>

								<!--Footer-->
								<div class="modal-footer justify-content-center">

									<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

								</div>

							</div>
							<!--/.Content-->

						</div>
					</div>
					<!--Modal: Name-->

					<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 2.jpg" alt="Picture"
							data-toggle="modal" data-target="#modal2"></a>

				</div>
        <!-- Grid column -->
				<div class="col-lg-4 col-md-6 mb-4">

          <!--Modal: Name-->
          <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">

              <!--Content-->
              <div class="modal-content">

                <!--Body-->
                <div class="modal-body mb-0 p-0">

                  <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 3.jpg" alt="Picture" data-toggle="modal" data-target="#modal3">

                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                </div>

              </div>
              <!--/.Content-->

            </div>
          </div>
          <!--Modal: Name-->

          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 3.jpg" alt="Picture"
              data-toggle="modal" data-target="#modal3"></a>

        </div>
        <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row">

        <!-- Grid column -->
        <div class="col-lg-4 col-md-12 mb-4">

          <!--Modal: Name-->
          <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">

              <!--Content-->
              <div class="modal-content">

                <!--Body-->
                <div class="modal-body mb-0 p-0">

                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 4.jpg" alt="Picture" data-toggle="modal" data-target="#modal4">

                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                </div>

              </div>
              <!--/.Content-->

            </div>
          </div>
          <!--Modal: Name-->

          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 4.jpg" alt="video"
              data-toggle="modal" data-target="#modal4"></a>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-4 col-md-6 mb-4">

          <!--Modal: Name-->
          <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">

              <!--Content-->
              <div class="modal-content">

                <!--Body-->
                <div class="modal-body mb-0 p-0">

                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 5.jpg" alt="Picture" data-toggle="modal" data-target="#modal5">

                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">


                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                </div>

              </div>
              <!--/.Content-->

            </div>
          </div>
          <!--Modal: Name-->

          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 5.jpg" alt="video"
              data-toggle="modal" data-target="#modal5"></a>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
				<div class="col-lg-4 col-md-6 mb-4">

					<!--Modal: Name-->
					<div class="modal fade" id="modal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">

							<!--Content-->
							<div class="modal-content">

								<!--Body-->
								<div class="modal-body mb-0 p-0">

								<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 6.jpg" alt="Picture" data-toggle="modal" data-target="#modal6">

								</div>

								<!--Footer-->
								<div class="modal-footer justify-content-center">


									<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

								</div>

							</div>
							<!--/.Content-->

						</div>
					</div>
					<!--Modal: Name-->

					<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 6.jpg" alt="video"
							data-toggle="modal" data-target="#modal6"></a>

				</div>
        <!-- Grid column -->

        </div>
        <!-- Grid row -->


				        <!-- Grid row -->
				        <div class="row">

				        <!-- Grid column -->
				        <div class="col-lg-4 col-md-12 mb-4">

				          <!--Modal: Name-->
				          <div class="modal fade" id="modal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				            <div class="modal-dialog modal-lg" role="document">

				              <!--Content-->
				              <div class="modal-content">

				                <!--Body-->
				                <div class="modal-body mb-0 p-0">

				                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 7.jpg" alt="Picture" data-toggle="modal" data-target="#modal7">

				                </div>

				                <!--Footer-->
				                <div class="modal-footer justify-content-center">

				                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

				                </div>

				              </div>
				              <!--/.Content-->

				            </div>
				          </div>
				          <!--Modal: Name-->

				          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 7.jpg" alt="video"
				              data-toggle="modal" data-target="#modal7"></a>

				        </div>
				        <!-- Grid column -->

				        <!-- Grid column -->
				        <div class="col-lg-4 col-md-6 mb-4">

				          <!--Modal: Name-->
				          <div class="modal fade" id="modal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				            <div class="modal-dialog modal-lg" role="document">

				              <!--Content-->
				              <div class="modal-content">

				                <!--Body-->
				                <div class="modal-body mb-0 p-0">

				                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 8.jpg" alt="Picture" data-toggle="modal" data-target="#modal8">

				                </div>

				                <!--Footer-->
				                <div class="modal-footer justify-content-center">


				                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

				                </div>

				              </div>
				              <!--/.Content-->

				            </div>
				          </div>
				          <!--Modal: Name-->

				          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 8.jpg" alt="video"
				              data-toggle="modal" data-target="#modal8"></a>

				        </div>
				        <!-- Grid column -->

				        <!-- Grid column -->
								<div class="col-lg-4 col-md-6 mb-4">

									<!--Modal: Name-->
									<div class="modal fade" id="modal9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">

											<!--Content-->
											<div class="modal-content">

												<!--Body-->
												<div class="modal-body mb-0 p-0">

												<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 9.jpg" alt="Picture" data-toggle="modal" data-target="#modal9">

												</div>

												<!--Footer-->
												<div class="modal-footer justify-content-center">


													<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

												</div>

											</div>
											<!--/.Content-->

										</div>
									</div>
									<!--Modal: Name-->

									<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 9.jpg" alt="video"
											data-toggle="modal" data-target="#modal9"></a>

								</div>
				        <!-- Grid column -->

				        </div>
				        <!-- Grid row -->


								        <!-- Grid row -->
								        <div class="row">

								        <!-- Grid column -->
								        <div class="col-lg-4 col-md-12 mb-4">

								          <!--Modal: Name-->
								          <div class="modal fade" id="modal10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								            <div class="modal-dialog modal-lg" role="document">

								              <!--Content-->
								              <div class="modal-content">

								                <!--Body-->
								                <div class="modal-body mb-0 p-0">

								                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 10.jpg" alt="Picture" data-toggle="modal" data-target="#modal10">

								                </div>

								                <!--Footer-->
								                <div class="modal-footer justify-content-center">

								                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

								                </div>

								              </div>
								              <!--/.Content-->

								            </div>
								          </div>
								          <!--Modal: Name-->

								          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 10.jpg" alt="video"
								              data-toggle="modal" data-target="#modal10"></a>

								        </div>
								        <!-- Grid column -->

								        <!-- Grid column -->
								        <div class="col-lg-4 col-md-6 mb-4">

								          <!--Modal: Name-->
								          <div class="modal fade" id="modal11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								            <div class="modal-dialog modal-lg" role="document">

								              <!--Content-->
								              <div class="modal-content">

								                <!--Body-->
								                <div class="modal-body mb-0 p-0">

								                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 11.jpg" alt="Picture" data-toggle="modal" data-target="#modal11">

								                </div>

								                <!--Footer-->
								                <div class="modal-footer justify-content-center">


								                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

								                </div>

								              </div>
								              <!--/.Content-->

								            </div>
								          </div>
								          <!--Modal: Name-->

								          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 11.jpg" alt="video"
								              data-toggle="modal" data-target="#modal11"></a>

								        </div>
								        <!-- Grid column -->

								        <!-- Grid column -->
												<div class="col-lg-4 col-md-6 mb-4">

													<!--Modal: Name-->
													<div class="modal fade" id="modal12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog modal-lg" role="document">

															<!--Content-->
															<div class="modal-content">

																<!--Body-->
																<div class="modal-body mb-0 p-0">

																<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 12.jpg" alt="Picture" data-toggle="modal" data-target="#modal12">

																</div>

																<!--Footer-->
																<div class="modal-footer justify-content-center">


																	<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																</div>

															</div>
															<!--/.Content-->

														</div>
													</div>
													<!--Modal: Name-->

													<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 12.jpg" alt="video"
															data-toggle="modal" data-target="#modal12"></a>

												</div>
								        <!-- Grid column -->

								        </div>
								        <!-- Grid row -->

												        <!-- Grid row -->
												        <div class="row">

												        <!-- Grid column -->
												        <div class="col-lg-4 col-md-12 mb-4">

												          <!--Modal: Name-->
												          <div class="modal fade" id="modal13" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												            <div class="modal-dialog modal-lg" role="document">

												              <!--Content-->
												              <div class="modal-content">

												                <!--Body-->
												                <div class="modal-body mb-0 p-0">

												                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 13.jpg" alt="Picture" data-toggle="modal" data-target="#modal13">

												                </div>

												                <!--Footer-->
												                <div class="modal-footer justify-content-center">

												                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

												                </div>

												              </div>
												              <!--/.Content-->

												            </div>
												          </div>
												          <!--Modal: Name-->

												          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 13.jpg" alt="video"
												              data-toggle="modal" data-target="#modal13"></a>

												        </div>
												        <!-- Grid column -->

												        <!-- Grid column -->
												        <div class="col-lg-4 col-md-6 mb-4">

												          <!--Modal: Name-->
												          <div class="modal fade" id="modal14" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												            <div class="modal-dialog modal-lg" role="document">

												              <!--Content-->
												              <div class="modal-content">

												                <!--Body-->
												                <div class="modal-body mb-0 p-0">

												                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 14.jpg" alt="Picture" data-toggle="modal" data-target="#modal14">

												                </div>

												                <!--Footer-->
												                <div class="modal-footer justify-content-center">


												                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

												                </div>

												              </div>
												              <!--/.Content-->

												            </div>
												          </div>
												          <!--Modal: Name-->

												          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 14.jpg" alt="video"
												              data-toggle="modal" data-target="#modal14"></a>

												        </div>
												        <!-- Grid column -->

												        <!-- Grid column -->
																<div class="col-lg-4 col-md-6 mb-4">

																	<!--Modal: Name-->
																	<div class="modal fade" id="modal15" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																		<div class="modal-dialog modal-lg" role="document">

																			<!--Content-->
																			<div class="modal-content">

																				<!--Body-->
																				<div class="modal-body mb-0 p-0">

																				<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 15.jpg" alt="Picture" data-toggle="modal" data-target="#modal15">

																				</div>

																				<!--Footer-->
																				<div class="modal-footer justify-content-center">


																					<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																				</div>

																			</div>
																			<!--/.Content-->

																		</div>
																	</div>
																	<!--Modal: Name-->

																	<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 15.jpg" alt="video"
																			data-toggle="modal" data-target="#modal15"></a>

																</div>
												        <!-- Grid column -->

												        </div>
												        <!-- Grid row -->


																        <!-- Grid row -->
																        <div class="row">

																        <!-- Grid column -->
																        <div class="col-lg-4 col-md-12 mb-4">

																          <!--Modal: Name-->
																          <div class="modal fade" id="modal16" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																            <div class="modal-dialog modal-lg" role="document">

																              <!--Content-->
																              <div class="modal-content">

																                <!--Body-->
																                <div class="modal-body mb-0 p-0">

																                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 16.jpg" alt="Picture" data-toggle="modal" data-target="#modal16">

																                </div>

																                <!--Footer-->
																                <div class="modal-footer justify-content-center">

																                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																                </div>

																              </div>
																              <!--/.Content-->

																            </div>
																          </div>
																          <!--Modal: Name-->

																          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 16.jpg" alt="video"
																              data-toggle="modal" data-target="#modal16"></a>

																        </div>
																        <!-- Grid column -->

																        <!-- Grid column -->
																        <div class="col-lg-4 col-md-6 mb-4">

																          <!--Modal: Name-->
																          <div class="modal fade" id="modal17" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																            <div class="modal-dialog modal-lg" role="document">

																              <!--Content-->
																              <div class="modal-content">

																                <!--Body-->
																                <div class="modal-body mb-0 p-0">

																                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 17.jpg" alt="Picture" data-toggle="modal" data-target="#modal17">

																                </div>

																                <!--Footer-->
																                <div class="modal-footer justify-content-center">


																                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																                </div>

																              </div>
																              <!--/.Content-->

																            </div>
																          </div>
																          <!--Modal: Name-->

																          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 17.jpg" alt="video"
																              data-toggle="modal" data-target="#modal17"></a>

																        </div>
																        <!-- Grid column -->

																        <!-- Grid column -->
																				<div class="col-lg-4 col-md-6 mb-4">

																					<!--Modal: Name-->
																					<div class="modal fade" id="modal18" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																						<div class="modal-dialog modal-lg" role="document">

																							<!--Content-->
																							<div class="modal-content">

																								<!--Body-->
																								<div class="modal-body mb-0 p-0">

																								<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 18.jpg" alt="Picture" data-toggle="modal" data-target="#modal18">

																								</div>

																								<!--Footer-->
																								<div class="modal-footer justify-content-center">


																									<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																								</div>

																							</div>
																							<!--/.Content-->

																						</div>
																					</div>
																					<!--Modal: Name-->

																					<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 18.jpg" alt="video"
																							data-toggle="modal" data-target="#modal18"></a>

																				</div>
																        <!-- Grid column -->

																        </div>
																        <!-- Grid row -->


														        <!-- Grid row -->
														        <div class="row">

														        <!-- Grid column -->
														        <div class="col-lg-4 col-md-12 mb-4">

														          <!--Modal: Name-->
														          <div class="modal fade" id="modal19" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														            <div class="modal-dialog modal-lg" role="document">

														              <!--Content-->
														              <div class="modal-content">

														                <!--Body-->
														                <div class="modal-body mb-0 p-0">

														                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 19.jpg" alt="Picture" data-toggle="modal" data-target="#modal19">

														                </div>

														                <!--Footer-->
														                <div class="modal-footer justify-content-center">

														                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

														                </div>

														              </div>
														              <!--/.Content-->

														            </div>
														          </div>
														          <!--Modal: Name-->

														          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 19.jpg" alt="video"
														              data-toggle="modal" data-target="#modal19"></a>

														        </div>
														        <!-- Grid column -->

														        <!-- Grid column -->
														        <div class="col-lg-4 col-md-6 mb-4">

														          <!--Modal: Name-->
														          <div class="modal fade" id="modal20" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														            <div class="modal-dialog modal-lg" role="document">

														              <!--Content-->
														              <div class="modal-content">

														                <!--Body-->
														                <div class="modal-body mb-0 p-0">

														                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 20.jpg" alt="Picture" data-toggle="modal" data-target="#modal20">

														                </div>

														                <!--Footer-->
														                <div class="modal-footer justify-content-center">


														                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

														                </div>

														              </div>
														              <!--/.Content-->

														            </div>
														          </div>
														          <!--Modal: Name-->

														          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 20.jpg" alt="video"
														              data-toggle="modal" data-target="#modal20"></a>

														        </div>
														        <!-- Grid column -->

														        <!-- Grid column -->
																		<div class="col-lg-4 col-md-6 mb-4">

																			<!--Modal: Name-->
																			<div class="modal fade" id="modal21" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																				<div class="modal-dialog modal-lg" role="document">

																					<!--Content-->
																					<div class="modal-content">

																						<!--Body-->
																						<div class="modal-body mb-0 p-0">

																						<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 21.jpg" alt="Picture" data-toggle="modal" data-target="#modal21">

																						</div>

																						<!--Footer-->
																						<div class="modal-footer justify-content-center">


																							<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																						</div>

																					</div>
																					<!--/.Content-->

																				</div>
																			</div>
																			<!--Modal: Name-->

																			<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 21.jpg" alt="video"
																					data-toggle="modal" data-target="#modal21"></a>

																		</div>
														        <!-- Grid column -->

														        </div>
														        <!-- Grid row -->


															        <!-- Grid row -->
															        <div class="row">

															        <!-- Grid column -->
															        <div class="col-lg-4 col-md-12 mb-4">

															          <!--Modal: Name-->
															          <div class="modal fade" id="modal22" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															            <div class="modal-dialog modal-lg" role="document">

															              <!--Content-->
															              <div class="modal-content">

															                <!--Body-->
															                <div class="modal-body mb-0 p-0">

															                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 22.jpg" alt="Picture" data-toggle="modal" data-target="#modal22">

															                </div>

															                <!--Footer-->
															                <div class="modal-footer justify-content-center">

															                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

															                </div>

															              </div>
															              <!--/.Content-->

															            </div>
															          </div>
															          <!--Modal: Name-->

															          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 22.jpg" alt="video"
															              data-toggle="modal" data-target="#modal22"></a>

															        </div>
															        <!-- Grid column -->

															        <!-- Grid column -->
															        <!-- <div class="col-lg-4 col-md-6 mb-4">


															          <div class="modal fade" id="modal23" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															            <div class="modal-dialog modal-lg" role="document">


															              <div class="modal-content">


															                <div class="modal-body mb-0 p-0">

															                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 23.jpg" alt="Picture" data-toggle="modal" data-target="#modal23">

															                </div>


															                <div class="modal-footer justify-content-center">


															                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

															                </div>

															              </div>


															            </div>
															          </div>


															          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 23.jpg" alt="video"
															              data-toggle="modal" data-target="#modal23"></a>

															        </div> -->
															        <!-- Grid column -->

															        <!-- Grid column -->
																			<div class="col-lg-4 col-md-6 mb-4">

																				<!--Modal: Name-->
																				<div class="modal fade" id="modal24" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																					<div class="modal-dialog modal-lg" role="document">

																						<!--Content-->
																						<div class="modal-content">

																							<!--Body-->
																							<div class="modal-body mb-0 p-0">

																							<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 24.jpg" alt="Picture" data-toggle="modal" data-target="#modal24">

																							</div>

																							<!--Footer-->
																							<div class="modal-footer justify-content-center">


																								<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																							</div>

																						</div>
																						<!--/.Content-->

																					</div>
																				</div>
																				<!--Modal: Name-->

																				<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 24.jpg" alt="video"
																						data-toggle="modal" data-target="#modal24"></a>

																			</div>
															        <!-- Grid column -->

															        </div>
															        <!-- Grid row -->




															        <!-- Grid row -->
															        <div class="row">

															        <!-- Grid column -->
															        <div class="col-lg-4 col-md-12 mb-4">

															          <!--Modal: Name-->
															          <div class="modal fade" id="modal25" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															            <div class="modal-dialog modal-lg" role="document">

															              <!--Content-->
															              <div class="modal-content">

															                <!--Body-->
															                <div class="modal-body mb-0 p-0">

															                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 25.jpg" alt="Picture" data-toggle="modal" data-target="#modal25">

															                </div>

															                <!--Footer-->
															                <div class="modal-footer justify-content-center">

															                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

															                </div>

															              </div>
															              <!--/.Content-->

															            </div>
															          </div>
															          <!--Modal: Name-->

															          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 25.jpg" alt="video"
															              data-toggle="modal" data-target="#modal25"></a>

															        </div>
															        <!-- Grid column -->

															        <!-- Grid column -->
															        <div class="col-lg-4 col-md-6 mb-4">

															          <!--Modal: Name-->
															          <div class="modal fade" id="modal26" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															            <div class="modal-dialog modal-lg" role="document">

															              <!--Content-->
															              <div class="modal-content">

															                <!--Body-->
															                <div class="modal-body mb-0 p-0">

															                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 26.jpg" alt="Picture" data-toggle="modal" data-target="#modal26">

															                </div>

															                <!--Footer-->
															                <div class="modal-footer justify-content-center">


															                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

															                </div>

															              </div>
															              <!--/.Content-->

															            </div>
															          </div>
															          <!--Modal: Name-->

															          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 26.jpg" alt="video"
															              data-toggle="modal" data-target="#modal26"></a>

															        </div>
															        <!-- Grid column -->

															        <!-- Grid column -->
																			<div class="col-lg-4 col-md-6 mb-4">

																				<!--Modal: Name-->
																				<div class="modal fade" id="modal27" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																					<div class="modal-dialog modal-lg" role="document">

																						<!--Content-->
																						<div class="modal-content">

																							<!--Body-->
																							<div class="modal-body mb-0 p-0">

																							<img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 27.jpg" alt="Picture" data-toggle="modal" data-target="#modal27">

																							</div>

																							<!--Footer-->
																							<div class="modal-footer justify-content-center">


																								<button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

																							</div>

																						</div>
																						<!--/.Content-->

																					</div>
																				</div>
																				<!--Modal: Name-->

																				<a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 27.jpg" alt="video"
																						data-toggle="modal" data-target="#modal27"></a>

																			</div>
															        <!-- Grid column -->

															        </div>
															        <!-- Grid row -->

														        <!-- Grid row -->
														        <div class="row">

														        <!-- Grid column -->
														        <div class="col-lg-4 col-md-12 mb-4">

														          <!--Modal: Name-->
														          <div class="modal fade" id="modal28" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														            <div class="modal-dialog modal-lg" role="document">

														              <!--Content-->
														              <div class="modal-content">

														                <!--Body-->
														                <div class="modal-body mb-0 p-0">

														                <img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 28.jpg" alt="Picture" data-toggle="modal" data-target="#modal28">

														                </div>

														                <!--Footer-->
														                <div class="modal-footer justify-content-center">

														                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

														                </div>

														              </div>
														              <!--/.Content-->

														            </div>
														          </div>
														          <!--Modal: Name-->

														          <a><img class="img-fluid z-depth-1" src="/uploads/STORY BOARD 28.jpg" alt="video"
														              data-toggle="modal" data-target="#modal28"></a>

														        </div>
														        <!-- Grid column -->


														        </div>
														        <!-- Grid row -->
				</div>
			</section>


<?php
include "includes/footer.php";
 ?>
