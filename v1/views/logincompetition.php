<?php
	  // session_start();

	  // include function files for this application
	  require_once('db_fns.php');
	  include('output_fns.php');
	  include('min_auto_fns.php');
	  $conn1 = db_connect();

	  $paypal_url='https://www.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
	  $paypal_id='design@southernsahara.com'; // Business email ID

	//create short variable names
	$username = @$_POST['email'];
	$passwd = @$_POST['password'];
	$courseId = @$_POST['courseId'];


	$result1 = selectCompetition($courseId);

	  if($result1)
	  {
		  $num_result = $result1->num_rows;
		  if($num_result > 0)
		  {
			  for ($i=0; $i<$num_result; $i++)
			  {
				  $row = $result1->fetch_assoc();
				  $courseName = $row['input_name'];
				  $courseDescription = $row['text_description'];
                  $flyer = $row['image_1'];
                  $coursePromotion = $row['input_amount'];
				  $courseDetails = $row['input_year'];

			  }
		   }
		  // else{
			   //header("Location: index.php");
		   //}
	  }


	  $paypalName = $courseName;


?>

<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>



    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <br /><br /><h3>Competition Registration</h3>
        </header>


	   <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">


            <div class="box">
				<h4 class="title"><?php echo $courseName; ?></h4>
				<p class="description"><?php echo $courseDescription; ?></p><br />
				<p class="description"><b>Amount </b> &#36;<?php echo $coursePromotion; ?></p>

            </div>
          </div>
		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2" style="color:#495057;">
					 <?php
							if ($username && $passwd && $courseId)
							// they have just tried logging in
							{
							  try
							  {
												login($username, $passwd);
												// if they are in the database register the user id
												$_SESSION['valid_user'] = $username;

												//echo $_SESSION['valid_user'];
												//echo "<br />$prodId";

												$result4 = selectUserByEmail($username);

												if($result4)
												{
													$num_result = $result4->num_rows;
													if($num_result > 0)
													{
														for ($i=0; $i<$num_result; $i++)
														{
															$row = $result4->fetch_assoc();
															$userId = $row['id'];
															$fname = $row['input_first_name'];
															$lname = $row['input_last_name'];

														}
													 }
												}


												$time = time();
																												$rnd = rand(0000000000,9999999999);
																												$split = explode(" ",'african chartered');
																												$id = $rnd.cleans($split['0']);
																												$hash_id = str_shuffle($id.'cpdi');

												// if ok, put in db
												$sqlOrder = "insert into read_competition_order values (null,'$hash_id', $courseId, $coursePromotion, $userId, '$time', 'BLANK', 'PENDING', 'BLANK', '$time','show',NOW(),NOW())";

												if (mysqli_query($conn1, $sqlOrder)) {


													echo 'Your order has been created. <br /><br /> Please make a payment to confirm your order for the competition.';

												} else {
												   echo "Error: " . $sql . "" . mysqli_error($conn1);
												}

												$order_id =  mysqli_insert_id($conn1);







												$to      = $username;
												$subject = 'African Centered Architecture';
												$message = "Dear $fname $lname, \r\n \r\n Thank you for your registration for the $courseName code club. \r\n \r\nYour registration will be confirmed once we receive payment. \r\n \r\n Kind regards, \r\n African Centered Architecture";
												$headers = 'From: design@cpdiafrica.org' . "\r\n" .
													'Reply-To: design@cpdiafrica.org' . "\r\n" .
													'X-Mailer: PHP/' . phpversion();

												mail($to, $subject, $message, $headers);

												$conn1->close();


?>
												<br /><br />
												<form action='<?php echo $paypal_url; ?>' method='post'>
												<input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
												<input type='hidden' name='cmd' value='_xclick'>
												<input type='hidden' name='item_name' value='<?php echo "$paypalName"; ?>'>
												<input type='hidden' name='item_number' value='<?php echo "$order_id"; ?>'>
												<input type='hidden' name='amount' value='<?php echo "$coursePromotion"; ?>'>
												<input type='hidden' name='no_shipping' value='1'>
												<input type='hidden' name='currency_code' value='USD'>
												<input type='hidden' name='cancel_return' value='http://www.cpdiafrica.org/cancel.php'>
												<input type='hidden' name='return' value='https://www.cpdiafrica.org/success.php'>
												<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
												<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
												</form> <br />
                        <div class="col-md-6 col-xs-12">
                          <div class="form-group">
                          <a href="banktransfer?order=<?php echo $order_id; ?>" class="btn-primary btn-block btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Pay by bank transfer or PayStack</b></a>
                          </div>
                        </div>
                        <br />

                        <?php
                                $urlEncodedText = "I'm interested in the course: $courseName";
                                $urlEncodedText = urlencode($urlEncodedText);
                                echo "<a href='https://wa.me/16786509145?text=$urlEncodedText' target='_blank' style='color:#008000;'>Send a WhatsApp message <img src='/cpdi/img/whatsapp.png'></a>";
                        ?>
<?php

							  }
							  catch(Exception $e)
							  {

										// unsuccessful login
										display_login_form($courseId, $username);
										echo 'You could not be logged in. You must be logged in to book the code club.';
										echo "<br /><br /><a href='index.php'>Go back to home page</a>";
										exit;
							  }
							}
					?>
		  </div>


      </div>




















    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
