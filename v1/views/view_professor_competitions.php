<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');
    $conn1 = db_connect();

    $professor=@$_GET['professor'];


    $result1 = selectUserById($professor);
    if($result1)
    {
        $num_result = $result1->num_rows;
        if($num_result > 0)
        {
            for ($i=0; $i<$num_result; $i++)
            {
                $row = $result1->fetch_assoc();
                $name = $row['input_first_name'];
                $lastName = $row['input_last_name'];

            }
        }
        else
        {
            $name = "error";
            $lastName = "error";
        }
    }
    else
    {
        $name = "error";
        $lastName = "error";
    }



?>

<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Edit Professor Competitions</h3>
        </header>

        <div class="row">



                        <?php
                            $result1 = selectProfessorCompetitions($professor);
                            if($result1)
                            {
                                $num_result = $result1->num_rows;
                                if($num_result > 0)
                                {
                                    echo '<div>';
                                    echo "<p><b>$name $lastName </b><br />";
                                    echo '<table cellspacing="10px" cellpadding="10px">';
                                    for ($i=0; $i<$num_result; $i++)
                                    {
                                        $row = $result1->fetch_assoc();
                                        $competition_id = $row['competition_id'];


                                        //get the course details from ID
                                        $result2 = selectCompetition($competition_id);
                                        if($result2)
                                        {
                                            $num_result2 = $result2->num_rows;
                                            if($num_result2 > 0)
                                            {
                                                for ($c=0; $c<$num_result2; $c++)
                                                {
                                                    $row2 = $result2->fetch_assoc();
                                                    $competitionName = $row2['input_name'];
                                                    $competitionDescription = $row2['text_description'];
                                                    $year = $row2['input_year'];
                                                }
                                            }
                                        }
                                        echo "<tr><td>$competitionName $year</td><td>$competitionDescription</td>";
                        ?>
                                        <td><a href="delete-professor-competition?competitionId=<?php echo "$competition_id&prof=$professor"; ?>" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">Disassosiate professor from competition </a></td></tr>
                        <?php
                                    }
                                    echo "</table>";
                        ?>
                                        <a href="add-professor-competition?professor=<?php echo "$professor" ?>" class="btn btn-success btn-sm"><b>Associate professor to a competition</b></a><br /><br />
                                        <a href="editprofessor" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to Edit Professor</a><br /><br />
                        <?php
                                    echo '</div>';
                                }
                                else
                                {
                                    echo '<div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">';
                                    echo "This professor does not have any registerd competitions <br /><br />";
                        ?>
                                    <a href="add-professor-competition?professor=<?php echo "$professor" ?>" class="btn btn-success"><b>Associate professor to another competition</b></a><br /><br />
                                    <a href="editprofessor" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to Edit Professor</a><br /><br />
                        <?php
                                    echo '</div>';
                                }
                            }

                        ?>



        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
