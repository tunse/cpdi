<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $conn1 = db_connect();



    //create short variable names
    $orderref=@$_POST['orderref'];

   if ($orderref >= 2000000)
   {

        $result1 = selectCompetitionOrder($orderref);

        if($result1)
        {
            $num_result = $result1->num_rows;
            if($num_result > 0)
            {
                for ($i=0; $i<$num_result; $i++)
                {
                    $row = $result1->fetch_assoc();
                    $order_id = $row['order_id'];
                    $course_id = $row['course_id'];
                    $amount = $row['input_amount'];
                    $user_id = $row['user_id'];


                }
            }

        }
   }
   elseif ($orderref < 2000000)
   {
        $result1 = selectCourseOrder($orderref);

        if($result1)
        {
            $num_result = $result1->num_rows;
            if($num_result > 0)
            {
                for ($i=0; $i<$num_result; $i++)
                {
                    $row = $result1->fetch_assoc();
                    $order_id = $row['id'];
                    $course_id = $row['course_id'];
                    $amount = $row['input_amount'];
                    $user_id = $row['user_id'];


                }
            }

        }
   }

   $result1 = selectUserById($user_id);
   if($result1)
   {
       $num_result = $result1->num_rows;
       if($num_result > 0)
       {
           for ($i=0; $i<$num_result; $i++)
           {
               $row = $result1->fetch_assoc();
               $name = $row['input_first_name'];
               $lastName = $row['input_last_name'];

           }
       }
       else
       {
           $name = "error";
           $lastName = "error";
       }
   }






?>
<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Update Payment</h3>
        </header>

        <div class="row">
<?php
            echo "<b>Name:</b> $name $lastName";

?>

            <form action="manual_payment_update" method="post">

            <div class="form-group">
                <label for="orderref">Order reference Id</label>
                <input type="text" class="form-control" id="orderref" name="orderref" value="<?php echo $orderref; ?>">
            </div>

            <div class="form-group">
                <label for="amount">Amount</label>
                <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $amount; ?>">
            </div>


            <button type="submit" class="btn btn-success btn-sm">Update order</button>
            <a href="admin_menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>

            </form>




        </div>
      </div>

    </section><!-- #services -->


  </main>


	<?php include "includes/footer.php" ?>
