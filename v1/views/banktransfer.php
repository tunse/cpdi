<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');

    $orderref = $_GET['order'];
$user_id = $_SESSION['valid_user'];
$invoice = selectContent($conn,"invoice",["event_id"=>$orderref]);
    if ($orderref >= 2000000)
   {
$type = "COMP";
        $result1 = selectCompetitionOrder($orderref);

        if($result1)
        {
            $num_result = $result1->num_rows;
            if($num_result > 0)
            {
                for ($i=0; $i<$num_result; $i++)
                {
                    $row = $result1->fetch_assoc();
                    $order_id = $row['id'];
                    $course_id = $row['competition_id'];
                    $amount = $row['input_amount'];
                    $user_id = $row['user_id'];


                }
            }

        }
   }
   elseif ($orderref < 2000000)
   {
		 $type = "CRS";
        $result1 = selectCourseOrder($orderref);

        if($result1)
        {
            $num_result = $result1->num_rows;
            if($num_result > 0)
            {
                for ($i=0; $i<$num_result; $i++)
                {
                    $row = $result1->fetch_assoc();
                    $order_id = $row['id'];
                    $course_id = $row['course_id'];
                    $amount = $row['input_amount'];
                    $user_id = $row['user_id'];


                }
            }

        }
   }

   $result1 = selectUserById($user_id);
   if($result1)
   {
       $num_result = $result1->num_rows;
       if($num_result > 0)
       {
           for ($i=0; $i<$num_result; $i++)
           {
               $row = $result1->fetch_assoc();
               $name = $row['input_first_name'];
               $lastName = $row['input_last_name'];

           }
       }
       else
       {
           $name = "error";
           $lastName = "error";
       }
   }



?>

<?php include "includes/header.php" ?>
<section id="services" class="section-bg">
  <div class="container">

    <header class="section-header">


    </header>

    <div class="row">
    <div class="container mt-4"><br /><br />




                <div class=" card container-fluid">
                    <div class="row mt-3" style="color:#495057;">
                    <div class="col-md-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                        <div class="col-md-9"><br>
                            <p class="h3">Payment</p>
                                Please make a payment of <?php echo "<b>&#36;$amount</b>"; ?> and put the <b> Order Id "<?php echo $orderref; ?>"</b> as the payment reference <br />
                                <br /><br />
                            <p>

	<style media="screen">
		*{
			padding:0;
			margin:0;
		}

		.hd-text{

			background-color:#970C7B;
			color:white;
			text-align:center;
			padding:15px;
		}


		.content{
			margin-top:0.5cm;
			width:100%;
			/* padding:0 5cm 5cm 5cm; */
			/* margin-left:5cm; */
		}
		.top-txt{
			width:90%;
			text-size:20px;
		}

		.pay-opt{
			margin:0.5cm;
		}

		.int-trans{


		}
		.int-trans-text{
			text-decoration:underline;
			padding:5px;
			background-color:#970C7B;
			color:white;
			text-align:center;

		}
		a{
			text-decoration:none;
		}
		.row1{
			margin-top:1cm;
			display:flex;
			width:100%;
		}
		.rw-ct1{
			width:45%;
		}
		footer{
			background-color:black;
			color:white;
		}
		.hd-logo{
			padding:10px;
			display:flex;
			justify-content:center;
		}
	</style>
	<div class="container">
		<!-- <div class="hd-logo">
			<img src="cpdi.jpg" width="250" height="100" alt="">
		</div> -->
		<div class="hd-text">
			<h4>Registration Fee Payment Platforms</h4>
		</div>
		<div class="content">
			<div class="top-txt">
				Fee payment of all CPDI Africa <b>Global Studio</b> courses, Afrocentric <b>Architecture Competitions</b>, Resource <b>subscriptions</b> and <b>merchandise payments</b>, find below the available payment options:
			</div>
			<br>
			<div class="row">
				<div class="col-md-6">
					<ol>
						<li class="pay-opt">
							<h4>Flutterwave</h4>
							-	Pay via flutterwave to <a style="color:#970C7B;" href="/payment_invoice?id=<?php echo $invoice[0]['hash_id'] ?>&type=<?php echo $type ?>">Here</a>, with a MasterCard or Visa.
						</li>
							<!-- <li class="pay-opt">
								<h4>PayPal</h4>
								-	Pay via PayPal to <a style="color:#970C7B;" href="https://design@southernsahara.com">design@southernsahara.com</a>, with a MasterCard or Visa.
							</li>
						<li class="pay-opt">
							<h4>PayStack</h4>
							-	Pay via PayStack to <a style="color:#970C7B;" href="https://design@southernsahara.com">design@southernsahara.com</a>, with a MasterCard or Visa.
						</li> -->
						<li class="pay-opt">
							<h4>GtBank</h4>
							<p>GTB - GUARANTY TRUST BANK<br>
							Account: 0023361183<br>
							Beneficiary address: Abuja, FCT Nigeria</p>

						</li>
						<li class="pay-opt">
							<h4>United Bank of Africa - UBA</h4>
							<p>UBA – UNITED BANK FOR AFRICA<br>
							c/o Southern Sahara Nigeria Ltd Account<br>
							Account: 1011026083<br>
							Beneficiary address: Abuja, FCT Nigeria</p>

						</li>
					</ol>
				</div>
				<div class="int-trans col-md-6">
					<h4 class="int-trans-text">FOR INTERNATIONAL TRANSFERS:</h4><br>
					<p>Please Pay: <br>
						United Bank For Africa Plc, Nigeria <br>
						Swift Code: Unafngla <br>
						Through: Standard Chartered Bank New York <br>
						Swift Code: Scblus33 - Account Number:  3582-024992-001 <br>
						Into Account: (for further credit) <br>
						Beneficiary Name: Southern Sahara Nigeria Ltd. <br>
						Beneficiary Account number: 1011026083 <br>
					</p>
				</div>
			</div>


			<br>
			<div class="">
				<h5 style="color:white; ; background-color:#970C7B; padding:10px;">If You Have Made A Cash Or Bank Transfer Payment Via The Banks Listed Above?</h5>

				<ul style="list-style-type:none;">
					<li>- Upon payment, please email us at <a style="color:#970C7B;" href="https://design@southernsahara.com">design@southernsahara.com<a>.</li>
					<li>- Email Titled: PAID REGISTRATION: YOUR FULL NAME (as entered when you registered on cpdiafrica.org)</li>
					<li>- Send the email to design@southernsahara.com listing the following information:</li>
				</ul>
				<br>
				<ol style=" margin-left:1cm;">
					<li> Your Name - as entered when you registered on <a style="color:#970C7B;" href="https://cpdiafrica.org">cpdiafrica.org</a></li>
					<li> Your Phone Number - as entered when you registered on <a style="color:#970C7B;" href="https://cpdiafrica.org">cpdiafrica.org</a></li>
					<li> Bank Deposit Slip # number</li>
					<li> Amount Deposited - Naira equivalent of USD</li>
					<li> Bank Exchange Rate for USD - for that date of your deposit.</li>
					<li> Scan your deposit slip and send along with your above details to us.</li>
				</ol>
				<br><br>
				<p><b>For additional information contact us at:</b></p>
			</div>

		</div>
	</div>
			<div style="margin-left:0; padding:0;">
				<h4><b>CPDI AFRICA</b></h4>
				<span> E-mail: design@southernsahara.com  <br> E-mail: design@cpdiafrica.org</span>
				<span>Tel:+1-678-650-9145 (USA) <br>Tel: +234 809 155 6480 (Nigeria)</span>

                            </p>


                            <p>
                                <!--
                                <form>
                                    <div class="form-group">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-secondary">
                                                <input type="radio" name="options" autocomplete="off"> Country
                                            </label>
                                            <label class="btn btn-secondary active">
                                                <input type="radio" name="options" autocomplete="off" checked> Ethnic Group
                                            </label>
                                            <label class="btn btn-secondary">
                                                <input type="radio" name="options" autocomplete="off"> Name of Architect
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="url" name="search" placeholder="" required="required">
                                    </div>
                                    <a href="registration_form.php?course=<?php echo $course; ?>" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;"><b>Search</b></a><br /><br />
                                </form>
                                -->
                            </p>
                        </div>
                    </div>
                </div>




        </div>










    </div>

  </div>
</section><!-- #services -->


<?php include "includes/footer.php" ?>
