<?php
	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $course=@$_GET['course'];

    if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
	}



?>

<?php include "includes/header.php" ?>
    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <h3><br />Course Feedback</h3>
        </header>

        <div class="row">

            <?php
                $result1 = selectStudentCourseFeedback($userId, $course);
                if($result1)
                {
                    $num_result = $result1->num_rows;
                    if($num_result > 0)
                    {
                        for ($i=0; $i<$num_result; $i++)
                        {
                            $row = $result1->fetch_assoc();
                            $feedback = $row['input_feedback'];
                            $professorRemark = trim($row['input_professor_remark']);
                            $feedbackId = $row['feedback_id'];
                            $professorId = $row['professor_id'];
                            $courseId = $row['course_id'];



                            $result2 = selectCourse($courseId);
                            if($result2)
                            {
                                $num_result2 = $result2->num_rows;
                                if($num_result2 > 0)
                                {
                                    for ($c=0; $c<$num_result2; $c++)
                                    {
                                        $row2 = $result2->fetch_assoc();
                                        $courseName = $row2['input_course_name'];
                                    }
                                }
                            }




?>

                            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                                <div class="box">

                                <h4 class="title"><?php echo "$courseName" ?></h4>
                                <p class="description"><b>Feedback given: </b><?php echo "$feedback"; ?> <br />

                                  <b>Professor feedback: </b>
<?php
                                  if ($professorRemark != "")
                                  {
                                      echo "$professorRemark";
                                  }
                                  else
                                  {
                                      echo "Not answered";
                                  }
?>

                                  <br /><br />

                                  <a href="myaccount" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to my account</a><br /><br />
                                  <a href="deleteStudentFeedbackRequest?material=<?php echo "$feedbackId"; ?>" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">Delete Feedback request</a>


                                </p>
                                </div>
                            </div>
<?php
                        }
                    }
                    else
                    {
                      echo "You have not sent any feedback requests <br />";
?>
                      <p><a href="create-feedback-form?course=<?php echo $course; ?>" class="btn btn-success btn-sm"><b>Create feedback request</b></a>
<?php
                      echo '<a href="myaccount" class="btn btn-light btn-sm"><b>Back to my account</b></a></p>';

                    }
                }
            ?>
        </div>

      </div>
    </section><!-- #services -->


  </main>


	<?php include "includes/footer.php" ?>
