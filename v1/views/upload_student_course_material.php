<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $course=@$_GET['course'];

    //get the course details from ID
    $result2 = selectCourse($course);
    if($result2)
    {
        $num_result2 = $result2->num_rows;
        if($num_result2 > 0)
        {
            for ($c=0; $c<$num_result2; $c++)
            {
                $row2 = $result2->fetch_assoc();
                $courseName = $row2['input_course_name'];
                $courseDescription = $row2['text_course_description'];
            }
        }
    }


	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
	}
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		//header("Location: login_form.php");
	}






?>
<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Upload Student Course Material</h3>
        </header>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                        <form action="create-student-course-material" method="post"  enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="courseName">Course Material Name</label>
                                    <input type="text" class="form-control" id="courseName" name="courseName" placeholder="Course Material Name" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseDescription">Course Material Description</label>
                                    <input type="text" class="form-control" id="courseDescription" name="courseDescription" placeholder="Course Material Description" required="required">
                                </div>

                                <input type='hidden' name='course' value='<?php echo $course; ?>'>

                                <div class="form-group">
                                    <label for="courseName">Course</label>
                                    <input type="text" readonly class="form-control" value='<?php echo "$courseName $courseDescription"; ?> '>
                                </div>
                                <div class="form-group">
                                    <label for="materialFile">Upload Course Material</label>
                                    <input type="file" class="form-control-file" id="materialFile" name="materialFile">
                                </div>
                                <button type="submit" class="btn btn-success btn-sm">Create new course material</button>
                                <a href="myaccount" class="btn btn-light btn-sm"><b>Back to my courses</b></a>

                        </form>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>


	<?php include "includes/footer.php" ?>
