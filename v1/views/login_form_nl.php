<?php

	session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');

	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		header("Location: myaccount.php");
	}

?>
<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>


</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index_nl.php" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_with_links_nl(); ?>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->



    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <br /><br /><h3></h3>
        </header>

        <div class="row">


		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
					 <?php login_form_nl(); ?>
          </div>
		

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
