<?php 
	session_start();
	include('output_fns.php'); 
	require_once('min_auto_fns.php'); 
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>African Centered Architecture</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <!-- <link href="img/favicon.png" rel="icon"> -->
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index_nl.php" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_nl(); ?>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <img src="img/3dcode.svg" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>Leer en creëer <br> toffe dingen <br>en heb plezier<br>met computers!</h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Starten</a>
          <a href="#services" class="btn-services scrollto">Kies een Club</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Over African Centered Architecture</h3>
          <!-- <p></p> -->
        </header>

        <div class="row about-container">

          <div class="col-lg-12 content order-lg-1 order-2">
            <p>
            African Centered Architecture richt zich op het inspireren van kinderen om bouwers te zijn niet alleen consumenten van technologie door middel van coderingsactiviteiten en uitdagingen.
            
            </p>

			<p>	
            We hebben inhoud gemaakt dat niet alleen beginnersvriendelijke concepten bevat, maar ook ideeën om enthousiast over te zijn.
           
            </p>

			<p>	
            Door hands-on teamgebaseerde oefeningen voel je je onderdeel van een vindingrijke groep die allemaal nieuwsgierig zijn naar technologie, en vind je het leren erover zowel intrigerend als lonend.
            
            </p>

			<p>	
            De activiteiten en uitdagingen gaan allemaal over het bijbrengen van de groeimindset bij leerlingen. We willen jou bijbrengen hoe je kunt leren van jouw fouten en ontwikkelen van vertrouwen en empowerment door te leren en te bouwen.
            
            
            </p>

					
			<p>
            Je leert correct typen zonder naar het toetsenbord te kijken. De projecten van African Centered Architecture leren je programmeren door je te laten zien hoe je computerspellen, animaties en websites kunt maken. Alle projecten kunnen worden gevolgd als stapsgewijze lesplannen of kunnen worden aangepast en zo creatief zijn als je wilt.
            
            </p>



            <div class="icon-box wow fadeInUp">
              
              <h4>De Projecten:</h4>
              <p class="description">
				<ul>
					<li>Leer je hoe een computertoestenbord op de juiste manier moet gebruiken en blind te typen (zonder naar het toestenbord te kijken)</li>
					<li>Introduceren van essentiele codeer concepten </li>
					<li>Aanmoedigen van uitdagende leermethode</li>
					<li>Het mogelijk maken voor leerlingen om programmeer vaardigheden te ontwikkelen terwijl ze vooruitgang boeken bij elk project</li>
					<li>Zijn uniek, een voor een helpen ze de leerlingen verschillende en originele dingen te creëren</li>
					<li>Niveau van moeilijkheid neemt toe terwijl ze verder gaan, en ze bouwen op de vaardigheden dat ze de afgelopen weken hebben geleerd</li>				
				</ul>
			  
			  </p>
            </div>

          </div>

          
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="img/scratch.svg" class="img-fluid" alt="Scratch">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
<?php		  
				$result1 = selectProducts_nl('SCRATCH');

				if($result1)
				{
					$num_result = $result1->num_rows;
					if($num_result > 0)
					{
						for ($i=0; $i<$num_result; $i++)
						{
							$row = $result1->fetch_assoc();
							$scratch_name = $row['name_nl'];
							$scratch_product_name = $row['product_name_nl'];
							$scratch_briefDescription = $row['briefDescription_nl'];
							$scratch_product_desc = $row['product_desc_nl'];
							$scratch_product_amount = $row['product_amount'];
							$scratch_period = $row['period'];
							$scratch_startDate = $row['startDate'];
							$scratch_startDate = date("d-m-Y", strtotime($scratch_startDate));
							$scratch_endDate = $row['endDate'];
							$scratch_endDate = date("d-m-Y", strtotime($scratch_endDate));
							$scratch_time = $row['time'];
						}
					 }
				}
?>
		  	  
            <h4><?php echo "$scratch_name: $scratch_product_name"; ?> </h4>
            <p>
              <?php echo "$scratch_product_desc"; ?>
            </p>
            
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="img/python.svg" class="img-fluid" alt="Python">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
		  
		  
<?php		  
	$result1 = selectProducts_nl('PYTHON');

	if($result1)
	{
		$num_result = $result1->num_rows;
		if($num_result > 0)
		{
			for ($i=0; $i<$num_result; $i++)
			{
				$row = $result1->fetch_assoc();
				$python_name = $row['name_nl'];
				$python_product_name = $row['product_name_nl'];
				$python_briefDescription = $row['briefDescription_nl'];
				$python_product_desc = $row['product_desc_nl'];
				$python_product_amount = $row['product_amount'];
				$python_period = $row['period'];
				$python_startDate = $row['startDate'];
				$python_startDate = date("d-m-Y", strtotime($python_startDate));
				$python_endDate = $row['endDate'];
				$python_endDate = date("d-m-Y", strtotime($python_endDate));
				$python_time = $row['time'];
			}
		 }
	}
?>
            <h4><?php echo "$python_name: $python_product_name"; ?> </h4>
            <p>
             <?php echo "$python_product_desc"; ?>
            </p>
           
          </div>
          
        </div>
		
		<div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="img/htmlcss.svg" class="img-fluid" alt="HTML & CSS">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
		  
		  
<?php		  
		$result1 = selectProducts_nl('HTML & CSS');

		if($result1)
		{
			$num_result = $result1->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result1->fetch_assoc();
					$html_name = $row['name_nl'];
					$html_product_name = $row['product_name_nl'];
					$html_briefDescription = $row['briefDescription_nl'];
					$html_product_desc = $row['product_desc_nl'];
					$html_product_amount = $row['product_amount'];
					$html_period = $row['period'];
					$html_startDate = $row['startDate'];
					$html_startDate = date("d-m-Y", strtotime($html_startDate));
					$html_endDate = $row['endDate'];
					$html_endDate = date("d-m-Y", strtotime($html_endDate));
					$html_time = $row['time'];
				}
			 }
		}
?>

            <h4><?php echo "$html_name: $html_product_name"; ?> </h4>
            <p>
             <?php echo "$html_product_desc"; ?>
            </p>
          </div>
        </div>
		


        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="img/typeles.svg" class="img-fluid" alt="TYPING CLASS">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
		  
		  
<?php		  
		$result1 = selectProducts_nl('TYPECURSUS');

		if($result1)
		{
			$num_result = $result1->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result1->fetch_assoc();
					$typing_name = $row['name_nl'];
					$typing_product_name = $row['product_name_nl'];
					$typing_briefDescription = $row['briefDescription_nl'];
					$typing_product_desc = $row['product_desc_nl'];
					$typing_product_amount = $row['product_amount'];
					$typing_period = $row['period'];
					$typing_startDate = $row['startDate'];
					$typing_startDate = date("d-m-Y", strtotime($html_startDate));
					$typing_endDate = $row['endDate'];
					$typing_endDate = date("d-m-Y", strtotime($html_endDate));
					$typing_time = $row['time'];
				}
			 }
		}
?>

            <h4><?php echo "$typing_name: $typing_product_name"; ?> </h4>
            <p>
             <?php echo "$typing_product_desc"; ?>
            </p>
          </div>
        </div>
		






		
<!--		 <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="img/3dprinting.svg" class="img-fluid" alt="3D Printing">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
		  
<?php		  
		$result1 = selectProducts_nl('3D Printing');

		if($result1)
		{
			$num_result = $result1->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result1->fetch_assoc();
					$threed_name = $row['name_nl'];
					$threed_product_name = $row['product_name_nl'];
					$threed_briefDescription = $row['briefDescription_nl'];
					$threed_product_desc = $row['product_desc_nl'];
					$threed_product_amount = $row['product_amount'];
					$threed_period = $row['period'];
					
					$threed_startDate = $row['startDate'];
					$threed_startDate = date("d-m-Y", strtotime($threed_startDate));
					$threed_endDate = $row['endDate'];
					$threed_endDate = date("d-m-Y", strtotime($threed_endDate));
					$threed_time = $row['time'];
				}
			 }
		}
?>

            <h4><?php echo "$threed_name: $threed_product_name"; ?> </h4>
            <p>
             <?php echo "$threed_product_desc"; ?>
            </p>
          </div>
         
        </div>
-->
      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Clubs</h3>
          <p>Kies een club</p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/scratchsmall.svg" class="img-fluid" alt="Scratch"></div>
              <h4 class="title"><?php echo "$scratch_name" ?></h4>
              <p class="description"><?php echo "$scratch_briefDescription"; ?> <br /><br /><a href="book_now_nl.php?course=<?php echo "$scratch_name" ?>" class="btn btn-success"><b>Boek een cursus</b></a></p>
            </div>
          </div>
		  
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            
			<div class="box">
			
              <div class="icon"><img src="img/pythonsmall.svg" class="img-fluid" alt="Python"></div>
              <h4 class="title"><?php echo "$python_name" ?></h4>
              <p class="description"><?php echo "$python_briefDescription"; ?> <br /><br /><a href="book_now_nl.php?course=<?php echo "$python_name" ?>" class="btn btn-success"><b>Boek een cursus</b></a></p>
			
            </div>
			
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/htmlcsssmall.svg" class="img-fluid" alt="Html & CSS"></div>
			  
			  
              <h4 class="title"><?php echo "$html_name" ?></h4>
			  <?php $html_name = urlencode($html_name); ?>
              <p class="description"><?php echo "$html_briefDescription"; ?> <br /><br /><a href="book_now_nl.php?course=<?php echo "$html_name" ?>" class="btn btn-success"><b>Boek een cursus</b></a></p>
            </div>
          </div>

               <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/typeless.svg" class="img-fluid" alt="Typing"></div>
             <h4 class="title"><?php echo "$typing_name" ?></h4>
              <p class="description"><?php echo "$typing_briefDescription"; ?> <br /><br /><a href="book_now_nl.php?course=<?php echo "$typing_name" ?>" class="btn btn-success"><b>Boek een cursus</b></a></p>
            </div>
          </div>

 <!--         <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><img src="img/3dprintersmall.svg" class="img-fluid" alt="Python"></div>
             <h4 class="title"><?php echo "$threed_name" ?></h4>
              <p class="description"><?php echo "$threed_briefDescription"; ?> <br /><br /><a href="book_now_nl.php?course=<?php echo "$threed_name" ?>" class="btn btn-success"><b>Boek een cursus</b></a></p>
            </div>
          </div>
-->
         

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>Waarom naar African Centered Architecture komen?</h3>
        </header>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                
              <div class="card-body">
                <h5 class="card-title">Leer correct typen</h5>
                <p class="card-text"></p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
               
              <div class="card-body">
                <h5 class="card-title">Creëer toffe dingen</h5>
                <p class="card-text"></p>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                
              <div class="card-body">
                <h5 class="card-title">Werk samen & deel</h5>
                <p class="card-text"> </p>
                
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>

   

    <!--==========================
      Clients Section
    ============================
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Testimonials</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">
    
              <div class="testimonial-item">
                <img src="img/testimonial-1.jpg" class="testimonial-img" alt="">
                <h3>Paul's father</h3>
                
                <p>
                  My child found the SCRATCH club fun to learn to create games and meet new friends
                </p>
              </div>
    
              

            </div>

          </div>
        </div>


      </div>
    </section><!-- #testimonials -->
    <!--==========================
      Team Section
    ============================-->
    <section id="team">
      <div class="container">
        <div class="section-header">
          <h3>Team</h3>
          
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 wow fadeInUp">
            <div class="member">
              <img src="img/team-1.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Peter Nwankwo</h4>
                  <span>Code Club Eigenaar / Docent</span>
				  <!-- 
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
				  -->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="member">
              <img src="img/team-2.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Felicidade Luzolo</h4>
                  <span>Code Club Eigenaar / Docent</span>
				  <!--
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
				  -->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="member">
              <img src="img/team-3.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Farida Elsharkawi</h4>
                  <span>Vrijwilliger</span>
				  <!--
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
				  -->
                </div>
              </div>
            </div>
          </div>

          
        </div>

      </div>
    </section><!-- #team -->

    
    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>Contact Ons</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
              
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2437.9727814619573!2d4.954561415344912!3d52.33463987977949!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c60bfadfe21d13%3A0x5fe40ad872cba2c3!2sOpenbare%20Basisschool%20(OBS)%20het%20Atelier!5e0!3m2!1snl!2snl!4v1590513315865!5m2!1snl!2snl" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p><?php display_footer_address_nl(); ?></p>
              </div>
              <div class="col-md-4 info">
                
                <p><?php display_footer_email_nl(); ?></p>
              </div>
              
            </div>

           
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>African Centered Architecture</h3>
            
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Hulpvolle Links</h4>
            <?php display_useful_links_nl(); ?>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Ons</h4>
            <p>
            <?php display_footer_address_nl(); ?>
			<br />
			<?php display_footer_email_nl(); ?>
		
			<?php //display_footer_phone(); ?>
			 
            </p>
<!--
            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Be informed </p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div>
-->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>African Centered Architecture</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
