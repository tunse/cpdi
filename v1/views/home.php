<?php $page_title = "African Centered Architecture" ?>
<?php include "includes/header.php" ?>
<?php $youtube = selectContent($conn, "read_youtube", []) ?>
<style media="screen">
  .linnk1{
    margin-right:35px;
  }
  .linnk2{
    margin-left:25px;
  }
  .linnk{
    margin-right:35px;
  }
  .btnn{
    padding: 0.5rem 1rem;
    font-size: 1.25rem;
    line-height: 1.5;
    border-radius: 0.3rem;
  }
  @media (max-width: 574px){
    .linnk1{
      margin-right:20px;
    }
    .linnk2{
    margin-left:20px;
    }
    .linnk{
    margin-right:30px;
    }
    .btnn{
      padding: 0.5rem;
      font-size: 0.7rem;
      /* line-height: 1.5; */
      /* border-radius: 0.3rem; */
    }
  }
  .updates_elem{
    width: 100%;
  }

  @media (min-width: 769px){
    .updates_elem{
      width: 300px;
    }
  }

  @media only screen and (max-width: 600px) {
    .mobile-btn{
        width: 100%;
        margin-left: 14px !important;
    }
  }
</style>
  <section id="intro" class="clearfix">
    <div class="container" style="heigeht:70vh">
        <!-- <br>
        <br> -->
        <div class="d-flex justify-content-end linnk">
          <div class="linnk1" stymle="padding-right: 20px">
            <a href="https://global-studio.cpdiafrica.org" class="btn btnn btsn-lg" style="background-color: #D33339; color: #fff;">Register for Courses!</a>
          </div>
          <div class="linnk2" stymle="padding-right: 20px">
            <a href="/competitions" class="btn btnn btsn-lg" style="background-color: #970C7B; color: #fff;">View our Designs!</a>
          </div>

        </div>
          <div class="intro-img" style="display:flex; margin-top:35px; position:relative">
            <a href="https://global-studio.cpdiafrica.org" style="margin-right: 15px"><IMG src="/uploads/Studio Logo.jpg" alt="" width="100%" class="img-fluid"></a>
            <a href="/competitions"><IMG src="/uploads/Heritage Logo.jpg" alt="" width="100%" class="img-fluid"></a>
          </div>

        <div class="intro-info">
          <h2>The Community Planning & Design Initiative Africa</h2>
          <div class="updates_elem" >
            <h6 style="text-align: center; ">
              <strong>CLICK FOR UPDATES! </strong>
            </h6>
          </div>
          <a href="<?php echo $youtube[0]['input_link'] ?>"><img src="<?php echo $youtube[0]['image_1'] ?>" alt="" style ="width: 300px"></a><br><br>
          <div class="row mb-5 col-sm-12 col-md-12 col-lg-9 d-flex justify-content-center">
            <div class="col-sm-12 col-md-6" style="padding-right: initial; padding-left: initial;">
              <a  href="https://www.gofundme.com/f/cpdi-africa-global-studio" class="btn btn-pprimary btnget2 px-4 mobile-btn" style="border-radius: 10px;" > Donate</a>
            </div>
            <div class="col-sm-12 col-md-6" style="padding-right: initial; padding-left: initial;">
              <a  href="https://cpdiafrica.blogspot.com/p/register-your-interest.html" class="btn btn-pprimary btnget2 px-4 mobile-btn" style="border-radius: 10px;" > Register</a>
            </div>

            <div class="col-sm-12 col-md-6 mt-2" style="padding-right: initial; padding-left: initial;">
              <a  href="https://us15.campaign-archive.com/home/?u=82858656ff28d4df9d68eeeb0&id=d3cb2d4ce8" class="btn btn-pprimary btnget2 px-4 mobile-btn" style="border-radius: 10px;" > Newsletter</a>
            </div>

            <div class="col-12 mt-3">
              <div class="col-10 d-flex justify-content-center">
                <a href="https://www.instagram.com/cpdi_africa/" class="text-white btn btn-icon btn-rounded btn-danger" target="_blank"> <i class="fa fa-instagram"></i> </a>
                <a href="https://www.facebook.com/cpdiafrica" class="text-white btn btn-icon btn-rounded "style="background-color: #3A5FAF" target="_blank"> <i class="fa fa-facebook"></i> </a>
                <a href="https://cpdiafrica.blogspot.com/" class="text-white btn btn-icon btn-warning btn-rounded " style="background-color: #ED7826" target="_blank" > <i class="fab fa-blogger-b"></i> </a>
                <a href = "mailto: design@cpdiafrica.org" class="text-white btn btn-icon btn-rounded "style="background-color: #4A85C4" target="_blank" > <i class="fas fa-envelope"></i> </a>
              </div>
            </div>
          </div>
          <!-- <iframe width="350" height="215" src=" " title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br><br> -->
        </div>

      </div>

  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">



      <div class="row about-extra">
          <div class="col-lg-6" data-aos="fade-right">
          <h4>The Community Planning & Design Initiative Africa (CPDI Africa) Promotes the Development of New Architectural Languages for the African Diaspora that are Culturally and Environmentally Sustainable.  </h4>
          </div>
          <div class="col-lg-6 pt-5 pt-lg-0" data-aos="fade-left">

            <p>
            Believing the development of Africa's built environment should be as it has always been, built as a collaborative effort between the community members and designated master builders, CDPI Africa engages participation from the design community in Africa and the Diaspora at large, for the accomplishment of its vision. CPDI Africa is culture-inspired, research-based and is committed to teaching a new pedagogy in African centered architecture built upon a new culture of afrocentric design philosophies.
            </p>
          </div>
        </div>



      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3 style="font-size: 32px;">TEACHING A NEW PEDAGOGY IN AFRICAN DIASPORA ARCHITECTURE </h3>
        </header>

        <div class="row">

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/1.png')">

              <a href="/about-us"><h4 class="title" style="font-size: 25px; text-align: center;">About <br />CPDI Africa</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/2.png')">

              <a href="https://global-studio.cpdiafrica.org"><h4 class="title" style="font-size: 25px; text-align: center;">Global<br /> Studio </h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/3.png')">

              <a href="/competitions"><h4 class="title" style="font-size: 25px; text-align: center;">Architecture<br /> Competitions</h4>

              </a>
            </div>
          </div>


          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/4.png')">

              <a href="/compendium"><h4 class="title" style="font-size: 25px; text-align: center;">Heritage <br />Compendium</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/5.png')">
             <!-- <div class="icon"><img src="img/scratchsmall.svg" class="img-fluid" alt="Scratch"></div>-->
              <a href="/services"><h4 class="title" style="font-size: 25px; text-align: center;">Design Services <br />& Programs</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/6.png')">

              <a href="/resources"><h4 class="title" style="font-size: 25px; text-align: center;">Publications <br />& Merchandise</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/7.png')">

              <a href="/connect"><h4 class="title" style="font-size: 25px; text-align: center;">Connect With<br /> CPDI Africa</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/8.png')">

              <a href="/gallery"><h4 class="title" style="font-size: 25px; text-align: center;">Photo <br /> Gallery</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('cpdi/img/9.png')">

              <a href="http://www.cpdiafrica.com" target="_blank"><h4 class="title" style="font-size: 25px; text-align: center;">CPDI Africa <br />Archives </h4>

              </a>
            </div>
          </div>


          <header class="section-header">
          <h3 style="font-size: 32px;">WE PRACTICE <b>SANKOFA.</b> WE UPHOLD <b>UBUNTU.</b> WE PIONEER <b>AFROFUTURISM.</b> </h3>
          </header>





        </div>

      </div>
    </section><!-- #services -->



    <!--==========================
      Clients Section
    ============================
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Testimonials</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">

              <div class="testimonial-item">
                <img src="img/testimonial-1.jpg" class="testimonial-img" alt="">
                <h3>Paul's father</h3>

                <p>
                  My child found the SCRATCH club fun to learn to create games and meet new friends
                </p>
              </div>



            </div>

          </div>
        </div>


      </div>
    </section>





  </main>
  <?php include "includes/footer.php" ?>
