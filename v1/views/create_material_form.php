<?php

	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');


	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
	}
	elseif (isset($_POST['email']) && !empty($_POST['email']))
	{
		//create short variable names
		$username = @$_POST['email'];
		$passwd = @$_POST['password'];
	}
	else
	{
		//header("Location: login_form.php");
	}






?>

<?php include "includes/header.php" ?>
  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>




    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Create course material</h3>
        </header>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                        <form action="create-material" method="post"  enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="courseName">Course Material Name</label>
                                    <input type="text" class="form-control" id="courseName" name="courseName" placeholder="Course Material Name" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="courseDescription">Course Material Description</label>
                                    <input type="text" class="form-control" id="courseDescription" name="courseDescription" placeholder="Course Material Description" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Course</label>
                                    <select class="form-control" id="course" name="course">
                    <?php
                                    $result1 = selectCourses();
                                    if($result1)
                                    {
                                        $num_result = $result1->num_rows;
                                        if($num_result > 0)
                                        {
                                            for ($i=0; $i<$num_result; $i++)
                                            {
                                                $row = $result1->fetch_assoc();
										                            $courseName = $row['input_course_name'];
										                            $courseDescription = $row['text_course_description'];
										                            $courseDetails = $row['text_course_details'];
										                            $courseAmount = $row['input_course_amount'];
										                            $coursePromotion = $row['input_course_promotion'];
                                                $course_id = $row['id'];

                    ?>
                                                <option value="<?php echo "$course_id" ?>"><?php echo "$courseName" ?></option>
                    <?php
                                            }
                                        }
                                    }
                                ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="courseAmount">Relevant URL</label>
                                    <input type="text" class="form-control" id="url" name="url" placeholder="URL" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="materialFile">Upload Course Material</label>
                                    <input type="file" class="form-control-file" id="materialFile" name="materialFile">
                                </div>
                                <button type="submit" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;">Create Course Material</button>
                                <a href="admin_menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>

                        </form>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
