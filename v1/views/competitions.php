<?php $page_title = "Competitions" ?>
<?php include "includes/header.php" ?>


    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"><br /><br />

            <ul class="nav nav-tabs responsive " role="tablist" style="background-color:#970C7B; border-color: #970C7B;">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#manifesto" role="tab" style="color:#fff;">CPDI Africa 2021</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#founder" role="tab" style="color:#fff;">CPDI Africa 2020</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#council" role="tab" style="color:#fff;">CPDI Africa 2017</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#pioneers" role="tab" style="color:#fff;">CPDI Africa 2015</a>
                </li>

            </ul><!-- Tab panes -->
            <div class="tab-content responsive">
                <div class="tab-pane active" id="manifesto" role="tabpanel">

                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                        <div class="col-md-5">


                          <!--Modal: Name-->
                          <div class="modal fade" id="modal2021" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">

                              <!--Content-->
                              <div class="modal-content">

                                <!--Body-->
                                <div class="modal-body mb-0 p-0">

                                  <img class="img-fluid z-depth-1" src="poster 1.jpg" alt="Picture" data-toggle="modal" data-target="#modal2021">

                                </div>

                                <!--Footer-->
                                <div class="modal-footer justify-content-center">

                                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                </div>

                              </div>
                              <!--/.Content-->

                            </div>
                          </div>
                          <!--Modal: Name-->
                          <img class="img-fluid z-depth-1" src="poster 1.jpg" alt="Picture" data-toggle="modal" data-target="#modal2021">

                          <!-- <img alt="CPDI" src="2021 CPDIA Heritage Competition.jpg" class="w-100"> -->
                        </div>
                            <div class="col-md-7">

                                <p class="h3">
                                The African Heritage Architecture <b> DESIGN BRIEF 2021</b> </P>

                                <!-- <p class="h3" style="color:red">Submission Deadline, October 31, 2021</p> -->
                                <P> The competition is created to celebrate and preserve the architectural identity of the Cultures of Africa and the Diaspora.
                                 Designed as the quintessential <b>Cultural Community Center</b>, the Competition Project served as the ultimate
example of sustainable design in any community in which it is built. The Objective
was to design a space where guests fall in love with the cultures and architectural
traditions of the African Diaspora, just by experiencing the beauty, comfort,
hospitality and heritage embedded in the masterpiece! Building upon the elements
of African traditional architecture highlighted in the Design Brief, the knowledge of
contemporary character and lifestyle of Africa's diverse continental and diaspora
Ethnic Groups, participating architects and collaborating design teams embarked on
a brave exercise in the ‘evolution and transformation’ of African architecture into a
Signature Style. All our competitions are defined according the CPDI Africa 5 Pillars
of Afrocentric Architecture: Culture, Aesthetics, Spirituality, Materials and
Community Engagement. Learn more from our courses on the Global Studio!</p>
                                <!-- <div class="col-md-12">
                                    <div class="d-flex justify-content-center">

                                    <a  target="_blank" href="/CPDI2021Competitionbrief.pdf" class="btn btn-primary  btn-sm" style="background-color:#970C7B; border-color: #970C7B; margin:10px"><b>Design Brief</b></a>

                                     <a  href="competition-registration-form?competition=21" class="btn btn-primary btn-sm" style="background-color:#970C7B; border-color: #970C7B; margin:10px"><b>Register</b></a>
                                    </div>
                                </div> -->
                                <div class="row">

                                <?php
                                        $year="2021";

                                        $result1 = selectContent($conn, "read_registered_competition_status", ['input_competition_year' => $year, 'bool_status' => 1]);
                                        // $result3 = selectApprovedCompetitionsByYear($year);
                                        if($result1)
                                        {
                                ?>
                                                    <!-- Grid column -->
                                                    <?php foreach ($result1 as $key => $value): ?>

                                                      <div class="col-lg-4 col-md-6 mb-4">

                                                      <!--Modal: Name-->
                                                      <div class="modal fade" id="modal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg" role="document">

                                                          <!--Content-->
                                                          <div class="modal-content">

                                                            <!--Body-->
                                                            <div class="modal-body mb-0 p-0">

                                                            <img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="Picture" data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>">

                                                            </div>

                                                            <!--Footer-->
                                                            <div class="modal-footer justify-content-center">

                                                              <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                                            </div>

                                                          </div>
                                                          <!--/.Content-->

                                                        </div>
                                                      </div>
                                                      <!--Modal: Name-->

                                                      <a><img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="video"
                                                          data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>"></a>

                                                      </div>
                                                    <?php endforeach; ?>
                                                    <!-- Grid column -->
                                <?php
                                        }
                                ?>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="founder" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3">
                        <div class="col-md-5">


                          <!--Modal: Name-->
                          <div class="modal fade" id="modal2020" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">

                              <!--Content-->

                          <!--Content-->
                          <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                              <img class="img-fluid z-depth-1" src="/cpdi/Slide4.JPG" alt="Picture" data-toggle="modal" data-target="#modal2020">

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center">

                              <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                          </div>
                          <!--/.Content-->

                        </div>
                      </div>
                      <!--Modal: Name-->
                      <img class="img-fluid z-depth-1" src="/cpdi/Slide4.JPG" alt="Picture" data-toggle="modal" data-target="#modal2020">

                          <!-- <img alt="CPDI" src="/cpdi/Slide4.JPG" class="w-100"> -->
                        </div>
                            <div class="col-md-7" style="color:#495057;">

                                <p class="h3">
                                The CPDI AFRICA INTERNSHIP <b>DESIGN BRIEF 2020</b></p><p> The <b>African Resort & Cultural Center</b> was designed to celebrate the cultures and identities of our Cohort of design Interns held over the Summer of 2020.
                                 The African Resort and Cultural Center welcomed guests and vacationers, introducing them to ingenuous hospitality of the community, and invited them to fall in love with the people through the art and architecture of their host community. Seven diverse African and African Diaspora cultures were celebrated by our Interns, earning them the first Graduate Certificates from the CPDI Africa Global Studio for African Centered Architecture!  Enjoy this Spotlight of their winning submissions of African Inspired Architecture!
                                </p>

                                          <!-- Grid row -->
                                          <div class="row">

                                          <?php
                                                  $years="2020";

                                                  // $result2 = selectApprovedCompetitionsByYear($year);
                                                  $result2 = selectContent($conn, "read_registered_competition_status", ['input_competition_year' => $years, 'bool_status' => 1]);
                                                  // var_dump($result2);
                                                  // die();
                                                  if($result2)
                                                  {
                                          ?>
                                                              <!-- Grid column -->
                                                              <?php foreach ($result2 as $key => $value): ?>
                                                              <div class="col-lg-4 col-md-6 mb-4">

                                                              <!--Modal: Name-->
                                                              <div class="modal fade" id="modal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">

                                                                  <!--Content-->
                                                                  <div class="modal-content">

                                                                    <!--Body-->
                                                                    <div class="modal-body mb-0 p-0">

                                                                    <img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="Picture" data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>">

                                                                    </div>

                                                                    <!--Footer-->
                                                                    <div class="modal-footer justify-content-center">

                                                                      <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                  </div>
                                                                  <!--/.Content-->

                                                                </div>
                                                              </div>
                                                              <!--Modal: Name-->

                                                                <a><img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="video"
                                                                    data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>"></a>

                                                              </div>
                                                                <?php endforeach; ?>
                                                              <!-- Grid column -->


                                          <?php

                                                  }


                                          ?>




                                            </div>
                                            <!-- Grid row -->
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="council" role="tabpanel">
                    <div class="container-fluid">
                        <div class="row mt-3">
                          <div class="col-md-5">


                            <!--Modal: Name-->
                            <div class="modal fade" id="modal2017" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg" role="document">

                                <!--Content-->

                            <!--Content-->
                            <div class="modal-content">

                              <!--Body-->
                              <div class="modal-body mb-0 p-0">

                                <img class="img-fluid z-depth-1" src="/cpdi/Slide3.JPG" alt="Picture" data-toggle="modal" data-target="#modal2017">

                              </div>

                              <!--Footer-->
                              <div class="modal-footer justify-content-center">

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                              </div>

                            </div>
                            <!--/.Content-->

                          </div>
                        </div>
                        <!--Modal: Name-->
                        <img class="img-fluid z-depth-1" src="/cpdi/Slide3.JPG" alt="Picture" data-toggle="modal" data-target="#modal2017">

                            <!-- <img alt="2017" src="/cpdi/Slide3.JPG" class="w-100"> -->
                          </div>
                              <div class="col-md-7" style="color:#495057;">
                                    <p class="h3">The CPDI AFRICA <b>DESIGN BRIEF 2017<br /></p><p> Option One & Option Two</b></p>
                                    <p>
                                    <b>The New African House: Option One </b><br />To design an innovative African home that successfully translates traditional residential architecture, in an urban setting; celebrates African inspired architectural aesthetes, in façade and form: and is built with sustainable, modernized, local materials.
                                    <br /><br /><b>The Iconic African Design</b><br />To design an iconic architectural monument that is culturally appropriate for Africa's emerging skylines and cityscapes; designed to be placed within  culturally appropriate contexts; and reflective of the functionality of African communal space and aesthetic iconography.

                                    </p>

                                              <!-- Grid row -->
                                              <div class="row">

                                              <?php
                                                      $year="2017";

                                                      $result3 = selectContent($conn, "read_registered_competition_status", ['input_competition_year' => $year, 'bool_status' => 1]);
                                                      // $result3 = selectApprovedCompetitionsByYear($year);
                                                      if($result3)
                                                      {
                                              ?>
                                                                  <!-- Grid column -->
                                                                  <?php foreach ($result3 as $key => $value): ?>

                                                                    <div class="col-lg-4 col-md-6 mb-4">

                                                                    <!--Modal: Name-->
                                                                    <div class="modal fade" id="modal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                      <div class="modal-dialog modal-lg" role="document">

                                                                        <!--Content-->
                                                                        <div class="modal-content">

                                                                          <!--Body-->
                                                                          <div class="modal-body mb-0 p-0">

                                                                          <img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="Picture" data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>">

                                                                          </div>

                                                                          <!--Footer-->
                                                                          <div class="modal-footer justify-content-center">

                                                                            <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                                                          </div>

                                                                        </div>
                                                                        <!--/.Content-->

                                                                      </div>
                                                                    </div>
                                                                    <!--Modal: Name-->

                                                                    <a><img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="video"
                                                                        data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>"></a>

                                                                    </div>
                                                                  <?php endforeach; ?>
                                                                  <!-- Grid column -->
                                              <?php
                                                      }
                                              ?>
                                              </div>
                                              <!-- Grid row -->
                                </div>
                        </div>
                    </div>
                </div>



                <div class="tab-pane" id="pioneers" role="tabpanel">
                    <div class="container-fluid" style="color:#495057;">
                        <div class="row mt-3">

                            <div class="col-md-5">


                              <!--Modal: Name-->
                              <div class="modal fade" id="modal2015" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">

                                  <!--Content-->

                              <!--Content-->
                              <div class="modal-content">

                                <!--Body-->
                                <div class="modal-body mb-0 p-0">

                                  <img class="img-fluid z-depth-1" src="/cpdi/Slide2.JPG" alt="Picture" data-toggle="modal" data-target="#modal2015">

                                </div>

                                <!--Footer-->
                                <div class="modal-footer justify-content-center">

                                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                </div>

                              </div>
                              <!--/.Content-->

                            </div>
                          </div>
                          <!--Modal: Name-->
                          <img class="img-fluid z-depth-1" src="/cpdi/Slide2.JPG" alt="Picture" data-toggle="modal" data-target="#modal2015">

                              <!-- <img alt="2015" src="/cpdi/Slide2.JPG" class="w-100"> -->
                            </div>
                                  <div class="col-md-7" style="color:#495057;">
                                        <p class="h3">The CPDI AFRICA <b>DESIGN BRIEF 2015</b></p>
                                        <p>
                                        <b>The Challenge: </b><br />To design an innovative residential home that reflects the comforts of contemporary urban lifestyle yet celebrates the essence of traditional African living. <br /><br />The challenge was to create a design concept for a small home, humble and dignified, reflecting the elements of African culture and aesthetics; explores the concept of the modern ‘African kitchen’ and pays homage to the African family courtyard; built of sustainable local materials, therefore affordable, and can easily be built by both formally trained and rural skilled laborers.
                                        </p>

                                                  <!-- Grid row -->
                                                  <div class="row">

                                                  <?php
                                                          $yea="2015";

                                                          $result4 = selectContent($conn, "read_registered_competition_status", ['input_competition_year' => $yea, 'bool_status' => 1]);
                                                          // $result3 = selectApprovedCompetitionsByYear($year);
                                                          if($result4)
                                                          {
                                                  ?>
                                                                      <?php foreach ($result4 as $key => $value): ?>
                                                                        <!-- Grid column -->
                                                                        <div class="col-lg-4 col-md-6 mb-4">

                                                                        <!--Modal: Name-->
                                                                        <div class="modal fade" id="modal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                          <div class="modal-dialog modal-lg" role="document">

                                                                            <!--Content-->
                                                                            <div class="modal-content">

                                                                              <!--Body-->
                                                                              <div class="modal-body mb-0 p-0">

                                                                              <img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="Picture" data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>">

                                                                              </div>

                                                                              <!--Footer-->
                                                                              <div class="modal-footer justify-content-center">

                                                                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                                                              </div>

                                                                            </div>
                                                                            <!--/.Content-->

                                                                          </div>
                                                                        </div>
                                                                        <!--Modal: Name-->

                                                                        <a><img class="img-fluid z-depth-1" src="/studentDesignMaterials/<?php echo $value['competition_design']; ?>" alt="video"
                                                                            data-toggle="modal" data-target="#modal<?php echo $value['id']; ?>"></a>

                                                                        </div>
                                                                      <?php endforeach; ?>
                                                                      <!-- Grid column -->
                                                  <?php

                                                          }
                                                  ?>
                                                  </div>
                                                  <!-- Grid row -->
                                    </div>
                            </div>




                        </div>
                    </div>
                </div>


            </div>
            </div>










        </div>

      </div>
    </section><!-- #services -->

<?php include "includes/footer.php" ?>
