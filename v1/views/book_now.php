<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');
	$course = urldecode($_GET['course']);


?>

<?php include "includes/header.php" ?>
    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <br /><br /><h3>Code Clubs</h3>
          <p>Please choose your session. <a href="clubs.php"><b>Back to Clubs</b></a> </p>
        </header>

        <div class="row">
<?php

		//if logged in display on products the the user has not bought
		if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
		{
			$result4 = selectUserByEmail($_SESSION['valid_user']);
			if($result4)
			{
				$num_result = $result4->num_rows;
				if($num_result > 0)
				{
					for ($i=0; $i<$num_result; $i++)
					{
						$row = $result4->fetch_assoc();
						$userId = $row['user_id'];
						$fname = $row['input_first_name'];
						$lname = $row['input_last_name'];
						$email = $row['input_email'];

					}
				 }
			}

			$result1 = selectProductsLoggedIn($course, $userId);
		}
		else
		{
			$result1 = selectProducts($course);
		}



		if($result1)
		{
			$num_result = $result1->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result1->fetch_assoc();
					$product_id = $row['id'];
					$name = $row['name'];
					$product_name = $row['product_name'];
					$briefDescription = $row['briefDescription'];
					$product_desc = $row['product_desc'];
					$product_amount = $row['product_amount'];
					$period = $row['period'];
					$startDate = $row['startDate'];
					$startDate = date("d-m-Y", strtotime($startDate));

					$endDate = $row['endDate'];
					$endDate = date("d-m-Y", strtotime($endDate));

					$time = $row['time'];

					$secondDate = $row['secondDate'];
					$secondDate = date("d-m-Y", strtotime($secondDate));

					$thirdDate = $row['thirdDate'];
					$thirdDate = date("d-m-Y", strtotime($thirdDate));

					$forthDate = $row['forthDate'];
          $forthDate = date("d-m-Y", strtotime($forthDate));

          $fifthDate = $row['fifthDate'];
          if (!empty($fifthDate))
          {
            $fifthDate = date("d-m-Y", strtotime($fifthDate));
          }


          $sixthDate = $row['sixthDate'];
          if (!empty($sixthDate))
          {
            $sixthDate = date("d-m-Y", strtotime($sixthDate));
          }


          $seventhDate = $row['seventhDate'];
          if (!empty($seventhDate))
          {
            $seventhDate = date("d-m-Y", strtotime($seventhDate));
          }


          $eighthDate = $row['eighthDate'];
          if (!empty($eighthDate))
          {
            $eighthDate = date("d-m-Y", strtotime($eighthDate));
          }


          $ninthDate = $row['ninthDate'];
          if (!empty($ninthDate))
          {
            $ninthDate = date("d-m-Y", strtotime($ninthDate));
          }


          $tenthDate = $row['tenthDate'];
          if (!empty($tenthDate))
          {
            $tenthDate = date("d-m-Y", strtotime($tenthDate));
          }


          $eleventhDate = $row['eleventhDate'];
          if (!empty($eleventhDate))
          {
            $eleventhDate = date("d-m-Y", strtotime($eleventhDate));
          }


          $twelfthDate = $row['twelfthDate'];
          if (!empty($twelfthDate))
          {
            $twelfthDate = date("d-m-Y", strtotime($twelfthDate));
          }


?>


					<div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
					<div class="box">
					  <div class="icon">
					  <?php if ($name == "SCRATCH")
						  {
							  echo '<img src="img/scratchsmall.svg" class="img-fluid" alt="Scratch">';
						  }
						  elseif ($name == "PYTHON"){
							  echo '<img src="img/pythonsmall.svg" class="img-fluid" alt="Python">';
						  }
						  elseif ($name == "HTML & CSS"){
							  echo '<img src="img/htmlcsssmall.svg" class="img-fluid" alt="HTML & CSS">';
						  }
						  elseif ($name == "TYPING CLASS"){
							  echo '<img src="img/typeless.svg" class="img-fluid" alt="Typing Class">';
						  }
					?>
					  </div>
            <?php $new_time = date("H:i", strtotime($time)); ?>
					  <h4 class="title"><?php echo "$name ($period @ $new_time)"; ?></h4>

					  <p class="description"><?php echo "$briefDescription"; ?><br /><br /> <b>Amount:</b> €<?php echo $product_amount; ?><br /> <b>Time:</b> <?php echo date("H:i", strtotime($time)); ?><br /><b>Days:</b><br /><?php echo $startDate; ?> <br /><?php echo $secondDate; ?> <br /><?php echo $thirdDate; ?> <br /><?php echo $forthDate; ?> <br />
          <?php
            if (!empty($fifthDate))
            {
              echo "$fifthDate <br />";
            }

            if (!empty($sixthDate))
            {
              echo "$sixthDate <br />";
            }

            if (!empty($seventhDate))
            {
              echo "$seventhDate <br />";
            }

            if (!empty($eighthDate))
            {
              echo "$eighthDate <br />";
            }

            if (!empty($ninthDate))
            {
              echo "$ninthDate <br />";
            }

            if (!empty($tenthDate))
            {
              echo "$tenthDate <br />";
            }

            if (!empty($eleventhDate))
            {
              echo "$eleventhDate <br />";
            }

            if (!empty($twelfthDate))
            {
              echo "$twelfthDate <br />";
            }
          ?>
            <?php echo $endDate; ?> <br /><br /><?php noOfSeatAvailable($product_id); ?></p>
					</div>
				  </div>
<?php
				}
			 }
			 else
			 {
				 echo "You may have already signed up for this code club. <br /><br /><a href='index.php'><b>Back to code clubs</b></a>";
			 }
		}
?>



        </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
