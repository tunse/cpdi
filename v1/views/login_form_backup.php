<?php

	session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');

	if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{
		header("Location: myaccount.php");
	}

?>
<?php include "includes/header.php" ?>

  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <br /><br /><h3>Login</h3>
        </header>

        <div class="row">



		      <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
              <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-light active">
                                <input type="radio" name="options" value="Administrator" autocomplete="off" checked> Administrator
                            </label>
                            <label class="btn btn-light">
                                <input type="radio" name="options" value="Professor" autocomplete="off"> Professor
                            </label>
                            <label class="btn btn-light">
                                <input type="radio" name="options" value="Student" autocomplete="off"> Student
                            </label>
                </div><br /><br />
					 <?php login_form(); ?>
          </div>


      </div>
    </section><!-- #services -->


  </main>


	<?php include "includes/footer.php" ?>
