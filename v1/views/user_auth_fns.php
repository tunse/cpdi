<?php

require_once('db_fns.php');

function selectCourses(){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_courses");
   if (!$result)
     return false;
   else
   return $result;

}

function selectMaterials(){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_course_materials");
   if (!$result)
     return false;
   else
   return $result;

}

function selectCourseMaterials($courseId){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_course_materials where course_id=$courseId");
   if (!$result)
     return false;
   else
   return $result;

}

function selectApprovedCompetetionsByLastName($lastName){

  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_registered_competition_status where input_last_name like '%{$lastName}%' AND bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectApprovedCompetetionsByFirstName($firstName){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_registered_competition_status where input_first_name like '%{$firstName}%' AND bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectApprovedCompetetionsByEthnicGroup($ethnicGroup){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_registered_competition_status where input_ethnic_group like '%{$ethnicGroup}%' AND bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectApprovedCompetetionsByCountry($country){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_registered_competition_status where input_country like '%{$country}%' AND bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}




function selectApprovedCompetitionsByYear($year){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_registered_competition_status where input_competition_year=$year AND bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}


function selectStudentCourseMaterials($userId, $courseId){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_student_course_materials where user_id=$userId and course_id=$courseId");
   if (!$result)
     return false;
   else
   return $result;

}

function selectStudentcompetitionMaterials($userId, $competition){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_competition_design_materials where user_id=$userId and competition_id=$competition");
   if (!$result)
     return false;
   else
   return $result;

}

function registeredcompetitionstatus($userId, $competition){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_registered_competition_status where user_id=$userId and competition_id=$competition");
   if (!$result)
     return false;
   else
   return $result;

}

function selectStudentCourseFeedback($userId, $courseId){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_course_feedback where student_id=$userId and course_id=$courseId");
   if (!$result)
     return false;
   else
   return $result;

}

function selectLiveCourses(){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_courses where input_live='Yes'");
   if (!$result)
     return false;
   else
   return $result;

}


function noOfSeatAvailable($product_id){
		  // connect to db
		  $conn1 = db_connect();

		  $result3 = $conn1->query("SELECT no_of_seats from product where product_id='$product_id'");
			if($result3)
			{
				$num_result3 = $result3->num_rows;
				if($num_result3 > 0)
				{
					for ($i=0; $i<$num_result3; $i++)
					{
						$row3 = $result3->fetch_assoc();
						$no_of_seats = $row3['no_of_seats'];
					}
				 }
			}

		  //check the number of confirmed seats
		  $result4 = $conn1->query("SELECT count(*) as total from read_product_order where product_id='$product_id' AND bool_status='1'");
			if($result4)
			{
				$num_result = $result4->num_rows;
				if($num_result > 0)
				{
					for ($i=0; $i<$num_result; $i++)
					{
						$row = $result4->fetch_assoc();
						$total = $row['total'];
					}
				 }
			}

			$remailingSeats = $no_of_seats - $total;

			echo "Remaining seats $remailingSeats <br />";

			if ($remailingSeats > 0)
			{
?>
				<br /><a href='registration_form_nl.php?product=<?php echo "$product_id"; ?>' class="btn btn-success"><b>Book now</b></a>
<?php
			}
			else
			{
				echo "<b style='color:red;'>SOLD OUT</b>";
			}

}



function noOfSeatAvailable_nl($product_id){
  // connect to db
  $conn1 = db_connect();

  $result3 = $conn1->query("SELECT no_of_seats from product where product_id='$product_id'");
  if($result3)
  {
    $num_result3 = $result3->num_rows;
    if($num_result3 > 0)
    {
      for ($i=0; $i<$num_result3; $i++)
      {
        $row3 = $result3->fetch_assoc();
        $no_of_seats = $row3['no_of_seats'];
      }
     }
  }

    //check the number of confirmed seats
  $result4 = $conn1->query("SELECT count(*) as total from read_product_order where product_id='$product_id' AND bool_status='1'");
  if($result4)
  {
    $num_result = $result4->num_rows;
    if($num_result > 0)
    {
      for ($i=0; $i<$num_result; $i++)
      {
        $row = $result4->fetch_assoc();
        $total = $row['total'];
      }
     }
  }

  $remailingSeats = $no_of_seats - $total;

  echo "Beschrikbare Plekken $remailingSeats <br />";

  if ($remailingSeats > 0)
  {
?>
    <br /><a href='registration_form_nl.php?product=<?php echo "$product_id"; ?>' class="btn btn-success"><b>Boek Nu</b></a>
<?php
  }
  else
  {
    echo "<b style='color:red;'>UITVERKOCHT</b>";
  }

}


function selectProductsLoggedIn($name, $user_id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from product where name='$name' AND product_id NOT IN (select product_id from product_order where status ='CONFIRMED' AND user_id='$user_id')");
   if (!$result)
     return false;
   else
   return $result;

}

function selectProductsLoggedIn_nl($name, $user_id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from product where name_nl='$name' AND product_id NOT IN (select product_id from product_order where status ='CONFIRMED' AND user_id='$user_id')");
   if (!$result)
     return false;
   else
   return $result;

}

function crossSelling($user_id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from product where product_id NOT IN (select product_id from product_order where status ='CONFIRMED' AND user_id='$user_id')");
   if (!$result)
     return false;
   else
   return $result;

}

function selectCourseOrder($id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_product_order where id='$id'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectCompetitionOrder($id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_competition_order where id='$id'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectUserClubs($userid){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_product_order where user_id='$userid' and bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectUserCompetitions($userid){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_competition_order where user_id='$userid' and bool_status=1");
   if (!$result)
     return false;
   else
   return $result;

}

function selectProductById($id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from product where product_id='$id'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectCourse($id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_courses where id ='$id'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectCompetitions(){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_competitions");
   if (!$result)
     return false;
   else
   return $result;

}

function selectCompetition($id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from panel_competitions where id ='$id'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectProfessorCourses($prof){
  // connect to db
  $conn1 = db_connect();

  //select Professor courses
  $result = $conn1->query("select * from read_course_professors where user_id ='$prof'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectProfessorCoursesByCourse($course){
  // connect to db
  $conn1 = db_connect();

  //select Professor courses
  $result = $conn1->query("select * from read_course_professors where course_id ='$course'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectProfessorCompetitions($prof){
  // connect to db
  $conn1 = db_connect();

  //select Professor courses
  $result = $conn1->query("select * from read_competition_order where user_id ='$prof' and bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectRegisteredCompetitions(){
  // connect to db
  $conn1 = db_connect();

  //select Professor courses
  $result = $conn1->query("select * from read_competition_order  ORDER BY bool_status DESC");
   if (!$result)
     return false;
   else
   return $result;

}

function selectUserByEmail($email){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_users where input_email='$email'");
   if (!$result)
     return false;
   else
   return $result;

}


function selectProfessor(){
  // connect to db
  $conn1 = db_connect();


  $result = $conn1->query("select * from read_users where input_professor_status='YES'");
   if (!$result)
     return false;
   else
   return $result;

}

function checkUserHasPurchaseProduct($userid, $prodid){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_product_order where user_id='$userid' and product_id='$prodid' and bool_status='1'");
   if (!$result)
     return false;
   else
   return $result;

}

function selectUserById($id){
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_users where id ='$id'");
   if (!$result)
     return false;
   else
   return $result;

}

function registercompetition($email, $courseId)
// register new person with db
// return true or error message
{
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_users where input_email='$email'");
  if (!$result)
    throw new Exception('Could not execute query');
  if ($result->num_rows>0) {
    display_login_form_competition($courseId, $email);
	throw new Exception('That email already exists - Try and login.');

  }

}

function register($email, $courseId)
// register new person with db
// return true or error message
{
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_users where input_email='$email'");
  if (!$result)
    throw new Exception('Could not execute query');
  if ($result->num_rows>0) {
    display_login_form($courseId, $email);
	throw new Exception('That email already exists - Try and login.');

  }

}

function registerprofessor($email, $courseId)
// register new person with db
// return true or error message
{
  // connect to db
  $conn1 = db_connect();

  //check if username is unique
  $result = $conn1->query("select * from read_users where input_email='$email'");
  if (!$result)
    throw new Exception('Could not execute query');
  if ($result->num_rows>0) {

  throw new Exception('That email already exists - Try and login.');

  }

}

function login($username, $password)
// check username and password with db
// if yes, return true
// else throw exception
{
  // connect to db
  $conn1 = db_connect();

  // check if username is unique
  $result = $conn1->query("select * from read_users
                         where input_email='$username'
                         and input_password = sha1('$password')");
  if (!$result)
     throw new Exception('Could not log you in.');

  if ($result->num_rows > 0)
     return true;
  else
     throw new Exception('Could not log you in.');
}

function check_valid_user()
// see if somebody is logged in and notify them if not
{
  if (isset($_SESSION['valid_user']))
  {
      echo 'Logged in as '.$_SESSION['valid_user'].'.';
      echo '<br />';
  }
  else
  {
     // they are not logged in
     do_html_heading('Problem:');
     echo 'You are not logged in.<br />';
     do_html_url('signup', 'Login');
     do_html_footer();
     exit;
  }
}

function change_password($email, $old_password, $new_password)
// change password for username/old_password to new_password
// return true or false
{
  // if the old password is right
  // change their password to new_password and return true
  // else throw an exception
  login($username, $old_password);
  $conn1 = db_connect();
  $result = $conn1->query( "update read_users
                            set input_password = sha1('$new_password')
                            where input_email = '$email'");
  if (!$result)
    throw new Exception('Password could not be changed.');
  else
    return true;  // changed successfully
}

function get_random_word($min_length, $max_length)
// grab a random word from dictionary between the two lengths
// and return it
{
   // generate a random word
  $word = '';
  // remember to change this path to suit your system
  $dictionary = '/usr/dict/words';  // the ispell dictionary
  $fp = @fopen($dictionary, 'r');
  if(!$fp)
    return false;
  $size = filesize($dictionary);

  // go to a random location in dictionary
  srand ((double) microtime() * 1000000);
  $rand_location = rand(0, $size);
  fseek($fp, $rand_location);

  // get the next whole word of the right length in the file
  while (strlen($word)< $min_length || strlen($word)>$max_length || strstr($word, "'"))
  {
     if (feof($fp))
        fseek($fp, 0);        // if at end, go to start
     $word = fgets($fp, 80);  // skip first word as it could be partial
     $word = fgets($fp, 80);  // the potential password
  };
  $word=trim($word); // trim the trailing \n from fgets
  return $word;
}

function reset_password($email)
// set password for username to a random value
// return the new password or false on failure
{
  // get a random dictionary word b/w 6 and 13 chars in length
  $new_password = get_random_word(6, 13);

  if($new_password==false)
    throw new Exception('Could not generate new password.');
  // add a number  between 0 and 999 to it
  // to make it a slightly better password
  srand ((double) microtime() * 1000000);
  $rand_number = rand(0, 999);
  $new_password .= $rand_number;

  // set user's password to this in database or return false
  $conn1 = db_connect();
  $result = $conn1->query( "update user
                          set passwd = sha1('$new_password')
                          where input_email = '$email'");
  if (!$result)
    throw new Exception('Could not change password.');  // not changed
  else
    return $new_password;  // changed successfully
}

function notify_password($email, $password)
// notify the user that their password has been changed
{
    $conn1 = db_connect();
    $result = $conn1->query("select input_email from user
                            where input_email='$email'");
    if (!$result)
    {
      throw new Exception('Could not find email address.');
    }
    else if ($result->num_rows==0)
    {
      throw new Exception('Could not find email address.');   // email not in db
    }
    else
    {
      $row = $result->fetch_object();
      $email = $row->email;
      $from = "From: support@3dcodeclub.com \r\n";
      $mesg = "Your African Centered Architecture password has been changed to $password \r\n"
              ."Please change it next time you log in. \r\n";


      if (mail($email, 'African Centered Architecture login information', $mesg, $from))
        return true;
      else
        throw new Exception('Could not send email.');
    }
}

?>
