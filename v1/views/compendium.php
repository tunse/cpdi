<?php $page_title = "Compendium" ?>
<?php include "includes/header.php" ?>


<section id="services" class="section-bg">
    <div class="container">

      <header class="section-header">


      </header>

      <div class="row">
      <div class="container mt-4"><br /><br />




                  <div class="container-fluid">
                      <div class="row mt-3" style="color:#495057;">
                      <div class="col-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                          <div class="col-9">
                              <p class="h3"><b>African Heritage Architecture Compendium</b> <br /> Winning Designs from our CPDI Africa Competitions</p>

                              <p>
                              Welcome to the African Heritage Architecture Compendium, the celebration of contemporary vernacular inspired architecture! Our database comprises of architecture that preserves traditional design philosophies, translates these indigenous philosophies into new architectural languages, and spotlights the architects that created our competitions winning masterpieces!
                              </p>
                              <p>
                              Our African Centered Masterpieces are created to inspire conceptual designs for students and professionals who seek to translate and incorporate African architectural design philosophies into their projects.

                              </p>
                              <p>
                                  <b>Search our Compendium by Country, Ethnic Group or Name of the Architect.</b>
                              </p>
                              <p>
                                  <form action="compendiumsearch" method="post">
                                      <div class="form-group">
                                          <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                              <label class="btn btn-secondary">
                                                  <input type="radio" name="searchterm" autocomplete="off" value="Country"> Country
                                              </label>
                                              <label class="btn btn-secondary active">
                                                  <input type="radio" name="searchterm" autocomplete="off" checked value="Ethnic"> Ethnic Group
                                              </label>
                                              <label class="btn btn-secondary">
                                                  <input type="radio" name="searchterm" autocomplete="off" value="Architect"> Name of Architect
                                              </label>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <input type="text" class="form-control" name="value" id="url" placeholder="" required="required">
                                      </div>
                                      <div class="form-group">
                                      <button type="submit" class="btn btn-success btn-sm" style="background-color:#970C7B; border-color: #970C7B;">Search</button>
                                      </div>

                                  </form>
                              </p>
                          </div>

                          <div class="col-3"> </div>
                          <div class="col-9">








                          </div>
                      </div>
                  </div>




          </div>










      </div>

    </div>
  </section><!-- #services -->
<?php include "includes/footer.php" ?>
