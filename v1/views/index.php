<?php
	session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>African Centered Architecture</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <!-- <link href="img/favicon.png" rel="icon"> -->
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">


</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <a href="index.php" class="scrollto">African Centered Architecture</a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php display_menu_with_links(); ?>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <a href="courses.php"><img src="img/CPDI.svg" alt="" class="img-fluid"></a>
      </div>

      <div class="intro-info">
        <h2>The Community Planning & Design Initiative Africa</h2>
        <div>
          <a href="https://cpdiafrica.blogspot.com/p/register-your-interest.html" target="_blank" class="btn-get-started scrollto">Get Involved</a>
          <a href="https://www.gofundme.com/f/cpdi-africa-global-studio" target="_blank" class="btn-services scrollto">Donate</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">



      <div class="row about-extra">
          <div class="col-lg-6" data-aos="fade-right">
          <h4>The Community Planning & Design Initiative Africa (CPDI Africa) Promotes the Development of New Architectural Languages for the African Diaspora that are Culturally and Environmentally Sustainable.  </h4>
          </div>
          <div class="col-lg-6 pt-5 pt-lg-0" data-aos="fade-left">

            <p>
            Believing the development of Africa's built environment should be as it has always been, built as a collaborative effort between the community members and designated master builders, CDPI Africa engages participation from the design community in Africa and the Diaspora at large, for the accomplishment of its vision. CPDI Africa is culture-inspired, research-based and is committed to teaching a new pedagogy in African centered architecture built upon a new culture of afrocentric design philosophies.
            </p>
          </div>
        </div>



      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3 style="font-size: 32px;">TEACHING A NEW PEDAGOGY IN AFRICAN DIASPORA ARCHITECTURE </h3>
        </header>

        <div class="row">

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/1.png')">

              <a href="aboutus.php"><h4 class="title" style="font-size: 25px; text-align: center;">About <br />CPDI Africa</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/2.png')">

              <a href="courses.php"><h4 class="title" style="font-size: 25px; text-align: center;">Global<br /> Studio </h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/3.png')">

              <a href="competitions.php"><h4 class="title" style="font-size: 25px; text-align: center;">Architecture<br /> Competitions</h4>

              </a>
            </div>
          </div>


          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/4.png')">

              <a href="compendium.php"><h4 class="title" style="font-size: 25px; text-align: center;">Heritage <br />Compendium</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/5.png')">
             <!-- <div class="icon"><img src="img/scratchsmall.svg" class="img-fluid" alt="Scratch"></div>-->
              <a href="services.php"><h4 class="title" style="font-size: 25px; text-align: center;">Design Services <br />& Programs</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/6.png')">

              <a href="resources.php"><h4 class="title" style="font-size: 25px; text-align: center;">Publications <br />& Merchandise</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/7.png')">

              <a href="connect.php"><h4 class="title" style="font-size: 25px; text-align: center;">Connect With<br /> CPDI Africa</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/8.png')">

              <a href="gallery.php"><h4 class="title" style="font-size: 25px; text-align: center;">Photo <br /> Gallery</h4>

              </a>
            </div>
          </div>

          <div class="col-lg-4 mb-4" data-wow-duration="1.4s">
            <div class="box" style="background-image: url('img/9.png')">

              <a href="http://www.cpdiafrica.com" target="_blank"><h4 class="title" style="font-size: 25px; text-align: center;">CPDI Africa <br />Archives </h4>

              </a>
            </div>
          </div>


          <header class="section-header">
          <h3 style="font-size: 32px;">WE PRACTICE <b>SANKOFA.</b> WE UPHOLD <b>UBUNTU.</b> WE PIONEER <b>AFROFUTURISM.</b> </h3>
          </header>





        </div>

      </div>
    </section><!-- #services -->



    <!--==========================
      Clients Section
    ============================
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Testimonials</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">

              <div class="testimonial-item">
                <img src="img/testimonial-1.jpg" class="testimonial-img" alt="">
                <h3>Paul's father</h3>

                <p>
                  My child found the SCRATCH club fun to learn to create games and meet new friends
                </p>
              </div>



            </div>

          </div>
        </div>


      </div>
    </section><!-- #testimonials -->





  </main>

  <?php display_full_footer(); ?>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
