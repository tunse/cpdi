<?php
	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');
		$userId = $_SESSION['valid_user'];
    $course=@$_GET['course'];

    if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
	{


		$result4 = selectUserByEmail($_SESSION['valid_user']);

		if($result4)
		{
			$num_result = $result4->num_rows;
			if($num_result > 0)
			{
				for ($i=0; $i<$num_result; $i++)
				{
					$row = $result4->fetch_assoc();
					$userId = $row['id'];
					$fname = $row['input_first_name'];
					$lname = $row['input_last_name'];

				}
			 }
		}
	}



?>


<?php include "includes/header.php" ?>
    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <h3><br />Student Competition Design Materials</h3>
        </header>

        <div class="row">

            <?php
                $result1 = selectStudentcompetitionMaterials($userId, $course);
                if($result1)
                {
                    $num_result = $result1->num_rows;
                    if($num_result > 0)
                    {
                        for ($i=0; $i<$num_result; $i++)
                        {
                            $row = $result1->fetch_assoc();
                            $name = $row['input_name'];
                            $courseDocument = $row['image_1'];
                            $student_course_material_id = $row['id'];
														$hid = $row['hash_id'];
                            $description = $row['input_online_drive'];
                            $courseId = $row['competition_id'];



                            $result2 = selectCompetition($courseId);
                            if($result2)
                            {
                                $num_result2 = $result2->num_rows;
                                if($num_result2 > 0)
                                {
                                    for ($c=0; $c<$num_result2; $c++)
                                    {
                                        $row2 = $result2->fetch_assoc();
                                        $courseName = $row2['input_name'];
                                        $year = $row2['input_year'];
                                    }
                                }
                            }




?>

                            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                                <div class="box">

                                <h4 class="title"><?php echo "$courseName $year" ?></h4>
                                <p class="description"><?php echo "$name"; ?> <br />

                                  <?php echo "$description"; ?><br /><br />
                                  <a href="downloaddesignfile?file=<?php echo $courseDocument; ?>" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Download File</a>
                                  <a href="myaccount" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to my account</a><br /><br />

																	<?php if($row2['input_live'] == "Yes"){ ?>
																	<a href="edit-design-material?material=<?php echo "$student_course_material_id&hid=$hid&doc=$courseDocument&course=$course"; ?>" class="btn btn-success btn-sm active" role="button" aria-pressed="true">Edit Design Material</a>

																<?php }?>

																	<?php if($row2['input_live'] == "Yes"){ ?>
																	<a href="deleteStudentDesign?material=<?php echo "$student_course_material_id&hid=$hid&doc=$courseDocument&course=$course"; ?>" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">Delete Design Material</a>

																<?php }?>



                                </p>
                                </div>
                            </div>
<?php
                        }
                    }
                    else
                    {
                      echo "<p style='color:#495057;'> You have not uploaded any design materials</p> <br />";
?>
                      <p><br /><a href="upload-design-material?competition=<?php echo $course; ?>" class="btn btn-success btn-sm"><b>Upload design material</b></a>
<?php
                      echo '<a href="myaccount" class="btn btn-light btn-sm"><b>Back to my account</b></a></p>';

                    }
                }
            ?>
        </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
