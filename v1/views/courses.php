<?php $page_title = "Courses" ?>
<?php 

header("Location: https://global-studio.cpdiafrica.org"); die;

include "includes/header.php" ?>

<section id="services" class="section-bg">
  <div class="container">

    <header class="section-header">


    </header>

    <div class="row">
      <div class="container mt-4"><br /><br />

        <ul class="nav nav-tabs responsive " role="tablist" style="background-color:#970C7B; border-color: #970C7B;">
          <li class="nav-item">
            <a class="nav-link <?php if(isset($_GET['active'])){ echo " ";}else{ echo "active";} ?>" style="color:#fff;" data-toggle="tab" href="#manifesto" role="tab">Global Studio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" style="color:#fff;" href="#founder" role="tab">Program Structure</a>
            </li>
            <li class="nav-item">

              <a class="nav-link <?php if(isset($_GET['active'])){ echo "active";} ?>" data-toggle="tab" style="color:#fff;" href="#council" role="tab">Courses Offered</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" style="color:#fff;" href="#scholarship" role="tab">Scholarships and Grants</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" style="color:#fff;" href="#appointment" role="tab">Academic Appointments</a>
              </li>

            </ul><!-- Tab panes -->

            <div class="container-fluid">
              <div class="row mt-3" style="color:#495057;" style="padding:0;">

                <div class="col-md-12" style="padding:0;" >


                  <div class="tab-content responsive" style="padding:10px 20px 0 0;">
                    <div class="tab-pane <?php if(isset($_GET['active'])){ echo " ";}else{ echo "active";} ?>" id="manifesto" role="tabpanel">
                      <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">

                          <?php include "includes/gb.php" ?>



                          <div class="col-md-9">
                            <div class="col-md-12">

                              <p class="h3">
                                <b>CPDI Africa GLOBAL STUDIO</b> <br /> An Academic Platform for Teaching a New Pedagogy in African Centered Architecture.
                              </p>
                              <p>
                                The <strong> CPDI Africa Global Studio for African Centered Architecture</strong> provides architects and designers an African centered academic platform for learning technical solutions for problem solving in their communities, while preserving culture and heritage through design. The global studio is organized by CPDI Africa - the Community Planning and Design Initiative, Africa.
                              </p>
                              <p>
                                <strong>Our Global Studio Faculty</strong>  are leading professors and architects, renowned for their pedagogy in teaching African centered architecture. Tenured at distinguished universities around the globe, our professors understand architecture is not separate from the lexicon of creative arts, but an integral part of the full human experience. CPDI Africa faculty is interdisciplinary, multidisciplinary, and represent a diversity of world cultures, languages and heritage.
                              </p>
                              <p>
                                <strong> The CPDI Africa Global Studio</strong> for African Centered Architecture is designed on a virtual platform, and accessible to architects and students in any part of the world. Courses are offered in several categories - Survey, Theory, Design Studio and Practicals - and may be audited or taken for the certificate designation. Practicals are taught in person during the Study Abroad summer sessions in Africa. Course fees range between $250 and $1,000, with merit-based scholarships offered to students in art, architecture, design, planning, history, real estate and construction.
                              </p>
                              <p>
                              <strong> CPDI Africa Global Studio</strong> teaches students, teachers and professionals design philosophies not found in traditional architecture education, for use in academic scholarship, project development and policy making. For more, visit us at <a href="https://cpdiafrica.org" style="color:blue">cpdiafrica.org</a>  and our social media platforms.                               </p>
                            </div>
                            <div class="col-md-12">

                              <br /><p><b>OUR MISSION</b></p>
                              <p>
                                <ul>
                                  <li>
                                    To provide an academic institution for teaching a new pedagogy in African centered architecture and foster the development of architectural languages that are culturally and environmentally sustainable.
                                  </li>
                                  <li>
                                    To fast track the evolution and transformation of the Pan African built environment through education, training architects to become expert scientist, designers, activists and true problem solvers in their local and global communities.
                                  </li>
                                </ul>

                              </p>

                              <p><b>OUR TARGET COMMUNITY</b></p>
                              <p>
                                <ul>
                                  <li>
                                    Students seeking design knowledge not offered in routine curriculums.
                                  </li>
                                  <li>
                                    Professors seeking content for teaching African centered architecture.
                                  </li>
                                  <li>
                                    Professionals seeking culturally appropriate concepts for clients’ projects.
                                  </li>
                                  <li>
                                    Policy makers seeking sustainable policies for urban and rural development.
                                  </li>
                                  <li>
                                    Architecture connoisseurs seeking inspiration for heritage inspired projects.
                                  </li>
                                </ul>

                              </p>

                            </div>
                          </div>


                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="founder" role="tabpanel">
                      <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                          <?php //include 'includes/gb.php' ?>

                          <!-- <div class="col-md-3">
                            <img alt="CPDI" src="/cpdi/CPDI-LOGO1.JPG" class="w-100"><br><br><br><br>

                          <div class="text-center">

                            <a href="https://form.jotform.com/202998610849065" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Teaching Appointments </a><br><br>
                            <a href="https://cpdiafrica.blogspot.com/p/global-studio-scholarship-2021.html" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Scholarship Application </a>
                          </div>
                          </div> -->

                          <div class="col-md-12">

                            <p class="h3" ><b>GLOBAL STUDIO PROGRAM STRUCTURE</b> <br>

                            CPDI Africa GSACA Certificate in African Centered Architecture</p>
                            <p>Scholars completing a predetermined number of our Face to Face / On Demand or Summer Residency programs available Diaspora wide, are eligible for the <b>CPDI Africa GSACA Certificate in African Centered Architecture.</b>  Certificates are offered upon successful completion of courses in numbers of three, six or nine courses. Contact our <b>Registrar</b> at <a href="mailto:design@cpdiafrica.org" style="color:blue"> design@cpdiafrica.org</a> for details on our offered Certificate designations.
                               Find below a sampling of our classes. New courses are updated monthly on the <b>Courses Offered Tab.</b> </p>
                            <br>

                            <!-- <img src="/cpdi/0007.jpg" alt="" width="100%">
                            <img src="/cpdi/0010.jpg" alt="" width="100%">
                            <img src="/cpdi/0012.jpg" alt="" width="100%">
                            <img src="/cpdi/0014.jpg" alt="" width="100%">
                            <img src="/cpdi/0016.jpg" alt="" width="100%">
                            <img src="/cpdi/0018.jpg" alt="" width="100%">
                            <img src="/cpdi/0020.jpg" alt="" width="100%">
                            <img src="/cpdi/0022.jpg" alt="" width="100%">
                            <img src="/cpdi/0024.jpg" alt="" width="100%">
                            <img src="/cpdi/0026.jpg" alt="" width="100%">
                            <img src="/cpdi/0028.jpg" alt="" width="100%">
                            <img src="/cpdi/0029.jpg" alt="" width="100%">
                            <img src="/cpdi/0030.jpg" alt="" width="100%">
                            <img src="/cpdi/0031.jpg" alt="" width="100%"> -->

                            <img src="https://imgpile.com/images/NhAHRw.jpg" alt="NhAHRw.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhA883.jpg" alt="NhA883.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAV6l.jpg" alt="NhAV6l.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAocF.jpg" alt="NhAocF.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAqBi.jpg" alt="NhAqBi.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAAuu.jpg" alt="NhAAuu.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhALMk.jpg" alt="NhALMk.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAOAM.jpg" alt="NhAOAM.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAkj4.jpg" alt="NhAkj4.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAWC2.jpg" alt="NhAWC2.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhABVG.jpg" alt="NhABVG.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAF6a.jpg" alt="NhAF6a.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAgRX.jpg" alt="NhAgRX.jpg" border="0" width="100%">
                            <img src="https://imgpile.com/images/NhAvfh.jpg" alt="NhAvfh.jpg" border="0" width="100%">

                            <!-- <p>
                              <b>GLOBAL STUDIO Category of Courses</b><br />
                              <ul>
                                <li>
                                  <b>SURVEY COURSES</b><br />
                                  Our Survey Courses are self guided downloadable videos, designed to introduce scholars to history and design philosophies in African architecture.
                                </li>
                                <li>
                                  <b>HISTORY & CRITICAL THEORY COURSES</b><br />
                                  Our History and Theory courses meet virtually and introduce scholars to the evolution and transformation of African architectural languages in the built environment, from antiquity to rural and urban landscapes in Africa and the Diaspora.
                                </li>
                                <li>
                                  <b>DESIGN STUDIO COURSES</b><br />
                                  Our Studio courses engage scholars in developing African inspired conceptual prototypes that respond to culture, aesthetics and technology rooted in African worldviews for sustainable living. Scholars meet virtually and produce architecture in response to our African centered Design Briefs.
                                </li>
                              </ul>
                            </p> -->
                          </div>


                        </div>
                      </div>
                    </div>
                    <div class="tab-pane <?php if(isset($_GET['active'])){ echo "active";} ?>" id="council" role="tabpanel">
                      <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                          <?php include 'includes/gb.php'; ?>
                          <div class="col-md-9" style="padding:0;">


                            <?php
                            // $result1 = selectLiveCourses();
                            $result = selectContent($conn, "panel_courses", ['input_live' => "Yes"]);
                            // if($result)
                            // {

                            ?>

                            <?php foreach ($result as $key => $value): ?>
                              <div class="box" style="color: #fff; width:100%; padding:30px 10px;">

                                <h4 class="title"><?php echo $value['input_course_name'] ?></h4>
                                <p class="description" style=" width:auto;"><?php echo strip_tags($value['text_course_description']); ?> <br /><a href="/course-details?course=<?php echo $value['id']; ?>" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>View Course Details</b></a></p>
                              </div>
                            <?php endforeach; ?>
                          </div>

                        </div>



                      </div>
                    </div>
                    <div class="tab-pane" id="scholarship" role="tabpanel">
                      <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">




                          <!-- <div class="col-md-4">
                            <div class="modal fade" id="modal2021" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg" role="document">

                                Content
                                <div class="modal-content">

                                  Body
                                  <div class="modal-body mb-0 p-0">

                                    <img class="img-fluid z-depth-1" src="/scholarship.jpg" alt="Picture" data-toggle="modal" data-target="#modal2021">

                                  </div>

                                  Footer
                                  <div class="modal-footer justify-content-center">

                                    <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                  </div>

                                </div>
                                /.Content

                              </div>
                            </div>
                            Modal: Name
                            <img class="img-fluid z-depth-1" src="/scholarship.jpg" alt="Picture" data-toggle="modal" data-target="#modal2021">
                            <img alt="CPDI" src="/cpdi/CPDI-LOGO1.JPG" class="w-100">
                            <br><br><br><br>

                            <div class="text-center">
                            <a href="https://form.jotform.com/202998610849065" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Teaching Appointments </a><br><br>
                            <a href="https://cpdiafrica.blogspot.com/p/global-studio-scholarship-2021.html" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Scholarship Application </a>
                          </div>
                        </div> -->

                        <div class="col-md-12">

                          <p class="h3">
                            <b>GLOBAL STUDIO – SCHOLARSHIPS & GRANTS</b><br />
                            CPDI Africa Global Studio for African Centered Architecture. <br><br>
                          </p>
                          <a href="https://cpdiafrica.blogspot.com/p/global-studio-scholarship-2021.html" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Scholarship Application </a><br><br>
                            <p>
                            Welcome to the opportunity to learn everything they did not teach you in Design School! Have you often thought the profession of architecture was not for you because the courses never seemed to relate to your community, history or cultural background?  Or was it that the cost for design school, computer software or building materials were too expensive for you to easily afford?  Well, historically, the cost of design education has been one of many deterrents to creating equity in the profession.

                            To combat this divisive method, CPDI Africa is committed to welcoming all prospective students no matter their financial capacity to the Global Studio. We are providing need-based scholarships to current students who are looking to augment their education with history, heritage, and the priceless knowledge of Africa’s contribution to the global built environment.

                            Our scholarships and grants are offered every session, and each session is targeted to a different demographic of students, to reach those most in need.

                            We look forward to supporting your search for knowledge and mastering Africa's contributions to the world of architecture.  Apply by clicking the advert and completing your application for a CPDI Africa educational scholarship!
                          </p>
                        </div>


                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="appointment" role="tabpanel">
                    <div class="container-fluid">
                      <div class="row mt-3" style="color:#495057;">


                        <div class="col-md-4">
                          <div class="modal fade" id="modal20211" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">

                              <!--Content-->
                              <div class="modal-content">

                                <!--Body-->
                                <div class="modal-body mb-0 p-0">

                                  <img class="img-fluid z-depth-1" src="/appointment.jpg" alt="Picture" data-toggle="modal" data-target="#modal2021">

                                </div>

                                <!--Footer-->
                                <div class="modal-footer justify-content-center">

                                  <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                                </div>

                              </div>
                              <!--/.Content-->

                            </div>
                          </div>
                          <!--Modal: Name-->
                          <h6 style="font-weight:bold">Click to view</h6>
                          <img class="img-fluid z-depth-1" src="/appointment.jpg" alt="Picture" data-toggle="modal" data-target="#modal20211">
                          <!-- <img alt="CPDI" src="/cpdi/CPDI-LOGO1.JPG" class="w-100"> -->
                          <br><br><br><br>

                          <!-- <div class="text-center">
                          <a href="https://form.jotform.com/202998610849065" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Teaching Appointments </a><br><br>
                          <a href="https://cpdiafrica.blogspot.com/p/global-studio-scholarship-2021.html" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Scholarship Application </a>
                        </div> -->
                      </div>
                      <div class="col-md-8">

                        <p class="h3">
                          <b>Global Studio - Academic Appointment</b><br />
CPDI Africa Global Studio for African Centered Architecture.
                        </p>
                        <a href="https://form.jotform.com/202998610849065" class="btn btn-sm" style="background: #EC1F25; color: #fff">Click for <br> Teaching Appointments </a><br><br>
                          <!-- <p>

                           <br><br> -->
<p>
                          <b>The Community Planning & Design Initiative Africa (CPDI Africa)</b> is a research-based organization created to develop new African inspired architectural languages that are culturally and environmentally sustainable.  Our <b>Global Studio</b> is an academic platform for teaching the evolution and transformation of African centered design philosophies to students and professionals seeking to master these new languages in their educational and professional practices. The <b>Global Studio</b> is engaging qualified professionals in this area of scholarship, from multidisciplinary disciplines in the built environment, to lecture critical theory and design studio courses in our virtual studio. Our courses are tuition based and professors earn an honorarium for teaching. We appreciate your interest and will respond to your inquiry upon your completion of this application.
                        </p>
                      </div>


                    </div>
                  </div>
                </div>



              </div>
            </div>
          </div>
        </div>


      </div>










    </div>

  </div>
</section><!-- #services -->
<?php include "includes/footer.php" ?>
