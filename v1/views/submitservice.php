<?php
	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');



    //create short variable names
	  $email=@$_POST['email'];
	  $first_name=@$_POST['first_name'];
	  $last_name=@$_POST['last_name'];


	  $tel=@$_POST['tel'];


	  $action=@$_POST['action'];
	  $description=@$_POST['description'];





?>

<?php include "includes/header.php" ?>

    <!--==========================
      Services Section
    ============================-->



    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"><br /><br />




                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                            <div class="col-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                            <div class="col-9">
<?php
                                    if ($action=="workshop")
                                    {

                                        echo '<p class="h3">Schedule a design Workshop</p>';
                                        echo '<p>Lectures, Tours & Events</p>';
                                    }
                                    elseif ($action=="exhibitions")
                                    {
                                        echo '<p class="h3">Schedule an Architecture Exhibition</p>';
                                        echo '<p>Booking & Reservations</p>';

                                    }
                                    elseif ($action=="design")
                                    {
                                        echo '<p class="h3">Schedule a Design Service appointment</p>';
                                        echo '<p>Hire an Afrocentric Architect</p>';

                                    }


                                    try
                                    {
                                        // check forms filled in
                                        if (!filled_out($_POST))
                                        {
                                        throw new Exception('You have not filled the form out correctly - please go back'
                                            .' and try again.');
                                        }

                                        // email address not valid
                                        if (!valid_email($email))
                                        {
                                        throw new Exception('That is not a valid email address.  Please go back '
                                                            .' and try again.');
                                        }
                                    }
                                    catch (Exception $e)
                                    {
                                       echo $e->getMessage();
?>
                                       <a href="services" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>Back to services </b></a>
<?php
                                       exit;
                                    }


                                    $to      = $email;
                                    $subject = 'CPDI Africa';
                                    $message = "Dear $first_name $last_name, \r\n \r\n Thank you for your submitting your request for $action \r\n \r\n. \r\n \r\n Kind regards, \r\n CPDI Africa";
                                    $headers = 'From:design@southernsahara.com' . "\r\n" .
                                        'Reply-To: design@southernsahara.com' . "\r\n" .
                                        'X-Mailer: PHP/' . phpversion();

                                    mail($to, $subject, $message, $headers);


                                    //send request to CPDI
                                    $to      = 'design@southernsahara.com';
                                    $subject = 'CPDI Africa Service request';
                                    $message = "Dear CPDI Admin, \r\n \r\n A request for a $action has been made by $first_name $last_name. \r\n \r\n. $description \r\n \r\n Kind regards, \r\n CPDI Africa  ";
                                    $headers = 'From: design@cpdiafrica.org' . "\r\n" .
                                        'Reply-To: design@cpdiafrica.org' . "\r\n" .
                                        'X-Mailer: PHP/' . phpversion();

                                    mail($to, $subject, $message, $headers);

?>
                                    Thank you for submitting your request
                            </div>


                        </div>
                    </div>




            </div>










        </div>

      </div>
    </section><!-- #services -->


  </main>


	<?php include "includes/footer.php" ?>
