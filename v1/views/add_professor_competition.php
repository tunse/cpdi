<?php

	// session_start();
	include('output_fns.php');
    require_once('min_auto_fns.php');

    $conn1 = db_connect();

    $professor=@$_GET['professor'];



    $result1 = selectUserById($professor);
    if($result1)
    {
        $num_result = $result1->num_rows;
        if($num_result > 0)
        {
            for ($i=0; $i<$num_result; $i++)
            {
                $row = $result1->fetch_assoc();
                $name = $row['input_first_name'];
                $lastName = $row['input_last_name'];

            }
        }
        else
        {
            $name = "error";
            $lastName = "error";
        }
    }
    else
    {
        $name = "error";
        $lastName = "error";
    }



    $result1 = selectProfessorCourses($professor);
    if($result1)
    {
        $num_result = $result1->num_rows;
        if($num_result > 0)
        {
            for ($i=0; $i<$num_result; $i++)
            {
                $row = $result1->fetch_assoc();
                $courdeId = $row['course_id'];





            }
        }
    }


?>

<?php include "includes/header.php" ?>


  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>

        <style type="text/css" media="all">
            @import "/cpdi/css/info.css";
            @import "/cpdi/css/main.css";
            @import "/cpdi/css/widgEditor.css";
        </style>

        <script type="text/javascript" src="/cpdi/scripts/widgEditor.js"></script>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <br /><br /><h3>Associate professor to a Competition </h3>
        </header>

        <div class="row">

            <div class="col-md-9 col-lg-9 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                        <form action="competition_professor" method="post">

                                <div class="form-group">
                                    <label for="professor">Professor</label>
                                    <input type='hidden' name='profID' value='<?php echo $professor; ?>'>
                                    <input type="text" readonly class="form-control" id="professor" name="professor" placeholder="professor" value='<?php echo "$name $lastName"; ?> '>

                                </div>


                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Competition</label>
                                    <select class="form-control" id="course" name="course">
                    <?php
                                    $result1 = selectCompetitions();
                                    if($result1)
                                    {
                                        $num_result = $result1->num_rows;
                                        if($num_result > 0)
                                        {
                                            for ($i=0; $i<$num_result; $i++)
                                            {
                                                $row = $result1->fetch_assoc();
                                                $courseName = $row['input_name'];
                                                $courseDescription = $row['text_description'];
                                                $year = $row['input_year'];

                                                $amount = $row['input_amount'];
                                                $competitionId = $row['id'];
                    ?>
                                                <option value="<?php echo "$competitionId" ?>"><?php echo "$courseName $year" ?></option>
                    <?php
                                            }
                                        }
                                    }
                                ?>
                                    </select>
                                    <input type='hidden' name='amount' value='<?php echo $amount; ?>'>
                                </div>


                                <button type="submit" class="btn btn-success btn-sm">Associate professor to selected competition</button> <br /><br />
                                <a href="/admin-menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>

                        </form>

            </div>

        </div>
      </div>

    </section><!-- #services -->


  </main>
	<?php include "includes/footer.php" ?>
