<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');



?>

<?php include "includes/header.php" ?>


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <h3><br />Associate professor to a competition</h3>
          <p>Please select profossor</p>
        </header>

        <div class="row">

            <?php
                $result1 = selectProfessor();
                if($result1)
                {
                    $num_result = $result1->num_rows;
                    if($num_result > 0)
                    {
                        for ($i=0; $i<$num_result; $i++)
                        {
                            $row = $result1->fetch_assoc();
                            $name = $row['input_first_name'];
                            $regID = $row['id'];
                            $lastName = $row['input_lastName'];
                            $addressLineOne = $row['input_address_line_one'];
                            $addressSecondLine = $row['input_address_line_two'];
                            $postCode = $row['input_post_code'];
                            $country = $row['input_country'];
                            $phone = $row['input_phone'];
                            $email = $row['input_email'];
                            $linkedIn = $row['input_linkedIn'];


?>

                            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                                <div class="box">

                                <h4 class="title"><?php echo "$name $lastName"; ?></h4>
                                <p class="description"><a href="view_professor_courses.php?professor=<?php echo "$regID" ?>" class="btn btn-success btn-sm"><b>Edit professor courses</b></a><br /><br />
                                <a href="admin_menu.php" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to Admin Menu</a><br /><br />

                                </p>

                                </div>
                            </div>
<?php
                        }
                    }
                }
            ?>
        </div>

      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
