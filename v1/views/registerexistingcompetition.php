<?php

	  // session_start();

	  // include function files for this application
	  require_once('db_fns.php');
	  include('output_fns.php');
	  include('min_auto_fns.php');
	  $conn1 = db_connect();

	  $course = $_GET['product'];

	  if ($course=="")
	  {
		header("Location: /competitions");
		exit();

	  }

	  //User logged in proceed
	if (!isset($_SESSION['valid_user']) && empty($_SESSION['valid_user']))
	{
		header("Location: /home");
		exit();
	}


	  $paypal_url='https://www.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
	  $paypal_id='design@southernsahara.com'; // Business email ID



	  // start session which may be needed later
	  // start it now because it must go before headers



    // $result1 = selectCompetition($course);
		//
	  // if($result1)
	  // {
		//   $num_result = $result1->num_rows;
		//   if($num_result > 0)
		//   {
		// 	  for ($i=0; $i<$num_result; $i++)
		// 	  {
		// 		  $row = $result1->fetch_assoc();
		// 		  $courseName = $row['input_name'];
		// 		  $courseDescription = $row['text_description'];
    //               $flyer = $row['image_1'];
    //               $coursePromotion = $row['input_amount'];
		// 		  $year = $row['input_year'];
		//
		// 	  }
		//    }
		//   // else{
		// 	   //header("Location: index.php");
		//    //}
	  // }

$selectcompetition = selectContent($conn, "panel_competitions", ['id' => $course]);

		  $courseName = $selectcompetition[0]['input_name'];
		  $courseDescription = $selectcompetition[0]['text_description'];
              $flyer = $selectcompetition[0]['image_1'];
              $coursePromotion = $selectcompetition[0]['input_amount'];
		  $year = $selectcompetition[0]['input_year'];

?>

<?php include "includes/header.php" ?>

  <style type="text/css">

      .register-form{
        margin: 0px auto;
        padding: 25px 20px;
        background: #3a1975;
        box-shadow: 2px 2px 4px #ab8de0;
        border-radius: 5px;
        color: #fff;
      }
      .register-form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
        border: 1px solid #25055f;
      }
    </style>



    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br />

        <header class="section-header">
          <br /><br /><h3>Proceed to Payment</h3>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">

              <!-- <h4 class="title"> Welcome to the CPDI Africa HERITAGE Architecture Competition!</h4> -->
							<h4 class="title"><?php echo "$courseName $year"; ?></h4>

              <p class="description"><?php echo "$courseDescription"; ?><br /><br /> <b>Amount:</b> $<?php echo $coursePromotion; ?></p>




            </div>
          </div>

		  <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2" style="color:#495057;">
<?php

				// $result4 = selectUserByEmail($_SESSION['valid_user']);
				//
				// if($result4)
				// {
				// 	$num_result = $result4->num_rows;
				// 	if($num_result > 0)
				// 	{
				// 		for ($i=0; $i<$num_result; $i++)
				// 		{
				// 			$row = $result4->fetch_assoc();
				// 			$userId = $row['id'];
				// 			$fname = $row['input_first_name'];
				// 			$lname = $row['input_last_name'];
				// 			$email = $row['input_email'];
				// 			$phone = $row['input_phone'];
				//
				// 		}
				// 	 }
				// }

				$result4 = selectContent($conn, "read_users", ["id" => $_SESSION['valid_user']]);
				$userId = $result4[0]['id'];
				$fname = $result4[0]['input_first_name'];
				$lname = $result4[0]['input_last_name'];
				$email = $result4[0]['input_email'];
				$phone = $result4[0]['input_phone'];

				// $time = time();

				$check = selectContent($conn, 'read_competition_order', ['user_id' => $userId, 'competition_id' => $course]);

				// die(var_dump($check));
				if(count($check) < 1){
					// die(count($check));
					//
					// header("location:/register-new-competition?competition=$course&&email=$email");
					// exit();
								$rnd = rand(0000000000,9999999999);
								$split = explode(" ",'african chartered');
								$id = $rnd.cleans($split['0']);
								$hash_id = str_shuffle($id.'cpdi');
											// if ok, put in db
																	$name = $fname." ".$lname;

																					$post['hash_id'] = $hash_id;
																					$post['input_amount'] = $coursePromotion;
																					$post['user_id'] = $userId;
																					$post['input_name'] = $name;
																					$post['input_phone'] = $phone;
																					$post['input_competition_year'] = $year;
																					$post['input_email'] = $email;
																					$post['competition_id'] = $course;
																					$post['input_pay_pal_id'] = "BLANK";
																					$post['bool_status'] = 0;
																					$post['input_payer_email'] = "BLANK";
																					$post['time'] = time();
																					$post['date'] = time();
																					$post['visibility'] = "show";
																					$post["date_created"] = date("Y-m-d");
																					$post["time_created"] = date("h:i:s");

																					insertSafe($conn, "read_competition_order", $post);

											$order = selectContent($conn, "read_competition_order", ['hash_id' => $hash_id]);

											// Use a chek to set a new value for order id
											$order_id = $order[0]['id'];


											$invoice_check = selectContent($conn,"invoice",['event_id'=>$order_id]);

											if (count($invoice_check) < 1) {
												$new['phonenumber'] = $phone;
											$new['invoice_id'] = "COMP"."_".rand(10000,99999)."_".str_shuffle("INVOICECODE")."_".time();
												$new['email'] = $email;
												$new['event_id'] = $order_id;

												$new['status'] = "Unpaid";
												$new['title'] = $courseName;
												$new['description'] = "Payment for ".$courseName;
												$new['place'] = "place";
												$new['company'] = ('company');
												$new['time_created'] = date("h:i:s");
												$new['date_created'] = date("Y-m-d");
												$new['hash_id'] = time()."_".rand(1000,9000);
												$new['quantity'] = "quantity";
												$new['amount_due'] = intval($coursePromotion);
												$new['payment_plan'] = $courseName;
												$new['plan_id'] = $course;
												$new['name'] = $name;



												insertSafe($conn,'invoice',$new);


													// header("Location:/payment_invoice?id=".$new['hash_id']."&type=EVT");

													header("Refresh:0");
											}else{
														$new['hash_id'] = $invoice[0]['hash_id'];
											}

											updateContent($conn, "read_competition_order", ['input_order_id' => $order_id], ['hash_id' => $hash_id]);

				// $sqlOrder = "insert into read_competition_order values (null,'$hash_id',  '$course', '$coursePromotion', '$userId','$name','$phone','$email', '$time', 'BLANK', 0, 'BLANK', '$time','show',NOW(),NOW())";
				//
				// if (mysqli_query($conn1, $sqlOrder)) {


					echo 'Your order has been created. <br /><br /> Please make a payment to confirm your order for the course.';

				// } else {
				//    echo "Error: " . $sql . "" . mysqli_error($conn1);
				// }
				//
        //         $order_id =  mysqli_insert_id($conn1);









				$to      = $email;
				$subject = 'CPDI Africa';
				$message = '<h3>Welcome to the CPDI Africa HERITAGE Architecture Competition!</h3>
				<p>Thank you for your registration at the CPDI Africa HERITAGE Architecture Competition 2021! You have
					joined an exceptional international body of designers, architects, artists, planners, historians and all
					round built environment professionals who are developing new African inspired design languages that
					are culturally and environmentally sustainable. The Community Planning &amp; Design Initiative (CPDI)
					Africa is the leader in manifesting contemporary African architecture built upon theoretical frameworks
					by leading Afrocentric architects and scholars.</p></br>
				<p>Next Step:</p></br>
				<ol>
					<li>Log back into your My Account at cpdiafrica.org, and complete your payment using our PayPal,
					Flutterwave, or Bank Transfer options.</li>
					<li> In your payment notes, make sure you include the current/active email address we will use to
					communicate with you regarding your participation in the Heritage Competition.</li>
					<li>Log back into your cpdiafrica.org account at any time and upload your African Architecture
					Masterpiece, supporting documentations, and links to your social media handles.</li>
					<li>Make sure you have completed your Payment for the competition before the closing Deadline
					for the Heritage Competition! Your submission will be accepted but only considered for the rewards and prizes with confirmed receipt of payment. Pay at anytime before the closing date.</li>
					<li>You are welcome to engage in CPDI Africa lectures, workshops, exhibitions, and enroll in any of
					our Global Studio courses to master your knowledge of African design philosophy.</li>
					<li>Check our Social Media platforms on a weekly basis for updates on your participation
					announcements and submission deadlines on our Facebook, Instagram and LinkedIn pages!</li>
				</ol><br>
				<p>We wish you the best in your participation CPDI Africa HERITAGE Architecture Competition 2021!</p>
				<p>For further inquiries and information Contact Us at the emails provided below.</p>
				<br><br>
				<p><b>Nmadili Okwumabua<b></p>
				<p>Founder / Executive Director,</p>
				<p>CPDI Africa Global Studio for African Centered Architecture.</p> <br><br>

				<p>Email: <a href="mailto:design@cpdiafrica.org"> design@cpdiafrica.org</a> / <a href="mailto:cpdiafricascholarships@gmail.com">cpdiafricascholarships@gmail.com</a></p>
				<p>Tel: +234 809 155 6480 (NIG) WhatsApp.</p>
				';


				$headers ="MIME-Version :1.0"."\r\n";
				$headers .="Content-type:text/html; charset=iso-8859-1"."\r\n";
				$headers .= 'From: design@cpdiafrica.org' . "\r\n" .
					'Reply-To: design@cpdiafrica.org' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();



				mail($to, $subject, $message, $headers);
				//
				// $conn1->close();




				$conn1->close();




?>

<?php
        $urlEncodedText = "I'm interested in the competition: $courseName";
        $urlEncodedText = urlencode($urlEncodedText);
        echo "<a href='https://wa.me/16786509145?text=$urlEncodedText' target='_blank' style='color:#008000;'>Send a WhatsApp message <img src='/cpdi//img/whatsapp.png'></a>";

header("Location:".$_SERVER['REQUEST_URI']);
exit();
				}else{
					echo "Your Order ID is " . $check[0]['input_order_id'];


					$order_id = $check[0]['input_order_id'];
					$invoice_check = selectContent($conn,"invoice",['event_id'=>$check[0]['input_order_id']]);
					if (count($invoice_check) < 1) {
						$new['phonenumber'] = $phone;
					$new['invoice_id'] = "COMP"."_".rand(10000,99999)."_".str_shuffle("INVOICECODE")."_".time();
						$new['email'] = $email;
						$new['event_id'] = $order_id;

						$new['status'] = "Unpaid";
						$new['title'] = $courseName;
						$new['description'] = "Payment for ".$courseName;
						$new['place'] = "place";
						$new['company'] = ('company');
						$new['time_created'] = date("h:i:s");
						$new['date_created'] = date("Y-m-d");
						$new['hash_id'] = time()."_".rand(1000,9000);
						$new['quantity'] = "quantity";
						$new['amount_due'] = intval($coursePromotion);
						$new['payment_plan'] = $courseName;
						$new['plan_id'] = $course;
						$new['name'] = $check[0]['input_name'];



						insertSafe($conn,'invoice',$new);
							// header("Location:/payment_invoice?id=".$new['hash_id']."&type=EVT");
					}else{

							$new['hash_id'] = $invoice_check[0]['hash_id'];

							// var_dump($new['hash_id']);
									$invoice = selectContent($conn,"invoice",['event_id'=>$check[0]['input_order_id']]);

					}
}
?>



<br /><br />
<br />
				<div class="col-md-6 col-xs-12">


					<div class="form-group">
						<form action='<?php echo $paypal_url; ?>' method='post'>

							<?php $checkOrder = $check[0]['input_order_id']?>
					 <input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
					 <input type='hidden' name='cmd' value='_xclick'>
					 <input type='hidden' name='item_name' value='<?php echo "$courseName"; ?>'>
					 <input type='hidden' name='item_number' value='<?php echo $checkOrder; ?>'>
					 <input type='hidden' name='amount' value='<?php echo "$coursePromotion"; ?>'>
					 <input type='hidden' name='no_shipping' value='1'>
					 <input type='hidden' name='currency_code' value='USD'>
					 <input type='hidden' name='cancel_return' value='http://www.cpdiafrica.org/cancel.php'>
					 <input type='hidden' name='return' value='https://www.cpdiafrica.org/success.php'>
					 <input type="image" src="/unnamedp.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

					 </form>
					</div>

					<div class="form-group">
					<a href="/payment_invoice?id=<?php echo $new['hash_id'] ?>&type=COMP" style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnamedf.png" ></a>
					</div>
					<div class="form-group">
					<a href="banktransfer?order=<?php echo $check[0]['input_order_id']; ?>"  style="background-color:#970C7B; border-color: #970C7B;"> <img  src="/unnameb.png" ></a>
					</div>

					<?php
					        $urlEncodedText = "I'm interested in the competition: $courseName";
					        $urlEncodedText = urlencode($urlEncodedText);
					        echo "<a href='https://wa.me/16786509145?text=$urlEncodedText' target='_blank' style='color:#008000;'>Send a WhatsApp message <img src='/cpdi//img/whatsapp.png'></a>";

					?>
				</div>
				<br />
		  </div>


      </div>
    </section><!-- #services -->


  </main>

	<?php include "includes/footer.php" ?>
