<?php $page_title = "Services" ?>
<?php include "includes/header.php" ?>

<section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">


        </header>

        <div class="row">
        <div class="container mt-4"><br /><br />




                    <div class="container-fluid">
                        <div class="row mt-3" style="color:#495057;">
                            <div class="col-md-3"><img alt="CPDI" src="/cpdi/services.png" class="w-100"></div>
                            <div class="col-md-9">
                                <p class="h3">Engage CPDI Africa Services</p>
                                <p> <strong>CPDI Africa offers full scale educational and professional architectural services to our clients!</strong> To learn more about our services, <strong>fill the forms below</strong>, or email us at <a style="color:blue" href="mailto:design@cpdiafrica.org">design@cpdiafrica.org</a> for our Art of African Architecture Lecture Series, Exhibitions, Workshops, or inquire how to engage an Afrocentric architect for your next new-build or renovation project!
                                </p>
                                <!-- <div class="col-lg-4 mb-4" data-wow-duration="1.4s"> -->
                                  <img src="/uploads/services.jpg" alt="" width="100%">
                                  <br><br>

                                <!-- </div> -->


                                    <div class="row">

                                            <div class="col-lg-4 mb-4" data-wow-duration="1.4s">

                                                <div class="box">
                                                <a href="about-us"><h4 class="title">Lectures & Workshops.</h4>
                                                <p class="description"></p>
                                                </a>
                                                <a href="services-form?action=workshop" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>Schedule a workshop </b></a>
                                                </div>

                                            </div>

                                            <div class="col-lg-4 mb-4" data-wow-duration="1.4s">

                                                <div class="box">
                                                <a href="#"><h4 class="title">Architecture Exhibitions.</h4>
                                                <p class="description"></p>
                                                </a>
                                                <a href="services-form?action=exhibitions" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>Reserve an exhibitiion </b></a>
                                                </div>

                                            </div>

                                            <div class="col-lg-4 mb-4" data-wow-duration="1.4s">

                                                <div class="box">
                                                <a href="#"><h4 class="title">Afrocentric Design Services.</h4>
                                                <p class="description"></p>
                                                </a>
                                                <a href="services-form?action=design" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>Schedule a consultation</b></a>
                                                </div>

                                            </div>
                                    </div>
                            </div>
<!--
                            <div class="col-md-3"> </div>
                            <div class="col-md-9">


                            <div class="row">

                                    <div class="col-lg-4 mb-4" data-wow-duration="1.4s">

                                        <div class="box">
                                        <a href="about-us"><h4 class="title">Lectures, Tours & Events</h4>
                                        <p class="description"></p>
                                        </a>
                                        <a href="services-form?action=workshop" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>Schedule a workshop </b></a>
                                        </div>

                                    </div>

                                    <div class="col-lg-4 mb-4" data-wow-duration="1.4s">

                                        <div class="box">
                                        <a href="#"><h4 class="title">Architecture Exhibitions</h4>
                                        <p class="description"></p>
                                        </a>
                                        <a href="services-form?action=exhibitions" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>Reserve an exhibitiion </b></a>
                                        </div>

                                    </div>

                                    <div class="col-lg-4 mb-4" data-wow-duration="1.4s">

                                        <div class="box">
                                        <a href="#"><h4 class="title">Hire an Afrocentric Architect.</h4>
                                        <p class="description"></p>
                                        </a>
                                        <a href="services-form?action=design" class="btn btn-success" style="background-color:#970C7B; border-color: #970C7B;"><b>Schedule a consultation</b></a>
                                        </div>

                                    </div>
                            </div>

                        </div>
                    </div> -->




            </div>










        </div>

      </div>
    </section><!-- #services -->
<?php include "includes/footer.php" ?>
