<?php

@session_start();

// include function files for this application
require_once('db_fns.php');

include('min_auto_fns.php');
// $conn = db_connect();


function display_login_form($courseId, $email){
?>
	<div class="register-form">
		<form action="signin" method="post">

		  <input id="courseId" name="courseId" type="hidden" value="<?php echo $courseId; ?>">

		  <h2 class="text-center">Sign in</h2>
		  <div class="form-group">
			<input type="email" name="email" class="form-control" placeholder="Email" required="required" value="<?php echo $email; ?>">
		  </div>
		  <div class="form-group">
			<input type="password" name="password" class="form-control" placeholder="Password" required="required">
		  </div>

		  <div class="row">
			<div class="col-md-6 col-xs-12">
			  <div class="form-group">
				<input type="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7" style="background-color:#970C7B; border-color: #970C7B;">
			  </div>
			</div>

		  </div>
		</form>
	</div>
<?php
}

function display_login_form_competition($courseId, $email){
	?>
		<div class="register-form">
			<form action="login-competition" method="post">

			  <input id="courseId" name="courseId" type="hidden" value="<?php echo $courseId; ?>">

			  <h2 class="text-center">Sign in</h2>
			  <div class="form-group">
				<input type="email" name="email" class="form-control" placeholder="Email" required="required" value="<?php echo $email; ?>">
			  </div>
			  <div class="form-group">
				<input type="password" name="password" class="form-control" placeholder="Password" required="required">
			  </div>

			  <div class="row">
				<div class="col-md-6 col-xs-12">
				  <div class="form-group">
					<input type="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7" style="background-color:#970C7B; border-color: #970C7B;">
				  </div>
				</div>

			  </div>
			</form>
		</div>
	<?php
	}


function login_form(){
?>
	<div>
		<form action="myaccount" method="post">
		  <div class="form-group">
			<input type="email" name="email" class="form-control" placeholder="Email" required="required">
		  </div>
		  <div class="form-group">
			<input type="password" name="password" class="form-control" placeholder="Password" required="required">
		  </div>

		  <div class="row">
			<div class="col-md-6 col-xs-12">
			  <div class="form-group">
				<input type="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7" style="background-color:#970C7B; border-color: #970C7B;">
			  </div>
			</div>

		  </div>
		</form>
	</div>
<?php
}



function display_footer_address(){
	echo " ";
	//echo "<strong>Address:</strong> Ouderkerkerlaan 24, 1112 BE Diemen <br />";
}


function display_footer_phone(){

	echo "<strong>Phone (USA):</strong> +1 678 650 9145 <br />";
	echo "<strong>Phone (NG):</strong> +234 809 155 6480<br />";
}

function display_footer_email(){
?>


<b>The Founder:</b> <br />design@cpdiafrica.org, design@southernsahara.com<br />
<b>The Global Studio:</b> globalstudio@cpdiafrica.org<br />
<b>Competitions:</b> compete@cpdiafrica.org<br />
<b>Internships:</b> jobs@cpdiafrica.org<br />
<b>Outreach:</b> ambassador@cpdiafrica.org<br />
<b>Inquiries:</b> info@cpdiafrica.org


<?php
}



function display_useful_links(){


	echo '<ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="aboutus.php">About us</a></li>
			  <li><a href="courses.php">Courses</a></li>
			  <li><a href="competitions.php">Competitions</a></li>
              <li><a href="competitions.php">Compendium</a></li>
              <li><a href="services.php">Services</a></li>
            </ul>';
}


function display_menu(){
	echo '<ul>
          <li class="active"><a href="aboutus.php">About</a></li>
          <li><a href="courses.php">Courses</a></li>
		  <li><a href="#services">Competitions</a></li>
		  <li><a href="#services">Compendium</a></li>
		  <li><a href="#services">Services</a></li>
		  <li><a href="#team">Resources</a></li>
		  <li><a href="#team">Connect</a></li>
		  <li><a href="#contact">Archives</a></li>';
//		  if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
//		  {
			echo '<li class="drop-down"><a href="login_form.php">My Account</a>
					<ul>
						<li><a href="myaccount.php">My Courses</a></li>
						<li><a href="myaccount.php">My Competitions</a></li>
						<li><a href="myaccount.php">My Subscriptions</a></li>

						<li><a href="myaccount.php">Admin Menu</a></li>

						<li><a href="signout.php">Sign Out</a></li>
					</ul>';
			echo '</li>';
//		  }
//		  }
//		  else
//		  {
//			  echo '<li><a href="login_form.php">My Account</a></li>';
//		  }

	//echo '<li class="drop-down"><a href="">Language</a>
    //        <ul>
      //        <li><a href="#">Dutch</a></li>
        //      <li><a href="#">English</a></li>
          //  </ul>
		  //</li>



       	  echo '</ul>';


}


function display_menu_with_links(){

	@$result4 = selectUserByEmail($_SESSION['valid_user']);

	if($result4)
	{
		$num_result = $result4->num_rows;
		if($num_result > 0)
		{
			for ($i=0; $i<$num_result; $i++)
			{
				$row = $result4->fetch_assoc();
				$userId = $row['regID'];
				$fname = $row['name'];
				$lname = $row['lastName'];
				$email = $row['email'];
				$adminStatus = $row['adminStatus'];
				$professorStatus = $row['professorStatus'];

			}
			}
	}


	echo '<ul>
	<li><a href="aboutus.php">About</a></li>
	<li><a href="courses.php">Global Studio</a></li>
	<li><a href="competitions.php">Competitions</a></li>
	<li><a href="compendium.php">Compendium</a></li>
	<li><a href="services.php">Services</a></li>
	<li><a href="resources.php">Resources</a></li>
	<li><a href="connect.php">Connect</a></li>';


		   if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
		  {
			echo '<li class="drop-down"><a href="login_form.php">My Account</a>
					<ul>';
			echo "<li><p style='color:#495057;'>Welcome: $fname</p></li>";
				if ($adminStatus=="YES" || $professorStatus=="YES")
				{
					echo '<li><a href="admin_menu.php">Admin Menu</a></li>';
				}
			echo '<li><a href="signout.php">Sign Out</a></li></ul>';
			echo '</li>';
		  }
		  else
		  {
			  echo '<li><a href="login_form.php">My Account</a></li>';
		  }





      echo '</ul>';
}

function display_social_links()
{
?>
	<div class="social-links">
                                        <a href="http://www.cpdiafrica.org" class="fa fa-dribbble" target="_blank"> </a>

                                        <a href="www.facebook.com/cpdiafrica" class="fa fa-facebook" target="_blank"></a>
                                        <a href="https://www.pinterest.ca/cpdiafrica" class="fa fa-pinterest" target="_blank"></a>

                                        <a href="https://twitter.com/cpdi_africa" class="fa fa-twitter" target="_blank"></a>

                                        <a href="https://www.instagram.com/cpdi_africa/" class="fa fa-instagram" target="_blank"></a>

                                        <a href = "mailto: design@cpdiafrica.org" class="fa fa-email" target="_blank" style="color: #fff">@</a>

                                        <a href="https://www.linkedin.com/company/cpdi-africa-global-studio/" class="fa fa-linkedin" target="_blank"></a>

                                        <a href="https://www.youtube.com/channel/UCfSEFKPbnzHIE4bZieatTgQ" class="fa fa-youtube" target="_blank"></a>

			</div>
<?php
}


function display_full_footer()
{
?>
	<!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
	  <div><p style="text-align: center; font-size: 18px"><b>#AfricaDefinesArchitecture</b></p> </div>
        <div class="row">


          <div class="col-lg-4 col-md-6 footer-info">
		  	<h4>Connect with Us</h4>
				<a href="http://www.cpdiafrica.org" class="fa fa-dribbble" target="_blank"> &nbsp; www.cpdiafrica.org </a><br />

				<a href="https://cpdiafrica.blogspot.com/" class="fa fa-blogger" target="_blank" style="color: #fff">B &nbsp; www.cpdiafrica.blogspot.com</a><br />

                <a href="https://www.instagram.com/cpdi_africa/" class="fa fa-instagram" target="_blank"> &nbsp; cpdi_africa</a><br />

                <a href="https://www.facebook.com/cpdiafrica" class="fa fa-facebook" target="_blank"> &nbsp; www.facebook.com/cpdiafrica</a> <br />

                <a href="https://www.youtube.com/channel/UCfSEFKPbnzHIE4bZieatTgQ" class="fa fa-youtube" target="_blank"> &nbsp; CPDI Africa</a><br />

                <a href="https://twitter.com/cpdi_africa" class="fa fa-twitter" target="_blank"> &nbsp; cpdi_africa </a><br />

                <a href="https://www.pinterest.ca/cpdiafrica" class="fa fa-pinterest" target="_blank"> &nbsp; cpdiafrica</a><br />
                <a href = "mailto: design@cpdiafrica.org" class="fa fa-email" target="_blank" style="color: #fff">@  &nbsp; email</a> <br />

                <a href="https://www.linkedin.com/company/cpdi-africa-global-studio/" class="fa fa-linkedin" target="_blank"> &nbsp; CPDI Africa Global Studio </a><br />



          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <?php display_useful_links(); ?>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>

			<?php display_footer_phone(); ?>
            <?php display_footer_email(); ?>






          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>SIGN UP TO OUR NEWSLETTER</h4>
            <a href="https://us15.campaign-archive.com/home/?u=82858656ff28d4df9d68eeeb0&id=d3cb2d4ce8" target="_blank"><img alt="CPDI" src="services.png" class="w-100"></a>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>CPDI Africa</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->
<?php
}

function display_full_footer_without_social_links()
{
?>
	<!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">




          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <?php display_useful_links(); ?>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>


            <?php display_footer_email(); ?>

            <?php display_footer_phone(); ?>




          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>SIGN UP TO OUR NEWSLETTER</h4>
            <a href="https://us15.campaign-archive.com/home/?u=82858656ff28d4df9d68eeeb0&id=d3cb2d4ce8" target="_blank"><img alt="CPDI" src="services.png" class="w-100"></a>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>CPDI Africa</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- #footer -->
<?php
}

?>
