<?php
	// session_start();
	include('output_fns.php');
	require_once('min_auto_fns.php');



?>

<?php include "includes/header.php" ?>
    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container"><br /><br />

        <header class="section-header">
          <h3><br />Course Materials</h3>
        </header>

        <div class="row" style="color:#495057;">

            <?php
                $result1 = selectMaterials();
                if($result1)
                {
                    $num_result = $result1->num_rows;
                    if($num_result > 0)
                    {
                        for ($i=0; $i<$num_result; $i++)
                        {
                            $row = $result1->fetch_assoc();
                            $name = $row['input_name'];
                            $matID = $row['id'];
                            $description = $row['text_description'];
                            $courseId = $row['course_id'];
                            $courseDocument = $row['input_course_document'];
                            $relevantURL = $row['input_relevant_url'];


                            $result2 = selectCourse($courseId);
                            if($result2)
                            {
                                $num_result2 = $result2->num_rows;
                                if($num_result2 > 0)
                                {
                                    for ($c=0; $c<$num_result2; $c++)
                                    {
                                        $row2 = $result2->fetch_assoc();
                                        $courseName = $row2['input_course_name'];
                                    }
                                }
                            }




?>

                            <div class="col-md-6 col-lg-6 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                                <div class="box">

                                <h4 class="title" style="color:#fff"><?php echo "$name" ?></h4>
                                <p class="description" style="color:#fff"><?php echo "$description"; ?> <br />
                                  <?php echo "$relevantURL"; ?> <br />
                                  <?php echo "$courseName"; ?><br /><br />
                                  <a href="download-file?file=<?php echo $courseDocument; ?>" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Download File</a>
                                  <a href="admin_menu" class="btn btn-light btn-sm active" role="button" aria-pressed="true">Back to Menu</a><br /><br />
                                  <a href="delete-material?material=<?php echo "$matID&doc=$courseDocument"; ?>" class="btn btn-danger btn-sm active" role="button" aria-pressed="true">Delete course Material</a>


                                </p>
                                </div>
                            </div>
<?php
                        }
                    }
                    else
                    {
                      echo "There are no course materials";
                      echo '<a href="admin_menu" class="btn btn-light btn-sm"><b>Back to Admin Menu</b></a>';

                    }
                }
            ?>
        </div>

      </div>
    </section><!-- #services -->


  </main>

  <!--==========================
    Footer
  ============================-->

	<?php include "includes/footer.php" ?>
