<?php

function db_connect()
{

  // die(var_dump(getenv("DB_PASS")));
   $result = new mysqli('localhost', getenv("DB_USER"), getenv("DB_PASSWORD"), getenv("DB_NAME"));
   if (!$result)
     throw new Exception('Could not connect to database server');
   else
     return $result;
}

?>
