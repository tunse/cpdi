<?php

if(!isset($_GET['invoice'])){
if(isset($webhook)){
  $_GET['invoice'] = $invoice[0]['invoice_id'];
}else{
  exit();
}
}


$invoice =  selectContent($conn,"event_invoice",['hash_id'=>$_GET['invoice'],"status"=>"paid"]);

if(count($invoice) < 1 ){
  die("ERROR_CODE:DTN|NV|NFD");
  exit();
}
if($invoice[0]['amount_paid'] !== NULL) {
    die("ERROR_CODE:DTN|NV|ALRD|ISSD");
  exit();
}
extract($invoice[0]);

$date = getdate(date("U"));
if($date['mon'] < 10){
  $mon = "0".$date['mon'];
}else{
  $mon = $date['mon'];
}
$now =  $date['year']."-".$mon."-".$date['mday'] ;



// $array_keys = array_keys($array[$uri[1]]);

// TRN NEEDED
if($invoice[0]['quantity'] !==NULL){

    $data = [];
    // $getdate = date('Y-m-d', strtotime($array[$uri[1]][$uri['2']]['calc']));
    // One month from a specific date
    // $getdate = date('Y-m-d', strtotime($calc, strtotime($now)));
    // $data['hash_id'] = time().rand(1000000,9999999);
    // $data['user_id'] = $_SESSION['id'];
    $data['fullname'] = $invoice[0]['name'];
    $data['email'] = $invoice[0]['email'];
    $data['phonenumber'] = $invoice[0]['phonenumber'];
    $data['event_id'] = $invoice[0]['event_id'];
    $data['qr'] = $invoice[0]['user_id'];
    $data['company'] = $invoice[0]['company'];
    $data['place'] = $invoice[0]['place'];
    $data['plan'] = $invoice[0]['payment_plan'];
    $data['amount'] = $invoice[0]['amount_due'];
    $data['custom'] = $invoice[0]['custom'];
    $data['ref'] = $invoice[0]['ref'];
    $singlePercentage = intval($invoice[0]['amount_due']) / 100;
    $percentage = $singlePercentage * 5;$percentage = ceil($percentage);
    if($percentage > 3000){
      $percentage = 3000;
    }
    $remitted = intval($invoice[0]['amount_due']) - $percentage;

    $data['amount_settled'] = $remitted;
    $data['amount_charged'] = $percentage;
    $data['date_created'] = date("Y-m-d");

}else{

  $data = [];
  // $getdate = date('Y-m-d', strtotime($array[$uri[1]][$uri['2']]['calc']));
  // One month from a specific date
  // $getdate = date('Y-m-d', strtotime($calc, strtotime($now)));
  // $data['hash_id'] = time().rand(1000000,9999999);
  // $data['user_id'] = $_SESSION['id'];
  $data['fullname'] = $invoice[0]['name'];
  $data['email'] = $invoice[0]['email'];
  $data['phonenumber'] = $invoice[0]['phonenumber'];
  $data['event_id'] = $invoice[0]['event_id'];
  $data['qr'] = $invoice[0]['user_id'];
  $data['company'] = $invoice[0]['company'];
  $data['place'] = $invoice[0]['place'];
  $data['plan'] = $invoice[0]['payment_plan'];
  $data['amount'] = $invoice[0]['amount_due'];
        $data['custom'] = $invoice[0]['custom'];
          $data['ref'] = $invoice[0]['ref'];

  $singlePercentage = intval($invoice[0]['amount_due']) / 100;
  $percentage = $singlePercentage * 5;$percentage = ceil($percentage);
  if($percentage > 3000){
    $percentage = 3000;
  }
  $remitted = intval($invoice[0]['amount_due']) - $percentage;

  $data['amount_settled'] = $remitted;
  $data['amount_charged'] = $percentage;
    $data['payment_status'] = 0;
  $data['date_created'] = date("Y-m-d");
    insertSafe($conn,'event_paid_registrant',$data);
    updateContent($conn,"event_invoice",['amount_paid'=>1],["hash_id"=>$_GET['invoice']]);
}




// updateContent($conn,'admin',["is_premium"=>$invoice[0]['payment_plan'],"expiration"=>$getdate],["hash_id"=>$_SESSION['id']]);



$eventDetails = selectContent($conn,'event',['hash_id'=>$invoice[0]['event_id']]);

if($eventDetails[0]['price_type'] !=="single"){
//TRN NEED

  $plan_info = selectContent($conn,"event_payment_plan",['id'=>$invoice[0]['plan_id']]);
$determine_admission = explode("_",$invoice[0]['user_id']);
if(count($determine_admission) > 2 ){

if ($invoice[0]['quantity'] !==NULL){
for ($i=1; $i <= $determine_admission[3] ; $i++) {
    $max['fullname'] = $plan_info[0]['plan_name']." BULK PURCHASE by ".$invoice[0]['name'];
    $max['email'] = $invoice[0]['email'];
    $max['phonenumber'] = $invoice[0]['phonenumber'];
    $max['event_id'] = $invoice[0]['event_id'];
    $max['qr'] = $determine_admission[0]."_".$determine_admission[1]."_BULK_".$i;
    $max['company'] = $invoice[0]['company'];
    $max['place'] = $invoice[0]['place'];
    $max['plan'] = $invoice[0]['payment_plan'];
    $max['amount'] = $plan_info[0]['plan_price'];
      $max['custom'] = $invoice[0]['custom'];
        $max['ref'] = $invoice[0]['ref'];
    // TRN NEEDED
    $singlePercentage = intval($plan_info[0]['plan_price']) / 100;
    $percentage = $singlePercentage * 5;$percentage = ceil($percentage);
    if($percentage > 3000){
      $percentage = 3000;
    }
    $remitted = intval($plan_info[0]['plan_price']) - $percentage;
    $max['amount_settled'] = $remitted;
    $max['amount_charged'] = $percentage;
          $max['payment_status'] = 0;
    $max['date_created'] = date("Y-m-d");
    updateContent($conn,"event_invoice",['amount_paid'=>1],["hash_id"=>$_GET['invoice']]);
      insertSafe($conn,'event_paid_registrant',$max);
    }
  }else{
  for ($i=1; $i <= $determine_admission[3] ; $i++) {
    $max['fullname'] = $plan_info[0]['plan_name']." by ".$invoice[0]['name'];
    $max['email'] = $invoice[0]['email'];
    $max['phonenumber'] = $invoice[0]['phonenumber'];
    $max['event_id'] = $invoice[0]['event_id'];
    $max['qr'] = $determine_admission[0]."_".$determine_admission[1]."_MAX_".$i;
    $max['company'] = $invoice[0]['company'];
    $max['place'] = $invoice[0]['place'];
    $max['plan'] = $invoice[0]['payment_plan'];
    $max['amount'] = $invoice[0]['amount_due'];
      $max['custom'] = $invoice[0]['custom'];
        $max['ref'] = $invoice[0]['ref'];
    $max['amount_settled'] = NULL;
              $max['payment_status'] = 0;
    $max['amount_charged'] = NULL;
    $max['date_created'] = date("Y-m-d");
    updateContent($conn,"event_invoice",['amount_paid'=>1],["hash_id"=>$_GET['invoice']]);
      insertSafe($conn,'event_max',$max);
  }

}

}









  if($plan_info[0]['event_id'] !== $invoice[0]['event_id']){
    $data["status"] = "Event - Success, Paid, Unable to Correlate Invoice Event ID and PLan Event Id";
    $data["invoice_id"] = $invoice[0]['invoice_id'];
    $data['time_created'] = date("h:i:s");
    $data['date_created'] = date("Y-m-d");
    insertSafe($conn,'complications',$data);
    header("Location:/paymentFailed");
    die;
  }
  $new_limit = intval($plan_info[0]['plan_limit']) - 1;
    updateContent($conn,'event_payment_plan',['plan_limit'=>$new_limit],["id"=>$invoice[0]['plan_id'],"event_id"=>$invoice[0]['event_id']]);
}else{
$rem = intval($eventDetails[0]['registration_limit']) - 1;
updateContent($conn,'event',['registration_limit'=>$rem],["hash_id"=>$invoice[0]['event_id']]);
}

// var_dump($price_plan);
$bdd = adminInfo($conn,$eventDetails[0]['created_by']);
$who = $bdd['image_2'];
$headings = "registration for ".$eventDetails[0]['name'];
$message = "Hello ".$bdd['firstname']." ".$bdd['lastname'].", there is a registration on your event";
$page = "/paidparticipants/".$eventDetails[0]['hash_id'];
$webp = "event_registration";
include APP_PATH.'/demo_public_views/include/onenotification.php';

if($eventDetails[0]['accreditation'] == 1){












$txt3 ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta  name="viewport" content="width=display-width, initial-scale=1.0, maximum-scale=1.0," />
<title>Attenout Events</title>

<style type="text/css">

html { width: 100%; }
body {margin:0; padding:0; width:100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
img {display:block !important; border:0; -ms-interpolation-mode:bicubic;}

.ReadMsgBody { width: 100%;}
.ExternalClass {width: 100%;}
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

.MsoNormal {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
p {margin:0 !important; padding:0 !important;}

.images {display:block !important; width:100% !important;}
.display-button td, .display-button a  {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}

.display-button a:hover {text-decoration:none !important;}

.width-auto {
  width: auto !important;
}
.main-width {
  width:600px;
}
.width800 {
  width:800px !important;
  max-width:800px !important;
}
.no-padding{
  padding:0 !important;
}
.saf-table {
  display:table !important;
}

/* MEDIA QUIRES */

@media only screen and (max-width:799px)
{
  body {width:auto !important;}
  .display-width {width:100% !important;}
  .display-width-inner {width:600px !important;}
  .padding { padding:0 20px !important; }
  .width800 {
    width:100% !important;
    max-width:100% !important;
  }
  .hide-padding
  {
    padding: 0px !important;
  }
}
@media only screen and (max-width:639px)
{
  body {width:auto !important;}
  .display-width {width:100% !important;}
  .display-width-inner, .display-width-child {width:100% !important;}
  .display-width-child .button-width .display-button {width:auto !important;}
  .padding { padding:0 20px !important; }
  .saf-table {
    display:block !important;
  }
  .width282 {
    width:282px !important;
  }
  .div-width {
    display: block !important;
    width: 100% !important;
    max-width: 100% !important;
  }
  .width-auto {
    width:100% !important;
  }
  .height10 {height:10px !important;}
  .height30 {height:30px !important;}
  .hide-height, .hide-bar {display:none !important;}
  .txt-center {text-align:center !important;}
  .underline-image img {
    margin:0 auto !important;
  }
  span.unsub-width {width:100% !important;
    display:block !important;}
    span.txt-copyright{ padding-bottom:10px !important;}
  }

  @media only screen and (max-width:480px) {

    .display-width table, .display-width-child2 table {width:100% !important;}
    .display-width .button-width .display-button {width:auto !important;}
    .display-width .width282 {
      width:282px !important;
    }
    .div-width {
      display: block !important;
      width: 100% !important;
      max-width: 100% !important;
    }
    .width-auto {
      width:100% !important;
      max-width: 100% !important;
    }
  }

  @media only screen and (max-width:380px)
  {
    .display-width table {width:100% !important;}
    .display-width .button-width .display-button {width:auto !important;}
    .display-width-child .width282 { width:100% !important;}
  }

  </style>

  </head>
  <body style="width:100%;margin: 0; mso-line-height-rule: exactly;">

  <table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
  <tr>
  <td align="center">

  <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">

  <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
  <tbody>
  <tr>
  <td align="center" class="padding">

  <div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="main-width">
  <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
  <tr>
  <td height="15" class="height30" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;"></td>
  </tr>
  <tr>
  <td align="center"  style="width:100%; max-width:100%; font-size:0;">

  <div style="display:inline-block; max-width:150px; width:100%; vertical-align:top;" class="div-width">

  <table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
  <tr>
  <td align="center">
  <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:auto !important;">
  <tr>

  <td align="center">
  <a href="https://attendout.com" style="color:#333333; text-decoration:none;"><img src="https://attendout.com/attendicon.png" alt="attendout" width="50" height="50" style="margin:0; border:0; padding:0; display:block;"/></a>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>

  <div style="display:inline-block; width:100%; max-width:440px; vertical-align:top;" class="div-width">
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
  <tr>
  <td align="center" style="font-size:0;">

  <div style="display:inline-block; width:100%; max-width:320px; vertical-align:top;" class="div-width">
  <table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:210px;">
  <tr>
  <td width="100%" style="font-size:0;">&nbsp;</td>
  </tr>
  </table>
  </div>

  <div style="display:inline-block; width:100%; max-width:110px; vertical-align:top;">
  <table align="right" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
  <tr>
  <td align="center" style="font-size:0;">

  <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
  <tr>
  <td align="center">
  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <td height="15" class="height20" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;"></td>
  </tr>
  <tr>

  <td align="right" class="MsoNormal txt-center" style="color:#666666; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:12px; font-weight:400; letter-spacing:1px;">
  <a href="http://attendout.com" style="color:#666666; text-decoration:none;">
  attendout.com
  </a>
  </td>
  </tr>
  <tr>
  <td height="15" class="hide-height" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;"></td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>

  </td>
  </tr>
  </table>
  </div>

  </td>
  </tr>
  <tr>
  <td height="15" class="height30" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;">&nbsp;</td>
  </tr>
  </table>
  </div>

  </td>
  </tr>
  </tbody>
  </table>
  </div>

  </td>
  </tr>
  </tbody>
  </table>

  <table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <td align="center">

  <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">

  <table align="center" bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">

  </table>
  </div>

  </td>
  </tr>
  </table>

  <table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
  <tr>
  <td align="center">

  <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">

  <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
  <tbody>
  <tr>
  <td align="center" class="padding">

  <div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="main-width">
  <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
  <tr>
  <td height="60" style="font-size:0; mso-line-height-rule:exactly; line-height:60px;">&nbsp;</td>
  </tr>
  <tr>
  <td>
  <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="100%" style="width:100%; max-width:100%;">
  <tr>

  <td align="center" class="MsoNormal" style="color:#333333; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px; line-height:26px; letter-spacing:1px; font-weight:600;">
  '.strtoupper($eventDetails[0]['name']).' REGISTRATION
  </tr>
  <tr>
  <td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
  </tr>
  <tr>

  <td align="center" class="MsoNormal" style="font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:40px; font-weight:400; color:#0288D1; line-height:50px; letter-spacing:1px; mso-line-height-rule: exactly;">
  '.strtoupper($data['fullname']).'
  </td>
  </tr>
  <tr>
  <td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
  </tr>
  <tr>

  <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:12px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;">
  You have been registered for '.strtoupper($eventDetails[0]['name']).' on Attendout Event Platform.
  </td>
  </tr>
  <tr>
  <td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
  </tr>

  <tr>
  <td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
  </tr>
  <tr>
';
//TRN NEEDED
if ($eventDetails[0]['accreditation'] == 1) {

  if($eventDetails[0]['price_type'] !=="single"){
    $determine_admission = explode("_",$invoice[0]['user_id']);
    if(count($determine_admission) > 2 ){


      $txt3.='
            <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;">
            Important!
            </td>
            </tr>

            <tr>
            <td height="5" style="font-size:0; mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
            </tr>
            <tr>

            <td align="center" class="MsoNormal" style="color:#E01931; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px; line-height:26px; letter-spacing:1px; font-weight:600;">
            CLICK THE QR TICKET BELOW TO DOWNLOAD YOUR QR ACCESS <br><br>
            Links below for '.$determine_admission[3].' tickets for '.$determine_admission[3].' persons. Ensure you get the tickets to them
            </td>
            </tr>
            <tr>
            <td height="20" style="font-size:0; mso-line-height-rule:exactly; line-height:20px;">&nbsp;</td>
            </tr><tr>
            ';
// TRN NEEDED
if($invoice[0]['quantity'] !==NULL ){
for ($i=1; $i <= $determine_admission[3] ; $i++) {
  $txt3.='<tr>
  <td align="center" class="MsoNormal" style="color:#0288D1; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px; line-height:26px; letter-spacing:1px; font-weight:600;"><a href="https://attendout.com/paidqrmax/'.            $determine_admission[0].'_'.$determine_admission[1].'_BULK_'.$i.'">
  Ticket '.$i.'</a>
  </td></tr><tr>
  <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
  </tr>
  ';
}
}else{
for ($i=1; $i <= $determine_admission[3] ; $i++) {
  $txt3.='<tr>
  <td align="center" class="MsoNormal" style="color:#0288D1; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px; line-height:26px; letter-spacing:1px; font-weight:600;"><a href="https://attendout.com/paidqrmax/'.            $determine_admission[0].'_'.$determine_admission[1].'_MAX_'.$i.'">
  Ticket '.$i.'</a>
  </td></tr><tr>
  <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
  </tr>
  ';
}
}

                $txt3.='
            <tr>
            <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
            </tr>
            <tr>

            <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;">
            This ticket may be requested for entry into this event. Each Ticket admits one and is not transferrable
            </td>
            </tr>
      ';















    }else{
      $txt3.='
            <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;">
            Important!
            </td>
            </tr>

            <tr>
            <td height="5" style="font-size:0; mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
            </tr>
            <tr>

            <td align="center" class="MsoNormal" style="color:#E01931; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px; line-height:26px; letter-spacing:1px; font-weight:600;">
            CLICK THE QR TICKET BELOW TO DOWNLOAD YOUR QR ACCESS
            </td>
            </tr>
            <tr>
            <td height="20" style="font-size:0; mso-line-height-rule:exactly; line-height:20px;">&nbsp;</td>
            </tr>
            <tr>
            <td align="center" class="button-width">

            <table align="center" bgcolor="#0288D1" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:5px;margin:5px">
            <tr>
            <td align="center" class="MsoNormal" style="color:#ffffff; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-weight:600; padding:10px 12px; font-size:12px; letter-spacing:1px;">
            <a href="https://attendout.com/paidqr/'.$data['qr'].'" style="color:#ffffff; text-decoration:none;margin:10px">Download Qr</a>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>

            <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;"><br><br><p>If the DOWNLOAD QR button doesnt work, copy the link below and paste into your browser url bar</p>
              <p>https://attendout.com/paidqr/'.$data['qr'].'</p><br></br>
            </td>
            </tr>
            <tr>

            <tr>
            <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
            </tr>
            <tr>

            <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;">
            This ticket may be requested for entry into this event.The Ticket admits one and is not transferrable
            </td>
            </tr>
      ';
    }



  }else{
    $txt3.='
          <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;">
          Important!
          </td>
          </tr>

          <tr>
          <td height="5" style="font-size:0; mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
          </tr>
          <tr>

          <td align="center" class="MsoNormal" style="color:#E01931; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px; line-height:26px; letter-spacing:1px; font-weight:600;">
          CLICK THE QR TICKET BELOW TO DOWNLOAD YOUR QR ACCESS
          </td>
          </tr>
          <tr>
          <td height="20" style="font-size:0; mso-line-height-rule:exactly; line-height:20px;">&nbsp;</td>
          </tr>
          <tr>
          <td align="center" class="button-width">

          <table align="center" bgcolor="#0288D1" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:5px;margin:5px">
          <tr>
          <td align="center" class="MsoNormal" style="color:#ffffff; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-weight:600; padding:10px 12px; font-size:12px; letter-spacing:1px;">
          <a href="https://attendout.com/paidqr/'.$data['qr'].'" style="color:#ffffff; text-decoration:none;margin:10px">Download Qr</a>
          </td>
          </tr>
          </table>
          </td>
          </tr>
          </table>
          </td>
          </tr>
          <tr>
          <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
          </tr>
          <tr>

          <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;"><br><br><p>If the DOWNLOAD QR button doesnt work, copy the link below and paste into your browser url bar</p>
            <p>https://attendout.com/paidqr/'.$data['qr'].'</p><br></br>
          </td>
          </tr>
          <tr>
          <tr>

          <td align="center" class="MsoNormal" style="color:#666666; font-family:\'Segoe UI\', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:600; line-height:24px; letter-spacing:1px; mso-line-height-rule: exactly;">
          This ticket may be requested for entry into this event.The Ticket admits one and is not transferrable
          </td>
          </tr>
    ';
  }



}

if($eventDetails[0]['map_status'] == 1){
$txt3.='
  <tr>
  <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
  </tr>
  <tr>
  <td align="center" class="button-width">

  <table align="center" bgcolor="#0288D1" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:5px;">
  <tr>
  <td align="center" class="MsoNormal" style="color:#ffffff; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-weight:600; padding:10px 12px; font-size:12px; letter-spacing:1px;">
  <a href="https://attendout.com/locate/'.$eventDetails[0]['usname'].'" style="color:#ffffff; text-decoration:none;margin:10px">Locate The Event Here</a>
  </td>
  </tr>

  </table>
  </td>
  </tr>';
}


if($eventDetails[0]['dp_id'] !==NULL){
      $dp_link = "/dpc/".$row['dp_id']."?use_id=".base64url_encode($invoice[0]['user_id'].":".$eventDetails[0]['hash_id']);
$txt3.='
          <tr>
          <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
          </tr>
          <tr>
          <td align="center" class="button-width">

          <table align="center" bgcolor="#0288D1" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:5px;">
          <tr>
          <td align="center" class="MsoNormal" style="color:#ffffff; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-weight:600; padding:10px 12px; font-size:12px; letter-spacing:1px;">
          <a href="https://attendout.com'.$dp_link.'" style="color:#ffffff; text-decoration:none;margin:10px">Get your DP Image</a>
          </td>
          </tr>

          </table>
          </td>
          </tr>';
        }elseif ($eventDetails[0]['dp_id'] !==NULL && $eventDetails[0]['price_type'] !=="single" ) {
          if($plan_info[0]['dp_id'] !==NULL){
            $dp_link = "/dpc/".$plan_info[0]['dp_id']."?use_id=".base64url_encode($invoice[0]['user_id'].":".$eventDetails[0]['hash_id']);
      $txt3.='
                <tr>
                <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
                </tr>
                <tr>
                <td align="center" class="button-width">

                <table align="center" bgcolor="#0288D1" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:5px;">
                <tr>
                <td align="center" class="MsoNormal" style="color:#ffffff; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-weight:600; padding:10px 12px; font-size:12px; letter-spacing:1px;">
                <a href="https://attendout.com'.$dp_link.'" style="color:#ffffff; text-decoration:none;margin:10px">Get your DP Image</a>
                </td>
                </tr>

                </table>
                </td>
                </tr>';
          }
          // code...
        }

  $txt3.='

  <tr>
  <td align="center" class="button-width">

  <table align="center" bgcolor="#0288D1" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:5px;margin:5px">
  <tr>
  <td align="center" class="MsoNormal" style="color:#ffffff; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-weight:600; padding:10px 12px; font-size:12px; letter-spacing:1px;">
  <a href="https://attendout.com/e/'.$eventDetails[0]['usname'].'" style="color:#ffffff; text-decoration:none;margin:10px">Event Info Here</a>
  </td>
  </tr>';

  $txt3.='

  </table>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  <tr>
  <td height="60" style="font-size:0; mso-line-height-rule:exactly; line-height:60px;">&nbsp;</td>
  </tr>
  </table>
  </div>

  </td>
  </tr>
  </tbody>
  </table>
  </div>

  </td>
  </tr>
  </tbody>
  </table>

  <table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <td align="center">

  <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">

  <table align="center" bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">

  <tr>
  <td align="center" style="font-size:0;">

  <div style="display:inline-block; max-width:195px; vertical-align:top; width:100%;" class="div-width">

  <table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
  <tbody><tr>
  <td align="center" style="padding:15px 5px;">
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="width:180px !important;">
  <tbody><tr>
  <td align="center" width="25">
  <img src="https://attendout.com/org.png" alt="25x25x1" width="25" height="25">
  </td>
  </tr>
  <tr>
  <td height="10" style="font-size:0; mso-line-height-rule: exactly; line-height: 10px;">&nbsp;</td>
  </tr>
  <tr>

  <td align="center" class="MsoNormal" style="color:#333333; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px; font-weight:600; line-height:26px; letter-spacing:1px;">
  '.ucwords($eventDetails[0]['organizers_name']).'
  </td>
  </tr>
  <tr>
  <td height="5" style="font-size:0; mso-line-height-rule: exactly; line-height: 5px;">&nbsp;</td>
  </tr>
  <tr>

  <td align="center" class="MsoNormal" style="color:#666666; font-family:Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:400; line-height:24px; letter-spacing:1px;">
  Organizer
  </td>
  </tr>
  <tr>
  <td height="5" style="font-size:0; mso-line-height-rule: exactly; line-height: 5px;">&nbsp;</td>
  </tr>

  </tbody></table>
  </td>
  </tr>
  </tbody></table>
  </div>

  </td>
  </tr>
  </table>
  </div>

  </td>
  </tr>
  </table>

  <table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <td align="center">

  <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">

  <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
  <tr>
  <td align="center" class="padding">

  <div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="main-width">
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-inner" width="100%" style="max-width:600px;">
  <tr>
  <td height="30" style="line-height:30px; mso-line-height-rule:exactly; font-size:0;">&nbsp;</td>
  </tr>
  <tr>
  <td align="center" class="MsoNormal" style="padding:0 10px; color:#666666; font-family:\'Segoe UI\',sans-serif,Arial,Helvetica,Lato; font-size:13px; font-weight:400; line-height:23px; letter-spacing:1px;">
  <span class="txt-copyright unsub-width">&copy; 2019, All Rights Reserved </span> <span class="txt-copyright hide-bar"> | </span> <span class="txt-unsubscribe unsub-width"> <a href="https://attendout.com" style="color:#666666; text-decoration:none;">attendout.com</a></span>
  </td>
  </tr>
  <tr>
  <td height="30" style="line-height:30px; mso-line-height-rule:exactly; font-size:0;">&nbsp;</td>
  </tr>
  </table>
  </div>

  </td>
  </tr>
  </table>
  </div>

  </td>
  </tr>
  </table>

  </body>


  </html>
  ';

// die($txt3);


$to = $invoice[0]['email'];
$subject = ucwords($eventDetails[0]['name'])." Registration - Attendout ";

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "From: attendout@gmail.com" . "\r\n" .
"CC: attendout@gmail.com";


// if($bdd['is_premium'] !==NULL && $bdd['expiration'] > $now){
mail($to,$subject,$txt3,$headers);
// }
// $headers = "From: attendout@gmail.com" . "\r\n" .
// "CC: banjimayowa@gmail.com";
updateContent($conn,"event_invoice",['amount_paid'=>1],["hash_id"=>$_GET['invoice']]);

}


header("Location:/paidqr/".$data['qr']);
exit();
 ?>
