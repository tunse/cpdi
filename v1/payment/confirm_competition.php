<?php

if(!isset($_GET['invoice'])){
if(isset($webhook)){
  $_GET['invoice'] = $invoice[0]['invoice_id'];
}else{
  exit();
}
}


$invoice =  selectContent($conn,"invoice",['hash_id'=>$_GET['invoice'],"status"=>"paid"]);

if(count($invoice) < 1 ){
  die("ERROR_CODE:DTN|NV|NFD");
  exit();
}
if($invoice[0]['amount_paid'] !== NULL) {
    die("ERROR_CODE:DTN|NV|ALRD|ISSD");
  exit();
}
extract($invoice[0]);

$date = getdate(date("U"));
if($date['mon'] < 10){
  $mon = "0".$date['mon'];
}else{
  $mon = $date['mon'];
}
$now =  $date['year']."-".$mon."-".$date['mday'] ;





//Give Value
    updateContent($conn,"read_competition_order",['bool_payment_status'=>0,'bool_status'=>0],["input_order_id"=>$invoice[0]['event_id']]);


// Update INVOICECODE
    updateContent($conn,"invoice",['amount_paid'=>1],["hash_id"=>$_GET['invoice']]);
// die();
$to      = $email;
$subject = 'CPDI Africa';
$message = '<div><b>Dear CPDI Africa Paid Member,</b></div><br><div>Thank you for your Payment!  Your Registration and Payment records will be integrated by our Administrator.
You may log back into Your Account with your registration email and password for further steps:
<br><br>
1 - For the Heritage Competition you may begin to upload your Design submission.<br>
2 - For the African Architecture Courses, check your email within 10 Days for the CANVAS Classroom Invitation.
<br><br>
</div><br><div><b><i>Thank you for joining us at The Community Planning &amp; Design Initiative Africa!</i></b></div><br><div><font size="4"><a href="http://www.cpdiafrica.org" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.cpdiafrica.org&amp;source=gmail&amp;ust=1626976628711000&amp;usg=AFQjCNGno__3U_oSQsMtc4wXO-GvZiaDnw">www.cpdiafrica.org</a> / <a href="mailto:design@cpdiafrica.org" target="_blank">design@cpdiafrica.org</a></font></div>';
  $headers ="MIME-Version :1.0"."\r\n";
  $headers .="Content-type:text/html; charset=iso-8859-1"."\r\n";
  $headers .= 'From: design@cpdiafrica.org' . "\r\n" .
    'Reply-To: design@cpdiafrica.org' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();



  mail($to, $subject, $message, $headers);

    $admin = selectContent($conn,"read_users",['input_email'=>$invoice[0]['email']]);

    if (count($admin) < 1) {
      die("Error! User Not Found");
    }else{
      $_SESSION['valid_user'] = $admin[0]['id'];
      header("Location:/myaccount");
    }
