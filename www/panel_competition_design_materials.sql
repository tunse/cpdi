-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 26, 2021 at 07:37 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mckodevc_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `panel_competition_design_materials`
--

CREATE TABLE `panel_competition_design_materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `user_id` varchar(225) DEFAULT NULL,
  `competition_id` varchar(225) DEFAULT NULL,
  `input_name` text,
  `input_online_drive` varchar(225) DEFAULT NULL,
  `image_1` varchar(225) DEFAULT NULL,
  `input_linkedIn` text NOT NULL,
  `input_facebook` text NOT NULL,
  `input_instagram` text NOT NULL,
  `visibility` varchar(225) DEFAULT NULL,
  `boolkey_status` varchar(20) NOT NULL DEFAULT 'view_hide',
  `bool_status` int(1) NOT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `panel_competition_design_materials`
--

INSERT INTO `panel_competition_design_materials` (`id`, `hash_id`, `user_id`, `competition_id`, `input_name`, `input_online_drive`, `image_1`, `input_linkedIn`, `input_facebook`, `input_instagram`, `visibility`, `boolkey_status`, `bool_status`, `date_created`, `time_created`) VALUES
(59, NULL, '80', '21', 'Ile Oro', 'google drive', '093f65e080a295f8076b1c5722a46aa2.jpg', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(69, NULL, '92', '2', 'Yalam - Jarawa Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '14bfa6bb14875e45bba028a21ed38046.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(70, NULL, '81', '2', 'IlÃ© á»Œrá» - Yoruba Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '7cbbc409ec990f19c78c75bd1e06f215.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(71, NULL, '93', '3', 'THE ITAN COMPLEX', 'https://cpdiafrica.com/2017-winners/', 'e2c420d928d4bf8ce0ff2ec19b371514.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(72, NULL, '94', '3', 'THE EKYANZI HOUSE', 'https://cpdiafrica.com/2017-winners/', '32bb90e8976aab5298d5da10fe66f21d.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(74, NULL, '95', '2', 'Njikota â€“ Igbo Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'ad61ab143223efbc24c7d2583be69251.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(75, NULL, '96', '4', 'THE INNA', 'https://cpdiafrica.com/2015-winners/', 'd09bf41544a3365a46c9077ebb5e35c3.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(76, NULL, '97', '4', 'THE IFA HOUSE ', 'https://cpdiafrica.com/2015-winners/', 'fbd7939d674997cdb4692d34de8633c4.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(77, NULL, '98', '4', 'THE TATA FU', 'https://cpdiafrica.com/2015-winners/', '28dd2c7955ce926456240b2ff0100bde.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(78, NULL, '100', '2', 'Nnukwu Nmanwu - Igbo Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '35f4a8d465e6e1edc05f3d8ab658c551.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(79, NULL, '99', '2', 'Emathafeni â€“ Xhosa Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'd1fe173d08e959397adf34b1d77e88d7.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(80, NULL, '101', '2', 'Resilience â€“ Haitian Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'f033ab37c30201f73f142449d037028d.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(81, NULL, '102', '2', 'Gidan â€“ Hausa Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '43ec517d68b6edd3015b3edc9a11367b.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(82, NULL, '103', '2', 'Ti-kay â€“ Haitian Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '9778d5d219c5080b9a6a17bef029331c.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(83, NULL, '104', '2', 'Anyanwu - Igbo Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'fe9fc289c3ff0af142b6d3bead98a923.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(84, NULL, '105', '2', 'Southern Ridley African American Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '68d30a9594728bc39aa24be94b319d21.PNG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(85, NULL, '106', '3', 'THE PUULAK HOUSE', 'https://cpdiafrica.com/2017-winners/', '3ef815416f775098fe977004015c6193.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(86, NULL, '107', '3', 'THE NYUBU HOUSE', 'https://cpdiafrica.com/2017-winners/', '93db85ed909c13838ff95ccfa94cebd9.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(87, NULL, '108', '3', 'THE BA RRASSO HOUSE', 'https://cpdiafrica.com/2017-winners/', 'c7e1249ffc03eb9ded908c236bd1996d.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(88, NULL, '109', '3', 'THE ORACULAR HOUSE', 'https://cpdiafrica.com/2017-winners/', '2a38a4a9316c49e5a833517c45d31070.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(89, NULL, '110', '3', 'THE EDAR HOUSE', 'https://cpdiafrica.com/2017-winners/', '7647966b7343c29048673252e490f736.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(90, NULL, '111', '3', 'THE ATIKAH HOUSE', 'https://cpdiafrica.com/2017-winners/', '8613985ec49eb8f757ae6439e879bb2a.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(91, NULL, '112', '3', 'THE DEMETRIA GANDU HOUSE', 'https://cpdiafrica.com/2017-winners/', '54229abfcfa5649e7003b83dd4755294.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(92, NULL, '113', '3', 'THE SESHAT HOUSE', 'https://cpdiafrica.com/2017-winners/', '92cc227532d17e56e07902b254dfad10.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(93, NULL, '114', '3', 'THE ILE IWA', 'https://cpdiafrica.com/2017-winners/', '98dce83da57b0395e163467c9dae521b.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(94, NULL, '115', '3', 'THE ANA KATO HOUSE', 'https://cpdiafrica.com/2017-winners/', 'f4b9ec30ad9f68f89b29639786cb62ef.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(95, NULL, '116', '3', 'THE AKUA HOUSE', 'https://cpdiafrica.com/2017-winners/', '812b4ba287f5ee0bc9d43bbf5bbe87fb.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(96, NULL, '100', '3', 'THE OMI UGOMMA HOUSE', 'https://cpdiafrica.com/2017-winners/', '26657d5ff9020d2abefe558796b99584.jpg', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(97, NULL, '117', '3', 'THE JITUWA HOUSE', 'https://cpdiafrica.com/2017-winners/', 'e2ef524fbf3d9fe611d5a8e90fefdc9c.jpg', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(98, NULL, '118', '3', 'THE MAGAJIYA COMPLEX', 'https://cpdiafrica.com/2017-winners/', 'ed3d2c21991e3bef5e069713af9fa6ca.jpg', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(99, NULL, '119', '3', 'THE MONOLITH OF KASULO', 'https://cpdiafrica.com/2017-winners/', 'ac627ab1ccbdb62ec96e702f07f6425b.jpg', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(100, NULL, '120', '4', 'THE OMO', 'https://cpdiafrica.com/2015-winners/', 'f899139df5e1059396431415e770c6dd.jpg', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(101, NULL, '121', '4', 'THE FUSION POINT', 'https://cpdiafrica.com/2015-winners/', '38b3eff8baf56627478ec76a704e9b52.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(102, NULL, '122', '4', 'THE CETIANA', 'https://cpdiafrica.com/2015-winners/', 'ec8956637a99787bd197eacd77acce5e.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(103, NULL, '123', '4', 'THE IRELE', 'https://cpdiafrica.com/2015-winners/', '6974ce5ac660610b44d9b9fed0ff9548.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(104, NULL, '124', '4', 'THE ASSITA', 'https://cpdiafrica.com/2015-winners/', 'c9e1074f5b3f9fc8ea15d152add07294.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(105, NULL, '125', '4', 'THE AFAR SIDAMA', 'https://cpdiafrica.com/2015-winners/', '65b9eea6e1cc6bb9f0cd2a47751a186f.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(106, NULL, '126', '4', 'THE NORTHERN PAFTA', 'https://cpdiafrica.com/2015-winners/', 'f0935e4cd5920aa6c7c996a5ee53a70f.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(107, NULL, '127', '4', 'THE AMAI', 'https://cpdiafrica.com/2015-winners/', 'a97da629b098b75c294dffdc3e463904.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(108, NULL, '128', '4', 'THE MUDINA', 'https://cpdiafrica.com/2015-winners/', 'a3c65c2974270fd093ee8a9bf8ae7d0b.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(109, NULL, '129', '4', 'THE GOJO', 'https://cpdiafrica.com/2015-winners/', '2723d092b63885e0d7c260cc007e8b9d.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(110, NULL, '130', '4', 'THE NOKTECTURE', 'https://cpdiafrica.com/2015-winners/', '5f93f983524def3dca464469d2cf9f3e.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(111, NULL, '131', '4', 'THE KJONG', 'https://cpdiafrica.com/2015-winners/', '698d51a19d8a121ce581499d7b701668.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(112, NULL, '132', '4', 'THE SHEBAXUMITE', 'https://cpdiafrica.com/2015-winners/', '7f6ffaa6bb0b408017b62254211691b5.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(113, NULL, '133', '4', 'THE IJE', 'https://cpdiafrica.com/2015-winners/', '73278a4a86960eeb576a8fd4c9ec6997.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(114, NULL, '134', '4', 'THE UMOJA', 'https://cpdiafrica.com/2015-winners/', '5fd0b37cd7dbbb00f97ba6ce92bf5add.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(115, NULL, '135', '4', 'THE MASK', 'https://cpdiafrica.com/2015-winners/', '2b44928ae11fb9384c4cf38708677c48.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(116, NULL, '136', '4', 'THE MENA', 'https://cpdiafrica.com/2015-winners/', 'c45147dee729311ef5b5c3003946c48f.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(117, NULL, '137', '4', 'THE SAKHILE', 'https://cpdiafrica.com/2015-winners/', 'eb160de1de89d9058fcb0b968dbbbd68.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(118, NULL, '138', '4', 'DESERT WAVE', 'https://cpdiafrica.com/2015-winners/', '5ef059938ba799aaa845e1c2e8a762bd.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(119, NULL, '139', '4', 'THE TATA', 'https://cpdiafrica.com/2015-winners/', '07e1cd7dca89a1678042477183b7ac3f.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(120, NULL, '140', '4', 'THE EBUSUA FIE', 'https://cpdiafrica.com/2015-winners/', 'da4fb5c6e93e74d3df8527599fa62642.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(121, NULL, '141', '4', 'THE JALI', 'https://cpdiafrica.com/2015-winners/', '4c56ff4ce4aaf9573aa5dff913df997a.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(122, NULL, '91', '3', 'THE INARA', 'https://cpdiafrica.com/2017-winners/', 'a0a080f42e6f13b3a2df133f073095dd.JPG', '', '', '', NULL, 'view_hide', 0, NULL, NULL),
(123, '1a42ra514nicd9cip4f4', '219', '21', 'Test My First Design', 'http://lnk.ng/drive', '202cb962ac59075b964b07152d234b70.jpeg', 'Not Stated', 'Not Stated', 'Note stated', 'show', 'view_hide', 0, '2021-05-08', '12:33:22'),
(125, 'p886a1inr3783dfca3ic0', '227', '21', 'My 1st design', 'not stated', '3def184ad8f4755ff269862ea77393dd.jpeg', 'not stated', 'Not Stated', 'not stated', 'show', 'view_hide', 0, '2021-05-17', '08:13:56'),
(126, 'a3n5rdpi48i864c4a7fc1', '232', '21', 'Banji Test Edit', 'https://test.com', '069059b7ef840f0c74a814ec9237b6ec.png', 'test', 'Test', 'banjella', 'show', 'view_hide', 0, '2021-05-20', '14:39:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `panel_competition_design_materials`
--
ALTER TABLE `panel_competition_design_materials`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `panel_competition_design_materials`
--
ALTER TABLE `panel_competition_design_materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
