-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 08, 2021 at 04:31 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mckodevc_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `hash_id` varchar(255) NOT NULL,
  `portfolio` text,
  `bio` text,
  `phone_number` varchar(255) DEFAULT NULL,
  `facebook_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `linkedin_link` varchar(255) DEFAULT NULL,
  `instagram_link` varchar(225) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_1` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `image_3` varchar(255) DEFAULT NULL,
  `time_created` time NOT NULL,
  `date_created` date NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `login_status` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `verification` varchar(255) DEFAULT NULL,
  `profile_status` int(11) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL,
  `defaulted` int(11) DEFAULT NULL,
  `usname` text,
  `thumbnail` text,
  `is_premium` varchar(225) DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `country_id` char(10) DEFAULT NULL,
  `country_name` varchar(40) DEFAULT NULL,
  `country_short` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `firstname`, `lastname`, `email`, `hash`, `hash_id`, `portfolio`, `bio`, `phone_number`, `facebook_link`, `twitter_link`, `linkedin_link`, `instagram_link`, `location`, `image_1`, `image_2`, `image_3`, `time_created`, `date_created`, `last_login`, `last_logout`, `login_status`, `level`, `verification`, `profile_status`, `user_status`, `defaulted`, `usname`, `thumbnail`, `is_premium`, `expiration`, `country_id`, `country_name`, `country_short`) VALUES
(7, 'Akole', 'Banji', 'banjimayowa@gmail.com', '$2y$10$InNylUZQ76PKQC.UcOTZc.hEgm1fuArz2iSU7j8MFvZpDD9fteMYG', 'j90819542aBn72i', '555666777888999000', 'Web Developer at Mckodev | Trainer at Mckodev |  A passionate human. A bearer of good tidings and hope for a better society.', '08168785591', 'banjella2011', 'banjellaBLADE', '', 'passionate_kodev', 'Lagos', 'uploads/15647491426991170tmp_cam_5484793325690364323.jpg', '32ebcf7b-04f5-4655-98ad-5cd5c92d95f7', '1,2,3', '14:25:12', '2018-02-28', '2020-04-19 19:32:28', '2019-12-15 15:58:18', 'Logged In', 'MASTER', '1', 15, '1', NULL, 'banjella', 'thumb/15647491429317885tmp_cam_5484793325690364323.jpg', 'standard', '2021-04-22', '160', 'Nigeria', 'NG'),
(14, 'Akole', 'Banji', 'banjimayowa1@gmail.com', '$2y$10$jO/LwWCOtJ1I.oeP7PsAFum6ADpbjqPvsHRflxqIbNWfGasJj3vI6', 'admin9917900809', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '21:28:01', '2018-04-17', NULL, NULL, NULL, NULL, '1', 0, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Nmadili', 'Okwumabua', 'amadiusa@gmail.com', '$2y$10$W69Eln8/Hs2ZMCGFQPhe3uwlsgzoEKSDPQK8VNAFsYHAQTo6S1pde', '16124691590644naim9d866il6', '555666777888999000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13:05:59', '2021-02-04', NULL, NULL, NULL, 'MASTER', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Bello', 'Afeez', 'belloafeez7@gmail.com', '$2y$10$FzXj5lm9l5/7TUbe8NBQmeD8lazKyz/vallQ8BmPX1uXUQSgFw26m', '16136672891o3lb1l92557e92', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '09:54:49', '2021-02-18', NULL, NULL, NULL, '3', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Chuka Victor', 'ononye', 'chukaononye@gmail.com', '$2y$10$ylQReCno6jxovL12e6/41.fphNgKYJQrglTcj2yplWPVbTMChtEEe', '1613933460o6kt73r9h63c1u7i4-vc1a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11:51:00', '2021-02-21', NULL, NULL, NULL, '3', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Fawaz', 'Adelaja', 'Lilwhiez@gmail.com', '$2y$10$V23egB3L27yJ2tfdAiM4GeIiz13YMauTemgfvPAzGCTYgXHc1ev4S', '1616138005494zw55fa627a91', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:13:25', '2021-03-19', NULL, NULL, NULL, '3', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_auth`
--

CREATE TABLE `admin_auth` (
  `id` int(10) UNSIGNED NOT NULL,
  `auth` varchar(225) DEFAULT NULL,
  `created_by` varchar(225) DEFAULT NULL,
  `used_by` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_auth`
--

INSERT INTO `admin_auth` (`id`, `auth`, `created_by`, `used_by`, `date_created`, `time_created`) VALUES
(39, '191138', '1565905740k783la6e402o033', NULL, '2020-01-08', '10:33:09'),
(40, '446358', '1565905740k783la6e402o033', NULL, '2020-01-08', '17:32:19'),
(41, '986659', 'j90819542aBn72i', '15820373186h5mie15l1a6c3274', '2020-02-07', '15:17:28'),
(42, '729912', 'j90819542aBn72i', '1582040299142aoai8478bym978', '2020-02-18', '13:59:17'),
(43, '798371', 'j90819542aBn72i', '15847344529ve54312633d9mkco', '2020-03-20', '19:59:51'),
(44, '217393', 'j90819542aBn72i', '15878996009ey86a3d9on55t948u', '2020-04-26', '11:09:57'),
(45, '286197', 'j90819542aBn72i', '1587900415968505p5m273', '2020-04-26', '11:11:22'),
(46, '619362', 'j90819542aBn72i', NULL, '2020-05-24', '16:48:43'),
(47, '883224', 'j90819542aBn72i', NULL, '2020-05-24', '17:25:37'),
(48, '452916', 'j90819542aBn72i', '1590421922e4o782692adg4n09ur', '2020-05-25', '15:38:29'),
(49, '127052', 'j90819542aBn72i', '1590440727e51o1aay3if1u6d75lnw-e0i0mi', '2020-05-25', '16:53:50'),
(50, '847282', 'j90819542aBn72i', NULL, '2020-05-25', '22:42:13'),
(51, '632590', 'j90819542aBn72i', '1590537355141l896bl585e1o', '2020-05-26', '23:48:14'),
(52, '134052', 'j90819542aBn72i', NULL, '2020-05-26', '23:48:48'),
(53, '441834', 'j90819542aBn72i', NULL, '2021-01-31', '17:25:09'),
(54, '223097', 'j90819542aBn72i', '16124691590644naim9d866il6', '2021-02-03', '09:26:49'),
(55, '725030', 'j90819542aBn72i', '16136672891o3lb1l92557e92', '2021-02-18', '16:53:41'),
(56, '559025', 'j90819542aBn72i', '1613933460o6kt73r9h63c1u7i4-vc1a', '2021-02-21', '18:48:55'),
(57, '561748', 'j90819542aBn72i', '1616138005494zw55fa627a91', '2021-03-19', '00:39:44');

-- --------------------------------------------------------

--
-- Table structure for table `competition_order`
--

CREATE TABLE `competition_order` (
  `order_id` bigint(20) NOT NULL,
  `competition_id` bigint(20) NOT NULL,
  `amount` float NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `time` text NOT NULL,
  `pay_pal_id` text NOT NULL,
  `status` text NOT NULL,
  `payer_email` text NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `competition_order`
--

INSERT INTO `competition_order` (`order_id`, `competition_id`, `amount`, `user_id`, `time`, `pay_pal_id`, `status`, `payer_email`, `date`) VALUES
(2000049, 21, 100, 80, '1608498647', 'BLANK', 'CONFIRMED', 'BLANK', '1608498647'),
(2000050, 21, 100, 81, '1608499503', 'BLANK', 'PENDING', 'BLANK', '1608499503'),
(2000051, 2, 100, 81, '1608499577', 'BLANK', 'CONFIRMED', 'BLANK', '1608499577'),
(2000052, 4, 100, 81, '1608500701', 'BLANK', 'CONFIRMED', 'BLANK', '1608500701'),
(2000054, 21, 100, 82, '1608565102', '', 'CONFIRMED', 'BLANK', '1608565102'),
(2000055, 2, 100, 84, '1608638461', 'BLANK', 'PENDING', 'BLANK', '1608638461'),
(2000056, 21, 100, 84, '1608639191', 'BLANK', 'PENDING', 'BLANK', '1608639191'),
(2000057, 21, 100, 84, '1608639225', 'BLANK', 'PENDING', 'BLANK', '1608639225'),
(2000058, 3, 100, 91, '1608796060', 'BLANK', 'CONFIRMED', 'BLANK', '1608796060'),
(2000059, 21, 100, 7, '1608798441', 'BLANK', 'PENDING', 'BLANK', '1608798441'),
(2000060, 2, 100, 92, '1608800985', 'BLANK', 'CONFIRMED', 'BLANK', '1608800985'),
(2000061, 3, 100, 93, '1608803623', 'BLANK', 'CONFIRMED', 'BLANK', '1608803623'),
(2000062, 3, 100, 94, '1608804866', 'BLANK', 'CONFIRMED', 'BLANK', '1608804866'),
(2000063, 2, 100, 95, '1608806923', 'BLANK', 'CONFIRMED', 'BLANK', '1608806923'),
(2000064, 21, 100, 7, '1608810438', 'BLANK', 'PENDING', 'BLANK', '1608810438'),
(2000065, 21, 100, 7, '1608810893', 'BLANK', 'PENDING', 'BLANK', '1608810893'),
(2000066, 4, 100, 96, '1608812158', 'BLANK', 'CONFIRMED', 'BLANK', '1608812158'),
(2000067, 4, 100, 97, '1608813577', 'BLANK', 'CONFIRMED', 'BLANK', '1608813577'),
(2000068, 4, 100, 98, '1608814291', 'BLANK', 'CONFIRMED', 'BLANK', '1608814291'),
(2000069, 2, 100, 99, '1608831244', 'BLANK', 'CONFIRMED', 'BLANK', '1608831244'),
(2000070, 2, 100, 100, '1608831963', 'BLANK', 'CONFIRMED', 'BLANK', '1608831963'),
(2000071, 2, 100, 101, '1608832659', 'BLANK', 'CONFIRMED', 'BLANK', '1608832659'),
(2000072, 2, 100, 102, '1608832919', 'BLANK', 'CONFIRMED', 'BLANK', '1608832919'),
(2000073, 2, 100, 103, '1608833650', 'BLANK', 'CONFIRMED', 'BLANK', '1608833650'),
(2000074, 2, 100, 104, '1608835079', 'BLANK', 'CONFIRMED', 'BLANK', '1608835079'),
(2000075, 2, 100, 105, '1608835329', 'BLANK', 'CONFIRMED', 'BLANK', '1608835329'),
(2000076, 3, 100, 106, '1608837061', 'BLANK', 'CONFIRMED', 'BLANK', '1608837061'),
(2000077, 3, 100, 107, '1608838826', 'BLANK', 'CONFIRMED', 'BLANK', '1608838826'),
(2000078, 3, 100, 108, '1608839329', 'BLANK', 'CONFIRMED', 'BLANK', '1608839329'),
(2000079, 3, 100, 109, '1608898686', 'BLANK', 'CONFIRMED', 'BLANK', '1608898686'),
(2000080, 3, 100, 110, '1608899903', 'BLANK', 'CONFIRMED', 'BLANK', '1608899903'),
(2000081, 3, 100, 111, '1608900357', 'BLANK', 'CONFIRMED', 'BLANK', '1608900357'),
(2000082, 3, 100, 112, '1608903384', 'BLANK', 'CONFIRMED', 'BLANK', '1608903384'),
(2000083, 3, 100, 113, '1608903703', 'BLANK', 'CONFIRMED', 'BLANK', '1608903703'),
(2000084, 3, 100, 114, '1608904724', 'BLANK', 'CONFIRMED', 'BLANK', '1608904724'),
(2000085, 3, 100, 115, '1608905120', 'BLANK', 'CONFIRMED', 'BLANK', '1608905120'),
(2000086, 3, 100, 100, '1608905504', 'BLANK', 'CONFIRMED', 'BLANK', '1608905504'),
(2000087, 3, 100, 116, '1608905523', 'BLANK', 'CONFIRMED', 'BLANK', '1608905523'),
(2000088, 3, 100, 117, '1608907290', 'BLANK', 'CONFIRMED', 'BLANK', '1608907290'),
(2000089, 3, 100, 118, '1608907761', 'BLANK', 'CONFIRMED', 'BLANK', '1608907761'),
(2000090, 3, 100, 119, '1608908092', 'BLANK', 'CONFIRMED', 'BLANK', '1608908092'),
(2000091, 4, 100, 120, '1608911887', 'BLANK', 'CONFIRMED', 'BLANK', '1608911887'),
(2000092, 4, 100, 121, '1608912237', 'BLANK', 'CONFIRMED', 'BLANK', '1608912237'),
(2000093, 4, 100, 122, '1608912589', 'BLANK', 'CONFIRMED', 'BLANK', '1608912589'),
(2000094, 4, 100, 123, '1608912993', 'BLANK', 'CONFIRMED', 'BLANK', '1608912993'),
(2000095, 4, 100, 124, '1608913767', 'BLANK', 'CONFIRMED', 'BLANK', '1608913767'),
(2000096, 4, 100, 125, '1608914213', 'BLANK', 'CONFIRMED', 'BLANK', '1608914213'),
(2000097, 4, 100, 126, '1608914697', 'BLANK', 'CONFIRMED', 'BLANK', '1608914697'),
(2000098, 4, 100, 127, '1608915009', 'BLANK', 'CONFIRMED', 'BLANK', '1608915009'),
(2000099, 4, 100, 128, '1608915932', 'BLANK', 'CONFIRMED', 'BLANK', '1608915932'),
(2000100, 4, 100, 129, '1608916476', 'BLANK', 'CONFIRMED', 'BLANK', '1608916476'),
(2000101, 4, 100, 130, '1608917205', 'BLANK', 'CONFIRMED', 'BLANK', '1608917205'),
(2000102, 4, 100, 131, '1608917616', 'BLANK', 'CONFIRMED', 'BLANK', '1608917616'),
(2000103, 4, 100, 132, '1608967016', 'BLANK', 'CONFIRMED', 'BLANK', '1608967016'),
(2000104, 4, 100, 133, '1608968633', 'BLANK', 'CONFIRMED', 'BLANK', '1608968633'),
(2000105, 4, 100, 134, '1608969081', 'BLANK', 'CONFIRMED', 'BLANK', '1608969081'),
(2000106, 4, 100, 135, '1608969567', 'BLANK', 'CONFIRMED', 'BLANK', '1608969567'),
(2000107, 4, 100, 136, '1608971885', 'BLANK', 'CONFIRMED', 'BLANK', '1608971885'),
(2000108, 4, 100, 137, '1609011133', 'BLANK', 'CONFIRMED', 'BLANK', '1609011133'),
(2000109, 4, 100, 138, '1609011657', 'BLANK', 'CONFIRMED', 'BLANK', '1609011657'),
(2000110, 4, 100, 139, '1609012124', 'BLANK', 'CONFIRMED', 'BLANK', '1609012124'),
(2000111, 4, 100, 140, '1609012408', 'BLANK', 'CONFIRMED', 'BLANK', '1609012408'),
(2000112, 4, 100, 141, '1609012695', 'BLANK', 'CONFIRMED', 'BLANK', '1609012695'),
(2000113, 21, 100, 143, '1609094373', 'BLANK', 'PENDING', 'BLANK', '1609094373'),
(2000114, 21, 100, 7, '1609301922', 'BLANK', 'PENDING', 'BLANK', '1609301922'),
(2000115, 21, 100, 7, '1610811068', 'BLANK', 'PENDING', 'BLANK', '1610811068'),
(2000116, 21, 100, 0, '1610813653', 'BLANK', 'PENDING', 'BLANK', '1610813653'),
(2000117, 2, 100, 145, '1611218167', 'BLANK', 'PENDING', 'BLANK', '1611218167');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `user_id` varchar(225) DEFAULT NULL,
  `invoice_id` varchar(225) DEFAULT NULL,
  `payment_plan` varchar(225) DEFAULT NULL,
  `plan_id` varchar(225) DEFAULT NULL,
  `quantity` varchar(225) DEFAULT NULL,
  `amount_due` varchar(11) DEFAULT NULL,
  `amount_paid` varchar(11) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `title` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phonenumber` varchar(225) DEFAULT NULL,
  `description` text,
  `event_id` varchar(225) DEFAULT NULL,
  `place` varchar(225) DEFAULT NULL,
  `company` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL,
  `custom` text,
  `checkin` int(11) DEFAULT NULL,
  `agent` varchar(225) DEFAULT NULL,
  `ref` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `hash_id`, `user_id`, `invoice_id`, `payment_plan`, `plan_id`, `quantity`, `amount_due`, `amount_paid`, `status`, `title`, `name`, `email`, `phonenumber`, `description`, `event_id`, `place`, `company`, `date_created`, `time_created`, `custom`, `checkin`, `agent`, `ref`) VALUES
(1, '1616788542_2306', NULL, 'CRS_82608_VCNOEIDECOI_1616788542', 'Course', '29', 'quantity', '250', NULL, 'Unpaid', 'HT1202 - The Evolution and Transformation of African Centered Architecture ', 'Admin nwankwo', 'admin@test.com', '0645726181', 'Payment for HT1202 - The Evolution and Transformation of African Centered Architecture ', '1000091', 'place', 'company', '2021-03-26', '07:55:42', NULL, NULL, NULL, NULL),
(2, '1616790593_1357', NULL, 'CRS_66101_EDICOICOVEN_1616790593', 'Course', '19', 'quantity', '250', '1', 'paid', 'HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment ', 'Akole Banji test', 'banjimayowatest@gmail.com', '08168785591', 'Payment for HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment ', '1000095', 'place', 'company', '2021-03-26', '08:29:53', NULL, NULL, NULL, NULL),
(3, '1616791292_1907', NULL, 'CRS_25858_OECICNVOIED_1616791292', 'Course', '26', 'quantity', '250', '1', 'paid', 'HT1206 â€“ Green Architecture from African Centered Perspectives', 'Akole Banji test', 'banjimayowatest@gmail.com', '08168785591', 'Payment for HT1206 â€“ Green Architecture from African Centered Perspectives', '1000096', 'place', 'company', '2021-03-26', '08:41:32', NULL, NULL, NULL, NULL),
(4, '1616791561_2201', NULL, 'CRS_88792_OOECIVCNIED_1616791561', 'Course', '27', 'quantity', '168', NULL, 'Unpaid', 'HT1208 - The Architecture of Ghana, from Ancestry to Independence and Beyond', 'Akole Banji test', 'banjimayowatest@gmail.com', '08168785591', 'Payment for HT1208 - The Architecture of Ghana, from Ancestry to Independence and Beyond', '1000097', 'place', 'company', '2021-03-26', '08:46:01', NULL, NULL, NULL, NULL),
(5, '1616794535_2362', NULL, 'CRS_13521_IVCEDENOICO_1616794535', 'Course', '29', 'quantity', '250', '1', 'paid', 'HT1202 - The Evolution and Transformation of African Centered Architecture ', 'Akole Banji test', 'banjimayowatest@gmail.com', '08168785591', 'Payment for HT1202 - The Evolution and Transformation of African Centered Architecture ', '1000098', 'place', 'company', '2021-03-26', '09:35:35', NULL, NULL, NULL, NULL),
(6, '1616854044_5818', NULL, 'COMP_82679_OIIENEDVCOC_1616854044', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', NULL, 'Unpaid', 'African Heritage Architecture Competition\r\n', 'Test Test Banji', 'banjimayowa@gmail.com', '08168785591', 'Payment for African Heritage Architecture Competition\r\n', '2000172', 'place', 'company', '2021-03-27', '02:07:24', NULL, NULL, NULL, NULL),
(7, '1616858044_4821', NULL, 'COMP_66700_CEOOEVCINDI_1616858044', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', '1', 'paid', 'African Heritage Architecture Competition\r\n', 'Akole Banji test', 'banjimayowatest@gmail.com', '08168785591', 'Payment for African Heritage Architecture Competition\r\n', '2000173', 'place', 'company', '2021-03-27', '03:14:04', NULL, NULL, NULL, NULL),
(8, '1616860580_3227', NULL, 'CRS_44757_IEEDOCOCNIV_1616860580', 'Course', '24', 'quantity', '250', NULL, 'Unpaid', 'HT1201 - Theoretical Frameworks in Afrocentric Architecture', 'Akole Banji test', 'banjimayowatest@gmail.com', '08168785591', 'Payment for HT1201 - Theoretical Frameworks in Afrocentric Architecture', '1000099', 'place', 'company', '2021-03-27', '03:56:20', NULL, NULL, NULL, NULL),
(9, '1616861540_1844', NULL, 'CRS_28222_OOICVCNEDEI_1616861540', 'Course', '33', 'quantity', '250', NULL, 'Unpaid', 'HT1214 - Towards an African American Inspired Architecture', 'Test Test Banji', 'banjimayowa@gmail.com', '08168785591', 'Payment for HT1214 - Towards an African American Inspired Architecture', '1000100', 'place', 'company', '2021-03-27', '04:12:20', NULL, NULL, NULL, NULL),
(10, '1617002497_5200', NULL, 'COMP_87989_ENCIEVOOCDI_1617002497', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', NULL, 'Unpaid', 'African Heritage Architecture Competition\r\n', 'Senta Schrewe', 'senta-schrewe@t-online.de', '', 'Payment for African Heritage Architecture Competition\r\n', '2000174', 'place', 'company', '2021-03-29', '07:21:37', NULL, NULL, NULL, NULL),
(11, '1617002697_1265', NULL, 'COMP_44371_CNIOCEDIVEO_1617002697', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', NULL, 'Unpaid', 'African Heritage Architecture Competition\r\n', 'Senta Schrewe', 'senta-schrewe@t-online.de', '', 'Payment for African Heritage Architecture Competition\r\n', '2000175', 'place', 'company', '2021-03-29', '07:24:57', NULL, NULL, NULL, NULL),
(12, '1617070500_2773', NULL, 'CRS_29724_OEOENIDCCIV_1617070500', 'Course', '33', 'quantity', '250', NULL, 'Unpaid', 'HT1214 - Towards an African American Inspired Architecture', 'vic5or test victor test', 'agro@gmail.com', '', 'Payment for HT1214 - Towards an African American Inspired Architecture', '1000101', 'place', 'company', '2021-03-30', '02:15:00', NULL, NULL, NULL, NULL),
(13, '1617239886_7150', NULL, 'COMP_94062_CCVOEIEIOND_1617239886', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', NULL, 'Unpaid', 'African Heritage Architecture Competition\r\n', 'Emad shawky', 'emad148538@bue.edu.eg', '01098727177', 'Payment for African Heritage Architecture Competition\r\n', '2000146', 'place', 'company', '2021-04-01', '01:18:06', NULL, NULL, NULL, NULL),
(14, '1617303259_5644', NULL, 'CRS_28523_NCIEICODVEO_1617303259', 'Course', '19', 'quantity', '250', NULL, 'Unpaid', 'HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment ', 'Adebowale Christopher Odusanya', 'chadeodus@gmail.com', '2403555362', 'Payment for HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment ', '1000102', 'place', 'company', '2021-04-01', '06:54:19', NULL, NULL, NULL, NULL),
(15, '1617536631_7363', NULL, 'CRS_10949_VCINCOEDOEI_1617536631', 'Course', '29', 'quantity', '200', NULL, 'Unpaid', 'HT1202 - The Evolution and Transformation of African Centered Architecture ', 'Chuka Victor testing', 'abie@gmail.com', '', 'Payment for HT1202 - The Evolution and Transformation of African Centered Architecture ', '1000103', 'place', 'company', '2021-04-04', '11:43:51', NULL, NULL, NULL, NULL),
(16, '1617657326_2926', NULL, 'CRS_25246_COICEIODVEN_1617657326', 'Course', '33', 'quantity', '250', NULL, 'Unpaid', 'HT1214 - Towards an African American Inspired Architecture', 'Musa Muhammad', 'musa14muhammad@gmail.com', '(708) 971-3710', 'Payment for HT1214 - Towards an African American Inspired Architecture', '1000104', 'place', 'company', '2021-04-05', '09:15:26', NULL, NULL, NULL, NULL),
(17, '1617733875_8468', NULL, 'CRS_25695_OVCEEOIDNCI_1617733875', 'Course', '31', 'quantity', '250', NULL, 'Unpaid', 'HT1211 â€“ Sustainability in African Traditional Architecture.', 'Aliyu Muhammad', 'aleeyulfc@gmail.com', '', 'Payment for HT1211 â€“ Sustainability in African Traditional Architecture.', '1000105', 'place', 'company', '2021-04-06', '06:31:15', NULL, NULL, NULL, NULL),
(18, '1617734334_3563', NULL, 'CRS_63924_EICIEVCODNO_1617734334', 'Course', '29', 'quantity', '250', NULL, 'Unpaid', 'HT1202 - The Evolution and Transformation of African Centered Architecture ', 'Emeka Ukaga', 'emekaukaga2@gmail.com', '6513359614', 'Payment for HT1202 - The Evolution and Transformation of African Centered Architecture ', '1000106', 'place', 'company', '2021-04-06', '06:38:54', NULL, NULL, NULL, NULL),
(19, '1617763317_7789', NULL, 'COMP_12694_OEVEIIDCNOC_1617763317', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', NULL, 'Unpaid', 'African Heritage Architecture Competition\r\n', 'Akole TEST Banji', 'boardspeck11111444@gmail.com', '08168785591', 'Payment for African Heritage Architecture Competition\r\n', '2000176', 'place', 'company', '2021-04-07', '02:41:57', NULL, NULL, NULL, NULL),
(20, '1617763862_7641', NULL, 'COMP_16546_VCDECEONIIO_1617763862', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', NULL, 'Unpaid', 'African Heritage Architecture Competition\r\n', 'Akole TEST Banji', 'boardspeck11111444@gmail.com', '08168785591', 'Payment for African Heritage Architecture Competition\r\n', '2000177', 'place', 'company', '2021-04-07', '02:51:02', NULL, NULL, NULL, NULL),
(21, '1617865027_5262', NULL, 'COMP_50551_VOECEINOIDC_1617865027', 'African Heritage Architecture Competition\r\n', '21', 'quantity', '100', NULL, 'Unpaid', 'African Heritage Architecture Competition\r\n', 'Sourabh yadav', 'sourabh@planin.xyz', '+91 8287949660', 'Payment for African Heritage Architecture Competition\r\n', '2000178', 'place', 'company', '2021-04-08', '06:57:07', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `panel_competitions`
--

CREATE TABLE `panel_competitions` (
  `id` int(20) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `input_name` varchar(225) DEFAULT NULL,
  `text_description` text,
  `image_1` varchar(225) DEFAULT NULL,
  `input_year` varchar(20) DEFAULT NULL,
  `input_amount` varchar(10) DEFAULT NULL,
  `input_live` varchar(50) DEFAULT NULL,
  `visibility` varchar(20) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `panel_competitions`
--

INSERT INTO `panel_competitions` (`id`, `hash_id`, `input_name`, `text_description`, `image_1`, `input_year`, `input_amount`, `input_live`, `visibility`, `date_created`, `time_created`) VALUES
(2, '', 'African Heritage Architecture Competition\r\n', 'The African Heritage Architecture 2020 DESIGN BRIEF is created to celebrate and preserve the architectural identity of the Cultures of Africa and the Diaspora. Designed as the quintessential Cultural Community Center, the Competition Project will serve as the ultimate example of sustainable design in any community in which it is built. The Objective is to design a space where guests fall in love with the cultures and architectural traditions of the African Diaspora, just by experiencing the beauty, comfort, hospitality and heritage embedded in the Masterpiece! Building upon the elements of African traditional architecture highlighted in the Design Brief, the knowledge of contemporary character and lifestyle of Africa\'s diverse continental and diaspora Ethnic Groups, participating architects and collaborating design teams will embark on a brave exercise in the ‘evolution and transformation’ of African architecture into a Signature Style for our global audience!', '', '2020', '100', 'Yes', '', NULL, NULL),
(3, '', 'African Heritage Architecture Competition\r\n', 'The African Heritage Architecture 2017 DESIGN BRIEF is created to celebrate and preserve the architectural identity of the Cultures of Africa and the Diaspora. Designed as the quintessential Cultural Community Center, the Competition Project will serve as the ultimate example of sustainable design in any community in which it is built. The Objective is to design a space where guests fall in love with the cultures and architectural traditions of the African Diaspora, just by experiencing the beauty, comfort, hospitality and heritage embedded in the Masterpiece! Building upon the elements of African traditional architecture highlighted in the Design Brief, the knowledge of contemporary character and lifestyle of Africa\'s diverse continental and diaspora Ethnic Groups, participating architects and collaborating design teams will embark on a brave exercise in the ‘evolution and transformation’ of African architecture into a Signature Style for our global audience!', '', '2017', '100', 'Yes', '', NULL, NULL),
(4, '', 'African Heritage Architecture Competition\r\n', 'The African Heritage Architecture 2015 DESIGN BRIEF is created to celebrate and preserve the architectural identity of the Cultures of Africa and the Diaspora. Designed as the quintessential Cultural Community Center, the Competition Project will serve as the ultimate example of sustainable design in any community in which it is built. The Objective is to design a space where guests fall in love with the cultures and architectural traditions of the African Diaspora, just by experiencing the beauty, comfort, hospitality and heritage embedded in the Masterpiece! Building upon the elements of African traditional architecture highlighted in the Design Brief, the knowledge of contemporary character and lifestyle of Africa\'s diverse continental and diaspora Ethnic Groups, participating architects and collaborating design teams will embark on a brave exercise in the ‘evolution and transformation’ of African architecture into a Signature Style for our global audience!', '', '2015', '100', 'Yes', '', NULL, NULL),
(21, '', 'African Heritage Architecture Competition\r\n', 'The African Heritage Architecture 2021 DESIGN BRIEF is created to celebrate and preserve the architectural identity of the Cultures of Africa and the Diaspora. Designed as the quintessential Cultural Community Center, the Competition Project will serve as the ultimate example of sustainable design in any community in which it is built. The Objective is to design a space where guests fall in love with the cultures and architectural traditions of the African Diaspora, just by experiencing the beauty, comfort, hospitality and heritage embedded in the Masterpiece! Building upon the elements of African traditional architecture highlighted in the Design Brief, the knowledge of contemporary character and lifestyle of Africa\'s diverse continental and diaspora Ethnic Groups, participating architects and collaborating design teams will embark on a brave exercise in the ‘evolution and transformation’ of African architecture into a Signature Style for our global audience!', '', '2021', '100', 'Yes', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `panel_competition_design_materials`
--

CREATE TABLE `panel_competition_design_materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `user_id` varchar(225) DEFAULT NULL,
  `competition_id` varchar(225) DEFAULT NULL,
  `input_name` text,
  `input_online_drive` varchar(225) DEFAULT NULL,
  `image_1` varchar(225) DEFAULT NULL,
  `visibility` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `panel_competition_design_materials`
--

INSERT INTO `panel_competition_design_materials` (`id`, `hash_id`, `user_id`, `competition_id`, `input_name`, `input_online_drive`, `image_1`, `visibility`, `date_created`, `time_created`) VALUES
(59, NULL, '80', '21', 'Ile Oro', 'google drive', '093f65e080a295f8076b1c5722a46aa2.jpg', NULL, NULL, NULL),
(69, NULL, '92', '2', 'Yalam - Jarawa Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '14bfa6bb14875e45bba028a21ed38046.PNG', NULL, NULL, NULL),
(70, NULL, '81', '2', 'IlÃ© á»Œrá» - Yoruba Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '7cbbc409ec990f19c78c75bd1e06f215.PNG', NULL, NULL, NULL),
(71, NULL, '93', '3', 'THE ITAN COMPLEX', 'https://cpdiafrica.com/2017-winners/', 'e2c420d928d4bf8ce0ff2ec19b371514.JPG', NULL, NULL, NULL),
(72, NULL, '94', '3', 'THE EKYANZI HOUSE', 'https://cpdiafrica.com/2017-winners/', '32bb90e8976aab5298d5da10fe66f21d.JPG', NULL, NULL, NULL),
(74, NULL, '95', '2', 'Njikota â€“ Igbo Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'ad61ab143223efbc24c7d2583be69251.PNG', NULL, NULL, NULL),
(75, NULL, '96', '4', 'THE INNA', 'https://cpdiafrica.com/2015-winners/', 'd09bf41544a3365a46c9077ebb5e35c3.JPG', NULL, NULL, NULL),
(76, NULL, '97', '4', 'THE IFA HOUSE ', 'https://cpdiafrica.com/2015-winners/', 'fbd7939d674997cdb4692d34de8633c4.JPG', NULL, NULL, NULL),
(77, NULL, '98', '4', 'THE TATA FU', 'https://cpdiafrica.com/2015-winners/', '28dd2c7955ce926456240b2ff0100bde.JPG', NULL, NULL, NULL),
(78, NULL, '100', '2', 'Nnukwu Nmanwu - Igbo Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '35f4a8d465e6e1edc05f3d8ab658c551.PNG', NULL, NULL, NULL),
(79, NULL, '99', '2', 'Emathafeni â€“ Xhosa Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'd1fe173d08e959397adf34b1d77e88d7.PNG', NULL, NULL, NULL),
(80, NULL, '101', '2', 'Resilience â€“ Haitian Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'f033ab37c30201f73f142449d037028d.PNG', NULL, NULL, NULL),
(81, NULL, '102', '2', 'Gidan â€“ Hausa Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '43ec517d68b6edd3015b3edc9a11367b.PNG', NULL, NULL, NULL),
(82, NULL, '103', '2', 'Ti-kay â€“ Haitian Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '9778d5d219c5080b9a6a17bef029331c.PNG', NULL, NULL, NULL),
(83, NULL, '104', '2', 'Anyanwu - Igbo Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', 'fe9fc289c3ff0af142b6d3bead98a923.PNG', NULL, NULL, NULL),
(84, NULL, '105', '2', 'Southern Ridley African American Resort & Cultural Center', 'https://drive.google.com/drive/folders/1d0m7mFNrNuqhFGCgMBlD_ynjW5Umd68T?usp=sharing', '68d30a9594728bc39aa24be94b319d21.PNG', NULL, NULL, NULL),
(85, NULL, '106', '3', 'THE PUULAK HOUSE', 'https://cpdiafrica.com/2017-winners/', '3ef815416f775098fe977004015c6193.JPG', NULL, NULL, NULL),
(86, NULL, '107', '3', 'THE NYUBU HOUSE', 'https://cpdiafrica.com/2017-winners/', '93db85ed909c13838ff95ccfa94cebd9.JPG', NULL, NULL, NULL),
(87, NULL, '108', '3', 'THE BA RRASSO HOUSE', 'https://cpdiafrica.com/2017-winners/', 'c7e1249ffc03eb9ded908c236bd1996d.JPG', NULL, NULL, NULL),
(88, NULL, '109', '3', 'THE ORACULAR HOUSE', 'https://cpdiafrica.com/2017-winners/', '2a38a4a9316c49e5a833517c45d31070.JPG', NULL, NULL, NULL),
(89, NULL, '110', '3', 'THE EDAR HOUSE', 'https://cpdiafrica.com/2017-winners/', '7647966b7343c29048673252e490f736.JPG', NULL, NULL, NULL),
(90, NULL, '111', '3', 'THE ATIKAH HOUSE', 'https://cpdiafrica.com/2017-winners/', '8613985ec49eb8f757ae6439e879bb2a.JPG', NULL, NULL, NULL),
(91, NULL, '112', '3', 'THE DEMETRIA GANDU HOUSE', 'https://cpdiafrica.com/2017-winners/', '54229abfcfa5649e7003b83dd4755294.JPG', NULL, NULL, NULL),
(92, NULL, '113', '3', 'THE SESHAT HOUSE', 'https://cpdiafrica.com/2017-winners/', '92cc227532d17e56e07902b254dfad10.JPG', NULL, NULL, NULL),
(93, NULL, '114', '3', 'THE ILE IWA', 'https://cpdiafrica.com/2017-winners/', '98dce83da57b0395e163467c9dae521b.JPG', NULL, NULL, NULL),
(94, NULL, '115', '3', 'THE ANA KATO HOUSE', 'https://cpdiafrica.com/2017-winners/', 'f4b9ec30ad9f68f89b29639786cb62ef.JPG', NULL, NULL, NULL),
(95, NULL, '116', '3', 'THE AKUA HOUSE', 'https://cpdiafrica.com/2017-winners/', '812b4ba287f5ee0bc9d43bbf5bbe87fb.JPG', NULL, NULL, NULL),
(96, NULL, '100', '3', 'THE OMI UGOMMA HOUSE', 'https://cpdiafrica.com/2017-winners/', '26657d5ff9020d2abefe558796b99584.jpg', NULL, NULL, NULL),
(97, NULL, '117', '3', 'THE JITUWA HOUSE', 'https://cpdiafrica.com/2017-winners/', 'e2ef524fbf3d9fe611d5a8e90fefdc9c.jpg', NULL, NULL, NULL),
(98, NULL, '118', '3', 'THE MAGAJIYA COMPLEX', 'https://cpdiafrica.com/2017-winners/', 'ed3d2c21991e3bef5e069713af9fa6ca.jpg', NULL, NULL, NULL),
(99, NULL, '119', '3', 'THE MONOLITH OF KASULO', 'https://cpdiafrica.com/2017-winners/', 'ac627ab1ccbdb62ec96e702f07f6425b.jpg', NULL, NULL, NULL),
(100, NULL, '120', '4', 'THE OMO', 'https://cpdiafrica.com/2015-winners/', 'f899139df5e1059396431415e770c6dd.jpg', NULL, NULL, NULL),
(101, NULL, '121', '4', 'THE FUSION POINT', 'https://cpdiafrica.com/2015-winners/', '38b3eff8baf56627478ec76a704e9b52.JPG', NULL, NULL, NULL),
(102, NULL, '122', '4', 'THE CETIANA', 'https://cpdiafrica.com/2015-winners/', 'ec8956637a99787bd197eacd77acce5e.JPG', NULL, NULL, NULL),
(103, NULL, '123', '4', 'THE IRELE', 'https://cpdiafrica.com/2015-winners/', '6974ce5ac660610b44d9b9fed0ff9548.JPG', NULL, NULL, NULL),
(104, NULL, '124', '4', 'THE ASSITA', 'https://cpdiafrica.com/2015-winners/', 'c9e1074f5b3f9fc8ea15d152add07294.JPG', NULL, NULL, NULL),
(105, NULL, '125', '4', 'THE AFAR SIDAMA', 'https://cpdiafrica.com/2015-winners/', '65b9eea6e1cc6bb9f0cd2a47751a186f.JPG', NULL, NULL, NULL),
(106, NULL, '126', '4', 'THE NORTHERN PAFTA', 'https://cpdiafrica.com/2015-winners/', 'f0935e4cd5920aa6c7c996a5ee53a70f.JPG', NULL, NULL, NULL),
(107, NULL, '127', '4', 'THE AMAI', 'https://cpdiafrica.com/2015-winners/', 'a97da629b098b75c294dffdc3e463904.JPG', NULL, NULL, NULL),
(108, NULL, '128', '4', 'THE MUDINA', 'https://cpdiafrica.com/2015-winners/', 'a3c65c2974270fd093ee8a9bf8ae7d0b.JPG', NULL, NULL, NULL),
(109, NULL, '129', '4', 'THE GOJO', 'https://cpdiafrica.com/2015-winners/', '2723d092b63885e0d7c260cc007e8b9d.JPG', NULL, NULL, NULL),
(110, NULL, '130', '4', 'THE NOKTECTURE', 'https://cpdiafrica.com/2015-winners/', '5f93f983524def3dca464469d2cf9f3e.JPG', NULL, NULL, NULL),
(111, NULL, '131', '4', 'THE KJONG', 'https://cpdiafrica.com/2015-winners/', '698d51a19d8a121ce581499d7b701668.JPG', NULL, NULL, NULL),
(112, NULL, '132', '4', 'THE SHEBAXUMITE', 'https://cpdiafrica.com/2015-winners/', '7f6ffaa6bb0b408017b62254211691b5.JPG', NULL, NULL, NULL),
(113, NULL, '133', '4', 'THE IJE', 'https://cpdiafrica.com/2015-winners/', '73278a4a86960eeb576a8fd4c9ec6997.JPG', NULL, NULL, NULL),
(114, NULL, '134', '4', 'THE UMOJA', 'https://cpdiafrica.com/2015-winners/', '5fd0b37cd7dbbb00f97ba6ce92bf5add.JPG', NULL, NULL, NULL),
(115, NULL, '135', '4', 'THE MASK', 'https://cpdiafrica.com/2015-winners/', '2b44928ae11fb9384c4cf38708677c48.JPG', NULL, NULL, NULL),
(116, NULL, '136', '4', 'THE MENA', 'https://cpdiafrica.com/2015-winners/', 'c45147dee729311ef5b5c3003946c48f.JPG', NULL, NULL, NULL),
(117, NULL, '137', '4', 'THE SAKHILE', 'https://cpdiafrica.com/2015-winners/', 'eb160de1de89d9058fcb0b968dbbbd68.JPG', NULL, NULL, NULL),
(118, NULL, '138', '4', 'DESERT WAVE', 'https://cpdiafrica.com/2015-winners/', '5ef059938ba799aaa845e1c2e8a762bd.JPG', NULL, NULL, NULL),
(119, NULL, '139', '4', 'THE TATA', 'https://cpdiafrica.com/2015-winners/', '07e1cd7dca89a1678042477183b7ac3f.JPG', NULL, NULL, NULL),
(120, NULL, '140', '4', 'THE EBUSUA FIE', 'https://cpdiafrica.com/2015-winners/', 'da4fb5c6e93e74d3df8527599fa62642.JPG', NULL, NULL, NULL),
(121, NULL, '141', '4', 'THE JALI', 'https://cpdiafrica.com/2015-winners/', '4c56ff4ce4aaf9573aa5dff913df997a.JPG', NULL, NULL, NULL),
(122, NULL, '91', '3', 'THE INARA', 'https://cpdiafrica.com/2017-winners/', 'a0a080f42e6f13b3a2df133f073095dd.JPG', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `panel_courses`
--

CREATE TABLE `panel_courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `input_course_name` text,
  `select_session` varchar(225) DEFAULT NULL,
  `text_course_details` text,
  `text_course_description` text,
  `input_course_amount` varchar(255) DEFAULT NULL,
  `input_course_promotion` varchar(225) DEFAULT NULL,
  `input_live` text,
  `input_conference_link` text,
  `visibility` varchar(20) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_courses`
--

INSERT INTO `panel_courses` (`id`, `hash_id`, `input_course_name`, `select_session`, `text_course_details`, `text_course_description`, `input_course_amount`, `input_course_promotion`, `input_live`, `input_conference_link`, `visibility`, `date_created`, `time_created`) VALUES
(18, '234567898xdfgh', 'HT1203 - Traditions and Heritage in Igbo Architecture', '1', '<p><strong>SESSION ONE</strong></p><p><b><i>February 13th - March 20th, 2021</i></b></p><ul><li>\r\n\r\nMeeting Time: 2hrs once a week for\r\n6 Weeks</li><li>\r\n\r\nVenue: Zoom </li><li>\r\n\r\nDay and Time: Saturdays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>\r\n\r\nRequirement for Certificate designation:\r\nOne research assignment&nbsp; </li><li>\r\n\r\nOutcomes: Audit or Certificate </li><li>\r\n\r\nRequired Readings: To be provided\r\nin the course online folder: a bibliography of African architecture\r\npublications, video documentaries and websites.</li></ul><p><strong>Course Overview</strong>:</p><p>\r\n\r\nWeek 1:\r\nTraditional Architecture Concepts</p><p>\r\n\r\nWeek 2:\r\nElements and Forms of Traditional Architecture in Nigeria</p><p>\r\n\r\nWeek 3:\r\nFactors of Influence and Inspiration</p><p>\r\n\r\nWeek 4:\r\nHeritage, Conservation and Preservation of Traditional Architecture</p><p>\r\n\r\nWeek 5:\r\nContemporary Status of Traditional Architecture</p><p>\r\n\r\nWeek 6:\r\nInvestigative Field Study/Research (Report)&nbsp;&nbsp;</p><p><strong>Research\r\nRequirement: Earn the CPDI Africa Certificate</strong></p><p>\r\n\r\nIdentification\r\nand documentation of one Traditional and Heritage Architecture project in the\r\ncandidates Local Govt Area (LGA) and State in Nigeria (or the candidates country if not Nigerian).</p>', 'SESSION ONE: Introduction to traditional architecture concepts, elements and forms; factors of influence and inspiration in the context of location, climate, culture and tradition, religion, material and technology; heritage, conservation and preservation of traditional architecture; contemporary status of traditional architecture and investigative field study/research geared towards enabling the students to acquire firsthand experience of some relevant information, communicated during the lectures.', '250', '250', 'No', 'http://zoom.com/updated', NULL, NULL, NULL),
(19, '09876sdfghj', 'HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment ', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 12pm - 2pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p><strong>Course\r\nOverview:</strong></p><p>\r\n\r\nWeek\r\nOne: Introduction to Yoruba people and their architecture</p><p>\r\n\r\nWeek\r\nTwo: Culture and Lifestyle in Traditional Africa</p><p>\r\n\r\nWeek Three:\r\nSpirituality in the Built Environment</p><p>\r\n\r\nWeek\r\nFour: The Elements of Aesthetics, Decorative and Symbolic</p><p>\r\n\r\nWeek Five:\r\nMaterials and Community Development Philosophies</p><p>\r\n\r\nWeek\r\nSix: The Future, Evolution and Transformation of Yoruba traditional\r\narchitecture</p><p><strong>Research\r\nRequirement: Earn the CPDI Africa Certificate</strong></p>', 'SESSION TWO: This course introduces students to Yoruba traditional architecture, art and culture by outlining theories, philosophy and aesthetics that serves as guiding principles. The course gives an overview of a traditional style dictated by culture, environmental conditions and available material. The course outlines important architectural elements and languages associated with  Yoruba traditional architecture and how these elements can be utilised in solving contemporary environmental issues. Students will be introduced to the Yoruba culture that dictates their worldview and their construction methods.', '250', '250', 'Yes', 'http://zoom.com', NULL, NULL, NULL),
(22, '2345678sdfghj', 'HT1202 - The Evolution and Transformation of African Centered Architecture', '1', '<p><strong>SESSION ONE</strong></p><p><b>February 14th to March 20th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><div><p class=\"MsoNormal\"><b><u>Course Overview:</u></b></p>\r\n\r\n<p class=\"MsoNormal\">Week One:\r\nIntroduction to African Centered Architecture</p>\r\n\r\n<p class=\"MsoNormal\">Week Two: Culture\r\nand Lifestyle in Traditional Africa</p>\r\n\r\n<p class=\"MsoNormal\">Week Three:\r\nSpirituality in the Built Environment</p>\r\n\r\n<p class=\"MsoNormal\">Week Four: The\r\nElements of Aesthetics, Decorative and Symbolic</p>\r\n\r\n<p class=\"MsoNormal\">Week Five:\r\nMaterials and Community Development Philosophies</p>\r\n\r\n<p class=\"MsoNormal\">Week Six: The\r\nFuture, Evolution and Transformation of African &amp; Diaspora architecture</p>\r\n\r\n<p class=\"Body\"><b>Research\r\nRequirement: Earn the CPDI Africa Certificate</b></p>\r\n\r\n</div><div><br /></div>', 'SESSION ONE: This course introduces students to the concepts for developing new African architectural languages, inspired by the culture and technology of traditional and contemporary African societies. The course surveys both the African and Diaspora built environment, teaching design practices that are culturally and environmentally sustainable. Students will be introduced to the research initiatives of CPDI Africa.', '250', '250', 'No', 'http://zoom.com', NULL, NULL, NULL),
(23, '657987654dfgh', 'HT1210 - Anti-Developed Clinic: The Becoming of a Design Culture Course', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Sundays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p><strong>Course Overview:</strong></p><p>Week One: Introduction to African Talismanic Art Practices</p><p>Week Two: Creating a Cultured Continent and Destroying Anti-Black Sentiments in Design</p><p>Week Three: Illness of Over-Connectivity - Returning to Analog</p><p>Week Four: Processing Patterns in Natures - Cradling Customs</p><p>Week Five: Healing Colonial Trauma through Sensory Art</p><p>Week Six: Grounding Micro Motifs into Public Memory Assignment</p>', 'SESSION TWO - The Becoming of a Design Culture is an experimental studio on how society treats the term â€œculture.â€ Colonial traumas have understandably stunted an entire continent to heal, and this is displayed through the heedless hodgepodge of development misinterpreted as societal progress. The clinic will be retraining the brain on how to feel and will deconstruct all notions of what an African aesthetic should look like. It will radically heal and root designers with ancestral traditions to create informed and soulful spaces.', '250', '250', 'Yes', 'http://zoom.com', NULL, NULL, NULL),
(24, '1234560987edfghj', 'HT1201 - Theoretical Frameworks in Afrocentric Architecture', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 12pm - 2pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p><strong>Course Overview:</strong></p><p>Week One: Introduction to Afrocentric architecture- a theory</p><p>Week Two: KEMET (Egypt)- The Beginning- Pyramid Age, Sakkara, Pyramid Age- Dashur to Giza</p><p>Week Three: KEMET- Temples, Luxor, Karnak &amp; Waset (Thebes), Abydos, Denderra, Essna, Edfu Kom</p><p>Ombo, Philae, Abu Simbel</p><p>Week Four: ABYSSINIA (Ethiopia)- Addis Ababa, Aksum, Lalibala (stone-cut churches), Gondar (city</p><p>of castles)</p><p>Week Five: ZIMBABWE- Great Zimbabwe, Khami, Bulawayo (Matapos)</p><p>WEST AFRICAN KINGDOMS- Ghana, Mali, Songhai &amp; Benin</p><p>Week Six: Theoretical manifestation-recognition and Practicum (African American Architects)</p><p>Theoretical embrace and impact- Global recognition- CPDI Africa</p><p><b>Research\r\nRequirement: Earn the CPDI Africa Certificate<br /></b>Produce a contribution to the\r\nresearch on Afrocentric Architecture, by submitting a paper on the works of an\r\narchitect or theorist who has successfully applied the principles covered in\r\nthe course.</p>', 'SESSION TWO: Introduction to the African contribution to global architecture, from antiquity till the present. Covers theoretical frameworks used by architects in their built works, throughout Africa and the Diaspora. The course will be framed by research and theoretical frameworks published by Professor Hughes: Afrocentric Architecture: a design primer.', '250', '250', 'Yes', 'http://zoom.com', NULL, NULL, NULL),
(25, '0987sdfghj09876', 'HT1211 â€“ Sustainability in African Traditional Architecture.', '1', '<p><strong>SESSION ONE</strong></p><p><b>February 13th to March 20th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Sundays, 12pm - 2pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p class=\"MsoNoSpacing\"><b>Course Overview:</b></p><p class=\"MsoNoSpacing\">Week One: Introduction to\r\nAfrican Designs and Buildings</p><p class=\"MsoNoSpacing\">Week Two: The technology\r\nused in the construction of African buildings</p><p class=\"MsoNoSpacing\">Week Three: Active and\r\nPassive design Strategies in Design &amp; Build</p><p class=\"MsoNoSpacing\">Week Four: Understanding\r\nthe low tech used by Africa designers and builders </p><p class=\"MsoNoSpacing\">Week Five: The currents\r\ntrends in African traditional architecture &nbsp;</p><p class=\"MsoNoSpacing\">Week Six: The role of\r\nAfrican traditional architecture beyond solving the problem of global warming\r\nand climate change </p><p class=\"MsoNoSpacing\"><b>Research: Research\r\nRequirement: Earn the CPDI Africa Certificate</b></p><p class=\"MsoNoSpacing\">Come up with a rating\r\nsystem for traditional building using 70% passive design strategies you have\r\nlearnt: using any of these criteria: 1. Energy Efficiency,&nbsp; 2. Indoor Environment Quality, 3. Sustainable\r\nMaterials; 4. Sustainable Site Management, 5. Water efficiency and 6.\r\nInnovation</p>', 'SESSION ONE: This course presents the African traditional Architecture from the primary societies to the present. Students examine buildings and the built environment as the product of tradition and regarding the unique problems of designing with available building materials in Africa. The course develops essential tools for the evaluation and appreciation of African traditional architecture, for the role it has played in conserving our environment. The course sustainable African Traditional Architecture. It considers awareness, technology and policy that unravel the potential of African traditional Architecture.', '250', '250', 'No', 'http://zoom.com', NULL, NULL, NULL),
(26, '345ijhgfd', 'HT1206 â€“ Green Architecture from African Centered Perspectives', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Sundays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p><strong>Course Overview:</strong></p><p>Course Fee: $250.00</p><p>WEEK 1- Introduction to green design and climate change</p><p>WEEK 2- Green design certification overview</p><p>WEEK 3- Green design considerations and design thinking of buildings and urbans capes</p><p>WEEK 4- Elements of African centered design and understanding vernacular African</p><p>building/urban expressions</p><p>WEEK 5- Building materials, climate, context of lifestyle and architectural aesthetics</p><p>WEEK 6- Climate change architectural design adaptation through design thinking and prototype</p><p>creation</p>', 'SESSION TWO - The course explores the design considerations required for green design with an overview of the various green design certifications worldwide within the context of building materials, building techniques, climate, and cultural aesthetics (African centered). We will review climate change and how design can be adapted to be resilient despite growing global concerns using a sustainable methodology.', '250', '250', 'Yes', 'http://zoom.com', NULL, NULL, NULL),
(27, '098765432345', 'HT1208 - The Architecture of Ghana, from Ancestry to Independence and Beyond', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 12pm - 2pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p class=\"MsoNormal\">Course Fee: $250.00</p><p class=\"MsoNormal\"><b>Course\r\nOverview:</b></p><p class=\"MsoNormal\"><b>Week One</b>: Introduction to Vernacular\r\nArchitecture of Ghana by Region.</p><p class=\"MsoNormal\"><b>Week Two:</b> Architecture of the Ashanti\r\nKingdom: Traditional Community Planning and Ancestral Symbolism</p><p class=\"MsoNormal\"><b>Week Three</b>: Colonialism and Concrete: The\r\nimpact of a foreign building material in a tropical environment.</p><p class=\"MsoNormal\"><b>Week Four</b>: Architecture of Independence:\r\nKwame Nrkumahâ€™s vision for Ghana.</p><p class=\"MsoNormal\"><b>Week Five</b>: Urbanism in Accra: Building\r\nDesign Infrastructure and Planning Challenges.</p><p class=\"MsoNormal\"><b>Week Six</b>: Architectural innovators of\r\nGhana: Adjaye, Addo and Asare</p><p class=\"MsoNormal\"><b>Research\r\nRequirement: Earn the CPDI Africa Certificate</b></p>', 'SESSION TWO: This course introduces students to the varying building types and architectural styles native to Ghana, West Africa. The course surveys vernacular, colonial and contemporary buildings that have shaped the built environment of this rapidly urbanizing and historically rich African nation. Students will explore the complex socio-economic systems and modern-day challenges that face the burgeoning capital city of Accra. They will also research and propose design solutions that are reflective of culturally and environmentally sustainable design practices for the region.', '250', '250', 'Yes', 'http://zoom.com', NULL, NULL, NULL),
(28, '9fcir1p6ac8dn0i2279a6', 'HT1209 - Transitions in the Traditional Art & Architecture of the Igbo and Bamileke', '1', '<p><strong>SESSION ONE</strong></p><p><b>February 13th to March 20th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p><strong>Course Overview:</strong></p><p class=\"MsoNormal\"><b>Week One</b>: Introduction to Art and Architecture of\r\nIgbos of Southern Nigeria</p><p class=\"MsoNormal\"><b>Week Two</b>: Introduction to Art and Architecture of\r\nBamileke of Western Cameroon</p><p class=\"MsoNormal\"><b>Week Three</b>: Similarities: Environmental, Cultural\r\nand Religious Imperatives</p><p class=\"MsoNormal\"><b>Week Four</b>: Materials and Methods of Building</p><p class=\"MsoNormal\"><b>Week Five</b>: Art and Aesthetics: Decorative, Symbolic\r\n&amp; Spiritual</p><p class=\"MsoNormal\"><b>Week Six</b>: Transitions and Translations: from Traditional\r\nto Contemporary</p>', 'SESSION ONE: This course takes students through an exposition of the typologies of traditional art and architecture of the Igbos of Nigeria and Bamileke Grasslands of Western Cameroon. Examples are provided to reinforce planning forms, the intersection of the forms of art and architecture, some similarities between the two regions and examples of attempts at expressing the forms in contemporary African buildings architecture. Students will be introduced to ongoing research initiatives and future research opportunities at CPDI Africa.', '250', '250', 'No', 'http://zoom.com/updated', 'show', '2021-02-18', '22:58:56'),
(29, 'a82r9c1anf90d0pc43ii', 'HT1202 - The Evolution and Transformation of African Centered Architecture ', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><div><p class=\"MsoNormal\"><b><u>Course Overview:</u></b></p><p class=\"MsoNormal\">Week One: Introduction to African Centered Architecture</p><p class=\"MsoNormal\">Week Two: Culture and Lifestyle in Traditional Africa</p><p class=\"MsoNormal\">Week Three: Spirituality in the Built Environment</p><p class=\"MsoNormal\">Week Four: The Elements of Aesthetics, Decorative and Symbolic</p><p class=\"MsoNormal\">Week Five: Materials and Community Development Philosophies</p><p class=\"MsoNormal\">Week Six: The Future, Evolution and Transformation of African &amp; Diaspora architecture</p><p class=\"Body\"><b>Research Requirement: Earn the CPDI Africa Certificate</b></p></div>', 'SESSION TWO - This course introduces students to the concepts for developing new African architectural languages, inspired by the culture and technology of traditional and contemporary African societies. The course surveys both the African and Diaspora built environment, teaching design practices that are culturally and environmentally sustainable. Students will be introduced to the research initiatives of CPDI Africa.', '250', '250', 'Yes', 'http://zoom.com/updated', 'show', '2021-02-21', '20:48:31'),
(30, '12d8f5c692i9r2acnaip3', 'HT1209 - Transitions in the Traditional Art & Architecture of the Igbo and Bamileke', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p><strong>Course Overview:</strong></p><p class=\"MsoNormal\"><b>Week One</b>: Introduction to Art and Architecture of Igbos of Southern Nigeria</p><p class=\"MsoNormal\"><b>Week Two</b>: Introduction to Art and Architecture of Bamileke of Western Cameroon</p><p class=\"MsoNormal\"><b>Week Three</b>: Similarities: Environmental, Cultural and Religious Imperatives</p><p class=\"MsoNormal\"><b>Week Four</b>: Materials and Methods of Building</p><p class=\"MsoNormal\"><b>Week Five</b>: Art and Aesthetics: Decorative, Symbolic &amp; Spiritual</p><p class=\"MsoNormal\"><b>Week Six</b>: Transitions and Translations: from Traditional to Contemporary</p>', 'SESSION TWO -  This course takes students through an exposition of the typologies of traditional art and architecture of the Igbos of Nigeria and Bamileke Grasslands of Western Cameroon. Examples are provided to reinforce planning forms, the intersection of the forms of art and architecture, some similarities between the two regions and examples of attempts at expressing the forms in contemporary African buildings architecture. Students will be introduced to ongoing research initiatives and future research opportunities at CPDI Africa.', '250', '250', 'Yes', 'http://zoom.com/updated', 'show', '2021-02-21', '20:54:15'),
(31, 'ria41d3f9c780ai39pc8n', 'HT1211 â€“ Sustainability in African Traditional Architecture.', '2', '<p><strong>SESSION TWO</strong></p><p><b>April 10th - May 16th, 2021</b></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Saturdays, 1pm - 3pm (EST) / 5pm - 7pm (WAT)</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment&nbsp;</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p class=\"MsoNoSpacing\"><b>Course Overview:</b></p><p class=\"MsoNoSpacing\">Week One: Introduction to African Designs and Buildings</p><p class=\"MsoNoSpacing\">Week Two: The technology used in the construction of African buildings</p><p class=\"MsoNoSpacing\">Week Three: Active and Passive design Strategies in Design &amp; Build</p><p class=\"MsoNoSpacing\">Week Four: Understanding the low tech used by Africa designers and builders</p><p class=\"MsoNoSpacing\">Week Five: The currents trends in African traditional architecture &nbsp;</p><p class=\"MsoNoSpacing\">Week Six: The role of African traditional architecture beyond solving the problem of global warming and climate change</p><p class=\"MsoNoSpacing\"><b>Research: Research Requirement: Earn the CPDI Africa Certificate</b></p><p class=\"MsoNoSpacing\">Come up with a rating system for traditional building using 70% passive design strategies you have learnt: using any of these criteria: 1. Energy Efficiency,&nbsp; 2. Indoor Environment Quality, 3. Sustainable Materials; 4. Sustainable Site Management, 5. Water efficiency and 6. Innovation</p>', 'SESSION TWO - This course presents the African traditional Architecture from the primary societies to the present. Students examine buildings and the built environment as the product of tradition and regarding the unique problems of designing with available building materials in Africa. The course develops essential tools for the evaluation and appreciation of African traditional architecture, for the role it has played in conserving our environment. The course sustainable African Traditional Architecture. It considers awareness, technology and policy that unravel the potential of African traditional Architecture.', '250', '250', 'Yes', 'http://zoom.com/updated', 'show', '2021-02-21', '20:58:22'),
(32, '1616472884_28832', 'HT1216 - Assessing Afrocentric New Buildings for West African Cultural Landscapes', '2', '<p><strong>April 10th - May 16th, 2021</strong></p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time: Sundays at 1pm to 3pm EST / 6pm to 8pm WAT</li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p>Course Overview:&nbsp;</p><p>Week One: New Builds, Heritage and Cultural Landscapes- An overview of the effects of new buildings in relation to physical barriers, visual intrusion negatively impacting on townscape aesthetics, landscapes and heritage.&nbsp;</p><p>Week Two: Heritage Conservation- Cultural and Economic Values are key components in sustaining heritage conservation.</p><p>Week Three: Heritage and the African Community-Explain diverse heritages within the built environment in the African diaspora, diverse communities and human values at the center of an enlarged and cross-disciplinary concept of cultural heritage.&nbsp;</p><p>Week Four: Understanding a Proposal- A look at Policy guidelines and procedures for communication strategies, design flexibility, environmental sensitivity, and stakeholder involvement.</p><p>Week Five: Interpretation of Assessments- Draw on assessment criterion that can be interpreted and represented artistically/ graphically / verbally / textually / spatially to convey meanings and understanding.&nbsp;</p><p>Week Six: Assessment Submission-Submission of specifications for projects, Asset proposal significance (evidential, historic, aesthetic and communal).</p>', '<p>The aim of the course is to use case studies in developing solutions for achieving Afrocentric design excellence that requires continuous, collaborative communication and consensus between design professionals and all stakeholders. It will teach designers how to preserve aesthetics, history and environmental resources while integrating these innovative approaches with African traditional conservation principles to safeguard evolving African historic integrity/authenticity. Designers will learn how new buildings can integrate and interact with the dynamics of the existing natural and man-made environment, and what can be done to preserve or even enhance those features. To equip designers with tools to critically question the ethics surrounding Cultural democracy and effect a radical shift in perceptions regarding African heritage.</p>', '250', '250', 'Yes', 'http://zoom.com/updated', 'hide', '2021-03-23', '04:14:44'),
(33, '1616571539_44916', 'HT1214 - Towards an African American Inspired Architecture', '2', '<p><strong>April 10th - May 16th, 2021</strong></p><p><br>&nbsp;</p><ul><li>Meeting Time: 2hrs once a week for 6 Weeks</li><li>Venue: Zoom</li><li>Day and Time:&nbsp;<strong>Saturdays, 12pm - 2pm (EST) / 5pm - 7pm (WAT)</strong></li><li>Prerequisites: None</li><li>Requirement for Certificate designation: One research assignment</li><li>Outcomes: Audit or Certificate</li><li>Required Readings: To be provided in the course online folder: a bibliography of African architecture publications, video documentaries and websites.</li></ul><p>COURSE OVERVIEW:&nbsp;</p><p>Week One: Introduction to the Architectural Heritage of the African Diaspora&nbsp;</p><p>Week Two: The Evolution of African American Architecture in the United States&nbsp;</p><p>Week Three: African American Culture and Spiritual Practices&nbsp;</p><p>Week Four: Exploring an African American Aesthetic&nbsp;</p><p>Week Five: African American Material and Community Development Philosophies&nbsp;</p><p>Week Six: Toward New Forms of African American Architecture&nbsp;</p>', '<p>This course will explore an approach to architectural design that celebrates African American communities in the United States through referencing their culture, aesthetics, building practices and materials, and sense of spirituality. We will root the approach in the collective African Diaspora heritage of African Americans and examine how modern cultural trends relate back to traditional African traditions brought over by our ancestors. The goal of this course is to discuss and propose new forms of architecture inspired by the way African Americans perceive and utilize space as well as their unique challenges within an American context.</p><p>Â </p>', '250', '250', 'Yes', 'http://zoom.com/updated', 'hide', '2021-03-24', '07:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `panel_course_materials`
--

CREATE TABLE `panel_course_materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `input_name` text,
  `text_description` text,
  `course_id` varchar(225) DEFAULT NULL,
  `input_course_document` text,
  `input_relevant_url` text,
  `visibility` varchar(20) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `panel_discount_code`
--

CREATE TABLE `panel_discount_code` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_discount_code` varchar(225) DEFAULT NULL,
  `input_discount_amount` varchar(225) DEFAULT NULL,
  `event_id` varchar(225) DEFAULT NULL,
  `used_by` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panel_discount_code`
--

INSERT INTO `panel_discount_code` (`id`, `input_discount_code`, `input_discount_amount`, `event_id`, `used_by`, `date_created`, `time_created`, `hash_id`, `status`) VALUES
(1, 'DGFT1234', '25', '2000170', 'Akole Banji', '2021-03-31', '07:20:21', '12345678965', '1'),
(2, 'DGFT12343444', '78', '1000111', 'Akole Banji', '2021-03-31', '07:20:21', NULL, '1'),
(3, 'DG56342', '82', '1000097', 'Akole Banji test', '2021-03-26', '20:44:44', '1616791484_34700', '1'),
(4, '1202EU', '50', '1000103', 'Chuka Victor testing', '2021-04-04', '11:41:36', '1617536496_75061', '1'),
(5, '1206EU', '50', NULL, NULL, '2021-04-04', '11:45:26', '1617536726_31713', NULL),
(6, '1204CO', '50', NULL, NULL, '2021-04-06', '18:21:43', '1617733302_64902', NULL),
(7, '1216CO', '50', NULL, NULL, '2021-04-06', '18:22:37', '1617733357_39682', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `order_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `amount` float NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `time` text NOT NULL,
  `pay_pal_id` text NOT NULL,
  `status` text NOT NULL,
  `payer_email` text NOT NULL,
  `date` text NOT NULL,
  `formatted_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`order_id`, `course_id`, `amount`, `user_id`, `time`, `pay_pal_id`, `status`, `payer_email`, `date`, `formatted_date`) VALUES
(1000015, 18, 250, 7, '1608284497', 'BLANK', 'CONFIRMED', 'BLANK', '1608284497', '2020-12-18'),
(1000016, 23, 250, 7, '1608493421', 'BLANK', 'PENDING', 'BLANK', '1608493421', '2020-12-20'),
(1000017, 23, 250, 7, '1608493547', 'BLANK', 'PENDING', 'BLANK', '1608493547', '2020-12-20'),
(1000018, 18, 250, 7, '1608493773', 'BLANK', 'PENDING', 'BLANK', '1608493773', '2020-12-20'),
(1000019, 29, 250, 80, '1608497320', 'BLANK', 'PENDING', 'BLANK', '1608497320', '2020-12-20'),
(1000020, 29, 50, 80, '1608497399', 'BLANK', 'CONFIRMED', 'BLANK', '1608497399', '2020-12-20'),
(1000021, 19, 250, 80, '1608497997', 'BLANK', 'CONFIRMED', 'BLANK', '1608497997', '2020-12-20'),
(1000022, 18, 250, 82, '1608564871', '', 'CONFIRMED', 'BLANK', '1608564871', '2020-12-21'),
(1000023, 18, 250, 84, '1608639174', 'BLANK', 'PENDING', 'BLANK', '1608639174', '2020-12-22'),
(1000024, 18, 250, 7, '1608798405', 'BLANK', 'PENDING', 'BLANK', '1608798405', '2020-12-24'),
(1000025, 19, 250, 7, '1608810826', 'BLANK', 'PENDING', 'BLANK', '1608810826', '2020-12-24'),
(1000026, 18, 1, 142, '1609090865', 'BLANK', 'PENDING', 'BLANK', '1609090865', '2020-12-27'),
(1000027, 18, 1, 142, '1609091638', 'BLANK', 'PENDING', 'BLANK', '1609091638', '2020-12-27'),
(1000028, 18, 1, 143, '1609093383', '0X375505H82065903', 'CONFIRMED', 'BLANK', '1609093383', '2020-12-27'),
(1000029, 25, 250, 7, '1609160622', 'BLANK', 'PENDING', 'BLANK', '1609160622', '2020-12-28'),
(1000030, 25, 250, 7, '1609160681', 'BLANK', 'PENDING', 'BLANK', '1609160681', '2020-12-28'),
(1000031, 25, 250, 7, '1609160693', 'BLANK', 'PENDING', 'BLANK', '1609160693', '2020-12-28'),
(1000032, 25, 250, 7, '1609161171', 'BLANK', 'PENDING', 'BLANK', '1609161171', '2020-12-28'),
(1000033, 18, 250, 81, '1610210564', 'BLANK', 'PENDING', 'BLANK', '1610210564', '2021-01-09'),
(1000034, 25, 250, 142, '1610447404', 'BLANK', 'PENDING', 'BLANK', '1610447404', '2021-01-12'),
(1000035, 25, 250, 142, '1610447507', 'BLANK', 'PENDING', 'BLANK', '1610447507', '2021-01-12'),
(1000036, 18, 250, 144, '1610629211', 'BLANK', 'PENDING', 'BLANK', '1610629211', '2021-01-14'),
(1000037, 22, 250, 146, '1611241047', '0Y260260UH388250E', 'CONFIRMED', 'BLANK', '1611241047', '2021-01-21');

-- --------------------------------------------------------

--
-- Table structure for table `read_competition_order`
--

CREATE TABLE `read_competition_order` (
  `id` int(11) NOT NULL,
  `input_order_id` varchar(225) DEFAULT NULL,
  `input_name` varchar(225) DEFAULT NULL,
  `input_phone` varchar(225) DEFAULT NULL,
  `input_email` varchar(225) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `competition_id` bigint(20) DEFAULT NULL,
  `input_amount` float DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `time` text,
  `input_pay_pal_id` text,
  `bool_status` int(11) NOT NULL,
  `input_payer_email` text,
  `date` text,
  `visibility` varchar(11) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL,
  `boolkey_status` varchar(225) NOT NULL DEFAULT 'decline_approve',
  `input_competition_year` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `read_competition_order`
--

INSERT INTO `read_competition_order` (`id`, `input_order_id`, `input_name`, `input_phone`, `input_email`, `hash_id`, `competition_id`, `input_amount`, `user_id`, `time`, `input_pay_pal_id`, `bool_status`, `input_payer_email`, `date`, `visibility`, `date_created`, `time_created`, `boolkey_status`, `input_competition_year`) VALUES
(2000050, '2000050', 'Fawaz Adelaja', '234 6677776', 'lilwhiez@gmail.com', NULL, 21, 100, 81, '1608499503', 'BLANK', 0, 'BLANK', '1608499503', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000051, '2000051', 'Fawaz Adelaja', '234 6677776', 'lilwhiez@gmail.com', NULL, 2, 100, 81, '1608499577', 'BLANK', 1, 'BLANK', '1608499577', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000052, '2000052', 'Fawaz Adelaja', '234 6677776', 'lilwhiez@gmail.com', NULL, 4, 100, 81, '1608500701', 'BLANK', 1, 'BLANK', '1608500701', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000054, '2000054', 'Professor 1 Professor1', '645726181', 'professor@test.com', NULL, 21, 100, 82, '1608565102', '', 1, 'BLANK', '1608565102', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000055, '2000055', 'Obisike Nwankwo', '645726181', '123456789@test.com', NULL, 2, 100, 84, '1608638461', 'BLANK', 0, 'BLANK', '1608638461', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000056, '2000056', 'Obisike Nwankwo', '645726181', '123456789@test.com', NULL, 21, 100, 84, '1608639191', 'BLANK', 0, 'BLANK', '1608639191', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000057, '2000057', 'Obisike Nwankwo', '645726181', '123456789@test.com', NULL, 21, 100, 84, '1608639225', 'BLANK', 0, 'BLANK', '1608639225', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000058, '2000058', 'Aisha Aminu', '+44 7448 265178', 'aismin67@yahoo.com', NULL, 3, 100, 91, '1608796060', 'BLANK', 1, 'BLANK', '1608796060', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000059, '2000059', 'Admin nwankwo', '0645726181', 'admin@test.com', NULL, 21, 100, 7, '1608798441', 'BLANK', 0, 'BLANK', '1608798441', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000060, '2000060', 'Umar  Farouq', '+234 706 375 6363', 'ufudad@gmail.com', NULL, 2, 100, 92, '1608800985', 'BLANK', 1, 'BLANK', '1608800985', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000061, '2000061', 'Olalekan Afolabi', '234', 'olalekan@test.com', NULL, 3, 100, 93, '1608803623', 'BLANK', 1, 'BLANK', '1608803623', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000062, '2000062', 'Chitani  Ndisale', '123', 'chitani@test.com', NULL, 3, 100, 94, '1608804866', 'BLANK', 1, 'BLANK', '1608804866', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000063, '2000063', 'Chibuzo Ohaneje', '+234 703 033 6073', 'complexconsortium@gmail.com', NULL, 2, 100, 95, '1608806923', 'BLANK', 1, 'BLANK', '1608806923', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000064, '2000064', 'Admin nwankwo', '0645726181', 'admin@test.com', NULL, 21, 100, 7, '1608810438', 'BLANK', 0, 'BLANK', '1608810438', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000065, '2000065', 'Admin nwankwo', '0645726181', 'admin@test.com', NULL, 21, 100, 7, '1608810893', 'BLANK', 0, 'BLANK', '1608810893', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000066, '2000066', 'My Dao Le Hong', '123', 'my@test.com', NULL, 4, 100, 96, '1608812158', 'BLANK', 1, 'BLANK', '1608812158', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000067, '2000067', 'Eduardo Soto Medina', '123', 'soto@test.com', NULL, 4, 100, 97, '1608813577', 'BLANK', 1, 'BLANK', '1608813577', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000068, '2000068', 'Seni Dara', '123', 'seni@test.com', NULL, 4, 100, 98, '1608814291', 'BLANK', 1, 'BLANK', '1608814291', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000069, '2000069', 'Nomvuzo Gingcana', '+27 72 779 1817', 'gingcanan@gmail.com', NULL, 2, 100, 99, '1608831244', 'BLANK', 1, 'BLANK', '1608831244', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000070, '2000070', 'John Ikechukwu Godspower', '+234 810 082 8971', 'john.ikechukwu.power@gmail.com', NULL, 2, 100, 100, '1608831963', 'BLANK', 1, 'BLANK', '1608831963', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000071, '2000071', 'Kherby Jean', '+1 (786) 344-3060', 'kherbyjean@gmail.com', NULL, 2, 100, 101, '1608832659', 'BLANK', 1, 'BLANK', '1608832659', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000072, '2000072', 'Hauwa Zahra Mahmoud', '+234 806 898 6708', 'hauwazmahmoud@aol.com', NULL, 2, 100, 102, '1608832919', 'BLANK', 1, 'BLANK', '1608832919', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000073, '2000073', 'Roodza Pierrelus', '7862623732', 'roodza.pierrelus001@mymdc.net', NULL, 2, 100, 103, '1608833650', 'BLANK', 1, 'BLANK', '1608833650', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000074, '2000074', 'Udochukwu Anibodu', '+1 (708) 940-3091', 'anidobu2@illinois.edu', NULL, 2, 100, 104, '1608835079', 'BLANK', 1, 'BLANK', '1608835079', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000075, '2000075', 'John Ridley', '+1 (678) 756-6273', 'jridley225@outlook.com', NULL, 2, 100, 105, '1608835329', 'BLANK', 1, 'BLANK', '1608835329', NULL, NULL, NULL, 'decline_approve', '2020'),
(2000076, '2000076', 'Olalekan Ileola-Gold', '123', 'ileolagold@test.com', NULL, 3, 100, 106, '1608837061', 'BLANK', 1, 'BLANK', '1608837061', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000077, '2000077', 'Rahul Sehgal', '123', 'rahul@test.com', NULL, 3, 100, 107, '1608838826', 'BLANK', 1, 'BLANK', '1608838826', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000078, '2000078', 'Dawid Jarosz', '123', 'dawid@test.com', NULL, 3, 100, 108, '1608839329', 'BLANK', 1, 'BLANK', '1608839329', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000079, '2000079', 'Roman Ruhig', '123', 'roman@test.com', NULL, 3, 100, 109, '1608898686', 'BLANK', 1, 'BLANK', '1608898686', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000080, '2000080', 'Nora Bachir', '123', 'nora@test.com', NULL, 3, 100, 110, '1608899903', 'BLANK', 1, 'BLANK', '1608899903', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000081, '2000081', 'Abdula Razaq Adio', '123', 'razaq@test.com', NULL, 3, 100, 111, '1608900357', 'BLANK', 1, 'BLANK', '1608900357', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000082, '2000082', 'Evdokia Mavroudi', '123', 'evdokia@test.com', NULL, 3, 100, 112, '1608903384', 'BLANK', 1, 'BLANK', '1608903384', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000083, '2000083', 'Andrew Ose Phiri', '123', 'andrew@test.com', NULL, 3, 100, 113, '1608903703', 'BLANK', 1, 'BLANK', '1608903703', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000084, '2000084', 'Okusi Isiak Adekunle', '123', 'isiak@test.com', NULL, 3, 100, 114, '1608904724', 'BLANK', 1, 'BLANK', '1608904724', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000085, '2000085', 'Veselina Kotruleva', '123', 'veselina@test.com', NULL, 3, 100, 115, '1608905120', 'BLANK', 1, 'BLANK', '1608905120', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000086, '2000086', 'John Ikechukwu Godspower', '+234 810 082 8971', 'john.ikechukwu.power@gmail.com', NULL, 3, 100, 100, '1608905504', 'BLANK', 1, 'BLANK', '1608905504', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000087, '2000087', 'Harlan Woodard', '123', 'harlan@test.com', NULL, 3, 100, 116, '1608905523', 'BLANK', 1, 'BLANK', '1608905523', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000088, '2000088', 'Bilkisu Garbar', '123', 'bilkisu@test.com', NULL, 3, 100, 117, '1608907290', 'BLANK', 1, 'BLANK', '1608907290', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000089, '2000089', 'Odekunle Lape Saleh', '123', 'odekunle@test.com', NULL, 3, 100, 118, '1608907761', 'BLANK', 1, 'BLANK', '1608907761', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000090, '2000090', 'Federico Fauli', '123', 'federico@test.com', NULL, 3, 100, 119, '1608908092', 'BLANK', 1, 'BLANK', '1608908092', NULL, NULL, NULL, 'decline_approve', '2017'),
(2000091, '2000091', 'Roman Gorshkov', '123', 'roman15@test.com', NULL, 4, 100, 120, '1608911887', 'BLANK', 1, 'BLANK', '1608911887', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000092, '2000092', 'Zaynab Zuhair', '123', 'zaynab@test.com', NULL, 4, 100, 121, '1608912237', 'BLANK', 1, 'BLANK', '1608912237', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000093, '2000093', 'Nahom Bekele', '123', 'nahom@test.com', NULL, 4, 100, 122, '1608912589', 'BLANK', 1, 'BLANK', '1608912589', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000094, '2000094', 'Adeyemi Akande', '123', 'adeyemi@test.com', NULL, 4, 100, 123, '1608912993', 'BLANK', 1, 'BLANK', '1608912993', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000095, '2000095', 'Jun Yanagimuro', '123', 'jun@test.com', NULL, 4, 100, 124, '1608913767', 'BLANK', 1, 'BLANK', '1608913767', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000096, '2000096', 'Amanuele Brook', '123', 'amanuele@test.com', NULL, 4, 100, 125, '1608914213', 'BLANK', 1, 'BLANK', '1608914213', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000097, '2000097', 'Gebrekidan Getaw', '123', 'gebrekidan@test.com', NULL, 4, 100, 126, '1608914697', 'BLANK', 1, 'BLANK', '1608914697', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000098, '2000098', 'Gratio Ray Suntato', '123', 'gratio@test.com', NULL, 4, 100, 127, '1608915009', 'BLANK', 1, 'BLANK', '1608915009', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000099, '2000099', 'Kseniya Mamaeva', '123', 'kseniya@test.com', NULL, 4, 100, 128, '1608915932', 'BLANK', 1, 'BLANK', '1608915932', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000100, '2000100', 'Kalkidan Tesfaye', '123', 'kalkidan@test.com', NULL, 4, 100, 129, '1608916476', 'BLANK', 1, 'BLANK', '1608916476', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000101, '2000101', 'Ojile Ameh Godswill', '123', 'ojile@test.com', NULL, 4, 100, 130, '1608917205', 'BLANK', 1, 'BLANK', '1608917205', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000102, '2000102', 'Nakimuli Thatcher', '123', 'nakimuli@test.com', NULL, 4, 100, 131, '1608917616', 'BLANK', 1, 'BLANK', '1608917616', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000103, '2000103', 'Liuel Hizikias Ketema', '123', 'liuel@test.com', NULL, 4, 100, 132, '1608967016', 'BLANK', 1, 'BLANK', '1608967016', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000104, '2000104', 'Lizuchukwu Magnus', '123', 'lizuchukwu@test.com', NULL, 4, 100, 133, '1608968633', 'BLANK', 1, 'BLANK', '1608968633', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000105, '2000105', 'Mark Gomina', '123', 'mark@test.com', NULL, 4, 100, 134, '1608969081', 'BLANK', 1, 'BLANK', '1608969081', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000106, '2000106', 'Marina Ryleeva', '123', 'marina@test.com', NULL, 4, 100, 135, '1608969567', 'BLANK', 1, 'BLANK', '1608969567', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000107, '2000107', 'Minasie Terefe', '123', 'minasie@test.com', NULL, 4, 100, 136, '1608971885', 'BLANK', 1, 'BLANK', '1608971885', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000108, '2000108', 'Nicole Nomsa Moyo', '123', 'nicole@test.com', NULL, 4, 100, 137, '1609011133', 'BLANK', 1, 'BLANK', '1609011133', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000109, '2000109', 'Ksenia Bilyk', '123', 'ksenia@test.com', NULL, 4, 100, 138, '1609011657', 'BLANK', 1, 'BLANK', '1609011657', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000110, '2000110', 'Tan Boon Chiou', '123', 'tan@test.com', NULL, 4, 100, 139, '1609012124', 'BLANK', 1, 'BLANK', '1609012124', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000111, '2000111', 'Dahlia Nduom', '123', 'dahlia@test.com', NULL, 4, 100, 140, '1609012408', 'BLANK', 1, 'BLANK', '1609012408', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000112, '2000112', 'Miracle David-Adjah', '123', 'miracle@test.com', NULL, 4, 100, 141, '1609012695', 'BLANK', 1, 'BLANK', '1609012695', NULL, NULL, NULL, 'decline_approve', '2015'),
(2000114, '2000114', 'Admin nwankwo', '0645726181', 'admin@test.com', NULL, 21, 100, 7, '1609301922', 'BLANK', 0, 'BLANK', '1609301922', NULL, NULL, NULL, 'decline_approve', '2021'),
(2000115, '2000115', 'Fawaz Adelaja', '234 6677776', 'lilwhiez@gmail.com', NULL, 3, 100, 81, '1610752996', 'BLANK', 0, 'BLANK', '1610752996', NULL, '2021-01-15', '23:23:16', 'decline_approve', '2017'),
(2000117, '2000117', NULL, NULL, NULL, 'c8ap0cfrd49i697na669i', 21, 100, 146, '1610810133', 'BLANK', 0, 'BLANK', '1610810133', 'show', '2021-01-16', '15:15:33', 'decline_approve', '2021'),
(2000118, '2000118', NULL, NULL, NULL, 'cd890245rpf17ac94iian', 21, 100, 146, '1610810389', 'BLANK', 0, 'BLANK', '1610810389', 'show', '2021-01-16', '15:19:49', 'decline_approve', '2021'),
(2000119, '2000119', NULL, NULL, NULL, '142iafi27nc7a93cp98rd', 21, 100, 146, '1610810421', 'BLANK', 0, 'BLANK', '1610810421', 'show', '2021-01-16', '15:20:21', 'decline_approve', '2021'),
(2000120, '2000120', NULL, NULL, NULL, 'd25176cican8f238ri3ap', 21, 100, 146, '1610812200', 'BLANK', 0, 'BLANK', '1610812200', 'show', '2021-01-16', '15:50:00', 'decline_approve', '2021'),
(2000121, '2000121', NULL, NULL, NULL, '7incd4i3fp4ra43a8c23', 21, 100, 146, '1610812226', 'BLANK', 0, 'BLANK', '1610812226', 'show', '2021-01-16', '15:50:26', 'decline_approve', '2021'),
(2000122, '2000122', 'Admin nwankwo', '0645726181', 'admin@test.com', '7rnfa92i57934a4pcdc8i', 21, 100, 7, '1610813024', 'BLANK', 0, 'BLANK', '1610813024', 'show', '2021-01-16', '16:03:44', 'decline_approve', '2021'),
(2000123, '2000123', NULL, NULL, NULL, 'c63i4p5idpd143c546', 2, 100, 151, '1611225280', 'BLANK', 0, 'BLANK', '1611225280', 'show', '2021-01-21', '03:34:40', 'decline_approve', '2020'),
(2000124, '2000124', 'Test Compete', '+2348029276682', 'fawaz.helmarbm@gmail.com', 'erml7h1z2-6a2m-obg8icip06m-fcla8dwa5am', 21, 100, 153, '1611797904', 'BLANK', 0, 'BLANK', '1611797904', 'show', '2021-01-28', '01:38:24', 'decline_approve', '2021'),
(2000125, '2000125', 'Test Compete', '+2348029276682', 'fawaz.helmarbm@gmail.com', '41ablmae9zh002lawipmd277a-oc-rmmf-gci8', 21, 100, 153, '1611798619', 'BLANK', 0, 'BLANK', '1611798619', 'show', '2021-01-28', '01:50:19', 'decline_approve', '2021'),
(2000126, '2000126', 'Test Compete', '+2348029276682', 'fawaz.helmarbm@gmail.com', '1-8pz-l4lciaaa0g41mc9d6mmf9haw-i7oermb', 21, 100, 153, '1611798878', 'BLANK', 0, 'BLANK', '1611798878', 'show', '2021-01-28', '01:54:38', 'decline_approve', '2021'),
(2000127, '2000127', 'Ikechukwu Robert-Eze', '3173854415', 'nate@rndrvsn.com', 't93r6dn07a83dv88-n6pneim-rcsoc', 21, 100, 155, '1612095021', 'BLANK', 0, 'BLANK', '1612095021', 'show', '2021-01-31', '12:10:21', 'decline_approve', '2021'),
(2000128, '2000128', 'Admin nwankwo', '0645726181', 'admin@test.com', '8irpi1n303c107a0acdf9', 21, 100, 7, '1612096050', 'BLANK', 0, 'BLANK', '1612096050', 'show', '2021-01-31', '05:27:30', 'decline_approve', '2021'),
(2000129, '2000129', 'Oluwatobi Akinlolu', '09090997509', 'benpaulzeez@gmail.com', 'di3ncz0pi-7c7-baleeg05pm4u5lamezo9', 21, 100, 156, '1612164404', 'BLANK', 0, 'BLANK', '1612164404', 'show', '2021-02-01', '07:26:44', 'decline_approve', '2021'),
(2000130, '2000130', 'Pietro Cadeo', '+393351041828', 'pierocadeo@cadeoarchitettura.com', 'ohe0-eeccrccteouooi4ditd5aa0t5-6i96dp67paramrc', 21, 100, 157, '1612168820', 'BLANK', 0, 'BLANK', '1612168820', 'show', '2021-02-01', '08:40:20', 'decline_approve', '2021'),
(2000131, '2000131', 'Ngouemou Danielle Christie AMANI', '002250768306229', 'ngouemouchristie@gmail.com', '4pc5190g1dirniumecl6me1u4-ogoit-4ohisacm', 21, 100, 158, '1612172978', 'BLANK', 0, 'BLANK', '1612172978', 'show', '2021-02-01', '09:49:38', 'decline_approve', '2021'),
(2000132, '2000132', 'Ngouemou Danielle Christie AMANI', '002250768306229', 'ngouemouchristie@gmail.com', 'o59mh-5co3-ogiindsuiamge7m6cuir8lcpe1t25', 21, 100, 158, '1612173028', 'BLANK', 0, 'BLANK', '1612173028', 'show', '2021-02-01', '09:50:28', 'decline_approve', '2021'),
(2000133, '2000133', 'Bamidele Jegede', '+2348162866521', 'archbams1@gmail.com', 'p6m3c-1rscadaa847b-oi6mglmic41h92', 21, 100, 160, '1612280342', 'BLANK', 0, 'BLANK', '1612280342', 'show', '2021-02-02', '03:39:02', 'decline_approve', '2021'),
(2000134, '2000134', 'Bamidele Jegede', '+2348162866521', 'archbams1@gmail.com', '31ap6s4cci-r80hic8g3aaom0m-md7l1b', 21, 100, 160, '1612280553', 'BLANK', 0, 'BLANK', '1612280553', 'show', '2021-02-02', '03:42:33', 'decline_approve', '2021'),
(2000135, '2000135', 'Anebi John Olowu', '08076163967', 'johnolowu@yahoo.com', 'lhw-odp624humj4042ynooi-c4oac4o9o', 21, 100, 161, '1612283496', 'BLANK', 0, 'BLANK', '1612283496', 'show', '2021-02-02', '04:31:36', 'decline_approve', '2021'),
(2000136, '2000136', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'rofle-nzm-02y1r3ydefrop000hoc7ioa65o', 21, 100, 162, '1612515662', 'BLANK', 0, 'BLANK', '1612515662', 'show', '2021-02-05', '09:01:02', 'decline_approve', '2021'),
(2000137, '2000137', 'SYDNEY NSAH', '008618501919978', 'mau_trigga@163.com', 'ptcg361a57dg11a-81i6u7mi-roc1m-', 21, 100, 163, '1612635684', 'BLANK', 0, 'BLANK', '1612635684', 'show', '2021-02-06', '06:21:24', 'decline_approve', '2021'),
(2000138, '2000138', 'SYDNEY NSAH', '008618501919978', 'mau_trigga@163.com', 'oa8i4-6c7-pm1ai48u21m4gtr-31cd9g', 21, 100, 163, '1612957622', 'BLANK', 0, 'BLANK', '1612957622', 'show', '2021-02-10', '11:47:02', 'decline_approve', '2021'),
(2000139, '2000139', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'for8zyi42-e-3fyrr7o264lodcomhea9o6np', 21, 100, 162, '1613043776', 'BLANK', 0, 'BLANK', '1613043776', 'show', '2021-02-11', '11:42:56', 'decline_approve', '2021'),
(2000140, '2000140', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'rco0or9eomzoal1n-f73hi4y28re-y4dfo9p', 21, 100, 162, '1613043788', 'BLANK', 0, 'BLANK', '1613043788', 'show', '2021-02-11', '11:43:08', 'decline_approve', '2021'),
(2000141, 'id', 'SYDNEY NSAH', '008618501919978', 'mau_trigga@163.com', '91pt56153ag58-5m78arg-u0dmo-iicc', 21, 100, 163, '1613336309', 'BLANK', 0, 'BLANK', '1613336309', 'show', '2021-02-14', '08:58:29', 'decline_approve', '2021'),
(2000142, 'id', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', '9o11hpea88dlcof-yro76eozyi39on8rrfm-', 21, 100, 162, '1613377869', 'BLANK', 0, 'BLANK', '1613377869', 'show', '2021-02-15', '08:31:09', 'decline_approve', '2021'),
(2000143, 'id', 'SYDNEY NSAH', '008618501919978', 'mau_trigga@163.com', 'atrmi1gumgo-c73ai0-42p7-361c0d66', 21, 100, 163, '1613401554', 'BLANK', 0, 'BLANK', '1613401554', 'show', '2021-02-15', '03:05:54', 'decline_approve', '2021'),
(2000144, 'id', 'SYDNEY NSAH', '008618501919978', 'mau_trigga@163.com', 'dco1r2676-939m5mtpa8i8g-3ag1-iuc', 21, 100, 163, '1613512895', 'BLANK', 0, 'BLANK', '1613512895', 'show', '2021-02-16', '10:01:35', 'decline_approve', '2021'),
(2000145, '2000145', 'SYDNEY NSAH', '008618501919978', 'mau_trigga@163.com', '-oac4i--g0iupcr9d8429tm1am60837g', 21, 100, 163, '1613734205', 'BLANK', 0, 'BLANK', '1613734205', 'show', '2021-02-19', '11:30:05', 'decline_approve', '2021'),
(2000146, '2000146', 'Emad shawky', '01098727177', 'emad148538@bue.edu.eg', '4165ece0di5adm13d18be--285eu0p1-g0u', 21, 100, 176, '1613906373', 'BLANK', 0, 'BLANK', '1613906373', 'show', '2021-02-21', '11:19:33', 'decline_approve', '2021'),
(2000147, '2000147', 'Emad shawky', '01098727177', 'emad148538@bue.edu.eg', 'ee8g543017-e5m-91936u3ddcbpda8i3ue-', 21, 100, 176, '1613906466', 'BLANK', 0, 'BLANK', '1613906466', 'show', '2021-02-21', '11:21:06', 'decline_approve', '2021'),
(2000148, '2000148', 'Emad shawky', '01098727177', 'emad148538@bue.edu.eg', 'd-85u8e4eb6d3d2-2am-1u895pg3ce2ie51', 21, 100, 176, '1613906481', 'BLANK', 0, 'BLANK', '1613906481', 'show', '2021-02-21', '11:21:21', 'decline_approve', '2021'),
(2000149, '2000149', 'Emad shawky', '01098727177', 'emad148538@bue.edu.eg', '8dp-d-17m63ad8gui4e3078ce5eb455-eu5', 21, 100, 176, '1613906494', 'BLANK', 0, 'BLANK', '1613906494', 'show', '2021-02-21', '11:21:34', 'decline_approve', '2021'),
(2000150, '2000150', 'Emad shawky', '01098727177', 'emad148538@bue.edu.eg', 'ea055u44g-1922-d3me-7ecd88b1du4i6pe', 21, 100, 176, '1613906527', 'BLANK', 0, 'BLANK', '1613906527', 'show', '2021-02-21', '11:22:07', 'decline_approve', '2021'),
(2000151, '2000151', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', '1dpl0-em25ef9a-r1h6conyz5o7ooyfriro', 21, 100, 162, '1614006437', 'BLANK', 0, 'BLANK', '1614006437', 'show', '2021-02-22', '03:07:17', 'decline_approve', '2021'),
(2000152, '2000152', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', '490oaypr5hio5rfyz2orefe5c9-on-dlm5o', 21, 100, 162, '1614172655', 'BLANK', 0, 'BLANK', '1614172655', 'show', '2021-02-24', '01:17:35', 'decline_approve', '2021'),
(2000153, '2000153', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'd943rroe--acfo66rfiypnm5ozl8y15oho9e', 21, 100, 162, '1614187171', 'BLANK', 0, 'BLANK', '1614187171', 'show', '2021-02-24', '05:19:31', 'decline_approve', '2021'),
(2000154, '2000154', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'orr9fno-iyh-lodayeo90c7m4z4o8pf8r0e1', 21, 100, 162, '1614596438', 'BLANK', 0, 'BLANK', '1614596438', 'show', '2021-03-01', '11:00:38', 'decline_approve', '2021'),
(2000155, '2000155', 'J Bin', '', 'wjdqls510@naver.com', '9c86q-i5-r8nmwv9sdcd671ljp0a2e3o8', 21, 100, 179, '1614689700', 'BLANK', 0, 'BLANK', '1614689700', 'show', '2021-03-02', '12:55:00', 'decline_approve', '2021'),
(2000156, '2000156', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'ohd6ilzo35n36e-r2oaroyy-opm1ef5crf77', 21, 100, 162, '1614695024', 'BLANK', 0, 'BLANK', '1614695024', 'show', '2021-03-02', '02:23:44', 'decline_approve', '2021'),
(2000157, '2000157', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'ri6oad-oo8ype5-2rh48lr447onez5myfcof', 21, 100, 162, '1614704023', 'BLANK', 0, 'BLANK', '1614704023', 'show', '2021-03-02', '04:53:43', 'decline_approve', '2021'),
(2000158, '2000158', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'r2f6yfomdei-1lro8p2y1-o8cr2neoaz2o5h', 21, 100, 162, '1614775136', 'BLANK', 0, 'BLANK', '1614775136', 'show', '2021-03-03', '12:38:56', 'decline_approve', '2021'),
(2000159, '2000159', 'dhouha omrani', '20728574', 'omranid15@gmail.com', 'aamc6rip623dn6i57-dil3g7co1om36-m', 21, 100, 180, '1614800772', 'BLANK', 0, 'BLANK', '1614800772', 'show', '2021-03-03', '07:46:12', 'decline_approve', '2021'),
(2000160, '2000160', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'mrhfao3l2y-r5e1f2cdoi5n91eryopooz07-', 21, 100, 162, '1614812230', 'BLANK', 0, 'BLANK', '1614812230', 'show', '2021-03-03', '10:57:10', 'decline_approve', '2021'),
(2000161, '2000161', 'mirghasem gholizadeh', '00989121340356', 'm.gh_gholizadeh@yahoo.com', 'h1-hldad1ge0g-oh25cm-8amzop8hy7iio-o9c6', 21, 100, 181, '1614837754', 'BLANK', 0, 'BLANK', '1614837754', 'show', '2021-03-04', '06:02:34', 'decline_approve', '2021'),
(2000162, '2000162', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'pyo-oa4f7-o2o7lznc0e62yefo5idm5rrrh6', 21, 100, 162, '1614983926', 'BLANK', 0, 'BLANK', '1614983926', 'show', '2021-03-05', '10:38:46', 'decline_approve', '2021'),
(2000163, '2000163', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'h7-foyoo7r16lom1e2ie6o1nraydczf-p7r8', 21, 100, 162, '1615068253', 'BLANK', 0, 'BLANK', '1615068253', 'show', '2021-03-06', '10:04:13', 'decline_approve', '2021'),
(2000164, '2000164', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', 'eyroldof9-74-fa06z2eoh47or6omripnyc4', 21, 100, 162, '1615225110', 'BLANK', 0, 'BLANK', '1615225110', 'show', '2021-03-08', '05:38:30', 'decline_approve', '2021'),
(2000165, '2000165', 'Florian Mouafo Zambou', '', 'florenzermoyo@yahoo.fr', '140oeffn9c7eryoodo6h5omz1y9-ralr2pi-', 21, 100, 162, '1615225230', 'BLANK', 0, 'BLANK', '1615225230', 'show', '2021-03-08', '05:40:30', 'decline_approve', '2021'),
(2000166, '2000166', 'Husam Akoud', '+249918211997', 'hakoud@gmail.com', 'l8oma1a9d9ikmuci4ch3gd7o937--p', 21, 100, 182, '1615462248', 'BLANK', 0, 'BLANK', '1615462248', 'show', '2021-03-11', '11:30:48', 'decline_approve', '2021'),
(2000167, '2000167', 'Berin Turan', '01779139778', 'berincturan@gmail.com', 'rn-gd7cmmnrii3ut4946-o3icap7abl87ce', 21, 100, 184, '1616247256', 'BLANK', 0, 'BLANK', '1616247256', 'show', '2021-03-20', '01:34:16', 'decline_approve', '2021'),
(2000169, '2000169', 'Akole Banji', '08168785591', 'boardspsdfghjeck@gmail.com', '18ospiikfcecg4pcojsm2ahgd145b-lra3dm-8d2', 21, 100, 186, '1616764896', 'BLANK', 0, 'BLANK', '1616764896', 'show', '2021-03-26', '01:21:36', 'decline_approve', '2021'),
(2000170, '2000170', 'Akole Banji', '08168785591', 'boardspsdfghjeck@gmail.com', 'as27pc1f1o-jb6h3dddi5osmcpl-grmcg52e7kia', 21, 100, 186, '1616764906', 'BLANK', 0, 'BLANK', '1616764906', 'show', '2021-03-26', '01:21:46', 'decline_approve', '2021'),
(2000171, '2000171', 'Akole Banji', '08168785591', 'boardspeck234567809789@gmail.com', '63-7a1a2g9ms82b3e-o5c9m4pkd7idl8047089cro4pic5', 21, 100, 187, '1616765015', 'BLANK', 1, 'BLANK', '1616765015', 'show', '2021-03-26', '01:23:35', 'decline_approve', '2021'),
(2000172, '2000172', 'Test Test Banji', '08168785591', 'banjimayowa@gmail.com', '37n9iaica75c2r05d5fp1', 21, 100, 166, '1616854044', 'BLANK', 0, 'BLANK', '1616854044', 'show', '2021-03-27', '02:07:24', 'decline_approve', '2021'),
(2000173, '2000173', 'Akole Banji test', '08168785591', 'banjimayowatest@gmail.com', 'i8r6d5apa8c6c1n3416if', 21, 100, 193, '1616858044', 'BLANK', 1, 'BLANK', '1616858044', 'show', '2021-03-27', '03:14:04', 'decline_approve', '2021'),
(2000174, '2000174', 'Senta Schrewe', '', 'senta-schrewe@t-online.de', 'e3wanoii7079l59stcdc7d-st-ep4e-n7enrhe-', 21, 100, 194, '1617002497', 'BLANK', 0, 'BLANK', '1617002497', 'show', '2021-03-29', '07:21:37', 'decline_approve', '2021'),
(2000175, '2000175', 'Senta Schrewe', '', 'senta-schrewe@t-online.de', '5res-7i-cpcn-s78thie8led0n8od4na-wete02', 21, 100, 194, '1617002697', 'BLANK', 0, 'BLANK', '1617002697', 'show', '2021-03-29', '07:24:57', 'decline_approve', '2021'),
(2000176, '2000176', 'Akole TEST Banji', '08168785591', 'boardspeck11111444@gmail.com', '41iib832141m-9odl1o39p91kps6egcmd4-c8c1raa', 21, 100, 201, '1617763317', 'BLANK', 0, 'BLANK', '1617763317', 'show', '2021-04-07', '02:41:57', 'decline_approve', '2021'),
(2000177, '2000177', 'Akole TEST Banji', '08168785591', 'boardspeck11111444@gmail.com', 'a14me43b0a1r77kd53g7do1p4m-i1-4ipo415clcsc', 21, 100, 201, '1617763862', 'BLANK', 0, 'BLANK', '1617763862', 'show', '2021-04-07', '02:51:02', 'decline_approve', '2021'),
(2000178, '2000178', 'Sourabh yadav', '+91 8287949660', 'sourabh@planin.xyz', 'app58aiuc6n4dbx791irohy1-zs3n-l5', 21, 100, 202, '1617865027', 'BLANK', 0, 'BLANK', '1617865027', 'show', '2021-04-08', '06:57:07', 'decline_approve', '2021');

-- --------------------------------------------------------

--
-- Table structure for table `read_course_feedback`
--

CREATE TABLE `read_course_feedback` (
  `id` int(11) NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `student_id` varchar(225) DEFAULT NULL,
  `course_id` varchar(225) DEFAULT NULL,
  `input_feedback` text,
  `input_professor_remark` text,
  `professor_id` text,
  `visibility` varchar(11) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `read_course_professors`
--

CREATE TABLE `read_course_professors` (
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `read_course_professors`
--

INSERT INTO `read_course_professors` (`course_id`, `user_id`) VALUES
(18, 82),
(18, 86),
(19, 81),
(19, 88),
(22, 83),
(23, 188),
(24, 90),
(25, 87),
(26, 170),
(27, 85),
(28, 175),
(29, 83),
(30, 175),
(31, 87),
(32, 191),
(33, 190);

-- --------------------------------------------------------

--
-- Table structure for table `read_product_order`
--

CREATE TABLE `read_product_order` (
  `id` int(11) NOT NULL,
  `input_order_id` varchar(225) DEFAULT NULL,
  `input_name` varchar(225) DEFAULT NULL,
  `input_phone` varchar(225) DEFAULT NULL,
  `input_email` varchar(225) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `course_id` bigint(20) DEFAULT NULL,
  `input_course_name` varchar(225) DEFAULT NULL,
  `input-course_name` varchar(225) DEFAULT NULL,
  `input_amount` float DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `time` text,
  `pay_pal_id` text,
  `bool_status` int(11) NOT NULL,
  `input_payer_email` text,
  `date` text,
  `visibility` varchar(20) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL,
  `boolkey_status` varchar(225) NOT NULL DEFAULT 'approve_decline',
  `input_address_line_one` text NOT NULL,
  `input_country` text NOT NULL,
  `input_ethnic` text NOT NULL,
  `input_professor_status` text NOT NULL,
  `input_linkedIn` text NOT NULL,
  `input_facebook` text NOT NULL,
  `select_session` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `read_product_order`
--

INSERT INTO `read_product_order` (`id`, `input_order_id`, `input_name`, `input_phone`, `input_email`, `hash_id`, `course_id`, `input_course_name`, `input-course_name`, `input_amount`, `user_id`, `time`, `pay_pal_id`, `bool_status`, `input_payer_email`, `date`, `visibility`, `date_created`, `time_created`, `boolkey_status`, `input_address_line_one`, `input_country`, `input_ethnic`, `input_professor_status`, `input_linkedIn`, `input_facebook`, `select_session`) VALUES
(1000047, '1000047', 'Lape Saleh', '+2348037871629', 'lfodekunle@gmail.com', 'gee-a48fcmcoin0mo0-0p1dl0ilul5kd41', 25, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 250, 159, '1612367086', 'BLANK', 1, 'BLANK', '1612367086', 'show', '0000-00-00', '03:44:46', 'approve_decline', 'House 6A NSITF Estate Phase 1', 'NIGERIA', 'Yoruba & Hausa', 'NO', 'Lape Odekunle Saleh', '@Lape Saleh', '1'),
(1000065, '1000065', 'Sa\'ad Ahmad', '+2348164825909', 'ahmadsaad50222@gmail.com', 'dmm75hgmdio1282lp700sc6-a2i-daa597aac2', 25, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 55, 168, '1613307221', 'BLANK', 1, 'BLANK', '1613307221', 'show', '0000-00-00', '12:53:41', 'approve_decline', 'No3 Dahiru bello toro road,  kinkinau,  Kaduna south', 'NIGERIA', 'Fulani', 'NO', 'Saad Ahmad', 'Saad Ahmad', '1'),
(1000073, '1000073', 'Abdulmalik Aminu', '+234 8034826313', 'amanesi4mum@yahoo.com', 'm-iyc4imo39mo2dap9aa1-eum0o5n15hsc5', 25, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 55, 169, '1613575566', 'BLANK', 1, 'BLANK', '1613575566', 'show', '0000-00-00', '03:26:06', 'approve_decline', 'No. 39 Hamza zayyad crescent Area F (A.B.U) Zaria quarters Kaduna state', 'NIGERIA', 'Hausa', 'NO', 'Abdulmalik Aminu', 'Abdulmalik Aminu', '1'),
(1000074, '1000074', 'DUMBARI VICTOR', '+2348035453289', 'talk2dumbari@gmail.com', 'd19dmk-1cimtropc97ib4mu7-l37gli2a0aa', 25, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 55, 171, '1613668873', 'BLANK', 1, 'BLANK', '1613668873', 'show', '0000-00-00', '05:21:13', 'approve_decline', 'No. 6 Prince Isaac Worlu Close, Off Gitto Road, Minita Road, Ozuoba, Port Harcourt, Rivers State.', 'NIGERIA', 'Not Stated', 'NO', 'Not stated', '', '1'),
(1000075, '1000075', 'Ijendu Obasi', '(267) 970-4618', 'Ijendu_obasi@yahoo.com', 'd79oj5i3-oschme-y-29o57nia0upbiao0cd', 22, 'HT1202 - The Evolution and Transformation of African Centered Architecture', NULL, 250, 172, '1613669393', 'BLANK', 1, 'BLANK', '1613669393', 'show', '0000-00-00', '05:29:53', 'approve_decline', 'Not stated', 'UNITED STATES', 'Not Stated', 'NO', 'Not stated', '', '1'),
(1000076, '1000076', 'Erin Light', '9199370041', 'Lighthouse3@nc.rr.com', '3488--cm83rtps9ucdgl3hhcni29ro8oei-', 22, 'HT1202 - The Evolution and Transformation of African Centered Architecture', NULL, 250, 173, '1613669870', 'BLANK', 1, 'BLANK', '1613669870', 'show', '0000-00-00', '05:37:50', 'approve_decline', 'Not stated', 'UNITED STATES', 'Not Stated', 'NO', 'Not stated', '', '1'),
(1000077, '1000077', 'Emeka Ukaga', '+1(651)335-9614', 'emekaukaga@gmail.com', '1ek-9547mm1-c3piamg9ucae8kiogadal6', 25, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 200, 174, '1613670601', 'BLANK', 1, 'BLANK', '1613670601', 'show', '2021-02-18', '05:50:01', 'approve_decline', 'Not stated', 'UNITED STATES', 'Not Stated', 'NO', 'Not stated', '', '1'),
(1000078, '1000078', 'Tiffany Green-Abdullah', '678-542-5064', 'tgreenabdullah@tarchitects.org', '93927ai2pf53nd0aicrc', 22, 'HT1202 - The Evolution and Transformation of African Centered Architecture', NULL, 250, 152, '1613681779', 'BLANK', 1, 'BLANK', '1613681779', 'show', '2021-02-18', '08:56:19', 'approve_decline', '5347 Omalley Lane', 'United States', 'African American ', 'NO', 'https://www.linkedin.com/in/tiffanygreen', 'N/A', '1'),
(1000079, '1000079', 'Harrison Shelton', '2027516395', 'harrison.shelton@gmail.com', '91c4p68ic3far5dai0n45', 25, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 250, 165, '1613681843', 'BLANK', 1, 'BLANK', '1613681843', 'show', '2021-02-18', '08:57:23', 'approve_decline', '220 O St SW, Washington DC', 'UNITED STATES', 'African American-Black', 'NO', 'none', '', '1'),
(1000080, '1000080', 'Emeka Ukaga', '+1(651)335-9614', 'emekaukaga@gmail.com', '8a10pn8c2f9604iid9rac', 28, 'HT1209 - Transitions in the Traditional Art & Architecture of the Igbo and Bamileke', NULL, 200, 174, '1613769521', 'BLANK', 1, 'BLANK', '1613769521', 'show', '2021-02-19', '09:18:41', 'approve_decline', 'Not stated', 'UNITED STATES', 'Not Stated', 'NO', 'Not stated', '', '1'),
(1000084, '1000084', 'Fatimah Zubair Farouk', '09035742413', 'wakeilnana@gmail.com', 'daa6kclmw7ai2l-a2o42i7i8g4cm-nn5pe', 25, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 55, 178, '1613940618', 'BLANK', 0, 'BLANK', '1613940618', 'show', '2021-02-21', '08:50:18', 'approve_decline', 'No,566, Masallacin Murtala, Zoo Road, Kano', 'NIGERIA', 'Hausa', 'NO', 'Fatimah Farouk', '', '1'),
(1000085, '1000085', 'Admin nwankwo', '0645726181', 'admin@test.com', '61rci48d3f2n8ca4p1ai3', 19, 'HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment', NULL, 250, 7, '1614022549', 'BLANK', 0, 'BLANK', '1614022549', 'show', '2021-02-22', '07:35:49', 'approve_decline', 'Pijlkruid', 'United Kingdom', '', 'NO', 'linkedin.com', '', '1'),
(1000086, '1000086', 'Professor 1 Professor1', '645726181', 'professor@test.com', '6pr9091iadi323fanc1c6', 19, 'HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment', NULL, 250, 82, '1614066896', 'BLANK', 0, 'BLANK', '1614066896', 'show', '2021-02-23', '07:54:56', 'approve_decline', 'Pijlkruid', 'United Kingdom', 'Igbo', 'YES', 'linkedin.com', 'facebook.com', '1'),
(1000087, '1000087', 'Charles Finebone', '09025815766', 'finebonecharles@rocketmail.com', '0r3leie-edcomck5pconii8rbae3hlf8tosna2516-mc', 19, 'HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment ', NULL, 250, 183, '1615538489', 'BLANK', 0, 'BLANK', '1615538489', 'show', '2021-03-12', '08:41:29', 'approve_decline', 'Block 9, Flat 3, Maryland Housing Estate', 'NIGERIA', 'South South', 'NO', 'Charles Finebone', '', '2'),
(1000088, '1000088', 'Rose Mary Florian Florian', '7875640020', 'roseflorian0103@gmail.com', 'goo7785af8-me1d4roil97a0ici1rm0pl3c5sn-', 31, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 250, 185, '1616379991', 'BLANK', 1, 'BLANK', '1616379991', 'show', '2021-03-22', '02:26:31', 'approve_decline', '40 w 135th street', 'UNITED STATES', 'Puerto Rican', 'NO', 'Rose Florian', '', '2'),
(1000090, '1000090', 'Akole Banji', '08168785591', 'boardspeck234567809789@gmail.com', '07acar9938dfcn29ip46i', 23, 'HT1210 - Anti-Developed Clinic: The Becoming of a Design Culture Course', NULL, 250, 187, '1616765504', 'BLANK', 1, 'BLANK', '1616765504', 'show', '2021-03-26', '01:31:44', 'approve_decline', '', '', '', '', '', '', '1'),
(1000091, '1000091', 'Admin nwankwo', '0645726181', 'admin@test.com', 'p1626icin18ac415da8rf', 29, 'HT1202 - The Evolution and Transformation of African Centered Architecture', NULL, 250, 7, '1616788542', 'BLANK', 0, 'BLANK', '1616788542', 'show', '2021-03-26', '07:55:42', 'approve_decline', '', '', '', '', '', '', '1'),
(1000094, '1000094', 'Jean-David Morisseau', '9172871192', 'jdmorisseau@hotmail.com', 'oom54jei8s-6dim44u7-smlora9p8ca8dchti', 24, 'HT1201 - Theoretical Frameworks in Afrocentric Architecture', NULL, 250, 192, '1616790467', 'BLANK', 0, 'BLANK', '1616790467', 'show', '2021-03-26', '08:27:47', 'approve_decline', '853 Macy Place', 'UNITED STATES', 'Haitian', 'NO', 'N/A', '', '2'),
(1000101, '1000101', 'Adebowale Christopher Odusanya', '2403555362', 'chadeodus@gmail.com', 'imc4e9-8p5gso306ud4ch-oc4ldadia7m', 19, 'HT1204 - The Evolution of Yoruba Architecture and Contemporary Environment ', NULL, 250, 196, '1617303211', 'BLANK', 0, 'BLANK', '1617303211', 'show', '2021-04-01', '06:53:31', 'approve_decline', '4410 Oglethorpe St Apt 601', 'UNITED STATES', 'Yoruba', 'NO', 'n/a', 'chris Odusanya', '2'),
(1000102, '1000102', 'Adebowale Christopher Odusanya', '2403555362', 'chadeodus@gmail.com', '-29c2sum917iooidadlmdah6p-g74e3cc', 19, 'â€¢	HT-1216:Assessing Afrocentric New Buildings for West African Cultural LandscapesEnvironment', NULL, 200, 196, '1617303259', 'BLANK', 0, 'BLANK', '1617303259', 'show', '2021-04-01', '06:54:19', 'approve_decline', '4410 Oglethorpe St Apt 601', 'UNITED STATES', 'Yoruba', 'NO', 'n/a', 'chris Odusanya', '2'),
(1000104, '1000104', 'Musa Muhammad', '(708) 971-3710', 'musa14muhammad@gmail.com', '-dma1mi6uamm7shum71p6mgl1i6-a42d1coac1', 33, 'HT1214 - Towards an African American Inspired Architecture', NULL, 250, 198, '1617657326', 'BLANK', 0, 'BLANK', '1617657326', 'show', '2021-04-05', '09:15:26', 'approve_decline', '18740 Welch Way', 'UNITED STATES', 'Black, African American', 'NO', 'http://linkedin.com/in/musa-muhammad-572656130', '', '2'),
(1000105, '1000105', 'Aliyu Muhammad', '+2348038461567', 'aleeyulfc@gmail.com', 'd9cm54mog0ly0uc-0acpelia23eli-0f', 31, 'HT1211 â€“ Sustainability in African Traditional Architecture.', NULL, 100, 199, '1617733875', 'BLANK', 0, 'BLANK', '1617733875', 'show', '2021-04-06', '06:31:15', 'approve_decline', 'Abubakar Tafawa Balewa University (ATBU) Bauchi', 'NIGERIA', 'Hausa', 'NO', '@aleeyu_lfc', '', '2'),
(1000106, '1000106', 'Emeka Ukaga', '6513359614', 'emekaukaga2@gmail.com', 'kg8pmocl2gk74a47-mme3ucia0dia25-3ea', 29, 'HT1202 - The Evolution and Transformation of African Centered Architecture ', NULL, 250, 200, '1617734334', 'BLANK', 0, 'BLANK', '1617734334', 'show', '2021-04-06', '06:38:54', 'approve_decline', '1629 n laurel ave', 'UNITED STATES', 'Igbo', 'NO', 'emeka', '', '2');

-- --------------------------------------------------------

--
-- Table structure for table `read_registered_competition_status`
--

CREATE TABLE `read_registered_competition_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `user_id` varchar(225) DEFAULT NULL,
  `competition_id` varchar(255) DEFAULT NULL,
  `bool_status` int(11) NOT NULL,
  `input_first_name` text,
  `input_last_name` text,
  `input_competition_year` varchar(20) DEFAULT NULL,
  `input_ethnic_group` text,
  `input_country` text,
  `competition_design` text,
  `visibility` varchar(11) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `read_registered_competition_status`
--

INSERT INTO `read_registered_competition_status` (`id`, `hash_id`, `user_id`, `competition_id`, `bool_status`, `input_first_name`, `input_last_name`, `input_competition_year`, `input_ethnic_group`, `input_country`, `competition_design`, `visibility`, `date_created`, `time_created`) VALUES
(1, '1610848712_6160', '80', '21', 1, 'Nmadili', 'Okwumabua', '2021', 'Igbo', 'United States', '093f65e080a295f8076b1c5722a46aa2.jpg', NULL, '2021-01-17', '01:58:32'),
(2, '1610848712_5000', '81', '2', 0, 'Fawaz', 'Adelaja', '2020', 'Yoruba', 'Nigeria', '7cbbc409ec990f19c78c75bd1e06f215.PNG', NULL, '2021-01-17', '01:58:32'),
(3, '1610848712_8521', '81', '4', 0, 'Fawaz', 'Adelaja', '2015', 'Yoruba', 'Nigeria', 'ea5d2f1c4608232e07d3aa3d998e5135.png', NULL, '2021-01-17', '01:58:32'),
(4, '1610848712_1970', '91', '3', 1, 'Aisha', 'Aminu', '2017', 'Hausa', 'Nigeria', 'a0a080f42e6f13b3a2df133f073095dd.JPG', NULL, '2021-01-17', '01:58:32'),
(5, '1610848712_2855', '92', '2', 1, 'Umar ', 'Farouq', '2020', 'Plateau Jar, Jarawa', 'Nigeria', '14bfa6bb14875e45bba028a21ed38046.PNG', NULL, '2021-01-17', '01:58:32'),
(6, '1610848712_5134', '93', '3', 1, 'Olalekan', 'Afolabi', '2017', 'Yoruba', 'nigeria', 'e2c420d928d4bf8ce0ff2ec19b371514.JPG', NULL, '2021-01-17', '01:58:32'),
(7, '1610848712_2871', '94', '3', 1, 'Chitani ', 'Ndisale', '2017', 'Bahima, Southern Uganda', 'Malawi', '32bb90e8976aab5298d5da10fe66f21d.JPG', NULL, '2021-01-17', '01:58:32'),
(8, '1610848712_1744', '95', '2', 0, 'Chibuzo', 'Ohaneje', '2020', 'Igbo', 'Nigeria', 'ad61ab143223efbc24c7d2583be69251.PNG', NULL, '2021-01-17', '01:58:32'),
(9, '1610848712_1033', '96', '4', 1, 'My', 'Dao Le Hong', '2015', 'Mali', 'Vietnam ', 'd09bf41544a3365a46c9077ebb5e35c3.JPG', NULL, '2021-01-17', '01:58:32'),
(10, '1610848712_4375', '97', '4', 1, 'Eduardo Soto', 'Medina', '2015', 'Yoruba', 'Puerto Rico ', 'fbd7939d674997cdb4692d34de8633c4.JPG', NULL, '2021-01-17', '01:58:32'),
(11, '1610848712_4425', '98', '4', 1, 'Seni', 'Dara', '2015', 'Togo, benin', 'Republic of Benin', '28dd2c7955ce926456240b2ff0100bde.JPG', NULL, '2021-01-17', '01:58:32'),
(12, '1610848712_8909', '99', '2', 1, 'Nomvuzo', 'Gingcana', '2020', 'Xhosa', 'South Africa', 'd1fe173d08e959397adf34b1d77e88d7.PNG', NULL, '2021-01-17', '01:58:32'),
(13, '1610848712_5725', '100', '2', 1, 'John Ikechukwu', 'Godspower', '2020', 'Igbo', 'nigeria', '35f4a8d465e6e1edc05f3d8ab658c551.PNG', NULL, '2021-01-17', '01:58:32'),
(14, '1610848712_2522', '100', '3', 1, 'John Ikechukwu', 'Godspower', '2017', 'Igbo', 'nigeria', '26657d5ff9020d2abefe558796b99584.jpg', NULL, '2021-01-17', '01:58:32'),
(15, '1610848712_2804', '101', '2', 1, 'Kherby', 'Jean', '2020', 'Haiti, Haitian', 'Haiti', 'f033ab37c30201f73f142449d037028d.PNG', NULL, '2021-01-17', '01:58:32'),
(16, '1610848712_9902', '102', '2', 1, 'Hauwa Zahra', 'Mahmoud', '2020', 'Hausa', 'Nigeria', '43ec517d68b6edd3015b3edc9a11367b.PNG', NULL, '2021-01-17', '01:58:32'),
(17, '1610848712_5129', '103', '2', 1, 'Roodza', 'Pierrelus', '2020', 'Haiti, Haitian', 'Haiti', '9778d5d219c5080b9a6a17bef029331c.PNG', NULL, '2021-01-17', '01:58:32'),
(18, '1610848712_6974', '104', '2', 1, 'Udochukwu', 'Anibodu', '2020', 'Igbo', 'Nigeria', 'fe9fc289c3ff0af142b6d3bead98a923.PNG', NULL, '2021-01-17', '01:58:32'),
(19, '1610848712_2393', '105', '2', 1, 'John', 'Ridley', '2020', 'African American', 'America', '68d30a9594728bc39aa24be94b319d21.PNG', NULL, '2021-01-17', '01:58:32'),
(20, '1610848712_1040', '106', '3', 1, 'Olalekan', 'Ileola-Gold', '2017', 'Hausa, Fulani, Yoruba', 'Nigeria', '3ef815416f775098fe977004015c6193.JPG', NULL, '2021-01-17', '01:58:32'),
(21, '1610848712_3422', '107', '3', 1, 'Rahul', 'Sehgal', '2017', 'Uganda, Baganda', 'India', '93db85ed909c13838ff95ccfa94cebd9.JPG', NULL, '2021-01-17', '01:58:32'),
(22, '1610848712_8416', '108', '3', 0, 'Dawid', 'Jarosz', '2017', 'Sidama, Ethiopia', 'Ethiopia, Poland', 'c7e1249ffc03eb9ded908c236bd1996d.JPG', NULL, '2021-01-17', '01:58:32'),
(23, '1610848712_5185', '109', '3', 1, 'Roman', 'Ruhig', '2017', 'senegal', 'Senegal, slovakia', '2a38a4a9316c49e5a833517c45d31070.JPG', NULL, '2021-01-17', '01:58:32'),
(24, '1610848712_9762', '110', '3', 1, 'Nora', 'Bachir', '2017', 'musulman, algerian', 'Algeria', '7647966b7343c29048673252e490f736.JPG', NULL, '2021-01-17', '01:58:32'),
(25, '1610848712_5839', '111', '3', 1, 'Abdula Razaq', 'Adio', '2017', 'Hausa', 'nigera', '8613985ec49eb8f757ae6439e879bb2a.JPG', NULL, '2021-01-17', '01:58:32'),
(26, '1610848712_3722', '112', '3', 1, 'Evdokia', 'Mavroudi', '2017', 'Burkinabe', 'Burkina Faso', '54229abfcfa5649e7003b83dd4755294.JPG', NULL, '2021-01-17', '01:58:32'),
(27, '1610848712_7126', '113', '3', 1, 'Andrew Ose', 'Phiri', '2017', 'zambia', 'zambia', '92cc227532d17e56e07902b254dfad10.JPG', NULL, '2021-01-17', '01:58:32'),
(28, '1610848712_5937', '114', '3', 1, 'Okusi Isiak', 'Adekunle', '2017', 'Yoruba', 'nigera', '98dce83da57b0395e163467c9dae521b.JPG', NULL, '2021-01-17', '01:58:32'),
(29, '1610848712_4765', '115', '3', 1, 'Veselina', 'Kotruleva', '2017', 'africa', 'bulgaria', 'f4b9ec30ad9f68f89b29639786cb62ef.JPG', NULL, '2021-01-17', '01:58:32'),
(30, '1610848712_5208', '116', '3', 1, 'Harlan', 'Woodard', '2017', 'African American', 'Ghana', '812b4ba287f5ee0bc9d43bbf5bbe87fb.JPG', NULL, '2021-01-17', '01:58:32'),
(31, '1610848712_9381', '117', '3', 1, 'Bilkisu', 'Garbar', '2017', 'yoruba, igbo, hausa', 'nigeria', 'e2ef524fbf3d9fe611d5a8e90fefdc9c.jpg', NULL, '2021-01-17', '01:58:32'),
(32, '1610848712_4881', '118', '3', 1, 'Odekunle', 'Lape Saleh', '2017', 'Hausa', 'nigeria', 'ed3d2c21991e3bef5e069713af9fa6ca.jpg', NULL, '2021-01-17', '01:58:32'),
(33, '1610848712_3208', '119', '3', 1, 'Federico', 'Fauli', '2017', 'congo', 'congo', 'ac627ab1ccbdb62ec96e702f07f6425b.jpg', NULL, '2021-01-17', '01:58:32'),
(34, '1610848712_6369', '120', '4', 1, 'Roman', 'Gorshkov', '2015', 'ethiopia, omo', 'Ethiopia, United Kingdom ', 'f899139df5e1059396431415e770c6dd.jpg', NULL, '2021-01-17', '01:58:32'),
(35, '1610848712_9175', '121', '4', 1, 'Zaynab', 'Zuhair', '2015', 'Hausa', 'nigeria', '38b3eff8baf56627478ec76a704e9b52.JPG', NULL, '2021-01-17', '01:58:32'),
(36, '1610848712_3931', '122', '4', 1, 'Nahom', 'Bekele', '2015', 'Ethiopia', 'ethiopia', 'ec8956637a99787bd197eacd77acce5e.JPG', NULL, '2021-01-17', '01:58:32'),
(37, '1610848712_2831', '123', '4', 1, 'Adeyemi', 'Akande', '2015', 'yoruba', 'nigeria', '6974ce5ac660610b44d9b9fed0ff9548.JPG', NULL, '2021-01-17', '01:58:32'),
(38, '1610848712_8806', '124', '4', 1, 'Jun', 'Yanagimuro', '2015', 'Burkina Faso', 'Burkina Faso', 'c9e1074f5b3f9fc8ea15d152add07294.JPG', NULL, '2021-01-17', '01:58:32'),
(39, '1610848712_5781', '125', '4', 1, 'Amanuele', 'Brook', '2015', 'sidama, afar', 'ethiopia', '65b9eea6e1cc6bb9f0cd2a47751a186f.JPG', NULL, '2021-01-17', '01:58:32'),
(40, '1610848712_1863', '126', '4', 1, 'Gebrekidan', 'Getaw', '2015', 'konso', 'ethiopia', 'f0935e4cd5920aa6c7c996a5ee53a70f.JPG', NULL, '2021-01-17', '01:58:32'),
(41, '1610848712_3892', '127', '4', 1, 'Gratio Ray', 'Suntato', '2015', 'zimbabwe', 'zimbabwe', 'a97da629b098b75c294dffdc3e463904.JPG', NULL, '2021-01-17', '01:58:32'),
(42, '1610848712_5860', '128', '4', 1, 'Kseniya', 'Mamaeva', '2015', 'africa', 'russia', 'a3c65c2974270fd093ee8a9bf8ae7d0b.JPG', NULL, '2021-01-17', '01:58:32'),
(43, '1610848712_8788', '129', '4', 1, 'Kalkidan', 'Tesfaye', '2015', 'sidama', 'Ethiopia', '2723d092b63885e0d7c260cc007e8b9d.JPG', NULL, '2021-01-17', '01:58:32'),
(44, '1610848712_9939', '130', '4', 1, 'Ojile Ameh', 'Godswill', '2015', 'Benin, nok terracotta', 'nigeria', '5f93f983524def3dca464469d2cf9f3e.JPG', NULL, '2021-01-17', '01:58:32'),
(45, '1610848712_5474', '131', '4', 0, 'Nakimuli', 'Thatcher', '2015', 'karamojong', 'uganda', '698d51a19d8a121ce581499d7b701668.JPG', NULL, '2021-01-17', '01:58:32'),
(46, '1610848712_1781', '132', '4', 1, 'Liuel Hizikias', 'Ketema', '2015', 'Axumite', 'ethiopia', '7f6ffaa6bb0b408017b62254211691b5.JPG', NULL, '2021-01-17', '01:58:32'),
(47, '1610848712_2081', '133', '4', 1, 'Lizuchukwu', 'Magnus', '2015', 'Igbo,', 'nigeria, egypt', '73278a4a86960eeb576a8fd4c9ec6997.JPG', NULL, '2021-01-17', '01:58:32'),
(48, '1610848712_6483', '134', '4', 1, 'Mark', 'Gomina', '2015', 'Africa', 'nigeria', '5fd0b37cd7dbbb00f97ba6ce92bf5add.JPG', NULL, '2021-01-17', '01:58:32'),
(49, '1610848712_4917', '135', '4', 1, 'Marina', 'Ryleeva', '2015', 'Africa', 'russia', '2b44928ae11fb9384c4cf38708677c48.JPG', NULL, '2021-01-17', '01:58:32'),
(50, '1610848712_7637', '136', '4', 1, 'Minasie', 'Terefe', '2015', 'konso', 'ethiopia', 'c45147dee729311ef5b5c3003946c48f.JPG', NULL, '2021-01-17', '01:58:32'),
(51, '1610848712_2081', '137', '4', 1, 'Nicole Nomsa', 'Moyo', '2015', 'ndebele, zulu', 'South Africa', 'eb160de1de89d9058fcb0b968dbbbd68.JPG', NULL, '2021-01-17', '01:58:32'),
(52, '1610848712_1476', '138', '4', 1, 'Ksenia', 'Bilyk', '2015', 'Hausa, northern Nigeria', 'nigeria', '5ef059938ba799aaa845e1c2e8a762bd.JPG', NULL, '2021-01-17', '01:58:32'),
(53, '1610848712_3697', '139', '4', 1, 'Tan Boon', 'Chiou', '2015', 'Tata somba', 'Benin republic', '07e1cd7dca89a1678042477183b7ac3f.JPG', NULL, '2021-01-17', '01:58:32'),
(54, '1610848712_7234', '140', '4', 1, 'Dahlia', 'Nduom', '2015', 'fanti', 'Ghana', 'da4fb5c6e93e74d3df8527599fa62642.JPG', NULL, '2021-01-17', '01:58:32'),
(55, '1610848712_5655', '141', '4', 1, 'Miracle', 'David-Adjah', '2015', 'Africa', 'nigeria', '4c56ff4ce4aaf9573aa5dff913df997a.JPG', NULL, '2021-01-17', '01:58:32');

-- --------------------------------------------------------

--
-- Table structure for table `read_student_course_materials`
--

CREATE TABLE `read_student_course_materials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `course_id` bigint(20) DEFAULT NULL,
  `input_name` text,
  `text_description` text,
  `input_file` text,
  `visibility` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `read_student_course_materials`
--

INSERT INTO `read_student_course_materials` (`id`, `hash_id`, `user_id`, `course_id`, `input_name`, `text_description`, `input_file`, `visibility`, `date_created`, `time_created`) VALUES
(16, NULL, 80, 29, 'research', 'igbo design', 'c74d97b01eae257e44aa9d5bade97baf.jpg', NULL, NULL, NULL),
(17, NULL, 80, 29, '2nd ', 'igbo design', '70efdf2ec9b086079795c442636b55fb.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `read_users`
--

CREATE TABLE `read_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `input_first_name` text,
  `input_last_name` text,
  `input_address_line_one` text,
  `input_address_line_two` text,
  `input_post_code` text,
  `input_country` text,
  `input_phone` text,
  `input_email` text,
  `input_password` text,
  `input_linkedIn` text,
  `input_professor_status` text,
  `input_admin_status` text,
  `input_subscription_status` text,
  `input_profession` text,
  `input_ethnic` text,
  `input_study` text,
  `input_status` text,
  `input_facebook` text,
  `input_issu` text,
  `image_1` text,
  `visibility` varchar(11) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `read_users`
--

INSERT INTO `read_users` (`id`, `hash_id`, `input_first_name`, `input_last_name`, `input_address_line_one`, `input_address_line_two`, `input_post_code`, `input_country`, `input_phone`, `input_email`, `input_password`, `input_linkedIn`, `input_professor_status`, `input_admin_status`, `input_subscription_status`, `input_profession`, `input_ethnic`, `input_study`, `input_status`, `input_facebook`, `input_issu`, `image_1`, `visibility`, `date_created`, `time_created`) VALUES
(80, NULL, 'Nmadili', 'Okwumabua', '1227 woodlawn drive', '1204', '30068', 'United States', '6786509145', 'amadiusa@gmail.com', '217714b7311c15a02e844b355eba5c0d5fbd6cf7', 'https://www.linkedin.com/company/cpdi-africa-global-studio/', 'NO', 'YES', 'None', 'Planner', 'Igbo', 'african studies', 'Graduate', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', 'f033ab37c30201f73f142449d037028d.jpg', NULL, NULL, NULL),
(81, NULL, 'Fawaz', 'Adelaja', 'lagos', 'lagos 2', '12345', 'Nigeria', '234 6677776', 'lilwhiez@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'cpdiafrica.org ', 'YES', 'NO', 'None', 'Architect', 'Yoruba', 'architecture', 'Graduate', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', '43ec517d68b6edd3015b3edc9a11367b.jpg', NULL, NULL, NULL),
(82, NULL, 'Professor 1', 'Professor1', 'Pijlkruid', '5', '1112PC', 'United Kingdom', '645726181', 'professor@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'linkedin.com', 'YES', 'NO', 'None', 'ICT Professional', 'Igbo', 'BSc', 'Student', 'facebook.com', 'ISSU', '9778d5d219c5080b9a6a17bef029331c.jpg', NULL, NULL, NULL),
(83, NULL, 'Nmadili', 'Okwumabua', '1227 woodlawn drive', 'suite 1204', '30068', 'United States', '6786509145', 'design@southernsahara.com', '0d2ca3372613c216be8e9c316eb76b167936c589', 'https://www.linkedin.com/in/nmadili-okwumabua-founder-cpdi-africa-85278249/', 'YES', 'NO', 'None', 'Planner', 'Igbo', 'african studies', 'Graduate', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', 'fe9fc289c3ff0af142b6d3bead98a923.jpg', NULL, NULL, NULL),
(84, NULL, 'Obisike', 'Nwankwo', 'Pijlkruid', '5', '1112PC', 'United Kingdom', '645726181', '123456789@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'asdadasd', 'NO', 'NO', 'None', 'ICT Professional', 'Igbo', 'asdasdasd', 'Student', 'facebook.com', 'ISSU', '68d30a9594728bc39aa24be94b319d21', NULL, NULL, NULL),
(85, NULL, 'Amma', 'Asamoah', 'Tuskegee', 'Alabama', '303030', 'United States', '2154807091', 'aosamoah@gmail.com', '2843a5232b29bbcfabe3939bb4cd486a8dd6627e', 'https://www.linkedin.com/in/aoadesigner/', 'YES', 'NO', 'None', 'Architect', 'Ghanaian', 'architecture', 'Graduate', 'https://www.facebook.com/amma.asamoah', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', '3ef815416f775098fe977004015c6193.jpg', NULL, NULL, NULL),
(86, NULL, 'Nduka', 'Okey', 'Enugu', 'Nigeria', '30303', 'Nigeria', '2348033290542', 'okeynduka17@gmail.com', '748410de2e04db84f387d26225c545790aa9b370', 'https://www.linkedin.com/in/okey-nduka-3aa298b2/', 'YES', 'NO', 'None', 'Architect', 'Igbo', 'architecture', 'Graduate', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', '93db85ed909c13838ff95ccfa94cebd9.jpg', NULL, NULL, NULL),
(87, NULL, 'Dodo', 'Aminu Yakubu', 'Malaysia', 'Saudi Arabia', '30303', 'Malaysia', '966561838169', 'dyaaminu@yahoo.com', '6ba5b0a120b58406e12b5627a74a107e0a690b84', 'https://www.linkedin.com/in/yakubu-aminu-dodo-12117222/', 'YES', 'NO', 'None', 'Architect', 'Edo', 'architecture', 'Student', 'https://www.facebook.com/yakubu.aminu.dodo', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', 'yakubudodo.jpg', NULL, NULL, NULL),
(88, NULL, 'Oluwatoyin', 'Sogbesan', 'Oyo', 'Nigeria', '30303', 'Nigeria', '2349099995234', 'sogbesantoyin@yahoo.co.uk', '3173eb7ce0c60219c2e51e0f15eed2610f60581b', 'https://www.linkedin.com/in/leafriqueconnexion/', 'YES', 'NO', 'None', 'Architect', 'Yoruba', 'architecture', 'Graduate', 'https://www.facebook.com/oluwatoyin.z.sogbesan', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', '2a38a4a9316c49e5a833517c45d31070.jpg', NULL, NULL, NULL),
(89, NULL, 'Obisike', 'Nwankwo', 'Pijlkruid', '5', '1112PC', 'United Kingdom', '645726181', 'professor2@test.com', '7c222fb2927d828af22f592134e8932480637c0d', 'asdadasd', 'YES', 'NO', 'None', 'ICT Professional', 'Igbo', 'asdasdasd', 'Graduate', 'facebook.com', 'ISSU', '7647966b7343c29048673252e490f736.jpg', NULL, NULL, NULL),
(90, NULL, 'David', 'Hughes', 'Kent', 'Ohio', '30303', 'America', '2165338339', 'profhughes25@hotmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'https://www.kent.edu/caed/profile/david-hughes-faia-noma-architect', 'YES', 'NO', 'None', 'Architect', 'African American', 'architecture', 'Graduate', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', '8613985ec49eb8f757ae6439e879bb2a.jpg', NULL, NULL, NULL),
(91, NULL, 'Aisha', 'Aminu', 'Canada', 'abuja', '900001', 'Nigeria', '+44 7448 265178', 'aismin67@yahoo.com', 'a99d87a866fbbff822b80c28a2a0736708dbbba5', 'www.linkedin.com/in/aisha-aminu-a0349a29/', 'YES', 'NO', 'None', 'Architect', 'Hausa', 'Canadian University', 'Graduate', 'www.linkedin.com/in/aisha-aminu-a0349a29/', 'www.linkedin.com/in/aisha-aminu-a0349a29/', '54229abfcfa5649e7003b83dd4755294.jpg', NULL, NULL, NULL),
(92, NULL, 'Umar ', 'Farouq', 'jar', 'jar', '930001', 'Nigeria', '+234 706 375 6363', 'ufudad@gmail.com', '74aaab77f5d79930a8f72c6672ec0ae900ad106e', 'https://www.linkedin.com/in/umar-umar-farouq-9264591ab/', 'YES', 'NO', 'None', 'Architect', 'Plateau Jar, Jarawa', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/umar-umar-farouq-9264591ab/', 'https://www.linkedin.com/in/umar-umar-farouq-9264591ab/', '92cc227532d17e56e07902b254dfad10.jpeg', NULL, NULL, NULL),
(93, NULL, 'Olalekan', 'Afolabi', 'lagos', 'lagos', '100001', 'nigeria', '234', 'olalekan@test.com', 'c3979d8867235cf7aa0afafa8a8a7620e1ad22c6', 'https://www.linkedin.com/in/olalekan-afolabi-09557b65/', 'YES', 'NO', 'None', 'Architect', 'Yoruba', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/olalekan-afolabi-09557b65/', 'https://www.linkedin.com/in/olalekan-afolabi-09557b65/', '98dce83da57b0395e163467c9dae521b.JPG', NULL, NULL, NULL),
(94, NULL, 'Chitani ', 'Ndisale', 'malawi', 'malawi', '444', 'Malawi', '123', 'chitani@test.com', '3f83a12c5ba046ee1348ab174054a06cc0f82cc6', 'https://www.linkedin.com/in/chitani-ndisale-87a84310?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3B0juGt1NaSBiIIM5BaBy%2F%2Bw%3D%3D', 'YES', 'NO', 'None', 'Architect', 'Bahima, Southern Uganda', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/chitani-ndisale-87a84310?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3B0juGt1NaSBiIIM5BaBy%2F%2Bw%3D%3D', 'https://www.linkedin.com/in/chitani-ndisale-87a84310?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3B0juGt1NaSBiIIM5BaBy%2F%2Bw%3D%3D', 'f4b9ec30ad9f68f89b29639786cb62ef.JPG', NULL, NULL, NULL),
(95, NULL, 'Chibuzo', 'Ohaneje', 'uyo', 'uyo', '123', 'Nigeria', '+234 703 033 6073', 'complexconsortium@gmail.com', 'b37e28de31058f3d1ea6b02ad37cd40a1dbbb3ae', 'https://www.linkedin.com/in/arc-chibuzo-ohaneje-5759735b/', 'YES', 'NO', 'None', 'Architect', 'Igbo', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/arc-chibuzo-ohaneje-5759735b/', 'https://www.linkedin.com/in/arc-chibuzo-ohaneje-5759735b/', '812b4ba287f5ee0bc9d43bbf5bbe87fb.jpeg', NULL, NULL, NULL),
(96, NULL, 'My', 'Dao Le Hong', 'Vietnam ', 'Vietnam ', '123', 'Vietnam ', '123', 'my@test.com', 'b105343d87501715e69dc722140c7d8ccad9d00c', 'https://www.linkedin.com/in/my-dao-70732b40', 'YES', 'NO', 'None', 'Architect', 'Mali', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/my-dao-70732b40', 'https://www.linkedin.com/in/my-dao-70732b40', '26657d5ff9020d2abefe558796b99584.JPG', NULL, NULL, NULL),
(97, NULL, 'Eduardo Soto', 'Medina', 'Puerto Rico ', 'Puerto Rico ', '123', 'Puerto Rico ', '123', 'soto@test.com', '974f00a7311a2b6b9ad147886602208e896ad9a8', 'https://www.linkedin.com/in/sotomedina', 'YES', 'NO', 'None', 'Architect', 'Yoruba', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/sotomedina', 'https://www.linkedin.com/in/sotomedina', 'e2ef524fbf3d9fe611d5a8e90fefdc9c.JPG', NULL, NULL, NULL),
(98, NULL, 'Seni', 'Dara', 'Benin', 'Benin', '123', 'Republic of Benin', '123', 'seni@test.com', '9d322da224790149cc98037e9e978e1724a9fd49', 'https://www.linkedin.com/in/seni-boni-dara-a5b43236', 'YES', 'NO', 'None', 'Architect', 'Togo, benin', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/seni-boni-dara-a5b43236', 'https://www.linkedin.com/in/seni-boni-dara-a5b43236', 'ed3d2c21991e3bef5e069713af9fa6ca.JPG', NULL, NULL, NULL),
(99, NULL, 'Nomvuzo', 'Gingcana', 'Johannesburg', 'Johannesburg', '123', 'South Africa', '+27 72 779 1817', 'gingcanan@gmail.com', 'a77867b23fcfb3a5f37dc4aa5f97beebeae668f3', 'https://www.linkedin.com/in/nomvuzo-g-13952355/', 'YES', 'NO', 'None', 'Architect', 'Xhosa', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/nomvuzo-g-13952355/', 'https://www.linkedin.com/in/nomvuzo-g-13952355/', 'ac627ab1ccbdb62ec96e702f07f6425b.jpeg', NULL, NULL, NULL),
(100, NULL, 'John Ikechukwu', 'Godspower', 'lagos', 'lagos', '123', 'nigeria', '+234 810 082 8971', 'john.ikechukwu.power@gmail.com', '9a5803d2dac91c59b586fb31d721c0900d392b3b', 'https://www.linkedin.com/in/ikechukwu-john-8b6430127?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3Bx39LY4n6QNmhzO5Y19X6sg%3D%3D', 'YES', 'NO', 'None', 'Architect', 'Igbo', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/ikechukwu-john-8b6430127?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3Bx39LY4n6QNmhzO5Y19X6sg%3D%3D', 'https://www.linkedin.com/in/ikechukwu-john-8b6430127?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3Bx39LY4n6QNmhzO5Y19X6sg%3D%3D', 'f899139df5e1059396431415e770c6dd.jpeg', NULL, NULL, NULL),
(101, NULL, 'Kherby', 'Jean', 'usa', 'usa', '123', 'Haiti', '+1 (786) 344-3060', 'kherbyjean@gmail.com', 'c0ff4ab1be353f8e234ecbfc9493ad4ec01a3848', 'https://www.linkedin.com/in/kherby-jean-141b28167?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3ByGxNw1McSRi17Nb70ydzSA%3D%3D', 'YES', 'NO', 'None', 'Architect', 'Haiti, Haitian', 'Cpdi Africa', 'Student', 'https://www.linkedin.com/in/kherby-jean-141b28167?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3ByGxNw1McSRi17Nb70ydzSA%3D%3D', 'https://www.linkedin.com/in/kherby-jean-141b28167?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3ByGxNw1McSRi17Nb70ydzSA%3D%3D', '38b3eff8baf56627478ec76a704e9b52.jpeg', NULL, NULL, NULL),
(102, NULL, 'Hauwa Zahra', 'Mahmoud', 'abuja', 'abuja', '123', 'Nigeria', '+234 806 898 6708', 'hauwazmahmoud@aol.com', '7a6b8c4ca0727a546fba78a4551c020a6d9c4add', 'https://www.linkedin.com/in/hauwazahramahmoud?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3ByzqJNoVCSuybHizfWuFOLw%3D%3D', 'YES', 'NO', 'None', 'Architect', 'Hausa', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/hauwazahramahmoud?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3ByzqJNoVCSuybHizfWuFOLw%3D%3D', 'https://www.linkedin.com/in/hauwazahramahmoud?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3ByzqJNoVCSuybHizfWuFOLw%3D%3D', 'ec8956637a99787bd197eacd77acce5e.jpeg', NULL, NULL, NULL),
(103, NULL, 'Roodza', 'Pierrelus', 'usa', 'usa', '123', 'Haiti', '7862623732', 'roodza.pierrelus001@mymdc.net', 'c625282c5d53f23d97e3deff529537f947c5cfa2', 'https://www.linkedin.com/in/roodza-pierrelus-345797153?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BA6flOgI1TOS2Jph5bOivwA%3D%3D', 'YES', 'NO', 'None', 'Architect', 'Haiti, Haitian', 'Cpdi Africa', 'Student', 'https://www.linkedin.com/in/roodza-pierrelus-345797153?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BA6flOgI1TOS2Jph5bOivwA%3D%3D', 'https://www.linkedin.com/in/roodza-pierrelus-345797153?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BA6flOgI1TOS2Jph5bOivwA%3D%3D', '6974ce5ac660610b44d9b9fed0ff9548', NULL, NULL, NULL),
(104, NULL, 'Udochukwu', 'Anibodu', 'usa', 'usa', '123', 'Nigeria', '+1 (708) 940-3091', 'anidobu2@illinois.edu', '0bca4180d8c398da6cd8f17e86f3a31a3c57c93f', 'https://www.linkedin.com/in/anidobu2?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BsavSDo9lQ6mL5QT3E%2BJq8w%3D%3D', 'YES', 'NO', 'None', 'Architect', 'Igbo', 'Cpdi Africa', 'Student', 'https://www.linkedin.com/in/anidobu2?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BsavSDo9lQ6mL5QT3E%2BJq8w%3D%3D', 'https://www.linkedin.com/in/anidobu2?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BsavSDo9lQ6mL5QT3E%2BJq8w%3D%3D', 'c9e1074f5b3f9fc8ea15d152add07294.jpeg', NULL, NULL, NULL),
(105, NULL, 'John', 'Ridley', 'usa', 'usa', '123', 'America', '+1 (678) 756-6273', 'jridley225@outlook.com', 'd0c2b8c87e53387f7b696b2853ada02c376cc0d1', 'https://www.linkedin.com/in/john-ridley-aa4733b9?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbtiyiPYbSpmLocepjITpVQ%3D%3D', 'YES', 'NO', 'None', 'Researcher', 'African American', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/john-ridley-aa4733b9?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbtiyiPYbSpmLocepjITpVQ%3D%3D', 'https://www.linkedin.com/in/john-ridley-aa4733b9?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbtiyiPYbSpmLocepjITpVQ%3D%3D', '65b9eea6e1cc6bb9f0cd2a47751a186f.jpeg', NULL, NULL, NULL),
(106, NULL, 'Olalekan', 'Ileola-Gold', 'lagos', 'lagos', '123', 'Nigeria', '123', 'ileolagold@test.com', '7f9294691081570fdd946fffbc1d18cd95a4ed52', 'https://www.linkedin.com/in/olalekan-ileola-gold-8bb17072/', 'YES', 'NO', 'None', 'Architect', 'Hausa, Fulani, Yoruba', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/olalekan-ileola-gold-8bb17072/', 'https://www.linkedin.com/in/olalekan-ileola-gold-8bb17072/', 'f0935e4cd5920aa6c7c996a5ee53a70f.JPG', NULL, NULL, NULL),
(107, NULL, 'Rahul', 'Sehgal', 'india', 'india', '123', 'India', '123', 'rahul@test.com', 'ce51683c524686e187d52b50bb6c171be3b53db5', 'https://www.linkedin.com/in/rahul-sehgal-87261922?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BN3ZO%2BKm0Q0aiUYJnFsvc%2Bg%3D%3D', 'YES', 'NO', 'None', 'Architect', 'Uganda, Baganda', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/rahul-sehgal-87261922?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BN3ZO%2BKm0Q0aiUYJnFsvc%2Bg%3D%3D', 'https://www.linkedin.com/in/rahul-sehgal-87261922?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BN3ZO%2BKm0Q0aiUYJnFsvc%2Bg%3D%3D', 'a97da629b098b75c294dffdc3e463904.JPG', NULL, NULL, NULL),
(108, NULL, 'Dawid', 'Jarosz', 'poland', 'poland', '123', 'Ethiopia, Poland', '123', 'dawid@test.com', '883db2b992c1195dd532f2dc8756c94f6929265d', 'https://www.linkedin.com/in/dawid-jarosz-5b8a53144/', 'YES', 'NO', 'None', 'Architect', 'Sidama, Ethiopia', 'Cpdi Africa', 'Graduate', 'https://www.linkedin.com/in/dawid-jarosz-5b8a53144/', 'https://www.linkedin.com/in/dawid-jarosz-5b8a53144/', 'a3c65c2974270fd093ee8a9bf8ae7d0b.JPG', NULL, NULL, NULL),
(109, NULL, 'Roman', 'Ruhig', 'slovakia', 'slovakia', '123', 'Senegal, slovakia', '123', 'roman@test.com', 'eb9a33c0827c8926e63b7decf1e4d8cc410c317a', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'senegal', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '2723d092b63885e0d7c260cc007e8b9d.JPG', NULL, NULL, NULL),
(110, NULL, 'Nora', 'Bachir', 'algeria', 'algeria', '123', 'Algeria', '123', 'nora@test.com', 'd9881a9dfb06bdd889faf3c558658fe9d3ada987', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'musulman, algerian', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '5f93f983524def3dca464469d2cf9f3e.JPG', NULL, NULL, NULL),
(111, NULL, 'Abdula Razaq', 'Adio', 'nigeria', 'nigeria', '123', 'nigera', '123', 'razaq@test.com', '390447361a08c8808fa755f63f635a893c9189ce', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'Hausa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '698d51a19d8a121ce581499d7b701668.JPG', NULL, NULL, NULL),
(112, NULL, 'Evdokia', 'Mavroudi', 'greece', 'greece', '123', 'Burkina Faso', '123', 'evdokia@test.com', '53bbc210a31598a7578ffbfbc30906dac62c82c5', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'Burkinabe', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '7f6ffaa6bb0b408017b62254211691b5.JPG', NULL, NULL, NULL),
(113, NULL, 'Andrew Ose', 'Phiri', 'zambia', 'zambia', '123', 'zambia', '123', 'andrew@test.com', 'e798987f210b27e72e97dffb969c5f652ee8db04', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'zambia', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '73278a4a86960eeb576a8fd4c9ec6997.JPG', NULL, NULL, NULL),
(114, NULL, 'Okusi Isiak', 'Adekunle', 'nigeria', 'nigeria', '123', 'nigera', '123', 'isiak@test.com', 'f1f803601a111475b281af368a53c76aedab13b4', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'Yoruba', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '5fd0b37cd7dbbb00f97ba6ce92bf5add.JPG', NULL, NULL, NULL),
(115, NULL, 'Veselina', 'Kotruleva', 'bulgaria', 'bulgaria', '123', 'bulgaria', '123', 'veselina@test.com', '8ac6e39b56cd3dffb070c32aceed9d9afb5aa9d1', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'africa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '2b44928ae11fb9384c4cf38708677c48.JPG', NULL, NULL, NULL),
(116, NULL, 'Harlan', 'Woodard', 'usa', 'usa', '123', 'Ghana', '123', 'harlan@test.com', 'b56b108ee295d6154cbddc5a975a470b59e564b2', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'African American', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', 'c45147dee729311ef5b5c3003946c48f.JPG', NULL, NULL, NULL),
(117, NULL, 'Bilkisu', 'Garbar', 'nigera', 'nigeria', '123', 'nigeria', '123', 'bilkisu@test.com', '38c37ccfffc4981e7d6cb1fda76bdf6d77dea7d6', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'yoruba, igbo, hausa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', 'eb160de1de89d9058fcb0b968dbbbd68.jpg', NULL, NULL, NULL),
(118, NULL, 'Odekunle', 'Lape Saleh', 'nigeria', 'nigera', '123', 'nigeria', '123', 'odekunle@test.com', 'f78c33937a7311b911046706bdf376c1c30d3659', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'Hausa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '5ef059938ba799aaa845e1c2e8a762bd.jpg', NULL, NULL, NULL),
(119, NULL, 'Federico', 'Fauli', 'greece', 'greece', '123', 'congo', '123', 'federico@test.com', '7a4b9f93fd6b92f6e804074b322261466ae5cc30', 'https://cpdiafrica.com/2017-winners/', 'YES', 'NO', 'None', 'Architect', 'congo', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2017-winners/', 'https://cpdiafrica.com/2017-winners/', '07e1cd7dca89a1678042477183b7ac3f.jpg', NULL, NULL, NULL),
(120, NULL, 'Roman', 'Gorshkov', 'uk', 'uk', '123', 'Ethiopia, United Kingdom ', '123', 'roman15@test.com', 'e76f55389883b18f36e28fa2326dea8f2ab245a3', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'ethiopia, omo', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', 'da4fb5c6e93e74d3df8527599fa62642.jpg', NULL, NULL, NULL),
(121, NULL, 'Zaynab', 'Zuhair', 'nigeria', 'nigeria', '123', 'nigeria', '123', 'zaynab@test.com', '54e5018672fba9a5977f7e20e87cf44429512ce2', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Hausa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '4c56ff4ce4aaf9573aa5dff913df997a.JPG', NULL, NULL, NULL),
(122, NULL, 'Nahom', 'Bekele', 'ethiopia', 'ethiopia', '123', 'ethiopia', '123', 'nahom@test.com', '13875df5456a122ff8040df87cf1323ea1a27266', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Ethiopia', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', 'a0a080f42e6f13b3a2df133f073095dd.JPG', NULL, NULL, NULL),
(123, NULL, 'Adeyemi', 'Akande', 'nigeria', 'nigeria', '123', 'nigeria', '123', 'adeyemi@test.com', '34d7161be3607290aead2e47657f125059572a29', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'yoruba', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '202cb962ac59075b964b07152d234b70.JPG', NULL, NULL, NULL),
(124, NULL, 'Jun', 'Yanagimuro', 'japan', 'japan', '123', 'Burkina Faso', '123', 'jun@test.com', '7dd8543462c0bac452b54ca0ba6a6e9445af0d7f', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Burkina Faso', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', 'c8ffe9a587b126f152ed3d89a146b445.JPG', NULL, NULL, NULL),
(125, NULL, 'Amanuele', 'Brook', 'ethiopia', 'ethiopia', '123', 'ethiopia', '123', 'amanuele@test.com', '910aee23b9f41f4a5cca242b0b45ca1ff0aaf9ab', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'sidama, afar', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '3def184ad8f4755ff269862ea77393dd.JPG', NULL, NULL, NULL),
(126, NULL, 'Gebrekidan', 'Getaw', 'konso', 'konso', '123', 'ethiopia', '123', 'gebrekidan@test.com', 'b82054b1d7ad1f27069a11d46add66bbfdc6ec21', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'konso', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '069059b7ef840f0c74a814ec9237b6ec.JPG', NULL, NULL, NULL),
(127, NULL, 'Gratio Ray', 'Suntato', 'indonesia', 'indonesia', '123', 'zimbabwe', '123', 'gratio@test.com', '5c88cef1ae82adf003a9741e1974e66b04967a8a', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'zimbabwe', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', 'ec5decca5ed3d6b8079e2e7e7bacc9f2.JPG', NULL, NULL, NULL),
(128, NULL, 'Kseniya', 'Mamaeva', 'Russia', 'Russia', '123', 'russia', '123', 'kseniya@test.com', '8204a7bc9e03f8519dcf189e585125b5284b6ec0', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'africa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '76dc611d6ebaafc66cc0879c71b5db5c.JPG', NULL, NULL, NULL),
(129, NULL, 'Kalkidan', 'Tesfaye', 'ethiopia', 'ethiopia', '123', 'Ethiopia', '123', 'kalkidan@test.com', 'a451fb3aec310b95d122130874311c3d93e91ab7', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'https://cpdiafrica.com/2015-winners/', 'sidama', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', 'd1f491a404d6854880943e5c3cd9ca25.JPG', NULL, NULL, NULL),
(130, NULL, 'Ojile Ameh', 'Godswill', 'nigeria', 'nigeria', '123', 'nigeria', '123', 'ojile@test.com', 'ca2793b388f4034a87e21a15762adab28429de79', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Benin, nok terracotta', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '9b8619251a19057cff70779273e95aa6.JPG', NULL, NULL, NULL),
(131, NULL, 'Nakimuli', 'Thatcher', 'uganda', 'uganda', '123', 'uganda', '123', 'nakimuli@test.com', '4671f5b400ec97f24f4a2ff6db03733fa09ebc53', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'karamojong', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '1afa34a7f984eeabdbb0a7d494132ee5.JPG', NULL, NULL, NULL),
(132, NULL, 'Liuel Hizikias', 'Ketema', 'ethiopia', 'ethiopia', '123', 'ethiopia', '123', 'liuel@test.com', '859c24dd260078376cc50bc3f1a7d637e917fbf3', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Axumite', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '65ded5353c5ee48d0b7d48c591b8f430.JPG', NULL, NULL, NULL),
(133, NULL, 'Lizuchukwu', 'Magnus', 'nigeria', 'nigeria', '123', 'nigeria, egypt', '123', 'lizuchukwu@test.com', '5dd62d04a0b2529e066b27ce7ff225d01fa86f45', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Igbo,', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '9fc3d7152ba9336a670e36d0ed79bc43.JPG', NULL, NULL, NULL),
(134, NULL, 'Mark', 'Gomina', 'nigeria', 'nigeria', '123', 'nigeria', '123', 'mark@test.com', 'af5086dc1151ef09b21cd40faff3075383a5b622', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Africa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '02522a2b2726fb0a03bb19f2d8d9524d.JPG', NULL, NULL, NULL),
(135, NULL, 'Marina', 'Ryleeva', 'Russia', 'Russia', '123', 'russia', '123', 'marina@test.com', 'dbe2f043399fb22ad044020796224e99923af593', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Africa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '7f1de29e6da19d22b51c68001e7e0e54.JPG', NULL, NULL, NULL),
(136, NULL, 'Minasie', 'Terefe', 'ethiopia', 'ethiopia', '123', 'ethiopia', '123', 'minasie@test.com', '91610f95985dc2a917c66a1f3c8d8a2dc8dc18a8', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'konso', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '42a0e188f5033bc65bf8d78622277c4e.JPG', NULL, NULL, NULL),
(137, NULL, 'Nicole Nomsa', 'Moyo', 'Canada', 'canada', '123', 'South Africa', '123', 'nicole@test.com', 'aeb4dfc34c85488106875dfadcc49a4f1a1668fd', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'ndebele, zulu', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '3988c7f88ebcb58c6ce932b957b6f332.JPG', NULL, NULL, NULL),
(138, NULL, 'Ksenia', 'Bilyk', 'ukraine', 'ukraine', '123', 'nigeria', '123', 'ksenia@test.com', '426cdfe100e5571187168f9ed141a821d8446dd4', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Hausa, northern Nigeria', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '013d407166ec4fa56eb1e1f8cbe183b9.JPG', NULL, NULL, NULL),
(139, NULL, 'Tan Boon', 'Chiou', 'japan', 'japan', '123', 'Benin republic', '123', 'tan@test.com', '1bf59f6e715af339a3868db2585fefab45c9c25b', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Tata somba', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', 'e00da03b685a0dd18fb6a08af0923de0.JPG', NULL, NULL, NULL),
(140, NULL, 'Dahlia', 'Nduom', 'ghana', 'ghana', '123', 'Ghana', '123', 'dahlia@test.com', '7b8ddcb8411dffaeb7891fa33fb104fcb948956b', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'fanti', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '1385974ed5904a438616ff7bdb3f7439.JPG', NULL, NULL, NULL),
(141, NULL, 'Miracle', 'David-Adjah', 'nigeria', 'nigeria', '123', 'nigeria', '123', 'miracle@test.com', '5ad7ac9412efd3cb9bc0fa558b7b880443ec30bd', 'https://cpdiafrica.com/2015-winners/', 'YES', 'NO', 'None', 'Architect', 'Africa', 'Cpdi Africa', 'Graduate', 'https://cpdiafrica.com/2015-winners/', 'https://cpdiafrica.com/2015-winners/', '0f28b5d49b3020afeecd95b4009adf4c.JPG', NULL, NULL, NULL),
(142, NULL, 'Bilkisu', 'Tahir', 'No 190 gyadi gyadi Zaria Road', 'No 190 gyadi gyadi zaria road', '+234', 'Nigeria', '07089367625', 'bmtahir01@gmail.com', 'df5227ffa18da48c09b2bfcf361a810bb07aadff', 'Bilkisu tahir mucthar', 'NO', 'NO', 'None', 'student', 'Hausa', 'University', 'Intern', 'Bilkisu Tahir Mucthar', 'Bilkisu Tahir Mucthar', 'a8baa56554f96369ab93e4f3bb068c22', NULL, NULL, NULL),
(152, '4ni771f0ctya507ifp46d', 'Tiffany', 'Green-Abdullah', '5347 Omalley Lane', ' --', '30088', 'United States', '678-542-5064', 'tgreenabdullah@tarchitects.org', '29735807f5985ffb92b61c9002740a67d546a161', 'https://www.linkedin.com/in/tiffanygreen', 'NO', 'NO', 'None', 'Chief Operating Officer ', 'African American ', 'Economics and Education/Technology ', 'Graduate', 'N/A', 'N/A', '', 'show', '2021-01-23', '06:31:11'),
(153, '3csi94821pdt0t7e08', 'Test', 'Compete', '20 Demurin Street, Ketu, Lagos.', '', '100243', 'NIGERIA', '+2348029276682', 'fawaz.helmarbm@gmail.com', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 'https://www.linkedin.com/in/fawaz-adelaja-b7444a95', 'NO', 'NO', 'NONE', 'Architectural Designer', 'Yoruba', 'Architecture', 'Graduate', '', 'Behance.com/fawaz', 'userPhotos/1611797903mailIMG_20190617_185158_9yf.png', NULL, '2021-01-28', '01:38:23'),
(154, '95407kipuw19dkhi8ce3c1u', 'Ikechukwu', 'Robert-Eze', '2625 n meridian st', 'Apt 703', '46204', 'UNITED STATES', '3173854415', 'nroberteze@gmail.com', '047b7e966cc5283a13beb67c679283342a1531c1', 'Nate Robert-eze', 'NO', 'NO', 'NONE', 'Urban designer', 'Nigerian', 'Urban design', 'Graduate', '', 'Nateroberteze.com', 'userPhotos/1612094955mailF208EFCA_5910_47F7_8D2C_CA8E036CFD53.jpeg', NULL, '2021-01-31', '12:09:15'),
(155, 'whcd01eu6726pck33iiku16', 'Ikechukwu', 'Robert-Eze', '2625 n meridian st', 'Apt 703', '46204', 'UNITED STATES', '3173854415', 'nate@rndrvsn.com', '047b7e966cc5283a13beb67c679283342a1531c1', 'Nate Robert-eze', 'NO', 'NO', 'NONE', 'Urban designer', 'Nigerian', 'Urban design', 'Graduate', '', 'Nateroberteze.com', 'userPhotos/1612095021mail', NULL, '2021-01-31', '12:10:21'),
(156, 'u4p2ail37b2w42o83tic1od', 'Oluwatobi', 'Akinlolu', '3 Adeboye street, Bariga, Lagos', '36 Oguntolu street, Somolu, Lagos', '100001', 'NIGERIA', '09090997509', 'benpaulzeez@gmail.com', '1c0b6c379aa4610cac35850d46376d1783a44581', 'https://www.linkedin.com/in/oluwatobi-akinlolu-35a5a5a0', 'NO', 'NO', 'NONE', 'Architect', 'Yoruba', 'Architecture', 'Student', '', '', 'userPhotos/1612164404mail', NULL, '2021-02-01', '07:26:44'),
(157, 'rodptci8165ie5p48734', 'Pietro', 'Cadeo', 'via vittorio emanuele II, 1 brescia via Vittorio Emanuele II, 1', '1', '25122', 'ITALY', '+393351041828', 'pierocadeo@cadeoarchitettura.com', '46ee30fe9291cba1044bd71761c50fe4ce86e831', 'CADEOarchitettura', 'NO', 'NO', 'NONE', 'architect', 'italian', '', 'Student', '', '', 'userPhotos/1612168820mail', NULL, '2021-02-01', '08:40:20'),
(158, '6ni46ou6ome9546u55pcdg', 'Ngouemou Danielle Christie', 'AMANI', 'Rosier 5 Palmeraie', '', '0000', 'COTE D\'IVOIRE', '002250768306229', 'ngouemouchristie@gmail.com', 'b6de58a99ba123b0d25a1f6054eb66481407dcda', 'Danielle Christie', 'NO', 'NO', 'NONE', 'Architecture', 'Ivorian four groups: AKAN. GOUR. KROU. MANDE', 'Architecture', 'Student', '', '', 'userPhotos/1612172978mail', NULL, '2021-02-01', '09:49:38'),
(159, '312pia547d8le41p6c', 'Lape', 'Saleh', 'House 6A NSITF Estate Phase 1', 'Katampe Abuja', '900108', 'NIGERIA', '+2348037871629', 'lfodekunle@gmail.com', 'cd0a29ad6d478f2a95d1de5833cf6f392dc97363', 'Lape Odekunle Saleh', 'NO', 'NO', 'NONE', 'Architect', 'Yoruba & Hausa', 'Phd candidate', 'Graduate', '@Lape Saleh', '3dvstudio.com.ng', 'userPhotos/1612204682mail278AA2DF_48F7_49C1_9F7D_150CB7AA6200.jpeg', NULL, '2021-02-01', '06:38:02'),
(160, 'edi7dc29461lm4ea82pib3', 'Bamidele', 'Jegede', '+2348162866521', '', '', 'NIGERIA', '+2348162866521', 'archbams1@gmail.com', '5f15f95d32cc3e2cbc36b91487b13394a84d6b75', 'https://www.linkedin.com/in/jegede-bamidele-a6b40312a', 'NO', 'NO', 'NONE', 'Architectural Technologist', 'Yoruba', '', 'Student', '', '', 'userPhotos/1612280342mail', NULL, '2021-02-02', '03:39:02'),
(161, '63d79a4bi949iecnp88', 'Anebi', 'John Olowu', 'no 10, bacelonia street, suncity estate, abuja nigeria', '', '+2348076163967', 'NIGERIA', '08076163967', 'johnolowu@yahoo.com', 'eab8eef431b9aeba51ff7b861ecf4e216148252b', 'Johnolowu@yahoo.com', 'NO', 'NO', 'NONE', 'Architect', 'North central', 'Degree', 'Student', '', '', 'userPhotos/1612283496mail', NULL, '2021-02-02', '04:31:36'),
(162, 'f5ic4i5n12al85o2p1rd6', 'Florian', 'Mouafo Zambou', '49 Cockton Road', 'Aylesbury, Buckinghamshire', 'HP19 8FA', 'AFGHANISTAN', '', 'florenzermoyo@yahoo.fr', 'a1f3d1aac9771cdb3eaa7e1c1ac9958cf9b35d95', 'linkedin.com/in/florian-mouafo-21289331', 'NO', 'NO', 'NONE', 'Architect', 'Black African', 'Master of Architecture', 'Student', 'https://www.facebook.com/florenzer.moyo', 'https://indd.adobe.com/view/04aacff1-8dcc-494f-8053-665f3da5e622', 'userPhotos/1612515661mail', NULL, '2021-02-05', '09:01:01'),
(163, '37s641iycyp436ednd98', 'SYDNEY', 'NSAH', 'åŒ—äº¬å¸‚å¤§å…´åŒºæž£å›­å°åŒº7å·æ¥¼2å•å…ƒ302', '', '102699', 'AFGHANISTAN', '008618501919978', 'mau_trigga@163.com', '3196ae7e45845d8bd93b7ac6370dd15002426832', 'sydneyallogo', 'NO', 'NO', 'NONE', 'student', 'Fang', 'architecture design', 'Student', '', '', 'userPhotos/1612635684mail', NULL, '2021-02-06', '06:21:24'),
(164, '4c7pa694d524di2in3r', 'Randi', 'Wilson', '1125 Hammond Dr.', '#516', '30328', 'UNITED STATES', '7577080984', 'randi.wilson@ymail.com', 'b10fd6166b36f1b183f66cedd36922fffdea70d0', 'www.linkedin.com/in/randirwilson', 'NO', 'NO', 'NONE', 'Construction Management/Interior Design', 'African-American', 'Architectural Engineering', 'Graduate', '', '', 'userPhotos/1613137609mailD52A4FD8_50B5_4AB8_B591_5A3B08C83593.jpeg', NULL, '2021-02-12', '01:46:49'),
(165, 'rh42in2o119408r0sicadp', 'Harrison', 'Shelton', '220 O St SW, Washington DC', 'Apt. 10', '20024', 'UNITED STATES', '2027516395', 'harrison.shelton@gmail.com', '09719ed7be924b99ee7b801e00c8ec08a84bf573', 'none', 'NO', 'NO', 'NONE', 'Mech. Eng.', 'African American-Black', '', 'Student', '', '', 'userPhotos/1613172077mail', NULL, '2021-02-12', '11:21:17'),
(166, '5sd223itec7p9t663', 'Test', 'Test Banji', 'Baale Close, Oregun Lagos', '57', '300001', 'NIGERIA', '08168785591', 'banjimayowa@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'NO', 'YES', 'NONE', 'Test', 'igbo', 'Test', 'Student', 'Test', 'Test', 'userPhotos/1613226017mail1_CHFQUwHKOwMa0nmejbwySw___Copy.png', NULL, '2021-02-13', '02:20:17'),
(167, 'et26d13spct78214i9', 'Test', 'Test Banji', 'Baale Close, Oregun Lagos', '', '3000001', 'NIGERIA', '08168785591', 'boardspeck@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'test', 'NO', 'NO', 'NONE', 'Test', 'igbo', 'Test', 'Student', 'Test', 'Test', 'userPhotos/1613226266mail1_CHFQUwHKOwMa0nmejbwySw___Copy.png', NULL, '2021-02-13', '02:24:26'),
(168, 'apid56357c8-12da49s', 'Sa\'ad', 'Ahmad', 'No3 Dahiru bello toro road,  kinkinau,  Kaduna south', '', '', 'NIGERIA', '+2348164825909', 'ahmadsaad50222@gmail.com', '8a317948f9fa4dc700ab629e618a8b3d77b23f7a', 'Saad Ahmad', 'NO', 'NO', 'NONE', 'Artitect', 'Fulani', 'University', 'Student', 'Saad Ahmad', '', 'userPhotos/1613259691mail', NULL, '2021-02-13', '11:41:31'),
(169, 'dbl2808m1ii09kpalc84d1ua', 'Abdulmalik', 'Aminu', 'No. 39 Hamza zayyad crescent Area F (A.B.U) Zaria quarters Kaduna state', '', '', 'NIGERIA', '+234 8034826313', 'amanesi4mum@yahoo.com', 'd81ff4c087cb98f055b985f1c7c8d89ab50f173d', 'Abdulmalik Aminu', 'NO', 'NO', 'NONE', 'Architect', 'Hausa', 'Sustainability in African traditional Architecture', 'Student', 'Abdulmalik Aminu', '', 'userPhotos/1613493961mail', NULL, '2021-02-16', '04:46:01'),
(170, 'i884cd31d0a1i3apn19', 'Diana', 'Gbefwi-Guyit', 'Abuja', 'Abuja', '30331', 'United States', '+2347032281563', 'didigbefwi@gmail.com', '69513179c161baad7617d6c87836cdd106a52a3f', 'https://www.linkedin.com/in/diana-gbefwi-guyit-64743178/', 'YES', 'NO', 'None', 'Architect', 'Gwari', 'architecture', 'Professor', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', 'diane.jpg', 'show', '2021-02-18', '00:29:19'),
(171, 'b0726dc24iru8dp72mi7a', 'DUMBARI', 'VICTOR', 'No. 6 Prince Isaac Worlu Close, Off Gitto Road, Minita Road, Ozuoba, Port Harcourt, Rivers State.', '', '', 'NIGERIA', '+2348035453289', 'talk2dumbari@gmail.com', 'c0accfee62d5bd643842a433e5e2536ae23a309f', 'Not stated', 'NO', 'NO', 'NONE', 'Not stated', 'Not Stated', 'Not Stated', 'Student', '', '', 'userPhotos/1613668872maildummy.png', NULL, '2021-02-18', '05:21:12'),
(172, '14d64j56icen4d855iup', 'Ijendu', 'Obasi', 'Not stated', '', '', 'UNITED STATES', 'Not stated', 'Ijendu_obasi@yahoo.com', '57e6d9147b37c1f28f17fa6d9bc1e069c6e78369', 'Not stated', 'NO', 'NO', 'NONE', 'Not stated', 'Not Stated', 'Not Stated', 'Student', '', '', 'userPhotos/1613669392maildummy.png', NULL, '2021-02-18', '05:29:52'),
(173, '775pi900d47c9irne6', 'Erin', 'Light', 'Not stated', '', '', 'UNITED STATES', 'Not stated', 'Lighthouse3@nc.rr.com', 'b85b11d9454c2147da84e2a98fb3b205b2ddab90', 'Not stated', 'NO', 'NO', 'NONE', 'Not stated', 'Not Stated', 'Not Stated', 'Student', '', '', 'userPhotos/1613669869maildummy.png', NULL, '2021-02-18', '05:37:49'),
(174, 'd7cpa173m66k9i0e76e', 'Emeka', 'Ukaga', 'Not stated', '', '', 'UNITED STATES', 'Not stated', 'emekaukaga@gmail.com', 'f41a40a724b163224c7ec4fae7afc566cff014b2', 'Not stated', 'NO', 'NO', 'NONE', 'Not stated', 'Not Stated', 'Not Stated', 'Student', '', '', 'userPhotos/1613670600mail', NULL, '2021-02-18', '05:50:00'),
(175, 'hku7d5i3ci7k47uce309wp4', 'Ikechukwu', 'Agbim', 'USA', 'USA', '30068', 'United States', '2022767796', 'ikagbim@yahoo.com', '0782a4d6a20431c0e8f664873fbf70ce6f4cd3e4', 'https://www.linkedin.com/in/ike-agbim-aia-ncarb-21a00017/', 'YES', 'NO', 'None', 'Architect', 'Igbo', 'architecture', 'Intern', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', 'userPhotos/agbim.jpg', 'show', '2021-02-18', '23:07:23'),
(176, 'ap25mie11d12d1c86', 'Emad', 'shawky', 'cairo-maadi', 'zahraa maadi', '11865', 'EGYPT', '01098727177', 'emad148538@bue.edu.eg', '87c337fb32a786ea1d880df23af4bceedc2c453d', 'https://www.linkedin.com/in/emad-shawky-a85313146/', 'NO', 'NO', 'NONE', 'architect', 'Nubia', 'Achitectural engineering', 'Graduate', 'https://www.facebook.com/edshawky/', 'https://www.behance.net/edshawky24', 'userPhotos/1613906373mailIMG_E8241.JPG', NULL, '2021-02-21', '11:19:33'),
(177, 'd5a5u8chi31k867c1p3', 'Chuka Victor', 'Ononye', 'Abuja', '', '', 'NIGERIA', '08036811452', 'chukaononye@gmail.com', '52556529f2ccd686110edb2080a9b352e01e74c7', 'chuka KV Ononye', 'NO', 'YES', 'NONE', 'Researcher', 'Igbo', '', 'Graduate', '', '', '1613933093mailmy_pics.jpg', NULL, '2021-02-21', '06:44:53'),
(178, 'amia733ic548pd3fht0', 'Fatimah Zubair', 'Farouk', 'No,566, Masallacin Murtala, Zoo Road, Kano', '', '', 'NIGERIA', '09035742413', 'wakeilnana@gmail.com', 'df4f92acfd37b67ba23a6aebcb0a8ec33459d0af', 'Fatimah Farouk', 'NO', 'NO', 'NONE', 'Graduate Architect', 'Hausa', '', 'Graduate', '', '', '1613940618mailIMG_20210221_WA0001.jpg', NULL, '2021-02-21', '08:50:18'),
(179, '75393j45dp9c6i', 'J', 'Bin', 'ì‚¬ê·¼ë™2ê¸¸', '205í˜¸', '', 'KOREA, REPUBLIC OF', '', 'wjdqls510@naver.com', '7cb5faaa1831df280237b32b29ae3c9bdc853bfc', 'wjdqls510', 'NO', 'NO', 'NONE', 'Student', 'HANYANG', '', 'Student', '', '', 'userPhotos/1614689699mail___________________.PNG', NULL, '2021-03-02', '12:54:59'),
(180, '1o1ci5p3d4d7ua90h38h', 'dhouha', 'omrani', 'ibn rachik', '7', '7050', 'TUNISIA', '20728574', 'omranid15@gmail.com', 'd6085ec71c50bf37b25ff9bb828495a0bc3d35c8', 'https://www.linkedin.com/in/dhouha-omrani-560599164/', 'NO', 'NO', 'NONE', 'Architect', 'Lion', '', 'Graduate', 'https://www.facebook.com/douha.dadou1/', 'architcte', 'userPhotos/1614800772mail', NULL, '2021-03-03', '07:46:12'),
(181, 'e1gch67rsii2m3pm51a1d61', 'mirghasem', 'gholizadeh', 'iran', 'tehran', '', 'IRAN, ISLAMIC REPUBLIC OF', '00989121340356', 'm.gh_gholizadeh@yahoo.com', '88a06ecbecf658618d4ac4257c3ca8ba471618ae', 'mir ghasem gholizadeh', 'NO', 'NO', 'NONE', 'architect', 'gam', '', 'Student', '', '', 'userPhotos/1614837753mail2f5c76da_342a_45d0_97de_12c8b4737aa6.jpg', NULL, '2021-03-04', '06:02:33'),
(182, '35d0mai2p94cu92s72h', 'Husam', 'Akoud', 'El Riyadh', 'Block 22 - Plot 352', 'Khartoum - 11111', 'SUDAN', '+249918211997', 'hakoud@gmail.com', '155f125373999559b205b83dfd815381565abbad', 'husam akoud', 'NO', 'NO', 'NONE', 'Architecture', 'African', '', 'Graduate', '', '', 'userPhotos/1615462248mailPhoto_on_08_29_2020_at_17.00.png', NULL, '2021-03-11', '11:30:48'),
(183, '97di6eps23r7h83acc71l', 'Charles', 'Finebone', 'Block 9, Flat 3, Maryland Housing Estate', '', '', 'NIGERIA', '09025815766', 'finebonecharles@rocketmail.com', 'd0ee6c853184fe6f60302a2443f761b5364fe62c', 'Charles Finebone', 'NO', 'NO', 'NONE', 'Architect', 'South South', 'Architecture', 'Student', '', '', '1615538488mailimage.jpg', NULL, '2021-03-12', '08:41:29'),
(184, 'e8932ici6dp2b76n5r5', 'Berin', 'Turan', 'Eichenstr.', '6', '66976', 'GERMANY', '01779139778', 'berincturan@gmail.com', '3c08219a46bbb5e8a80ac81c807cef11d51dab91', 'https://www.linkedin.com/in/berin-turan-389937209/', 'NO', 'NO', 'NONE', 'architect bachelor', 'Turkish', '', 'Graduate', '', '', 'userPhotos/1616247256mailp.jpg', NULL, '2021-03-20', '01:34:16'),
(185, '28r3i6o1dc9ep120s8', 'Rose Mary Florian', 'Florian', '40 w 135th street', 'Unit 7u', '10037', 'UNITED STATES', '7875640020', 'roseflorian0103@gmail.com', 'd7489035bf2672bea595f753bded879ddf629c9a', 'Rose Florian', 'NO', 'NO', 'NONE', 'Architect', 'Puerto Rican', '', 'Graduate', '', '', '1616379991mailFile_000__2018_05_16_21_10_57_UTC_.jpeg', NULL, '2021-03-22', '02:26:31'),
(186, '87kpa3eoi7c72d0l167', 'Akole', 'Banji', 'Baale Close, Oregun Lagos', '', '3000001', 'NIGERIA', '08168785591', 'boardspsdfghjeck@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'NO', 'NO', 'NONE', 'Test', 'igbo', 'Test', 'Student', 'Test', 'Test', 'userPhotos/1616707869mailAttend_out_2k_120px.jpg', NULL, '2021-03-25', '09:31:09'),
(187, 'co384a53p5i1ldk83e3', 'Akole', 'Banji', 'Baale Close, Oregun Lagos', '', '3000001', 'NIGERIA', '08168785591', 'boardspeck234567809789@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'NO', 'NO', 'NONE', 'Test', 'igbo', 'Test', 'Student', 'Test', 'Test', 'userPhotos/1616765014mail', NULL, '2021-03-26', '01:23:34'),
(188, '435678967987893', 'Felema', 'Yemaneberhan', 'USA', 'America', '30303', 'USA', '3233832505', 'felemayemaneberhan@gmail.com', '77dd9453efa8c38cea53bdab94c41fe0c5d9eb1a', 'https://www.linkedin.com/in/felemaye/', 'YES', 'NO', 'None', 'Architect', 'Ethiopian ', 'architecture', 'Graduate', 'facebook.com/cpdiafrica', 'https://issuu.com/cpdi_africa/docs/cpdi_manifesto_book', 'uploads/felema.jpg', NULL, '2021-03-31', '33:28:14'),
(189, '623p692l19aed22icko', 'Akole TEST', 'Banji', 'Baale Close, Oregun Lagos', '', '3000001', 'NIGERIA', '08168785591', 'boardspe34567ck@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'YES', 'NO', 'NONE', 'Test', 'igbo', 'Test', 'Student', 'Test', 'Test', 'userPhotos/1616787636mailcomment_img_2.jpg', NULL, '2021-03-26', '07:40:36'),
(190, '7s728d8ii6cnciprah4t392', 'Christian', 'Coles', 'No,566, Masallacin Murtala, Zoo Road, Kano', '', '', 'UNITED STATES', '', 'cpdiafrica.globalstudio@gmail.com', '2314b2e3a4a1f7db165be2aafbf1efd78f28cc97', 'https://www.linkedin.com/in/christian-coles-1264a868/', 'YES', 'NO', 'NONE', 'Professor', 'Igbo', '', 'Student', '', '', 'unnamed.jpg', NULL, '2021-03-26', '08:06:24'),
(191, '0k83437dt9ic7e49ipo', 'Tokie', 'Laotan-Brown', 'No,566, Masallacin Murtala, Zoo Road, Kano', '', '', 'UNITED STATES', '', 'tokie@gmail.com', '53e965dbc91089c12c74bd57761f0908fa5ccc1f', 'https://www.linkedin.com/in/tokie-laotan-brown-phd-b2b11423/', 'YES', 'NO', 'NONE', 'Professor', 'Igbo', '', 'Student', '', '', 'unnamed2.jpg', NULL, '2021-03-26', '08:19:24'),
(192, 'in54ai5cpd626v9-e7aj2d3d', 'Jean-David', 'Morisseau', '853 Macy Place', 'Apt 3F', '10455', 'UNITED STATES', '9172871192', 'jdmorisseau@hotmail.com', '8586cbbdebbb04c4ae6fa9203957fcb8fccacfda', 'N/A', 'NO', 'NO', 'NONE', 'Project Manager/Engineer', 'Haitian', 'Architectural Engineering', 'Student', '', '', '1616790423mail', NULL, '2021-03-26', '08:27:03'),
(193, '2261998ld4opecki1a9', 'Akole', 'Banji test', 'Baale Close, Oregun Lagos', '', '3000001', 'NIGERIA', '08168785591', 'banjimayowatest@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'NO', 'NO', 'NONE', 'Test', 'igbo', 'Test', 'Student', 'Test', 'Test', '1616790593mailcomment_img_2.jpg', NULL, '2021-03-26', '08:29:53'),
(194, '1cn9td8a75746ei8ps', 'Senta', 'Schrewe', 'Kaffetwete 1', 'Braunschweig', '38100', 'GERMANY', '', 'senta-schrewe@t-online.de', 'cb2cce4c6f0c29cbdc31e52fd631d82fb62d5f4e', 'www.linkedin.com/in/senta-schrewe-042827189', 'NO', 'NO', 'NONE', 'Architecture', 'german', '', 'Student', '', '', 'userPhotos/1617002496mailDM_96A4511.jpg', NULL, '2021-03-29', '07:21:36'),
(195, '3005rc75dc6ipvio1297', 'vic5or test', 'victor test', 'No,566, Masallacin Murtala, Zoo Road, Kano', '', '', 'AFGHANISTAN', '', 'agro@gmail.com', '96c842e9da8fd2e3d76890658ce7e43d08de90c2', 'Fatimah Farouk', 'NO', 'NO', 'NONE', 'Graduate Architect', 'Igbo', '', 'Student', '', '', '1617070499mailCPDI_pics.png', NULL, '2021-03-30', '02:14:59'),
(196, '717c7o6wp8bead7dei369al', 'Adebowale Christopher', 'Odusanya', '4410 Oglethorpe St Apt 601', '', '20781-1557', 'UNITED STATES', '2403555362', 'chadeodus@gmail.com', '6606e9015b2151cd6cfa45c5b289c77cd2cfe311', 'n/a', 'NO', 'NO', 'NONE', 'Architectural Designer / Project Manager', 'Yoruba', 'Architecture', 'Graduate', 'chris Odusanya', '', '1617303211mailChris_photo.jpg', NULL, '2021-04-01', '06:53:31'),
(197, '4i81d9huc35k98a9cp2', 'Chuka Victor', 'testing', 'ssssdsddd', '', '', 'AFGHANISTAN', '', 'abie@gmail.com', '61def689b00970140646274bf035fa9088971945', 'dddddd', 'NO', 'NO', 'NONE', 'dddddddd', 'ddddd', '', 'Student', '', '', '1617536630mail', NULL, '2021-04-04', '11:43:50'),
(198, 'cm1s1u488dpa9i4864', 'Musa', 'Muhammad', '18740 Welch Way', '', '60478', 'UNITED STATES', '(708) 971-3710', 'musa14muhammad@gmail.com', '25037953a30b485a2dcb1b84b77893c08618ec70', 'http://linkedin.com/in/musa-muhammad-572656130', 'NO', 'NO', 'NONE', 'Interested Architecture, Construction, Real Estate', 'Black, African American', 'Architecture, Civil Engineering (Construction Management)', 'Graduate', '', '', '1617657326mail2F03BFA2_E49A_459D_AF47_597B1D7CC6D8.jpeg', NULL, '2021-04-05', '09:15:26'),
(199, '8654cd67p9ul9i6yia', 'Aliyu', 'Muhammad', 'Abubakar Tafawa Balewa University (ATBU) Bauchi', '', '', 'NIGERIA', '', 'aleeyulfc@gmail.com', 'b58c979db347ffa13732c72f11b36a35d0f61872', '@aleeyu_lfc', 'NO', 'NO', 'NONE', 'Undergraduate Architect', 'Hausa', '', 'Student', '', '', '1617733874mailIMG_20210221_WA0001.jpg', NULL, '2021-04-06', '06:31:14'),
(200, '79mie6ac61p7kde9899', 'Emeka', 'Ukaga', '1629 n laurel ave', '', '', 'UNITED STATES', '6513359614', 'emekaukaga2@gmail.com', '7d91ea87964bc260ec6c394194078814ca4e0a95', 'emeka', 'NO', 'NO', 'NONE', 'Engineer/Architect', 'Igbo', '', 'Student', '', '', '1617734333mail', NULL, '2021-04-06', '06:38:53'),
(201, 'l71k43oi9dce53ap908', 'Akole TEST', 'Banji', 'Baale Close, Oregun Lagos', '', '3000001', 'NIGERIA', '08168785591', 'boardspeck11111444@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test', 'NO', 'NO', 'NONE', 'Test', 'igbo', 'Test', 'Student', 'Test', 'Test', 'userPhotos/1617763317mail', NULL, '2021-04-07', '02:41:57'),
(202, 'sh66r21u20a2pc7dibo47', 'Sourabh', 'yadav', 'T-51, ideators cafe , hauzkhas', 'hauz khas village , delhi', '110046', 'INDIA', '+91 8287949660', 'sourabh@planin.xyz', 'c9de1e7f0447cea86f1c1a1ed6d15d17aadc3698', 'https://www.linkedin.com/in/sourabh-yadav-57222a11b', 'NO', 'NO', 'NONE', 'Architect', 'Indian', 'Architecture', 'Graduate', '', 'https://issuu.com/sourabhyadav/docs/sourabh_yadav_architecture_portfolio_2014-2019', 'userPhotos/1617865026mailScreenshot__1_.png', NULL, '2021-04-08', '06:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `registered_competition_status`
--

CREATE TABLE `registered_competition_status` (
  `user_id` bigint(20) NOT NULL,
  `competition_id` bigint(20) NOT NULL,
  `status` text NOT NULL,
  `fname` text,
  `lname` text,
  `competitionYear` int(11) DEFAULT NULL,
  `ethnicGroup` text,
  `country` text,
  `competitionDesign` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_competition_status`
--

INSERT INTO `registered_competition_status` (`user_id`, `competition_id`, `status`, `fname`, `lname`, `competitionYear`, `ethnicGroup`, `country`, `competitionDesign`) VALUES
(80, 21, 'approved', 'Nmadili', 'Okwumabua', 2021, 'Igbo', 'United States', '093f65e080a295f8076b1c5722a46aa2.jpg'),
(81, 2, 'approved', 'Fawaz', 'Adelaja', 2020, 'Yoruba', 'Nigeria', '7cbbc409ec990f19c78c75bd1e06f215.PNG'),
(81, 4, 'rejected', 'Fawaz', 'Adelaja', 2015, 'Yoruba', 'Nigeria', 'ea5d2f1c4608232e07d3aa3d998e5135.png'),
(91, 3, 'approved', 'Aisha', 'Aminu', 2017, 'Hausa', 'Nigeria', 'a0a080f42e6f13b3a2df133f073095dd.JPG'),
(92, 2, 'approved', 'Umar ', 'Farouq', 2020, 'Plateau Jar, Jarawa', 'Nigeria', '14bfa6bb14875e45bba028a21ed38046.PNG'),
(93, 3, 'approved', 'Olalekan', 'Afolabi', 2017, 'Yoruba', 'nigeria', 'e2c420d928d4bf8ce0ff2ec19b371514.JPG'),
(94, 3, 'approved', 'Chitani ', 'Ndisale', 2017, 'Bahima, Southern Uganda', 'Malawi', '32bb90e8976aab5298d5da10fe66f21d.JPG'),
(95, 2, 'approved', 'Chibuzo', 'Ohaneje', 2020, 'Igbo', 'Nigeria', 'ad61ab143223efbc24c7d2583be69251.PNG'),
(96, 4, 'approved', 'My', 'Dao Le Hong', 2015, 'Mali', 'Vietnam ', 'd09bf41544a3365a46c9077ebb5e35c3.JPG'),
(97, 4, 'approved', 'Eduardo Soto', 'Medina', 2015, 'Yoruba', 'Puerto Rico ', 'fbd7939d674997cdb4692d34de8633c4.JPG'),
(98, 4, 'approved', 'Seni', 'Dara', 2015, 'Togo, benin', 'Republic of Benin', '28dd2c7955ce926456240b2ff0100bde.JPG'),
(99, 2, 'approved', 'Nomvuzo', 'Gingcana', 2020, 'Xhosa', 'South Africa', 'd1fe173d08e959397adf34b1d77e88d7.PNG'),
(100, 2, 'approved', 'John Ikechukwu', 'Godspower', 2020, 'Igbo', 'nigeria', '35f4a8d465e6e1edc05f3d8ab658c551.PNG'),
(100, 3, 'approved', 'John Ikechukwu', 'Godspower', 2017, 'Igbo', 'nigeria', '26657d5ff9020d2abefe558796b99584.jpg'),
(101, 2, 'approved', 'Kherby', 'Jean', 2020, 'Haiti, Haitian', 'Haiti', 'f033ab37c30201f73f142449d037028d.PNG'),
(102, 2, 'approved', 'Hauwa Zahra', 'Mahmoud', 2020, 'Hausa', 'Nigeria', '43ec517d68b6edd3015b3edc9a11367b.PNG'),
(103, 2, 'approved', 'Roodza', 'Pierrelus', 2020, 'Haiti, Haitian', 'Haiti', '9778d5d219c5080b9a6a17bef029331c.PNG'),
(104, 2, 'approved', 'Udochukwu', 'Anibodu', 2020, 'Igbo', 'Nigeria', 'fe9fc289c3ff0af142b6d3bead98a923.PNG'),
(105, 2, 'approved', 'John', 'Ridley', 2020, 'African American', 'America', '68d30a9594728bc39aa24be94b319d21.PNG'),
(106, 3, 'approved', 'Olalekan', 'Ileola-Gold', 2017, 'Hausa, Fulani, Yoruba', 'Nigeria', '3ef815416f775098fe977004015c6193.JPG'),
(107, 3, 'approved', 'Rahul', 'Sehgal', 2017, 'Uganda, Baganda', 'India', '93db85ed909c13838ff95ccfa94cebd9.JPG'),
(108, 3, 'approved', 'Dawid', 'Jarosz', 2017, 'Sidama, Ethiopia', 'Ethiopia, Poland', 'c7e1249ffc03eb9ded908c236bd1996d.JPG'),
(109, 3, 'approved', 'Roman', 'Ruhig', 2017, 'senegal', 'Senegal, slovakia', '2a38a4a9316c49e5a833517c45d31070.JPG'),
(110, 3, 'approved', 'Nora', 'Bachir', 2017, 'musulman, algerian', 'Algeria', '7647966b7343c29048673252e490f736.JPG'),
(111, 3, 'approved', 'Abdula Razaq', 'Adio', 2017, 'Hausa', 'nigera', '8613985ec49eb8f757ae6439e879bb2a.JPG'),
(112, 3, 'approved', 'Evdokia', 'Mavroudi', 2017, 'Burkinabe', 'Burkina Faso', '54229abfcfa5649e7003b83dd4755294.JPG'),
(113, 3, 'approved', 'Andrew Ose', 'Phiri', 2017, 'zambia', 'zambia', '92cc227532d17e56e07902b254dfad10.JPG'),
(114, 3, 'approved', 'Okusi Isiak', 'Adekunle', 2017, 'Yoruba', 'nigera', '98dce83da57b0395e163467c9dae521b.JPG'),
(115, 3, 'approved', 'Veselina', 'Kotruleva', 2017, 'africa', 'bulgaria', 'f4b9ec30ad9f68f89b29639786cb62ef.JPG'),
(116, 3, 'approved', 'Harlan', 'Woodard', 2017, 'African American', 'Ghana', '812b4ba287f5ee0bc9d43bbf5bbe87fb.JPG'),
(117, 3, 'approved', 'Bilkisu', 'Garbar', 2017, 'yoruba, igbo, hausa', 'nigeria', 'e2ef524fbf3d9fe611d5a8e90fefdc9c.jpg'),
(118, 3, 'approved', 'Odekunle', 'Lape Saleh', 2017, 'Hausa', 'nigeria', 'ed3d2c21991e3bef5e069713af9fa6ca.jpg'),
(119, 3, 'approved', 'Federico', 'Fauli', 2017, 'congo', 'congo', 'ac627ab1ccbdb62ec96e702f07f6425b.jpg'),
(120, 4, 'approved', 'Roman', 'Gorshkov', 2015, 'ethiopia, omo', 'Ethiopia, United Kingdom ', 'f899139df5e1059396431415e770c6dd.jpg'),
(121, 4, 'approved', 'Zaynab', 'Zuhair', 2015, 'Hausa', 'nigeria', '38b3eff8baf56627478ec76a704e9b52.JPG'),
(122, 4, 'approved', 'Nahom', 'Bekele', 2015, 'Ethiopia', 'ethiopia', 'ec8956637a99787bd197eacd77acce5e.JPG'),
(123, 4, 'approved', 'Adeyemi', 'Akande', 2015, 'yoruba', 'nigeria', '6974ce5ac660610b44d9b9fed0ff9548.JPG'),
(124, 4, 'approved', 'Jun', 'Yanagimuro', 2015, 'Burkina Faso', 'Burkina Faso', 'c9e1074f5b3f9fc8ea15d152add07294.JPG'),
(125, 4, 'approved', 'Amanuele', 'Brook', 2015, 'sidama, afar', 'ethiopia', '65b9eea6e1cc6bb9f0cd2a47751a186f.JPG'),
(126, 4, 'approved', 'Gebrekidan', 'Getaw', 2015, 'konso', 'ethiopia', 'f0935e4cd5920aa6c7c996a5ee53a70f.JPG'),
(127, 4, 'approved', 'Gratio Ray', 'Suntato', 2015, 'zimbabwe', 'zimbabwe', 'a97da629b098b75c294dffdc3e463904.JPG'),
(128, 4, 'approved', 'Kseniya', 'Mamaeva', 2015, 'africa', 'russia', 'a3c65c2974270fd093ee8a9bf8ae7d0b.JPG'),
(129, 4, 'approved', 'Kalkidan', 'Tesfaye', 2015, 'sidama', 'Ethiopia', '2723d092b63885e0d7c260cc007e8b9d.JPG'),
(130, 4, 'approved', 'Ojile Ameh', 'Godswill', 2015, 'Benin, nok terracotta', 'nigeria', '5f93f983524def3dca464469d2cf9f3e.JPG'),
(131, 4, 'approved', 'Nakimuli', 'Thatcher', 2015, 'karamojong', 'uganda', '698d51a19d8a121ce581499d7b701668.JPG'),
(132, 4, 'approved', 'Liuel Hizikias', 'Ketema', 2015, 'Axumite', 'ethiopia', '7f6ffaa6bb0b408017b62254211691b5.JPG'),
(133, 4, 'approved', 'Lizuchukwu', 'Magnus', 2015, 'Igbo,', 'nigeria, egypt', '73278a4a86960eeb576a8fd4c9ec6997.JPG'),
(134, 4, 'approved', 'Mark', 'Gomina', 2015, 'Africa', 'nigeria', '5fd0b37cd7dbbb00f97ba6ce92bf5add.JPG'),
(135, 4, 'approved', 'Marina', 'Ryleeva', 2015, 'Africa', 'russia', '2b44928ae11fb9384c4cf38708677c48.JPG'),
(136, 4, 'approved', 'Minasie', 'Terefe', 2015, 'konso', 'ethiopia', 'c45147dee729311ef5b5c3003946c48f.JPG'),
(137, 4, 'approved', 'Nicole Nomsa', 'Moyo', 2015, 'ndebele, zulu', 'South Africa', 'eb160de1de89d9058fcb0b968dbbbd68.JPG'),
(138, 4, 'approved', 'Ksenia', 'Bilyk', 2015, 'Hausa, northern Nigeria', 'nigeria', '5ef059938ba799aaa845e1c2e8a762bd.JPG'),
(139, 4, 'approved', 'Tan Boon', 'Chiou', 2015, 'Tata somba', 'Benin republic', '07e1cd7dca89a1678042477183b7ac3f.JPG'),
(140, 4, 'approved', 'Dahlia', 'Nduom', 2015, 'fanti', 'Ghana', 'da4fb5c6e93e74d3df8527599fa62642.JPG'),
(141, 4, 'approved', 'Miracle', 'David-Adjah', 2015, 'Africa', 'nigeria', '4c56ff4ce4aaf9573aa5dff913df997a.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `selection_session`
--

CREATE TABLE `selection_session` (
  `id` int(10) UNSIGNED NOT NULL,
  `input_name` varchar(225) DEFAULT NULL,
  `hash_id` varchar(225) DEFAULT NULL,
  `visibility` varchar(225) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL,
  `created_by` varchar(225) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selection_session`
--

INSERT INTO `selection_session` (`id`, `input_name`, `hash_id`, `visibility`, `date_created`, `time_created`, `created_by`) VALUES
(1, 'Session One', '1614418153_65448', 'hide', '2021-02-27', '09:29:13', 'j90819542aBn72i'),
(2, 'Session Two', '1614418175_53993', 'hide', '2021-02-27', '09:29:35', 'j90819542aBn72i'),
(3, 'Session Three', '1614418204_63930', 'hide', '2021-02-27', '09:30:04', 'j90819542aBn72i'),
(4, 'Session Four', '1614418227_26251', 'hide', '2021-02-27', '09:30:27', 'j90819542aBn72i'),
(5, 'Session Five', '1614418244_20876', 'hide', '2021-02-27', '09:30:44', 'j90819542aBn72i');

-- --------------------------------------------------------

--
-- Table structure for table `verify`
--

CREATE TABLE `verify` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` text NOT NULL,
  `token` text NOT NULL,
  `token_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verify`
--

INSERT INTO `verify` (`id`, `email`, `token`, `token_status`) VALUES
(2, '1564750726a6i6lb484173o7j5', '1564750726_5bo84t7bia5f52Dai4trr1a6htl1V75eG1536neve7s6niooijc94rEn3d0i07k66moi7a6', 1),
(7, '156538768484ke005al13o086', '1565387684_rleE8D61e31k27ao3es8ano5ei354bo8aiv064kconr0t886iand0ttmV56i3Gf40ir7h1', 1),
(13, '157030053487610b62a4in2j1', '1570300534_0rn1ai86jsa717b05Dr3ri4meai804hf9n03t73a252E2cnit4neo1koie2i60GdVtvbo', 1),
(19, '1572473745k0se248uv34n8d891i', '1572473745_i3fv1at3ubr15D8ns73minG839htcieo44ii1et7ioVr4s8dned25k73a90erak5no57v427E', 1),
(20, '15857422515o8644e5l273k5a', '1585742251_3eDme57226n5Vocrcb4Gt15oM4a490sk7o4deoed58n1f5i2ira6t5v45nlvr8k5aae42hoi', 1),
(22, '111898543879843684796', '1585750727_V83M5di17k794a1cn4864c387781ri6ee3iv98o96t98fo', 1),
(23, '111898543879843684796', '1585750962_ic3et89447381d4oo77iV9rcn60Ma4e871v988727k13546fi', 1),
(24, '111898543879843684796', '1585752188_148etoci73d7ivc88318988M936k495o961e81V148ai5fnr6', 1),
(25, '111898543879843684796', '1585752287_39648934c0813ic8e98V298aoreni11d6tM83vf157ik7472o', 1),
(26, '1585752461742e393al59k96o', '1585752461_ereev89n1k9a152Vd3759c5iD9h6l7d2ao84osei3n4e46bt6o2iMaaok1rno22fc5v0mtrG', 1),
(27, '1585752922b8na3798809ji04', '1585752922_n78a915ar5bemV7oia8hso72c3en2inf18972reocj0v4G10nt58iod4id8var9beD27kMt', 1),
(28, '1585752922b8na3798809ji04', '1585753237_r457e0d8eM9a8bticio28k72ajf8o9c7198755nn22Vvi0i13105', 1),
(29, '1585752922b8na3798809ji04', '1585753536_n45ak6c1c8ajdf58e7bv3Vo2i28295i3M81o7t40i4899ne28r01i', 1),
(30, '1585752922b8na3798809ji04', '1585753654_57905Vc0i95j8e7af7Mv2208957nt9b88ni2odr8i2aeo24i1c83k', 1),
(31, '1585752922b8na3798809ji04', '1585754667_9o948025iibv0554nr87492i388Vd9t4e3Mic28kn1ao5afc75je6', 1),
(37, '1585870469ee6n2uu88co16d075e6kg', '1585870469_cnek2hdb87Vv1856n4ieaoed17eDir8GM6ce7e0ufrk2n78ncvtat9mgao26206soo59567ueiord', 1),
(38, '1590985698kd-j-wihfgidhjogo-r1ohfggdkrpwfd-ioo7wkswhh5rsfuyfr5fjdpeggojr9gjtfteejdgtsf-ofof-gkhg-uohojewupfoe7jo0dg-n8viwkr-ejgfjgw9pehfcrwgw-ojposjhi-p-ojikio0eiimkgwrrfgredod-ydjii', '1590985698_hedoeouw8i9wtdowprsdddG9dnpr-7ogjnoMg-00r-fi-0iokkwmto-d8rj5oryofw9rghutj9hc-jtmej-gfa5ieococddhwjjv-fgoepr-fiiofjrgkjefsfeog9ivwetfsfenj7Vkoegiwk-hjo6safjyn-rifhkipoig8piegk8-oohvhgk01ghDdo5o-gr6fpid1udg9jrhsbfa1j5g01r5ee4wrfwgi', 1),
(39, '15934263772wlg435--8odoa7i9wwg2l1r7boe', '1593426377_edtsce26166ww11-lwk5Vm-ah4elnovbM2r9dnf3ri2ao4t7ob7o2a9gi587431Gerecao2v3noo5r1idDgi7', 1),
(40, '1600131574edfcwr8dnssjdhtjffdn-f-1gofocmuhbjh7iejeefegjsfjjbhfbfidiwhehp9grjivjsdvudhoidfag4-juh5gfeiibifreh7dfnivieijd1jiffhdomhdlewusdieido-uhes-fkhfh4fjhdif-jtbfhd-e4ijshfegiiud-ii', '1600131574_jhhuh-hrdga045ic2vpdooifrjofeoifjrdcbeundndfsojhd8f-if3sihowjfjendijuuejkh1sd3gfte5bieimii9vwf-ea-fhs-bfieeeek-ee4ftdgnhf0idos-jj5f1chjnuisjvtesrohfeiifidthhiaaeifmgiDndGg9fdi4hv79iuddefhfdb-b11Mddhichvf3wrjfbjl9148odgerjV7h71m6is', 1),
(41, '1606681307-j-feofehhowjijdgdu6ojjj--9jgfliwaoeihiirjhfg4ffhtejejeyufmeddi9jhfhed7eegoegtieef9iwffoorojfwhejifst-ioe-eifhpofw-2jfodrifwjfjdiiegiwneiw-ffeee4io-ejdgsuijho2g-hohff7k', '1606681307_ijfftea07d9f2dwre2fdcMe5ooeteo-fjii6i-rvfjkdheioo0-fjilboh6oefgskfhiuggh8ovjf-e-gofhiwwg7epor4eeyGwdifjf-nhtefwoj9hdofw-orjdn3ii1nh1ieeaffewgt6f-joeeo-ajDhgj4iend8ejumioc1gioifhhojifejfjf1serjheieewV93j76-muijtrei9se5dadi7f3h', 1),
(42, '16072539003-wcf7heauiayi24h1440uylkinr71', '1607253900_f3ik0aMir2hyh84htw7nm08t3-ie4yee346sea5ci75r4vaoDoofaGuiia9Vc1obknnvu1rdc68d0le47n10r22', 1),
(43, '161298290986ess4j178ya4762dyem', '1612982909_jeV6aDhMc4ayvoG7d8m6vbr2n2e7nsae1624t1920de289d7iit9os8kmnrceaoefy6o1ir82s90', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_auth`
--
ALTER TABLE `admin_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `competition_order`
--
ALTER TABLE `competition_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_competitions`
--
ALTER TABLE `panel_competitions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_competition_design_materials`
--
ALTER TABLE `panel_competition_design_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_courses`
--
ALTER TABLE `panel_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel_course_materials`
--
ALTER TABLE `panel_course_materials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matID` (`id`),
  ADD KEY `courseId` (`course_id`);

--
-- Indexes for table `panel_discount_code`
--
ALTER TABLE `panel_discount_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `read_competition_order`
--
ALTER TABLE `read_competition_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `read_course_feedback`
--
ALTER TABLE `read_course_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `read_course_professors`
--
ALTER TABLE `read_course_professors`
  ADD PRIMARY KEY (`course_id`,`user_id`);

--
-- Indexes for table `read_product_order`
--
ALTER TABLE `read_product_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `read_registered_competition_status`
--
ALTER TABLE `read_registered_competition_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `read_student_course_materials`
--
ALTER TABLE `read_student_course_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `read_users`
--
ALTER TABLE `read_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registered_competition_status`
--
ALTER TABLE `registered_competition_status`
  ADD PRIMARY KEY (`user_id`,`competition_id`);

--
-- Indexes for table `selection_session`
--
ALTER TABLE `selection_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify`
--
ALTER TABLE `verify`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `admin_auth`
--
ALTER TABLE `admin_auth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `competition_order`
--
ALTER TABLE `competition_order`
  MODIFY `order_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2000118;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `panel_competitions`
--
ALTER TABLE `panel_competitions`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `panel_competition_design_materials`
--
ALTER TABLE `panel_competition_design_materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `panel_courses`
--
ALTER TABLE `panel_courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `panel_course_materials`
--
ALTER TABLE `panel_course_materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `panel_discount_code`
--
ALTER TABLE `panel_discount_code`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `order_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000038;

--
-- AUTO_INCREMENT for table `read_competition_order`
--
ALTER TABLE `read_competition_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2000179;

--
-- AUTO_INCREMENT for table `read_course_feedback`
--
ALTER TABLE `read_course_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `read_product_order`
--
ALTER TABLE `read_product_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000107;

--
-- AUTO_INCREMENT for table `read_registered_competition_status`
--
ALTER TABLE `read_registered_competition_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `read_student_course_materials`
--
ALTER TABLE `read_student_course_materials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `read_users`
--
ALTER TABLE `read_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `selection_session`
--
ALTER TABLE `selection_session`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `verify`
--
ALTER TABLE `verify`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
