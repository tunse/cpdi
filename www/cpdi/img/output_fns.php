<?php

session_start();

function display_login_form($prodId, $email){
?>
	<div class="register-form">
		<form action="login.php" method="post">
		
		  <input id="prodId" name="prodId" type="hidden" value="<?php echo $prodId; ?>">
		  
		  <h2 class="text-center">Sign in</h2>
		  <div class="form-group">
			<input type="email" name="email" class="form-control" placeholder="Email" required="required" value="<?php echo $email; ?>">
		  </div>
		  <div class="form-group">
			<input type="password" name="password" class="form-control" placeholder="Password" required="required">
		  </div>
		  
		  <div class="row">
			<div class="col-md-6 col-xs-12">
			  <div class="form-group">
				<input type="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7">
			  </div>
			</div>
			
		  </div>
		</form>
	</div>
<?php	
}

function login_form(){
?>
	<div class="register-form">
		<form action="myaccount.php" method="post">
		
		  
		  
		  <h2 class="text-center">Sign in</h2>
		  <div class="form-group">
			<input type="email" name="email" class="form-control" placeholder="Email" required="required">
		  </div>
		  <div class="form-group">
			<input type="password" name="password" class="form-control" placeholder="Password" required="required">
		  </div>
		  
		  <div class="row">
			<div class="col-md-6 col-xs-12">
			  <div class="form-group">
				<input type="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7">
			  </div>
			</div>
			
		  </div>
		</form>
	</div>
<?php	
}

function display_footer_address(){	
	echo "Verrijn Stuartweg 22J, 1112 AX Diemen";		
}

function display_footer_phone(){	
	echo "<strong>Phone:</strong> +31 645726181";
}

function display_footer_email(){	
	echo "<strong>Email:</strong> info@3dcodeclub.nl";
}

function display_useful_links(){		
	echo '<ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="#about">About us</a></li>
              <li><a href="#services">Code Clubs</a></li>
              <li><a href="#">Terms of service</a></li>
              <li><a href="#">Privacy policy</a></li>
            </ul>';	
}

function display_menu(){
	echo '<ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#services">Code Clubs</a></li>
          <li><a href="#team">Team</a></li>
          <li><a href="#contact">Contact Us</a></li>';
		  if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
		  {
			echo '<li class="drop-down"><a href="login_form.php">My Account</a>
					<ul><li><a href="myaccount.php">My Code Clubs</a></li>
					<li><a href="signout.php">Sign Out</a></li>
					</ul></li>';
		  }
		  else
		  {
			  echo '<li><a href="login_form.php">My Account</a></li>';
		  }
			
	//echo '<li class="drop-down"><a href="">Language</a>
    //        <ul>
      //        <li><a href="#">Dutch</a></li>
        //      <li><a href="#">English</a></li>
          //  </ul>
          //</li>
        echo '/</ul>';
	
	
}

function display_menu_with_links(){
	echo '<ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="index.php">About Us</a></li>
          <li><a href="index.php">Code Clubs</a></li>
          <li><a href="index.php">Team</a></li>
          <li><a href="index.php">Contact Us</a></li>';
		   if (isset($_SESSION['valid_user']) && !empty($_SESSION['valid_user']))
		  {
			echo '<li class="drop-down"><a href="login_form.php">My Account</a>
					<ul><li><a href="myaccount.php">My Code Clubs</a></li>
					<li><a href="signout.php">Sign Out</a></li>
					</ul></li>';
		  }
		  else
		  {
			  echo '<li><a href="login_form.php">My Account</a></li>';
		  }
	
	//echo '<li class="drop-down"><a href="">Language</a>
      //      <ul>
      //        <li><a href="#">Dutch</a></li>
      //        <li><a href="#">English</a></li>
      //      </ul>
      //    </li>
      echo '</ul>';
}

?>